#version 450 core

const vec2 quad[4] = vec2[4]( vec2(-1.0, -1.0), vec2(1.0, -1.0), vec2(1.0, 1.0), vec2(-1.0, 1.0) );

void main(void)
{
	gl_Position = vec4(quad[gl_VertexID], 0.0, 0.0);
}
