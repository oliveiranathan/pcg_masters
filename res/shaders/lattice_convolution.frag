#version 450 core

uniform usampler2D p;
uniform sampler2D valueTab;
uniform sampler2D table;

float catrom2(float d)
{
    if (d >= 4.0)
        return 0.0;
    d = d * 100.0 + 0.5;
    int i = int(floor(d));
    if (i >= 401)
        return 0.0;
    return texelFetch(table, ivec2(i, 0), 0).r;
}

int perm(int i)
{
    return int(texelFetch(p, ivec2(i & 255, 0), 0).r);
}

int index(int x, int y, int z)
{
    return perm(x + perm(y + perm(z)));
}

float vlattice(int ix, int iy, int iz)
{
    return texelFetch(valueTab, ivec2(index(ix, iy, iz), 0), 0).r;
}

float noise(vec4 pos)
{
    float sum = 0.0;
    int ix = int(floor(pos.x));
    float fx = pos.x - float(ix);
    int iy = int(floor(pos.y));
    float fy = pos.y - float(iy);
    int iz = int(floor(pos.z));
    float fz = pos.z - float(iz);
    for (int k = -1; k <= 2; k++) {
        float dz = float(k) - fz;
        dz = dz*dz;
        for (int j = -1; j <= 2; j++) {
            float dy = float(j) - fy;
            dy = dy*dy;
            for (int i = -1; i <= 2; i++) {
                float dx = float(i) - fx;
                dx = dx*dx;
                sum += vlattice(ix + i, iy + j, iz + k)
                        * catrom2(dx + dy + dz);
            }
        }
    }
    return sum;
}