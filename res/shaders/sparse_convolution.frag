#version 450 core

uniform usampler2D p;
uniform sampler2D impulseTab;
uniform sampler2D table;

int next(int h)
{
    return (h + 1) & 255;
}

float catrom2(float d)
{
    if (d >= 4.0)
        return 0.0;
    d = d * 100.0 + 0.5;
    int i = int(floor(d));
    if (i >= 401)
        return 0.0;
    return texelFetch(table, ivec2(i, 0), 0).r;
}

int perm(int i)
{
    return int(texelFetch(p, ivec2(i & 255, 0), 0).r);
}

int index(int x, int y, int z)
{
    return perm(x + perm(y + perm(z)));
}

float noise(vec4 pos)
{
    int ix = int(floor(pos.x));
    float fx = pos.x - float(ix);
    int iy = int(floor(pos.y));
    float fy = pos.y - float(iy);
    int iz = int(floor(pos.z));
    float fz = pos.z - float(iz);
    float sum = 0.0;
    for (int i = -2; i <= 2; i++) {
        for (int j = -2; j <= 2; j++) {
            for (int k = -2; k <= 2; k++) {
                int h = index(ix + i, iy + j, iz + k);
                for (int n = 3; n > 0; n--, h = next(h)) {
					int baseIdx = h * 4;
                    float dx = fx - float(i) + texelFetch(impulseTab, ivec2(baseIdx, 0), 0).r;
                    float dy = fy - float(j) + texelFetch(impulseTab, ivec2(baseIdx + 1, 0), 0).r;
                    float dz = fz - float(k) + texelFetch(impulseTab, ivec2(baseIdx + 2, 0), 0).r;
                    float distsq = dx * dx + dy * dy + dz * dz;
                    sum += catrom2(distsq) * texelFetch(impulseTab, ivec2(baseIdx + 3, 0), 0).r;
                }
            }
        }
    }
    return sum / 3.0;
}
