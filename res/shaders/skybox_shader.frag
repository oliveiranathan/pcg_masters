#version 450 core

uniform samplerCube tex_cubemap;

in VS_OUT {
	vec3 tc;
} fs_in;
out vec4 color;

void main()
{
	color = texture(tex_cubemap, fs_in.tc);
}