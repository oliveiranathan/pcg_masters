out vec4 color;

float noise(vec4 pos);

void main(void)
{
	float c = clamp(noise(gl_FragCoord * 0.0625), -1.0f, 1.0f);
	c += 1.0f;
	c /= 2.0f;
	color = vec4(c, c, c, 0.0);
}
