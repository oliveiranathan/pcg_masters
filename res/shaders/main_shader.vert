#version 450

uniform mat4 projection;
uniform mat4 model_view;

in vec3 vert;
in vec2 vertTexCoord;
out vec2 fragTexCoord;

void main() {
    fragTexCoord = vertTexCoord;
    
    gl_Position = projection * model_view * vec4(vert, 1);
}
