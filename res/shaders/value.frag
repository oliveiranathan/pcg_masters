#version 450 core

uniform usampler2D p;
uniform sampler2D valueTab;


int perm(int i)
{
    return int(texelFetch(p, ivec2(i & 255, 0), 0).r);
}

int index(int x, int y, int z)
{
    return perm(x + perm(y + perm(z)));
}

float vlattice(int ix, int iy, int iz)
{
    return texelFetch(valueTab, ivec2(index(ix, iy, iz), 0), 0).r;
}

float spline(float x, float knot[4])
{
    float c3 = -0.5 * knot[0] +  1.5 * knot[1] + -1.5 * knot[2] +  0.5 * knot[3];
    float c2 =  knot[0] + -2.5 * knot[1] +  2.0 * knot[2] + -0.5 * knot[3];
    float c1 = -0.5 * knot[0] +  0.5 * knot[2];
    float c0 =  knot[1];
    return ((c3 * x + c2) * x + c1) * x + c0;
}

float noise(vec4 pos)
{
    float xknots[4], yknots[4], zknots[4];
    int ix = int(floor(pos.x));
    float fx = pos.x - float(ix);
    int iy = int(floor(pos.y));
    float fy = pos.y - float(iy);
    int iz = int(floor(pos.z));
    float fz = pos.z - float(iz);
    for (int k = -1; k <= 2; k++) {
        for (int j = -1; j <= 2; j++) {
            for (int i = -1; i <= 2; i++)
                xknots[i + 1] = vlattice(ix + i, iy + j, iz + k);
            yknots[j + 1] = spline(fx, xknots);
        }
        zknots[k + 1] = spline(fy, yknots);
    }
    return spline(fz, zknots);
}
