#version 450 core

uniform usampler2D p;
uniform sampler2D gradientTab;

float lerp(float t, float x0, float x1)
{
    return x0 + t * (x1 - x0);
}

float smoothStep(float x)
{
    return x * x * (3.0 - 2.0 * x);
}

int perm(int i)
{
    return int(texelFetch(p, ivec2(i & 255, 0), 0).r);
}

int index(int x, int y, int z)
{
    return perm(x + perm(y + perm(z)));
}

float glattice(int ix, int iy, int iz, float fx, float fy, float fz)
{
	int baseIdx = index(ix, iy, iz) * 3;
    return texelFetch(gradientTab, ivec2(baseIdx, 0), 0).r * fx
         + texelFetch(gradientTab, ivec2(baseIdx + 1, 0), 0).r * fy
         + texelFetch(gradientTab, ivec2(baseIdx + 2, 0), 0).r * fz;
}

float noise(vec4 pos)
{
    int ix = int(floor(pos.x));
    float fx0 = pos.x - float(ix);
    float fxl = fx0 - 1.0;
    float wx = smoothStep(fx0);
    int iy = int(floor(pos.y));
    float fy0 = pos.y - float(iy);
    float fyl = fy0 - 1.0;
    float wy = smoothStep(fy0);
    int iz = int(floor(pos.z));
    float fz0 = pos.z - float(iz);
    float fzl = fz0 - 1.0;
    float wz = smoothStep(fz0);
    float vx0 = glattice(ix, iy, iz, fx0, fy0, fz0);
    float vxl = glattice(ix + 1, iy, iz, fxl, fy0, fz0);
    float vy0 = lerp(wx, vx0, vxl);
    vx0 = glattice(ix, iy + 1, iz, fx0, fyl, fz0);
    vxl = glattice(ix + 1, iy + 1, iz, fxl, fyl, fz0);
    float vyl = lerp(wx, vx0, vxl);
    float vz0 = lerp(wy, vy0, vyl);
    vx0 = glattice(ix, iy, iz + 1, fx0, fy0, fzl);
    vxl = glattice(ix + 1, iy, iz + 1, fxl, fy0, fzl);
    vy0 = lerp(wx, vx0, vxl);
    vx0 = glattice(ix, iy + 1, iz + 1, fx0, fyl, fzl);
    vxl = glattice(ix + 1, iy + 1, iz + 1, fxl, fyl, fzl);
    vyl = lerp(wx, vx0, vxl);
    float vzl = lerp(wy, vy0, vyl);
    return lerp(wz, vz0, vzl);
}
