#version 450 core

uniform uint seed;

uint morton(uint x, uint y)
{
    uint z = 0u;
    for (uint i = 0u; i < 4u * 8u; ++i) {
        z |= ((x & (1u << i)) << i) | ((y & (1u << i)) << (i + 1u));
    }
    return z;
}

float uniform_0_1(inout uint s)
{
	s *= 3039177861u;
	return float(s) / float(4294967295u);
}

float uniform_1_1(inout uint s)
{
	return uniform_0_1(s) * 2.0 - 1.0;
}

uint poisson(float mean, inout uint s)
{
	float g_ = exp(-mean);
	uint em = 0u;
	float t = uniform_0_1(s);
	while (t > g_) {
		++em;
		t *= uniform_0_1(s);
	}
	return em;
}

float cell(int i, int j, float x, float y)
{
	float M_PI = 3.1415926535897932384626433832795;
	float kernel_radius_ = sqrt(-log(0.05) / M_PI) / 0.05;
    uint s = morton(uint(i), uint(j)) + seed;
    if (s == 0u)
        s = 1u;
    float number_of_impulses_per_cell = 64.0 / M_PI;
    uint number_of_impulses = poisson(number_of_impulses_per_cell, s);
    float noise = 0.0;
    for (uint i = 0u; i < number_of_impulses; ++i) {
        float x_i = uniform_0_1(s);
        float y_i = uniform_0_1(s);
        float w_i = uniform_1_1(s);
        float x_i_x = x - x_i;
        float y_i_y = y - y_i;
		float x_i_x_2_t_y_i_y_2 = x_i_x * x_i_x + y_i_y * y_i_y;
        if (x_i_x_2_t_y_i_y_2 < 1.0) {
			float gabor = 1.0 * exp(-2.99573 * x_i_x_2_t_y_i_y_2) *
					cos(0.392699 * ((x_i_x * 13.8099) + (y_i_y * 13.8099)));
			noise += w_i * gabor;
        }
    }
    return noise;
}

float noise(vec4 pos)
{
	pos /= 0.0625;
	float M_PI = 3.1415926535897932384626433832795;
	float kernel_radius_ = sqrt(-log(0.05) / M_PI) / 0.05;
	float x = pos.x, y = pos.y;
    x /= kernel_radius_;
	y /= kernel_radius_;
    float int_x = floor(x), int_y = floor(y);
    float frac_x = x - int_x, frac_y = y - int_y;
    int i = int(int_x), j = int(int_y);
    float noise = 0.0;
    for (int di = -1; di <= +1; ++di) {
        for (int dj = -1; dj <= +1; ++dj) {
            noise += cell(i + di, j + dj, frac_x - float(di), frac_y - float(dj));
        }
    }
    return noise / 6.0;
}
