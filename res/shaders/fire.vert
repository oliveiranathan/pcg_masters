#version 450 core

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

out vec2 vTextureCoord;

const vec2 vertexes[4] = vec2[4](vec2(-1.0, 1.0), vec2(1.0, 1.0), vec2(1.0, -1.0), vec2(-1.0, -1.0));
const vec2 tex_coord[4] = vec2[4](vec2(0.0, 0.0), vec2(1.0, 0.0), vec2(1.0, 1.0), vec2(0.0, 1.0));

void main(void)
{
	gl_Position = uPMatrix * uMVMatrix * vec4(vertexes[gl_VertexID], 0.0, 1.0);
	vTextureCoord = tex_coord[gl_VertexID];
}
