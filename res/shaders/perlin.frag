#version 450 core

uniform usampler2D p;
uniform sampler2D g;

float lerp(float t, float a, float b)
{
    return (1.0 - t) * a + t * b;
}

float s_curve(float t)
{
    return t * t * (3.0 - 2.0 * t);
}

void setup(float arg, inout int b0, inout int b1, inout float r0, inout float r1)
{
    float t = arg + 4096.0;
    b0 = int(t) & 255;
    b1 = (b0 + 1) & 255;
    r0 = t - float(int(t));
    r1 = r0 - 1.0;
}

float at(int index, float rx, float ry, float rz)
{
    return rx * texelFetch(g, ivec2(index, 0), 0).r
         + ry * texelFetch(g, ivec2(index, 1), 0).r
         + rz * texelFetch(g, ivec2(index, 2), 0).r;
}

float noise(vec4 pos)
{
    int bx0, bx1, by0, by1, bz0, bz1;
    float rx0, rx1, ry0, ry1, rz0, rz1;
    setup(pos.x, bx0, bx1, rx0, rx1);
    setup(pos.y, by0, by1, ry0, ry1);
    setup(pos.z, bz0, bz1, rz0, rz1);
    int i = int(texelFetch(p, ivec2(bx0, 0), 0).r);
    int j = int(texelFetch(p, ivec2(bx1, 0), 0).r);
    int b00 = int(texelFetch(p, ivec2(i + by0, 0), 0).r);
    int b10 = int(texelFetch(p, ivec2(j + by0, 0), 0).r);
    int b01 = int(texelFetch(p, ivec2(i + by1, 0), 0).r);
    int b11 = int(texelFetch(p, ivec2(j + by1, 0), 0).r);
    float t = s_curve(rx0);
    float sy = s_curve(ry0);
    float sz = s_curve(rz0);
    float u = at(b00 + bz0, rx0, ry0, rz0);
    float v = at(b10 + bz0, rx1, ry0, rz0);
    float a = lerp(t, u, v);
    u = at(b01 + bz0, rx0, ry1, rz0);
    v = at(b11 + bz0, rx1, ry1, rz0);
    float b = lerp(t, u, v);
    float c = lerp(sy, a, b);
    u = at(b00 + bz1, rx0, ry0, rz1);
    v = at(b10 + bz1, rx1, ry0, rz1);
    a = lerp(t, u, v);
    u = at(b01 + bz1, rx0, ry1, rz1);
    v = at(b11 + bz1, rx1, ry1, rz1);
    b = lerp(t, u, v);
    float d = lerp(sy, a, b);
    return lerp(sz, c, d);
}
