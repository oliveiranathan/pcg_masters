#version 450 core

in vec3 position;

out vec3 obj_pos;

uniform mat4 mvp;

void main(void)
{
    obj_pos = position;
    gl_Position = mvp * vec4(position, 1.0);
}
