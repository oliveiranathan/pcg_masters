#version 450

uniform sampler2D tex;

in vec2 fragTexCoord;
out vec4 finalColor;

void main() {
	if( textureSize( tex, 0).x > 0)
		finalColor = texture(tex, fragTexCoord);
	else
		finalColor = vec4(0.3, 0.3, 0.3, 1.0);
}
