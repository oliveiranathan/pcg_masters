#version 450 core

uniform usampler2D perm;
uniform usampler2D permGradIndex3D;
uniform isampler2D gradients3D;

float extrapolate(int xsb, int ysb, int zsb, float dx, float dy, float dz)
{
    int mip = int(texelFetch(perm, ivec2(xsb & 0xFF, 0), 0).r);
    int op = int(texelFetch(perm, ivec2((mip + ysb) & 0xFF, 0), 0).r);
    int index = int(texelFetch(permGradIndex3D, ivec2((op + zsb) & 0xFF, 0), 0).r);
    return float(texelFetch(gradients3D, ivec2(index, 0), 0).r) * dx
         + float(texelFetch(gradients3D, ivec2(index + 1, 0), 0).r) * dy
         + float(texelFetch(gradients3D, ivec2(index + 2, 0), 0).r) * dz;
}

float noise(vec4 pos)
{
    float stretchOffset = (pos.x + pos.y + pos.z) * (-1.0 / 6.0);
    float xs = pos.x + stretchOffset;
    float ys = pos.y + stretchOffset;
    float zs = pos.z + stretchOffset;
    int xsb = int(floor(xs));
    int ysb = int(floor(ys));
    int zsb = int(floor(zs));
    float squishOffset = float(xsb + ysb + zsb) * (1.0 / 3.0);
    float xb = float(xsb) + squishOffset;
    float yb = float(ysb) + squishOffset;
    float zb = float(zsb) + squishOffset;
    float xins = xs - float(xsb);
    float yins = ys - float(ysb);
    float zins = zs - float(zsb);
    float inSum = xins + yins + zins;
    float dx0 = pos.x - xb;
    float dy0 = pos.y - yb;
    float dz0 = pos.z - zb;
    float dx_ext0, dy_ext0, dz_ext0;
    float dx_ext1, dy_ext1, dz_ext1;
    int xsv_ext0, ysv_ext0, zsv_ext0;
    int xsv_ext1, ysv_ext1, zsv_ext1;
    float value = 0.0;
    if (inSum <= 1.0) {
        int aPoint = 0x01;
        float aScore = xins;
        int bPoint = 0x02;
        float bScore = yins;
        if (aScore >= bScore && zins > bScore) {
            bScore = zins;
            bPoint = 0x04;
        } else if (aScore < bScore && zins > aScore) {
            aScore = zins;
            aPoint = 0x04;
        }
        float wins = 1.0 - inSum;
        if (wins > aScore || wins > bScore) {
            int c = (bScore > aScore ? bPoint : aPoint);
            if ((c & 0x01) == 0) {
                xsv_ext0 = xsb - 1;
                xsv_ext1 = xsb;
                dx_ext0 = dx0 + 1.0;
                dx_ext1 = dx0;
            } else {
                xsv_ext0 = xsv_ext1 = xsb + 1;
                dx_ext0 = dx_ext1 = dx0 - 1.0;
            }
            if ((c & 0x02) == 0) {
                ysv_ext0 = ysv_ext1 = ysb;
                dy_ext0 = dy_ext1 = dy0;
                if ((c & 0x01) == 0) {
                    ysv_ext1 -= 1;
                    dy_ext1 += 1.0;
                } else {
                    ysv_ext0 -= 1;
                    dy_ext0 += 1.0;
                }
            } else {
                ysv_ext0 = ysv_ext1 = ysb + 1;
                dy_ext0 = dy_ext1 = dy0 - 1.0;
            }
            if ((c & 0x04) == 0) {
                zsv_ext0 = zsb;
                zsv_ext1 = zsb - 1;
                dz_ext0 = dz0;
                dz_ext1 = dz0 + 1.0;
            } else {
                zsv_ext0 = zsv_ext1 = zsb + 1;
                dz_ext0 = dz_ext1 = dz0 - 1.0;
            }
        } else {
            int c = aPoint | bPoint;
            if ((c & 0x01) == 0) {
                xsv_ext0 = xsb;
                xsv_ext1 = xsb - 1;
                dx_ext0 = dx0 - 2.0 * (1.0 / 3.0);
                dx_ext1 = dx0 + 1.0 - (1.0 / 3.0);
            } else {
                xsv_ext0 = xsv_ext1 = xsb + 1;
                dx_ext0 = dx0 - 1.0 - 2.0 * (1.0 / 3.0);
                dx_ext1 = dx0 - 1.0 - (1.0 / 3.0);
            }
            if ((c & 0x02) == 0) {
                ysv_ext0 = ysb;
                ysv_ext1 = ysb - 1;
                dy_ext0 = dy0 - 2.0 * (1.0 / 3.0);
                dy_ext1 = dy0 + 1.0 - (1.0 / 3.0);
            } else {
                ysv_ext0 = ysv_ext1 = ysb + 1;
                dy_ext0 = dy0 - 1.0 - 2.0 * (1.0 / 3.0);
                dy_ext1 = dy0 - 1.0 - (1.0 / 3.0);
            }
            if ((c & 0x04) == 0) {
                zsv_ext0 = zsb;
                zsv_ext1 = zsb - 1;
                dz_ext0 = dz0 - 2.0 * (1.0 / 3.0);
                dz_ext1 = dz0 + 1.0 - (1.0 / 3.0);
            } else {
                zsv_ext0 = zsv_ext1 = zsb + 1;
                dz_ext0 = dz0 - 1.0 - 2.0 * (1.0 / 3.0);
                dz_ext1 = dz0 - 1.0 - (1.0 / 3.0);
            }
        }
        float attn0 = 2.0 - dx0 * dx0 - dy0 * dy0 - dz0 * dz0;
        if (attn0 > 0.0) {
            attn0 *= attn0;
            value += attn0 * attn0 * extrapolate(xsb + 0, ysb + 0, zsb + 0, dx0, dy0, dz0);
        }
        float dx1 = dx0 - 1.0 - (1.0 / 3.0);
        float dy1 = dy0 - 0.0 - (1.0 / 3.0);
        float dz1 = dz0 - 0.0 - (1.0 / 3.0);
        float attn1 = 2.0 - dx1 * dx1 - dy1 * dy1 - dz1 * dz1;
        if (attn1 > 0.0) {
            attn1 *= attn1;
            value += attn1 * attn1 * extrapolate(xsb + 1, ysb + 0, zsb + 0, dx1, dy1, dz1);
        }
        float dx2 = dx0 - 0.0 - (1.0 / 3.0);
        float dy2 = dy0 - 1.0 - (1.0 / 3.0);
        float dz2 = dz1;
        float attn2 = 2.0 - dx2 * dx2 - dy2 * dy2 - dz2 * dz2;
        if (attn2 > 0.0) {
            attn2 *= attn2;
            value += attn2 * attn2 * extrapolate(xsb + 0, ysb + 1, zsb + 0, dx2, dy2, dz2);
        }
        float dx3 = dx2;
        float dy3 = dy1;
        float dz3 = dz0 - 1.0 - (1.0 / 3.0);
        float attn3 = 2.0 - dx3 * dx3 - dy3 * dy3 - dz3 * dz3;
        if (attn3 > 0.0) {
            attn3 *= attn3;
            value += attn3 * attn3 * extrapolate(xsb + 0, ysb + 0, zsb + 1, dx3, dy3, dz3);
        }
    } else if (inSum >= 2.0) {
        int aPoint = 0x06;
        float aScore = xins;
        int bPoint = 0x05;
        float bScore = yins;
        if (aScore <= bScore && zins < bScore) {
            bScore = zins;
            bPoint = 0x03;
        } else if (aScore > bScore && zins < aScore) {
            aScore = zins;
            aPoint = 0x03;
        }
        float wins = 3.0 - inSum;
        if (wins < aScore || wins < bScore) {
            int c = (bScore < aScore ? bPoint : aPoint);
            if ((c & 0x01) != 0) {
                xsv_ext0 = xsb + 2;
                xsv_ext1 = xsb + 1;
                dx_ext0 = dx0 - 2.0 - 3.0 * (1.0 / 3.0);
                dx_ext1 = dx0 - 1.0 - 3.0 * (1.0 / 3.0);
            } else {
                xsv_ext0 = xsv_ext1 = xsb;
                dx_ext0 = dx_ext1 = dx0 - 3.0 * (1.0 / 3.0);
            }
            if ((c & 0x02) != 0) {
                ysv_ext0 = ysv_ext1 = ysb + 1;
                dy_ext0 = dy_ext1 = dy0 - 1.0 - 3.0 * (1.0 / 3.0);
                if ((c & 0x01) != 0) {
                    ysv_ext1 += 1;
                    dy_ext1 -= 1.0;
                } else {
                    ysv_ext0 += 1;
                    dy_ext0 -= 1.0;
                }
            } else {
                ysv_ext0 = ysv_ext1 = ysb;
                dy_ext0 = dy_ext1 = dy0 - 3.0 * (1.0 / 3.0);
            }
            if ((c & 0x04) != 0) {
                zsv_ext0 = zsb + 1;
                zsv_ext1 = zsb + 2;
                dz_ext0 = dz0 - 1.0 - 3.0 * (1.0 / 3.0);
                dz_ext1 = dz0 - 2.0 - 3.0 * (1.0 / 3.0);
            } else {
                zsv_ext0 = zsv_ext1 = zsb;
                dz_ext0 = dz_ext1 = dz0 - 3.0 * (1.0 / 3.0);
            }
        } else {
            int c = aPoint & bPoint;
            if ((c & 0x01) != 0) {
                xsv_ext0 = xsb + 1;
                xsv_ext1 = xsb + 2;
                dx_ext0 = dx0 - 1.0 - (1.0 / 3.0);
                dx_ext1 = dx0 - 2.0 - 2.0 * (1.0 / 3.0);
            } else {
                xsv_ext0 = xsv_ext1 = xsb;
                dx_ext0 = dx0 - (1.0 / 3.0);
                dx_ext1 = dx0 - 2.0 * (1.0 / 3.0);
            }
            if ((c & 0x02) != 0) {
                ysv_ext0 = ysb + 1;
                ysv_ext1 = ysb + 2;
                dy_ext0 = dy0 - 1.0 - (1.0 / 3.0);
                dy_ext1 = dy0 - 2.0 - 2.0 * (1.0 / 3.0);
            } else {
                ysv_ext0 = ysv_ext1 = ysb;
                dy_ext0 = dy0 - (1.0 / 3.0);
                dy_ext1 = dy0 - 2.0 * (1.0 / 3.0);
            }
            if ((c & 0x04) != 0) {
                zsv_ext0 = zsb + 1;
                zsv_ext1 = zsb + 2;
                dz_ext0 = dz0 - 1.0 - (1.0 / 3.0);
                dz_ext1 = dz0 - 2.0 - 2.0 * (1.0 / 3.0);
            } else {
                zsv_ext0 = zsv_ext1 = zsb;
                dz_ext0 = dz0 - (1.0 / 3.0);
                dz_ext1 = dz0 - 2.0 * (1.0 / 3.0);
            }
        }
        float dx3 = dx0 - 1.0 - 2.0 * (1.0 / 3.0);
        float dy3 = dy0 - 1.0 - 2.0 * (1.0 / 3.0);
        float dz3 = dz0 - 0.0 - 2.0 * (1.0 / 3.0);
        float attn3 = 2.0 - dx3 * dx3 - dy3 * dy3 - dz3 * dz3;
        if (attn3 > 0.0) {
            attn3 *= attn3;
            value += attn3 * attn3 * extrapolate(xsb + 1, ysb + 1, zsb + 0, dx3, dy3, dz3);
        }
        float dx2 = dx3;
        float dy2 = dy0 - 0.0 - 2.0 * (1.0 / 3.0);
        float dz2 = dz0 - 1.0 - 2.0 * (1.0 / 3.0);
        float attn2 = 2.0 - dx2 * dx2 - dy2 * dy2 - dz2 * dz2;
        if (attn2 > 0.0) {
            attn2 *= attn2;
            value += attn2 * attn2 * extrapolate(xsb + 1, ysb + 0, zsb + 1, dx2, dy2, dz2);
        }
        float dx1 = dx0 - 0.0 - 2.0 * (1.0 / 3.0);
        float dy1 = dy3;
        float dz1 = dz2;
        float attn1 = 2.0 - dx1 * dx1 - dy1 * dy1 - dz1 * dz1;
        if (attn1 > 0.0) {
            attn1 *= attn1;
            value += attn1 * attn1 * extrapolate(xsb + 0, ysb + 1, zsb + 1, dx1, dy1, dz1);
        }
        dx0 = dx0 - 1.0 - 3.0 * (1.0 / 3.0);
        dy0 = dy0 - 1.0 - 3.0 * (1.0 / 3.0);
        dz0 = dz0 - 1.0 - 3.0 * (1.0 / 3.0);
        float attn0 = 2.0 - dx0 * dx0 - dy0 * dy0 - dz0 * dz0;
        if (attn0 > 0.0) {
            attn0 *= attn0;
            value += attn0 * attn0 * extrapolate(xsb + 1, ysb + 1, zsb + 1, dx0, dy0, dz0);
        }
    } else {
        float aScore;
        int aPoint;
        bool aIsFurtherSide;
        float bScore;
        int bPoint;
        bool bIsFurtherSide;
        float p1 = xins + yins;
        if (p1 > 1.0) {
            aScore = p1 - 1.0;
            aPoint = 0x03;
            aIsFurtherSide = true;
        } else {
            aScore = 1.0 - p1;
            aPoint = 0x04;
            aIsFurtherSide = false;
        }
        float p2 = xins + zins;
        if (p2 > 1.0) {
            bScore = p2 - 1.0;
            bPoint = 0x05;
            bIsFurtherSide = true;
        } else {
            bScore = 1.0 - p2;
            bPoint = 0x02;
            bIsFurtherSide = false;
        }
        float p3 = yins + zins;
        if (p3 > 1.0) {
            float score = p3 - 1.0;
            if (aScore <= bScore && aScore < score) {
                aPoint = 0x06;
                aIsFurtherSide = true;
            } else if (aScore > bScore && bScore < score) {
                bPoint = 0x06;
                bIsFurtherSide = true;
            }
        } else {
            float score = 1.0 - p3;
            if (aScore <= bScore && aScore < score) {
                aPoint = 0x01;
                aIsFurtherSide = false;
            } else if (aScore > bScore && bScore < score) {
                bPoint = 0x01;
                bIsFurtherSide = false;
            }
        }
        if (aIsFurtherSide == bIsFurtherSide) {
            if (aIsFurtherSide) {
                dx_ext0 = dx0 - 1.0 - 3.0 * (1.0 / 3.0);
                dy_ext0 = dy0 - 1.0 - 3.0 * (1.0 / 3.0);
                dz_ext0 = dz0 - 1.0 - 3.0 * (1.0 / 3.0);
                xsv_ext0 = xsb + 1;
                ysv_ext0 = ysb + 1;
                zsv_ext0 = zsb + 1;
                int c = aPoint & bPoint;
                if ((c & 0x01) != 0) {
                    dx_ext1 = dx0 - 2.0 - 2.0 * (1.0 / 3.0);
                    dy_ext1 = dy0 - 2.0 * (1.0 / 3.0);
                    dz_ext1 = dz0 - 2.0 * (1.0 / 3.0);
                    xsv_ext1 = xsb + 2;
                    ysv_ext1 = ysb;
                    zsv_ext1 = zsb;
                } else if ((c & 0x02) != 0) {
                    dx_ext1 = dx0 - 2.0 * (1.0 / 3.0);
                    dy_ext1 = dy0 - 2.0 - 2.0 * (1.0 / 3.0);
                    dz_ext1 = dz0 - 2.0 * (1.0 / 3.0);
                    xsv_ext1 = xsb;
                    ysv_ext1 = ysb + 2;
                    zsv_ext1 = zsb;
                } else {
                    dx_ext1 = dx0 - 2.0 * (1.0 / 3.0);
                    dy_ext1 = dy0 - 2.0 * (1.0 / 3.0);
                    dz_ext1 = dz0 - 2.0 - 2.0 * (1.0 / 3.0);
                    xsv_ext1 = xsb;
                    ysv_ext1 = ysb;
                    zsv_ext1 = zsb + 2;
                }
            } else {
                dx_ext0 = dx0;
                dy_ext0 = dy0;
                dz_ext0 = dz0;
                xsv_ext0 = xsb;
                ysv_ext0 = ysb;
                zsv_ext0 = zsb;
                int c = aPoint | bPoint;
                if ((c & 0x01) == 0) {
                    dx_ext1 = dx0 + 1.0 - (1.0 / 3.0);
                    dy_ext1 = dy0 - 1.0 - (1.0 / 3.0);
                    dz_ext1 = dz0 - 1.0 - (1.0 / 3.0);
                    xsv_ext1 = xsb - 1;
                    ysv_ext1 = ysb + 1;
                    zsv_ext1 = zsb + 1;
                } else if ((c & 0x02) == 0) {
                    dx_ext1 = dx0 - 1.0 - (1.0 / 3.0);
                    dy_ext1 = dy0 + 1.0 - (1.0 / 3.0);
                    dz_ext1 = dz0 - 1.0 - (1.0 / 3.0);
                    xsv_ext1 = xsb + 1;
                    ysv_ext1 = ysb - 1;
                    zsv_ext1 = zsb + 1;
                } else {
                    dx_ext1 = dx0 - 1.0 - (1.0 / 3.0);
                    dy_ext1 = dy0 - 1.0 - (1.0 / 3.0);
                    dz_ext1 = dz0 + 1.0 - (1.0 / 3.0);
                    xsv_ext1 = xsb + 1;
                    ysv_ext1 = ysb + 1;
                    zsv_ext1 = zsb - 1;
                }
            }
        } else {
            int c1, c2;
            if (aIsFurtherSide) {
                c1 = aPoint;
                c2 = bPoint;
            } else {
                c1 = bPoint;
                c2 = aPoint;
            }
            if ((c1 & 0x01) == 0) {
                dx_ext0 = dx0 + 1.0 - (1.0 / 3.0);
                dy_ext0 = dy0 - 1.0 - (1.0 / 3.0);
                dz_ext0 = dz0 - 1.0 - (1.0 / 3.0);
                xsv_ext0 = xsb - 1;
                ysv_ext0 = ysb + 1;
                zsv_ext0 = zsb + 1;
            } else if ((c1 & 0x02) == 0) {
                dx_ext0 = dx0 - 1.0 - (1.0 / 3.0);
                dy_ext0 = dy0 + 1.0 - (1.0 / 3.0);
                dz_ext0 = dz0 - 1.0 - (1.0 / 3.0);
                xsv_ext0 = xsb + 1;
                ysv_ext0 = ysb - 1;
                zsv_ext0 = zsb + 1;
            } else {
                dx_ext0 = dx0 - 1.0 - (1.0 / 3.0);
                dy_ext0 = dy0 - 1.0 - (1.0 / 3.0);
                dz_ext0 = dz0 + 1.0 - (1.0 / 3.0);
                xsv_ext0 = xsb + 1;
                ysv_ext0 = ysb + 1;
                zsv_ext0 = zsb - 1;
            }
            dx_ext1 = dx0 - 2.0 * (1.0 / 3.0);
            dy_ext1 = dy0 - 2.0 * (1.0 / 3.0);
            dz_ext1 = dz0 - 2.0 * (1.0 / 3.0);
            xsv_ext1 = xsb;
            ysv_ext1 = ysb;
            zsv_ext1 = zsb;
            if ((c2 & 0x01) != 0) {
                dx_ext1 -= 2.0;
                xsv_ext1 += 2;
            } else if ((c2 & 0x02) != 0) {
                dy_ext1 -= 2.0;
                ysv_ext1 += 2;
            } else {
                dz_ext1 -= 2.0;
                zsv_ext1 += 2;
            }
        }
        float dx1 = dx0 - 1.0 - (1.0 / 3.0);
        float dy1 = dy0 - 0.0 - (1.0 / 3.0);
        float dz1 = dz0 - 0.0 - (1.0 / 3.0);
        float attn1 = 2.0 - dx1 * dx1 - dy1 * dy1 - dz1 * dz1;
        if (attn1 > 0.0) {
            attn1 *= attn1;
            value += attn1 * attn1 * extrapolate(xsb + 1, ysb + 0, zsb + 0, dx1, dy1, dz1);
        }
        float dx2 = dx0 - 0.0 - (1.0 / 3.0);
        float dy2 = dy0 - 1.0 - (1.0 / 3.0);
        float dz2 = dz1;
        float attn2 = 2.0 - dx2 * dx2 - dy2 * dy2 - dz2 * dz2;
        if (attn2 > 0.0) {
            attn2 *= attn2;
            value += attn2 * attn2 * extrapolate(xsb + 0, ysb + 1, zsb + 0, dx2, dy2, dz2);
        }
        float dx3 = dx2;
        float dy3 = dy1;
        float dz3 = dz0 - 1.0 - (1.0 / 3.0);
        float attn3 = 2.0 - dx3 * dx3 - dy3 * dy3 - dz3 * dz3;
        if (attn3 > 0.0) {
            attn3 *= attn3;
            value += attn3 * attn3 * extrapolate(xsb + 0, ysb + 0, zsb + 1, dx3, dy3, dz3);
        }
        float dx4 = dx0 - 1.0 - 2.0 * (1.0 / 3.0);
        float dy4 = dy0 - 1.0 - 2.0 * (1.0 / 3.0);
        float dz4 = dz0 - 0.0 - 2.0 * (1.0 / 3.0);
        float attn4 = 2.0 - dx4 * dx4 - dy4 * dy4 - dz4 * dz4;
        if (attn4 > 0.0) {
            attn4 *= attn4;
            value += attn4 * attn4 * extrapolate(xsb + 1, ysb + 1, zsb + 0, dx4, dy4, dz4);
        }
        float dx5 = dx4;
        float dy5 = dy0 - 0.0 - 2.0 * (1.0 / 3.0);
        float dz5 = dz0 - 1.0 - 2.0 * (1.0 / 3.0);
        float attn5 = 2.0 - dx5 * dx5 - dy5 * dy5 - dz5 * dz5;
        if (attn5 > 0.0) {
            attn5 *= attn5;
            value += attn5 * attn5 * extrapolate(xsb + 1, ysb + 0, zsb + 1, dx5, dy5, dz5);
        }
        float dx6 = dx0 - 0.0 - 2.0 * (1.0 / 3.0);
        float dy6 = dy4;
        float dz6 = dz5;
        float attn6 = 2.0 - dx6 * dx6 - dy6 * dy6 - dz6 * dz6;
        if (attn6 > 0.0) {
            attn6 *= attn6;
            value += attn6 * attn6 * extrapolate(xsb + 0, ysb + 1, zsb + 1, dx6, dy6, dz6);
        }
    }
    float attn_ext0 = 2.0 - dx_ext0 * dx_ext0 - dy_ext0 * dy_ext0 - dz_ext0 * dz_ext0;
    if (attn_ext0 > 0.0) {
        attn_ext0 *= attn_ext0;
        value += attn_ext0 * attn_ext0 * extrapolate(xsv_ext0, ysv_ext0, zsv_ext0, dx_ext0, dy_ext0, dz_ext0);
    }
    float attn_ext1 = 2.0 - dx_ext1 * dx_ext1 - dy_ext1 * dy_ext1 - dz_ext1 * dz_ext1;
    if (attn_ext1 > 0.0) {
        attn_ext1 *= attn_ext1;
        value += attn_ext1 * attn_ext1 * extrapolate(xsv_ext1, ysv_ext1, zsv_ext1, dx_ext1, dy_ext1, dz_ext1);
    }
    return value / 103.0;
}
