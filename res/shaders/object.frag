in vec3 obj_pos;

out vec4 color;

void main(void)
{
    float c = clamp(noise(vec4(obj_pos, 1.0) * 16), -1.0f, 1.0f);
    c += 1.0f;
    c /= 2.0f;
    color = vec4(c, c, c, 0.0);
}