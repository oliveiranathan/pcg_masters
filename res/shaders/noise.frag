#version 450 core

uniform sampler2D s;

out vec4 color;

void main(void)
{
	vec4 tmp = texelFetch(s, ivec2(gl_FragCoord.xy), 0);
	color = vec4(tmp.x, tmp.x, tmp.x, 0);
}
