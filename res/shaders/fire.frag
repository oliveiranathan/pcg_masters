#version 450 core

in vec2 vTextureCoord;
uniform float uTime;
uniform float effectWidth;
uniform float effectHeight;

out vec4 color;

float permute(float x)
{
  return mod((34.0 * x + 1.0)*x, 289.0);
}

vec2 grad2(vec2 p, float rot)
{
  float u = permute(permute(p.x) + p.y) * 0.0243902439 + rot;
  u = 4.0 * fract(u) - 2.0;
  return vec2(abs(u)-1.0, abs(abs(u+1.0)-2.0)-1.0);
}

float srdnoise(in vec2 P, in float rot, out vec2 grad)
{
  vec2 Ps = P + dot(P, vec2(0.366025403));
  vec2 Pi = floor(Ps);
  vec2 P0 = Pi - dot(Pi, vec2(0.211324865));
  vec2 v0 = P - P0;
  vec2 i1 = (v0.x > v0.y) ? vec2(1.0, 0.0) : vec2 (0.0, 1.0);
  vec2 v1 = v0 - i1 + 0.211324865;
  vec2 v2 = v0 - 0.57735027;
  Pi = mod(Pi, 289.0);
  vec3 t = max(0.5 - vec3(dot(v0,v0), dot(v1,v1), dot(v2,v2)), 0.0);
  vec3 t2 = t*t;
  vec3 t4 = t2*t2;
  vec2 g0 = grad2(Pi, rot);
  vec2 g1 = grad2(Pi + i1, rot);
  vec2 g2 = grad2(Pi + 1.0, rot);
  vec3 gv = vec3(dot(g0,v0), dot(g1,v1), dot(g2,v2));
  vec3 n = t4 * gv;
  vec3 temp = t2 * t * gv;
  vec3 gradx = temp * vec3(v0.x, v1.x, v2.x);
  vec3 grady = temp * vec3(v0.y, v1.y, v2.y);
  grad.x = -8.0 * (gradx.x + gradx.y + gradx.z);
  grad.y = -8.0 * (grady.x + grady.y + grady.z);
  grad.x += dot(t4, vec3(g0.x, g1.x, g2.x));
  grad.y += dot(t4, vec3(g0.y, g1.y, g2.y));
  grad *= 40.0;
  return 40.0 * (n.x + n.y + n.z);
}

float fireFlowNoise(vec2 p, float t)
{
	vec2 g1, g2;
	float n = 0.32*abs(srdnoise(vec2(8.0*p.x, 2.0*p.y+0.4*t), 0.1*t, g1)); 
	n += 0.5*abs(srdnoise(vec2(16.0*p.x, 4.0*p.y+0.3*t), 0.2*g1.x, g1));
	n += 0.8 * abs(srdnoise(vec2(8.0*p.x+0.1*t, p.y+0.2*t), 0.15*t, g1))+0.3;
	return 1.0-abs(n);

}

float circleEffect(vec2 p)
{
    p -= 0.5;
	return (p.x*p.x+p.y*p.y)*6.0;
}

void main(void)
{
	float n = fireFlowNoise(vTextureCoord, uTime);
	float perturb = ((1.0 - vTextureCoord.y) * 4.5) + 0.1;
	n = n * perturb + circleEffect(vTextureCoord);
	color = vec4(1.5 - n, 1.0 - n, 0.5 - n, 1.0);
}
