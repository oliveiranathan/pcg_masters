#version 450 core

uniform mat4 projection;
uniform mat4 model_view;

in vec3 vertex;
out VS_OUT {
	vec3 tc;
} vs_out;

void main(void)
{
	vs_out.tc = mat3(model_view) * vertex;
	gl_Position = projection * model_view * vec4(vertex, 1.0);
}
