/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <string>

namespace Utils {

	std::string execute_command(const std::string& command);

	float get_cpu_temperature();

	float get_gpu_temperature();

}

void run_all_methods();
