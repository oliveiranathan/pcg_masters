/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * Some parts of this file where based on the application code from the OpenGL SuperBible,
 * and are subject to the following license:
 * Copyright © 2012-2013 Graham Sellers
 *
 * This code is part of the OpenGL SuperBible, 6th Edition.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "program.h"

#include <iostream>
#include <chrono>
#include <cmath>
#include <memory>
#include <stack>
#include <string>


namespace OpenGL {

	class glfw_init_failure : public std::runtime_error {
	public:
		glfw_init_failure(const std::string& __arg) : runtime_error(__arg)
		{
		}
	};

	class window_init_failure : public std::runtime_error {
	public:
		window_init_failure(const std::string& __arg) : runtime_error(__arg)
		{
		}
	};

	void APIENTRY debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity,
								 GLsizei length, const GLchar* message, const void* userParam);

	class Application;

	class Renderer {
	public:
		virtual void startup()
		{
		}

		virtual void render(double)
		{
		}

		virtual void shutdown()
		{
		}

		virtual void on_key(int, int)
		{
		}

		virtual void on_mouse_button(int, int)
		{
		}

		virtual void on_mouse_move(int, int)
		{
		}

		virtual void on_mouse_wheel(int)
		{
		}

		virtual std::string specific_metrics();

	protected:
		int width_;
		int height_;

		void set_size(int w, int h)
		{
			width_ = w;
			height_ = h;
		}

		Application* app;

		void set_application_pointer(OpenGL::Application* application)
		{
			app = application;
		}

		friend class Application;
	};

	using Renderer_ptr = std::shared_ptr<Renderer>;

	class Noise_renderer : public Renderer {
	public:
		Noise_renderer(const std::string& noise_shader_path);

	protected:
		void startup() override;

		void render(double) override;

		void shutdown() override;

		virtual std::string specific_metrics() override;

		OpenGL::Program_ptr program;

	private:

		std::string shader_path;
		GLuint vao{ 0 };
		std::vector<GLuint> queries;
		unsigned long long samples_passed;

		friend class Application;
	};

	class Application {
	public:
		Application(Renderer_ptr renderer = std::make_shared<Renderer>(), bool debug = false,
					bool fullscreen = false, std::string title = "Application",
					int width = 3840, int height = 2160, bool vsync = false, int samples = 1)
				: debug_{ debug }, fullscreen_{ fullscreen }, title_{ title },
				  width_{ width }, height_{ height }, vsync_{ vsync }, samples_{ samples }
		{
			if (renderer)
				renderers.push(renderer);
			else
				renderers.push(std::make_shared<Renderer>());
			renderers.top()->set_application_pointer(this);
		}

		void push_renderer(Renderer_ptr renderer);

		Renderer_ptr top_renderer();

		void pop_renderer();

		void measure(Renderer_ptr renderer, std::string label, size_t no_runs);

		void measure(Renderer_ptr renderer, std::string label, std::chrono::minutes min_runtime);

		void measure(std::shared_ptr<Noise_renderer> renderer, std::string label, size_t min_size,
					 size_t max_size);

		virtual ~Application();

		virtual void run();

		virtual void measure(std::string label, size_t no_runs);

		void measure(std::string label, std::chrono::minutes min_runtime);

		void measure(std::string label, size_t min_size, size_t max_size);

		void set_window_title(std::string title);

		virtual void on_key(int key, int action)
		{
			renderers.top()->on_key(key, action);
		}

		virtual void on_mouse_button(int button, int action)
		{
			renderers.top()->on_mouse_button(button, action);
		}

		virtual void on_mouse_move(int x, int y)
		{
			renderers.top()->on_mouse_move(x, y);
		}

		virtual void on_mouse_wheel(int pos)
		{
			renderers.top()->on_mouse_wheel(pos);
		}

		virtual void on_debug_message(GLenum, GLenum type, GLuint id,
									  GLenum severity, GLsizei, const GLchar* message);

		void get_mouse_position(int& x, int& y);

	protected:
		GLFWwindow* window = nullptr;
		std::stack<Renderer_ptr> renderers;

		static inline Application* getApplication(GLFWwindow* window)
		{
			return (Application*) glfwGetWindowUserPointer(window);
		}

		static inline void glfw_on_key(GLFWwindow* window, int key, int, int action, int)
		{
			getApplication(window)->on_key(key, action);
		}

		static inline void glfw_on_mouse_button(GLFWwindow* window, int button, int action, int)
		{
			getApplication(window)->on_mouse_button(button, action);
		}

		static inline void glfw_on_mouse_move(GLFWwindow* window, double x, double y)
		{
			getApplication(window)->on_mouse_move(static_cast<int> (x),
												  static_cast<int> (y));
		}

		static inline void glfw_on_mouse_wheel(GLFWwindow* window, double, double yoffset)
		{
			getApplication(window)->on_mouse_wheel(static_cast<int> (yoffset));
		}

		void init();

		void startup_stack();

		void shutdown_stack();

		void terminate();

		bool debug_;
		bool fullscreen_;
		std::string title_;
		int width_;
		int height_;
		bool vsync_;
		int samples_;
	};

}
