/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <string>
#include <tuple>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>

namespace OpenGL {

	///Only works with vertexes and normals because that is what I need right now.
	///Ditto for triangles and the index mode.
	///Loads only the vertexes, as I do not use the normals.
	///Requires a OpenGL context, since it uploads the data to a VBO
	std::tuple<GLuint, GLuint> load_obj_file(const std::string& file);

	///Requires a OpenGL context, since it uploads the data to a texture
	GLuint load_texture(std::vector<unsigned char> data);

	///Requires a OpenGL context, since it uploads the data to a texture
	GLuint load_texture(std::vector<float> data);

}
