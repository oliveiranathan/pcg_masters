/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "object_renderer.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

OpenGL::Object_renderer::Object_renderer(const std::string& noise_shader_path,
										 const std::string& object_filename)
		: shader_path{ noise_shader_path }, filename{ object_filename }
{
}

void OpenGL::Object_renderer::startup()
{
	std::vector<OpenGL::Shader_ptr> shaders;
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/object.vert", OpenGL::Shader::Stage::vertex));
	std::vector<std::string> fss = { shader_path, "res/shaders/object.frag" };
	shaders.push_back(OpenGL::Shader::from_files(fss, OpenGL::Shader::Stage::fragment));
	program = OpenGL::Program::create(shaders);
	program->use();
	mvp_location = glGetUniformLocation(program->get_program(), "mvp");
	std::tie(vbo, vertexes) = OpenGL::load_obj_file(filename);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}

void OpenGL::Object_renderer::render(double current_time)
{
	GLuint query;
	glGenQueries(1, &query);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 perspective = glm::perspective(fov, (float) width_ / height_, 0.01f, 100.f);
	glm::mat4 mvp;
	mvp = glm::translate(mvp, glm::vec3{ 0.0, 0.0, -5.0 });
	mvp = glm::rotate(mvp, (float) std::cos(current_time) + 135.f, glm::vec3{ 0.0, 1.0, 0.0 });
	mvp = glm::rotate(mvp, (float) std::sin(current_time), glm::vec3{ 1.0, 0.0, 0.0 });
	mvp = perspective * mvp;
	glUniformMatrix4fv(mvp_location, 1, GL_FALSE, glm::value_ptr(mvp));
	glBeginQuery(GL_SAMPLES_PASSED, query);
	glDrawArrays(GL_TRIANGLES, 0, vertexes);
	glEndQuery(GL_SAMPLES_PASSED);
	queries.push_back(query);
}

void OpenGL::Object_renderer::shutdown()
{
	samples_passed = 0;
	for (GLuint query : queries) {
		unsigned long value = 0;
		glGetQueryObjectui64v(query, GL_QUERY_RESULT, &value);
		samples_passed += value;
	}
	glDeleteQueries((GLsizei) queries.size(), &queries[0]);
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
}

std::string OpenGL::Object_renderer::specific_metrics()
{
	return "Samples passed: " + std::to_string(samples_passed);
}
