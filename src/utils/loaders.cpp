/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "loaders.h"

#include <fstream>
#include <sstream>
#include <vector>

#include <glm/vec3.hpp>

std::tuple<GLuint, GLuint> OpenGL::load_obj_file(const std::string& file)
{
	std::vector<glm::vec3> vertexes;
	vertexes.emplace_back(0.f, 0.f, 0.f);///To match with the obj indexing mode
	std::vector<glm::vec3> buffer_data;
	std::ifstream obj_input{ file };
	std::string next_line;
	while (std::getline(obj_input, next_line)) {
		std::istringstream line{ next_line };
		std::string type;
		if (line >> type) {
			if (type == "v") {///Vertex
				glm::vec3 vertex;
				line >> vertex.x >> vertex.y >> vertex.z;
				vertexes.push_back(vertex);
			} else if (type == "f") {///Face
				for (char index = 0; index < 3; ++index) {
					unsigned int vertex_index, normal_index;
					line >> vertex_index;
					line.ignore(2);///Eats the //
					line >> normal_index;///Eats the normal
					buffer_data.push_back(vertexes[vertex_index]);
				}
			}
		}
	}
	GLuint vbo;
	glCreateBuffers(1, &vbo);
	glNamedBufferData(vbo, buffer_data.size() * sizeof(glm::vec3), &buffer_data[0], GL_STATIC_DRAW);
	return std::make_tuple(vbo, buffer_data.size());
}

GLuint OpenGL::load_texture(std::vector<unsigned char> data)
{
	GLuint tex;
	glCreateTextures(GL_TEXTURE_2D, 1, &tex);
	if (tex) {
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTextureStorage2D(tex, 1, GL_R8UI, GLsizei(data.size()), 1);
		glTextureSubImage2D(tex, 0, 0, 0, GLsizei(data.size()), 1,
							GL_RED_INTEGER, GL_UNSIGNED_BYTE, &data[0]);
		glPixelStorei(GL_PACK_ALIGNMENT, 4);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	}
	return tex;
}

GLuint OpenGL::load_texture(std::vector<float> data)
{
	GLuint tex;
	glCreateTextures(GL_TEXTURE_2D, 1, &tex);
	if (tex) {
		glTextureStorage2D(tex, 1, GL_R32F, GLsizei(data.size()), 1);
		glTextureSubImage2D(tex, 0, 0, 0, GLsizei(data.size()), 1,
							GL_RED, GL_FLOAT, &data[0]);
	}
	return tex;
}
