/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "texture_renderer.h"

#include <algorithm>

void OpenGL::Texture_renderer::startup()
{
	std::vector<OpenGL::Shader_ptr> shaders;
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/noise.vert", OpenGL::Shader::Stage::vertex));
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/noise.frag", OpenGL::Shader::Stage::fragment));
	program = OpenGL::Program::create(shaders);
	program->use();
	uint8_texture image = create_texture(width_, height_);
	unsigned char* data = new unsigned char[width_ * height_];
	for (int j = 0; j < height_; ++j) {
		std::copy(image[j].begin(), image[j].end(), &data[j * width_]);
	}
	glCreateTextures(GL_TEXTURE_2D, 1, &texture);
	glTextureStorage2D(texture, 1, GL_R32F, width_, height_);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTextureSubImage2D(texture, 0, 0, 0, width_, height_, GL_RED, GL_UNSIGNED_BYTE, data);
	delete[] data;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
}

void OpenGL::Texture_renderer::render(double)
{
	static const GLfloat green[] = { 0.0f, 0.25f, 0.0f, 1.0f };
	glClearBufferfv(GL_COLOR, 0, green);
	program->use();
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void OpenGL::Texture_renderer::shutdown()
{
	if (texture) {
		glDeleteTextures(1, &texture);
	}
	if (vao) {
		glDeleteVertexArrays(1, &vao);
	}
	program = nullptr;
}
