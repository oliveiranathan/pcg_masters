/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "shader.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace OpenGL {

	class Program;

	using Program_ptr = std::shared_ptr<Program>;

	class program_creation_error : public std::runtime_error {
	public:

		program_creation_error(const std::string& __arg) : runtime_error(__arg)
		{
		}

	};

	class program_link_error : public std::runtime_error {
	public:

		program_link_error(const std::string& __arg) : runtime_error(__arg)
		{
		}

	};

	class Program {
	public:
		~Program();

		void use();

		void close();

		GLuint get_program();

		void relink();

		static inline Program_ptr create(const std::vector<Shader_ptr>& shaders)
		{
			return Program_ptr{ new Program(shaders) };
		}

	private:
		Program(const std::vector<Shader_ptr>& shaders);

		GLuint program_id;
	};

}
