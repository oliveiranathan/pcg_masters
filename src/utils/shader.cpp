/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "shader.h"

#include <iostream>

namespace OpenGL {

	Shader::Shader(const std::string& source, Stage stage) : shader_id{ 0 }, stage_{ stage }
	{
		create_shader_object();
		const char* c_source = source.c_str();
		glShaderSource(shader_id, 1, &c_source, nullptr);
		glCompileShader(shader_id);
		check_shader();
	}

	Shader::Shader(const std::vector<std::string>& sources, Shader::Stage stage)
			: shader_id{ 0 }, stage_{ stage }
	{
		std::string all = "";
		for (const std::string& source : sources) {
			all += source + "\n";
		}
		create_shader_object();
		const char* c_source = all.c_str();
		glShaderSource(shader_id, 1, &c_source, nullptr);
		glCompileShader(shader_id);
#ifdef DEBUG
		check_shader();
#endif
	}

	void Shader::create_shader_object()
	{
		switch (stage_) {
			case Stage::fragment:
				shader_id = glCreateShader(GL_FRAGMENT_SHADER);
				break;
			case Stage::geometry:
				shader_id = glCreateShader(GL_GEOMETRY_SHADER);
				break;
			case Stage::tesselation_control:
				shader_id = glCreateShader(GL_TESS_CONTROL_SHADER);
				break;
			case Stage::tesselation_evaluation:
				shader_id = glCreateShader(GL_TESS_EVALUATION_SHADER);
				break;
			case Stage::vertex:
				shader_id = glCreateShader(GL_VERTEX_SHADER);
				break;
		}
		if (!shader_id) {
			throw shader_creation_error{ "Error allocating the shader." };
		}
#ifdef DEBUG
		std::cerr << "SHADER#" << shader_id << "\n";
#endif
	}

	void Shader::check_shader()
	{
		GLint status = { 0 };
		glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
		if (!status) {
			GLint max_length = 1024;
			char* raw = new char[max_length];
			glGetShaderInfoLog(shader_id, max_length, &max_length, raw);
			std::cerr << "LOG: " << raw << "\n";
			glDeleteShader(shader_id);
			std::string l{ raw };
			delete[] raw;
			throw shader_compilation_error{ l };
		}
	}

	Shader::~Shader()
	{
#ifdef DEBUG
		std::cerr << "SHADER#" << shader_id << " deleted.\n";
#endif
		glDeleteShader(shader_id);
	}

	Shader_ptr Shader::from_string(const std::string& source, Shader::Stage stage)
	{
		return Shader_ptr{ new Shader(source, stage) };
	}

	Shader_ptr Shader::from_file(const std::string& filename, Shader::Stage stage)
	{
		std::ifstream in;
		in.open(filename);
		if (!in) {
			throw shader_creation_error("File " + filename + " not found.");
		}
		std::stringstream ss;
		ss << in.rdbuf();
		return from_string(ss.str(), stage);
	}

	Shader_ptr Shader::from_strings(const std::vector<std::string>& sources,
												 Shader::Stage stage)
	{
		return Shader_ptr{ new Shader(sources, stage) };
	}

	Shader_ptr Shader::from_files(const std::vector<std::string>& filenames,
											   Shader::Stage stage)
	{
		std::vector<std::string> strings;
		for (const std::string& filename : filenames) {
			std::ifstream in;
			in.open(filename);
			if (!in) {
				throw shader_creation_error("File " + filename + " not found.");
			}
			std::stringstream ss;
			ss << in.rdbuf();
			strings.push_back(ss.str());
		}
		return from_strings(strings, stage);
	}

}