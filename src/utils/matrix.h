/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <stdexcept>
#include <string>
#include <vector>

namespace Utils {

	class Matrix_exception : public std::runtime_error {
	public:

		Matrix_exception(const std::string& __arg) : runtime_error(__arg)
		{
		}

	};

	template <typename T>
	class Matrix {
	public:
		Matrix(long width, long height)
				: width_{ width }, height_{ height }
		{
#ifdef DEBUG
			if (width <= 0 || height <= 0) {
				throw Matrix_exception("Width and Height should be > 0.");
			}
#endif
			data.resize(width_ * height_);
		}

		inline long get_width() const
		{
			return width_;
		}

		inline long get_height() const
		{
			return height_;
		}

		inline T& operator()(long w, long h)
		{
#ifdef DEBUG
			if (w >= width_ || w < 0) {
				throw Matrix_exception("Parameter w > width or w < 0. w = " + std::to_string(w) + ".");
			}
			if (h >= height_ || h < 0) {
				throw Matrix_exception("Parameter h > height or h < 0. h = " + std::to_string(h) + ".");
			}
#endif
			return data[h * width_ + w];
		}

		inline T operator()(long w, long h) const
		{
#ifdef DEBUG
			if (w >= width_ || w < 0) {
				throw Matrix_exception("Parameter w > width or w < 0.");
			}
			if (h >= height_ || h < 0) {
				throw Matrix_exception("Parameter h > height or h < 0.");
			}
#endif
			return data[h * width_ + w];
		}

	private:
		long width_;
		long height_;
		std::vector<T> data;
	};

}
