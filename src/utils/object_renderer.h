/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "application.h"
#include "loaders.h"

#include <glm/mat4x4.hpp>

namespace OpenGL {

	class Object_renderer : public OpenGL::Renderer {
	public:
		/// The armadillo obj file was obtained from http://www.prinmath.com/csci5229/OBJ/index.html
		Object_renderer(const std::string& noise_shader_path,
						const std::string& object_filename = "res/assets/armadillo.obj");

		virtual void startup() override;

		virtual void render(double current_time) override;

		virtual void shutdown() override;

		virtual std::string specific_metrics() override;

	protected:
		std::string shader_path;
		std::string filename;
		OpenGL::Program_ptr program;
		GLuint vbo;
		GLuint vao;
		GLuint vertexes;
		GLint mvp_location;
		static float constexpr fov = 45.f;
		std::vector<GLuint> queries;
		unsigned long long samples_passed;
	};

}
