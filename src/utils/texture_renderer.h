/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "application.h"
#include "program.h"

#include <memory>
#include <vector>

namespace OpenGL {

	using uint8_texture = std::vector<std::vector<unsigned char>>;

	class Texture_renderer : public Renderer {
	public:
		virtual uint8_texture create_texture(long w, long h) = 0;

	protected:
		void startup() override;

		void render(double) override;

		void shutdown() override;

	private:
		OpenGL::Program_ptr program;
		GLuint vao{ 0 };
		GLuint texture{ 0 };
	};

}
