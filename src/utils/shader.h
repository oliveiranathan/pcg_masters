/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <fstream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace OpenGL {

	class Shader;

	using Shader_ptr = std::shared_ptr<Shader>;

	class shader_creation_error : public std::runtime_error {
	public:

		shader_creation_error(const std::string& __arg) : runtime_error(__arg)
		{
		}

	};

	class shader_compilation_error : public std::runtime_error {
	public:

		shader_compilation_error(const std::string& __arg) : runtime_error(__arg)
		{
		}

	};

	class Shader {
	public:

		friend class Program;

		enum class Stage {
			vertex, tesselation_control, tesselation_evaluation, geometry, fragment
		};

		static Shader_ptr from_string(const std::string& source, Stage stage);

		static Shader_ptr from_file(const std::string& filename, Stage stage);

		static Shader_ptr from_strings(const std::vector<std::string>& sources, Stage stage);

		static Shader_ptr from_files(const std::vector<std::string>& filenames, Stage stage);

		~Shader();

	private:
		Shader(const std::string& source, Stage stage);

		Shader(const std::vector<std::string>& sources, Stage stage);

		void create_shader_object();

		void check_shader();

		GLuint shader_id;
		Stage stage_;
	};

}
