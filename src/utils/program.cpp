/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "program.h"

#include <iostream>


namespace OpenGL {

	Program::Program(const std::vector<Shader_ptr>& shaders) : program_id{ glCreateProgram() }
	{
		if (!program_id) {
			throw program_creation_error{ "Error allocating the program." };
		}
#ifdef DEBUG
		std::cerr << "PROGRAM#" << program_id << "\n";
#endif
		for (auto& shader : shaders) {
			glAttachShader(program_id, shader->shader_id);
		}
		relink();
	}

	void Program::relink()
	{
		glLinkProgram(program_id);
#ifdef DEBUG
		GLint status = 0;
		glGetProgramiv(program_id, GL_LINK_STATUS, &status);
		if (!status) {
			GLint max_length = 1024;
			char* raw = new char[max_length];
			glGetProgramInfoLog(program_id, max_length, &max_length, raw);
			std::cerr << "LOG: " << raw << "\n";
			glDeleteProgram(program_id);
			std::string l{ raw };
			delete[] raw;
			throw program_link_error{ l };
		}
#endif
	}

	Program::~Program()
	{
		close();
	}

	void Program::close()
	{
		if (program_id) {
#ifdef DEBUG
			std::cerr << "PROGRAM#" << program_id << " deleted.\n";
#endif
			glDeleteProgram(program_id);
			program_id = 0;
		}
	}

	void Program::use()
	{
		glUseProgram(program_id);
	}

	GLuint Program::get_program()
	{
		return program_id;
	}

}