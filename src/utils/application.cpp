/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * Some parts of this file where based on the application code from the OpenGL SuperBible,
 * and are subject to the following license:
 * Copyright © 2012-2013 Graham Sellers
 *
 * This code is part of the OpenGL SuperBible, 6th Edition.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "application.h"

#include <glm/gtc/matrix_transform.hpp>

void APIENTRY OpenGL::debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity,
									 GLsizei length, const GLchar* message, const void* userParam)
{
	((Application*) userParam)->on_debug_message(source, type, id, severity, length, message);
}

OpenGL::Application::~Application()
{
	if (window) {
		while (!renderers.empty()) {
			renderers.top()->shutdown();
			renderers.pop();
		}
		glfwDestroyWindow(window);
		glfwTerminate();
		std::cerr << "OpenGL terminated.\n";
	}
}

void OpenGL::Application::push_renderer(Renderer_ptr renderer)
{
	if (window) {
		renderer->set_size(width_, height_);
		renderer->startup();
	}
	renderers.push(renderer);
}

OpenGL::Renderer_ptr OpenGL::Application::top_renderer()
{
	return renderers.top();
}

void OpenGL::Application::pop_renderer()
{
	renderers.top()->shutdown();
	renderers.pop();
	if (renderers.empty()) {
		renderers.push(std::make_shared<Renderer>());
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}

void OpenGL::Application::startup_stack()
{
	std::stack<Renderer_ptr> tmp;
	while (!renderers.empty()) {
		tmp.push(renderers.top());
		renderers.pop();
	}
	while (!tmp.empty()) {
		tmp.top()->set_size(width_, height_);
		tmp.top()->startup();
		renderers.push(tmp.top());
		tmp.pop();
	}
}

void OpenGL::Application::shutdown_stack()
{
	while (!renderers.empty()) {
		renderers.top()->shutdown();
		renderers.pop();
	}
}

void OpenGL::Application::run()
{
	init();
	startup_stack();
	bool should_escape = false;
	do {
		renderers.top()->render(glfwGetTime());
		glfwSwapBuffers(window);
		glfwPollEvents();
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			should_escape = true;
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_RELEASE && should_escape) {
			pop_renderer();
			should_escape = false;
		}
	} while (!glfwWindowShouldClose(window));
	shutdown_stack();
	terminate();
}

void OpenGL::Application::measure(std::string label, size_t no_runs)
{
	init();
	auto begin = std::chrono::high_resolution_clock::now();
	startup_stack();
	auto started = std::chrono::high_resolution_clock::now();
	GLuint queries[2];
	glGenQueries(2, queries);
	glQueryCounter(queries[0], GL_TIMESTAMP);
	for (size_t run = 0; run < no_runs; ++run) {
		renderers.top()->render(glfwGetTime());
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glQueryCounter(queries[1], GL_TIMESTAMP);
	auto stopped = std::chrono::high_resolution_clock::now();
	auto top = renderers.top();
	shutdown_stack();
	auto shutdown = std::chrono::high_resolution_clock::now();
	GLuint64 ini, end;
	glGetQueryObjectui64v(queries[0], GL_QUERY_RESULT, &ini);
	glGetQueryObjectui64v(queries[1], GL_QUERY_RESULT, &end);
	glDeleteQueries(2, queries);
	std::cout << label << ":\n"
			  << "Executed " << no_runs << " times. Total times (in ns) are:"
			  << "\nCPU time:\nStart: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(started
																	  - begin).count()
			  << "\nRun: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(stopped
																	  - started).count()
			  << "\nStop: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(shutdown
																	  - stopped).count()
			  << "\nGPU time: " << (end - ini)
			  << "\nAveraged results:"
			  << "\nCPU Run: "
			  << (std::chrono::duration_cast<std::chrono::nanoseconds>(stopped
																	   - started).count() / no_runs)
			  << "\nGPU time: " << ((end - ini) / no_runs) << "\n";
	std::string specific = top->specific_metrics();
	if (!specific.empty())
		std::cout << specific << "\n";
	std::cout << "-----------------------------------------"
			  << "---------------------------------------\n";
	terminate();
}

void OpenGL::Application::measure(std::string label, std::chrono::minutes min_runtime)
{
	init();
	auto begin = std::chrono::high_resolution_clock::now();
	startup_stack();
	auto started = std::chrono::high_resolution_clock::now();
	GLuint queries[2];
	glGenQueries(2, queries);
	glQueryCounter(queries[0], GL_TIMESTAMP);
	size_t runs_done = 0; /// If the minimum runtime is too big, this could overflow...
	while (min_runtime > (std::chrono::high_resolution_clock::now() - started)) {
		renderers.top()->render(glfwGetTime());
		++runs_done;
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glQueryCounter(queries[1], GL_TIMESTAMP);
	auto stopped = std::chrono::high_resolution_clock::now();
	auto top = renderers.top();
	shutdown_stack();
	auto shutdown = std::chrono::high_resolution_clock::now();
	GLuint64 ini, end;
	glGetQueryObjectui64v(queries[0], GL_QUERY_RESULT, &ini);
	glGetQueryObjectui64v(queries[1], GL_QUERY_RESULT, &end);
	glDeleteQueries(2, queries);
	std::cout << label << ":\n"
			  << "Executed " << runs_done << " times. Total times (in ns) are:"
			  << "\nCPU time:\nStart: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(started
																	  - begin).count()
			  << "\nRun: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(stopped
																	  - started).count()
			  << "\nStop: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(shutdown
																	  - stopped).count()
			  << "\nGPU time: " << (end - ini)
			  << "\nAveraged results:"
			  << "\nCPU Run: "
			  << (std::chrono::duration_cast<std::chrono::nanoseconds>(stopped
																	   - started).count() / runs_done)
			  << "\nGPU time: " << ((end - ini) / runs_done) << "\n";
	std::string specific = top->specific_metrics();
	if (!specific.empty())
		std::cout << specific << "\n";
	std::cout << "-----------------------------------------"
			  << "---------------------------------------\n";
	terminate();
}

void OpenGL::Application::measure(std::string label, size_t min_size, size_t max_size)
{
	init();
	startup_stack();
	GLuint fbo{ 0 };
	GLuint texture{ 0 };
	size_t num_execs{ 8 };
	glCreateFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	std::cout << label << "\n";
	for (size_t size = min_size; size <= max_size; size *= 2) {
		long time = 0;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, size, size);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);
		static const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, draw_buffers);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		glViewport(0, 0, size, size);
		for (size_t exec = 0; exec < num_execs; ++exec) {
			static const GLfloat green[] = { 0.0f, 0.25f, 0.0f, 1.0f };
			glClearBufferfv(GL_COLOR, 0, green);
			glGetError();
			GLuint queries[2];
			glGenQueries(2, queries);
			glQueryCounter(queries[0], GL_TIMESTAMP);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glQueryCounter(queries[1], GL_TIMESTAMP);
			GLuint64 begin, end;
			glGetQueryObjectui64v(queries[0], GL_QUERY_RESULT, &begin);
			glGetQueryObjectui64v(queries[1], GL_QUERY_RESULT, &end);
			glDeleteQueries(2, queries);
			time += (end - begin) / num_execs;
			glfwSwapBuffers(window);
			glfwPollEvents();
		}
		glDeleteTextures(1, &texture);
		std::cout << "sides: " << size << " -> time: " << time << "\n";
	}
	glDeleteFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, width_, height_);
	shutdown_stack();
	std::cout << "-----------------------------------------"
			  << "---------------------------------------\n";
	terminate();
}

void OpenGL::Application::measure(Renderer_ptr renderer,
								  std::string label, size_t no_runs)
{
	renderer->set_size(width_, height_);
	renderers.push(renderer);
	auto begin = std::chrono::high_resolution_clock::now();
	renderer->startup();
	auto started = std::chrono::high_resolution_clock::now();
	GLuint queries[2];
	glGenQueries(2, queries);
	glQueryCounter(queries[0], GL_TIMESTAMP);
	for (size_t run = 0; run < no_runs; ++run) {
		renderer->render(glfwGetTime());
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glQueryCounter(queries[1], GL_TIMESTAMP);
	auto stopped = std::chrono::high_resolution_clock::now();
	renderer->shutdown();
	auto shutdown = std::chrono::high_resolution_clock::now();
	GLuint64 ini, end;
	glGetQueryObjectui64v(queries[0], GL_QUERY_RESULT, &ini);
	glGetQueryObjectui64v(queries[1], GL_QUERY_RESULT, &end);
	glDeleteQueries(2, queries);
	std::cout << label << ":\n"
			  << "Executed " << no_runs << " times. Total times (in ns) are:"
			  << "\nCPU time:\nStart: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(started
																	  - begin).count()
			  << "\nRun: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(stopped
																	  - started).count()
			  << "\nStop: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(shutdown
																	  - stopped).count()
			  << "\nGPU time: " << (end - ini)
			  << "\nAveraged results:"
			  << "\nCPU Run: "
			  << (std::chrono::duration_cast<std::chrono::nanoseconds>(stopped
																	   - started).count() / no_runs)
			  << "\nGPU time: " << ((end - ini) / no_runs) << "\n";
	std::string specific = renderer->specific_metrics();
	if (!specific.empty())
		std::cout << specific << "\n";
	renderers.pop();
	std::cout << "-----------------------------------------"
			  << "---------------------------------------\n";
}

void OpenGL::Application::measure(Renderer_ptr renderer,
								  std::string label, std::chrono::minutes min_runtime)
{
	set_window_title(label);
	renderer->set_size(width_, height_);
	renderers.push(renderer);
	auto begin = std::chrono::high_resolution_clock::now();
	renderer->startup();
	auto started = std::chrono::high_resolution_clock::now();
	GLuint queries[2];
	glGenQueries(2, queries);
	glQueryCounter(queries[0], GL_TIMESTAMP);
	size_t runs_done = 0; /// If the minimum runtime is too big, this could overflow...
	while (min_runtime > (std::chrono::high_resolution_clock::now() - started)) {
		renderer->render(glfwGetTime());
		++runs_done;
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glQueryCounter(queries[1], GL_TIMESTAMP);
	auto stopped = std::chrono::high_resolution_clock::now();
	renderer->shutdown();
	auto shutdown = std::chrono::high_resolution_clock::now();
	GLuint64 ini, end;
	glGetQueryObjectui64v(queries[0], GL_QUERY_RESULT, &ini);
	glGetQueryObjectui64v(queries[1], GL_QUERY_RESULT, &end);
	glDeleteQueries(2, queries);
	std::cout << label << ":\n"
			  << "Executed " << runs_done << " times. Total times (in ns) are:"
			  << "\nCPU time:\nStart: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(started
																	  - begin).count()
			  << "\nRun: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(stopped
																	  - started).count()
			  << "\nStop: "
			  << std::chrono::duration_cast<std::chrono::nanoseconds>(shutdown
																	  - stopped).count()
			  << "\nGPU time: " << (end - ini)
			  << "\nAveraged results:"
			  << "\nCPU Run: "
			  << (std::chrono::duration_cast<std::chrono::nanoseconds>(stopped
																	   - started).count() / runs_done)
			  << "\nGPU time: " << ((end - ini) / runs_done) << "\n";
	std::string specific = renderer->specific_metrics();
	if (!specific.empty())
		std::cout << specific << "\n";
	renderers.pop();
	std::cout << "-----------------------------------------"
			  << "---------------------------------------\n";
}

void OpenGL::Application::measure(std::shared_ptr<Noise_renderer> renderer,
								  std::string label, size_t min_size, size_t max_size)
{
	renderer->startup();
	GLuint fbo{ 0 };
	GLuint texture{ 0 };
	size_t num_execs{ 8 };
	glCreateFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	std::cout << label << "\n";
	for (size_t size = min_size; size <= max_size; size *= 2) {
		long time = 0;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, size, size);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);
		static const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, draw_buffers);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		glViewport(0, 0, size, size);
		for (size_t exec = 0; exec < num_execs; ++exec) {
			static const GLfloat green[] = { 0.0f, 0.25f, 0.0f, 1.0f };
			glClearBufferfv(GL_COLOR, 0, green);
			glGetError();
			GLuint queries[2];
			glGenQueries(2, queries);
			glQueryCounter(queries[0], GL_TIMESTAMP);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glQueryCounter(queries[1], GL_TIMESTAMP);
			GLuint64 begin, end;
			glGetQueryObjectui64v(queries[0], GL_QUERY_RESULT, &begin);
			glGetQueryObjectui64v(queries[1], GL_QUERY_RESULT, &end);
			glDeleteQueries(2, queries);
			time += (end - begin) / num_execs;
			glfwSwapBuffers(window);
			glfwPollEvents();
		}
		glDeleteTextures(1, &texture);
		std::cout << "sides: " << size << " -> time: " << time << "\n";
	}
	glDeleteFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, width_, height_);
	renderer->shutdown();
	std::cout << "-----------------------------------------"
			  << "---------------------------------------\n";
}

void OpenGL::Application::set_window_title(std::string title)
{
	glfwSetWindowTitle(window, title.c_str());
}

void OpenGL::Application::get_mouse_position(int& x, int& y)
{
	double dx, dy;
	glfwGetCursorPos(window, &dx, &dy);
	x = static_cast<int> (std::floor(dx));
	y = static_cast<int> (std::floor(dy));
}

void OpenGL::Application::init()
{
	if (!glfwInit()) {
		throw glfw_init_failure{ "Failure to init GLFW." };
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, samples_);
	if (debug_) {
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	}
	window = glfwCreateWindow(width_, height_, title_.c_str(),
							  fullscreen_ ? glfwGetPrimaryMonitor() : nullptr, nullptr);
	if (!window) {
		throw window_init_failure{ "Failure to create the window." };
	}
	glfwSetWindowUserPointer(window, this);
	glfwMakeContextCurrent(window);
	glfwSwapInterval((int) vsync_);
	glfwSetKeyCallback(window, glfw_on_key);
	glfwSetMouseButtonCallback(window, glfw_on_mouse_button);
	glfwSetCursorPosCallback(window, glfw_on_mouse_move);
	glfwSetScrollCallback(window, glfw_on_mouse_wheel);
	glewExperimental = true;
	glewInit();
	glGetError(); /// eats the 1280 error from the init method...
	if (debug_) {
		glDebugMessageCallback(debug_callback, this);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	}
}

void OpenGL::Application::terminate()
{
	glfwDestroyWindow(window);
	window = nullptr;
	glfwTerminate();
}

void OpenGL::Application::on_debug_message(GLenum, GLenum type, GLuint id, GLenum severity,
										   GLsizei, const GLchar* message)
{
	std::cerr << "---------------------opengl-callback-start------------\n";
	std::cerr << "message: " << message << "\n";
	std::cerr << "type: ";
	switch (type) {
		case GL_DEBUG_TYPE_ERROR:
			std::cerr << "ERROR";
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			std::cerr << "DEPRECATED_BEHAVIOR";
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			std::cerr << "UNDEFINED_BEHAVIOR";
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			std::cerr << "PORTABILITY";
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			std::cerr << "PERFORMANCE";
			break;
		case GL_DEBUG_TYPE_OTHER:
			std::cerr << "OTHER";
			break;
		default:
			break;
	}
	std::cerr << "\n";
	std::cerr << "id: " << id << "\n";
	std::cerr << "severity: ";
	switch (severity) {
		case GL_DEBUG_SEVERITY_LOW:
			std::cerr << "LOW";
			break;
		case GL_DEBUG_SEVERITY_MEDIUM:
			std::cerr << "MEDIUM";
			break;
		case GL_DEBUG_SEVERITY_HIGH:
			std::cerr << "HIGH";
			break;
		case GL_DEBUG_SEVERITY_NOTIFICATION:
			std::cerr << "NOTIFICATION";
			break;
		default:
			break;
	}
	std::cerr << "\n";
	std::cerr << "---------------------opengl-callback-end--------------\n";
}

OpenGL::Noise_renderer::Noise_renderer(const std::string& noise_shader_path)
		: shader_path{ noise_shader_path }
{
}

void OpenGL::Noise_renderer::startup()
{
	std::vector<OpenGL::Shader_ptr> shaders;
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/noise.vert", OpenGL::Shader::Stage::vertex));
	std::vector<std::string> fss = { shader_path, "res/shaders/noise_glsl.frag" };
	shaders.push_back(OpenGL::Shader::from_files(fss, OpenGL::Shader::Stage::fragment));
	program = OpenGL::Program::create(shaders);
	program->use();
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
}

void OpenGL::Noise_renderer::render(double)
{
	GLuint query;
	glGenQueries(1, &query);
	glClear(GL_COLOR_BUFFER_BIT);
	glBeginQuery(GL_SAMPLES_PASSED, query);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glEndQuery(GL_SAMPLES_PASSED);
	queries.push_back(query);
}

void OpenGL::Noise_renderer::shutdown()
{
	samples_passed = 0;
	for (GLuint query : queries) {
		unsigned long value = 0;
		glGetQueryObjectui64v(query, GL_QUERY_RESULT, &value);
		samples_passed += value;
	}
	glDeleteQueries(queries.size(), &queries[0]);
	glDeleteVertexArrays(1, &vao);
}

std::string OpenGL::Noise_renderer::specific_metrics()
{
	return "Samples passed: " + std::to_string(samples_passed);
}

std::string OpenGL::Renderer::specific_metrics()
{
	return "";
}
