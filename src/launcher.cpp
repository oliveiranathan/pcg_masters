/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "launcher.h"
#include "utils/application.h"
#include "pcg/elements/fire.h"
#include "pcg/elements/particles.h"
#include "pcg/elements/ocean.h"
#include "pcg/elements/clouds.h"
#include "pcg/noise/noise_glsl.h"
#include "pcg/noise/value_noise.h"
#include "pcg/noise/gradient_noise.h"
#include "pcg/noise/lattice_convolution.h"
#include "pcg/noise/sparse_convolution.h"
#include "pcg/noise/perlin_noise.h"
#include "pcg/noise/opensimplex.h"
#include "pcg/noise/gabor.h"
#include "pcg/towns/buildings.h"
#include "pcg/towns/agents.h"
#include "pcg/towns/roads.h"
#include "pcg/height_maps/diamond_square.h"
#include "pcg/height_maps/bissection.h"
#include "pcg/names/finite_state.h"
#include "pcg/names/markov.h"
#include "pcg/names/markov_n.h"
#include "pcg/vegetation/proctree.h"
#include "pcg/vegetation/ifs.h"
#include "pcg/vegetation/l_system.h"
#include "pcg/indoor_maps/ca_cave.h"
#include "pcg/indoor_maps/dla_cave.h"
#include "pcg/indoor_maps/maze.h"
#include "pcg/indoor_maps/dungeon_a.h"
#include "pcg/indoor_maps/dungeon_b.h"
#include "pcg/noise/noise_glsl_object.h"

#include <thread>

#include <png++/png.hpp>

namespace {

	using Image = std::vector<std::vector<float>>;

	size_t noise_num_threads = 8;
	size_t noise_num_execs = 10;
	size_t noise_min_size = 256;
	size_t noise_max_size = 16384;

	template <class Noise>
	void block_runner(Image& image, size_t minLine, size_t maxLine, Noise& noise)
	{
		size_t side = image.size();
		for (size_t h = minLine; h <= maxLine; ++h) {
			for (size_t w = 0; w < side; ++w) {
				image[h][w] = noise.noise((float) w * 0.0625f, (float) h * 0.0625f, 0.0f);
			}
		}
	}

	template <class Noise>
	void measure_noise(const std::string& name, Noise& noise)
	{
		std::cout << name << " noise 3D:\n";
		for (size_t size = noise_min_size; size <= noise_max_size; size *= 2) {
			long time = 0;
			Image image;
			image.resize(size);
			for (size_t i = 0; i < size; ++i) {
				image[i].resize(size);
			}
			for (size_t exec = 0; exec < noise_num_execs; ++exec) {
				std::vector<std::thread> threads;
				size_t lot_size = size / noise_num_threads;
				auto start = std::chrono::high_resolution_clock::now();
				for (size_t thread = 0; thread < noise_num_threads; ++thread) {
					threads.push_back(std::thread(block_runner<Noise>,
												  std::ref(image),
												  thread * lot_size,
												  (thread + 1) * lot_size - 1,
												  std::ref(noise)));
				}
				for (auto& th : threads)
					th.join();
				auto end = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
				time += runtime.count() / noise_num_execs;
			}
			std::cout << "sides: " << size << " -> time: " << time << "\n";
		}
		std::cout << "-----------------------------------------"
				  << "---------------------------------------\n";
	}

	size_t no_runs = 10;

	void town_agents()
	{
		std::cout << "Town - Agents with 5000 iterations:\n";
		unsigned long long time = 0;
		bool snapshot = false;
		for (size_t run = 0; run < no_runs; ++run) {
			auto start = std::chrono::high_resolution_clock::now();
			PCG::Towns::Agent_based app{ 5000, 10, 100, 100 };
			auto end = std::chrono::high_resolution_clock::now();
			std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
			time += runtime.count() / no_runs;
			if (!snapshot) {
				snapshot = true;
				app.export_to_png("tmp.png");
			}
		}
		std::cout << "time " << time << "\n";
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void town_l_systems()
	{
		std::cout << "Town - L-systems:\n";
		unsigned long long time = 0;
		bool snapshot = false;
		for (size_t run = 0; run < no_runs; ++run) {
			auto start = std::chrono::high_resolution_clock::now();
			PCG::Towns::L_systems_based app{ 700, 700, "res/assets/dmap.png", "res/assets/hmap.png", 0, 350 };
			auto end = std::chrono::high_resolution_clock::now();
			std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
			time += runtime.count() / no_runs;
			if (!snapshot) {
				snapshot = true;
				app.export_to_png("tmp.png");
			}
		}
		std::cout << "time " << time << "\n";
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	size_t min_size = 256;
	size_t max_size = 4096;

	void heightmap_ds()
	{
		std::cout << "Heightmap - DS:\n";
		bool snapshot = false;
		for (size_t size = min_size; size <= max_size; size *= 2) {
			unsigned long long time = 0;
			for (size_t run = 0; run < no_runs; ++run) {
				auto start = std::chrono::high_resolution_clock::now();
				PCG::Heightmaps::Fractal_diamond_square app{ long(size), 1, 0.5 };
				auto end = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
				time += runtime.count() / no_runs;
				if (size == max_size && !snapshot) {
					snapshot = true;
					app.export_to_png("tmp.png");
				}
			}
			std::cout << "time for " << size << " sides: " << time << "\n";
		}
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void heightmap_bissection()
	{
		std::cout << "Heightmap - bissection:\n";
		bool snapshot = false;
		for (size_t size = min_size; size <= max_size; size *= 2) {
			unsigned long long time = 0;
			for (size_t run = 0; run < no_runs; ++run) {
				auto start = std::chrono::high_resolution_clock::now();
				PCG::Heightmaps::Fractal_bissection app{ long(size), long(size), 5000, 50, 12 };
				auto end = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
				time += runtime.count() / no_runs;
				if (size == max_size && !snapshot) {
					snapshot = true;
					app.export_to_png("tmp.png");
				}
			}
			std::cout << "time for " << size << " sides: " << time << "\n";
		}
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	size_t no_names = 100000;

	void names_fsm()
	{
		std::cout << "Names - Finite state (does not have big init costs):\n";
		PCG::Names::Finite_state app;
		std::cout << "Generating " << no_names << " with at most 9 letters...\n";
		auto start = std::chrono::high_resolution_clock::now();
		size_t i = 0;
		std::string sample;
		for (; i < 100; ++i)
			sample += app(9) + "\n";
		for (; i < no_names; ++i)
			std::string str = app(9);
		auto end = std::chrono::high_resolution_clock::now();
		std::cout << "First 100 as sample:\n" << sample;
		std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
		std::cout << "time: " << runtime.count() << "\n";
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	/// For the words, were used two names lists, one for people and one for places.
	/// These lists came from: http://www.gutenberg.org/ebooks/3201

	void names_markov_sep()
	{
		std::cout << "Names - Markov separate statistics:\n";
		auto start = std::chrono::high_resolution_clock::now();
		PCG::Names::Markov_special_statistics app_p{ "res/assets/places.txt" };
		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
		std::cout << "init(places) time: " << runtime.count() << "\n";
		start = std::chrono::high_resolution_clock::now();
		PCG::Names::Markov_special_statistics app{ "res/assets/names.txt" };
		end = std::chrono::high_resolution_clock::now();
		runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
		std::cout << "init(names) time: " << runtime.count() << "\n";
		std::cout << "Generating " << no_names << " with at least 3 and at most 9 letters...\n";
		start = std::chrono::high_resolution_clock::now();
		size_t i = 0;
		std::string sample;
		for (; i < 100; ++i)
			sample += app(3, 9) + "\n";
		for (; i < no_names; ++i)
			std::string str = app(3, 9);
		end = std::chrono::high_resolution_clock::now();
		std::cout << "First 100 as sample:\n" << sample;
		runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
		std::cout << "time: " << runtime.count() << "\n";
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void names_markov_n()
	{
		std::cout << "Names - Markov n:\n";
		auto start = std::chrono::high_resolution_clock::now();
		PCG::Names::Markov_order_n app_p{ "res/assets/places.txt", 3, 4 };
		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
		std::cout << "init(places) time: " << runtime.count() << "\n";
		start = std::chrono::high_resolution_clock::now();
		PCG::Names::Markov_order_n app{ "res/assets/names.txt", 3, 4 };
		end = std::chrono::high_resolution_clock::now();
		runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
		std::cout << "init(names) time: " << runtime.count() << "\n";
		std::cout << "Generating " << no_names << " with at least 4...\n";
		start = std::chrono::high_resolution_clock::now();
		size_t i = 0;
		std::string sample;
		for (; i < 100; ++i)
			sample += app() + "\n";
		for (; i < no_names; ++i)
			std::string str = app();
		end = std::chrono::high_resolution_clock::now();
		std::cout << "First 100 as sample:\n" << sample;
		runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
		std::cout << "time: " << runtime.count() << "\n";
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	size_t no_runs_vegetation = 1000;

	void vegetation_random_sub()
	{
		std::cout << "Vegetation - random subdivision:\n";
		unsigned long long time = 0;
		for (size_t run = 0; run < no_runs_vegetation; ++run) {
			auto start = std::chrono::high_resolution_clock::now();
			PCG::Vegetation::Random_subdivision app;
			app.generate();
			auto end = std::chrono::high_resolution_clock::now();
			std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
			time += runtime.count() / no_runs_vegetation;
		}
		std::cout << "time " << time << "\n";
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void vegetation_ifs()
	{
		std::cout << "Vegetation - IFS:\n";
		unsigned long long time = 0;
		for (size_t run = 0; run < no_runs_vegetation; ++run) {
			auto start = std::chrono::high_resolution_clock::now();
			PCG::Vegetation::IFS app;
			app.generate();
			auto end = std::chrono::high_resolution_clock::now();
			std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
			time += runtime.count() / no_runs_vegetation;
		}
		std::cout << "time " << time << "\n";
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void vegetation_l_system()
	{
		std::cout << "Vegetation - L-systems:\n";
		unsigned long long time = 0;
		for (size_t run = 0; run < no_runs_vegetation; ++run) {
			auto start = std::chrono::high_resolution_clock::now();
			PCG::Vegetation::L_system app{ "res/assets/tree.l++", 0 };
			auto end = std::chrono::high_resolution_clock::now();
			std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
			time += runtime.count() / no_runs_vegetation;
		}
		std::cout << "time " << time << "\n";
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void indoor_ca_single()
	{
		std::cout << "Indoor - CA cave (single threaded):\n";
		std::vector<PCG::Indoor_maps::Cellular_automata_cave::Generation_params> vec;
		vec.resize(1);
		vec[0].r1_cutoff = 5;
		vec[0].r2_cutoff = 2;
		vec[0].reps = 5;
		bool snapshot = false;
		for (size_t size = min_size; size <= max_size; size *= 2) {
			unsigned long long time = 0;
			for (size_t run = 0; run < no_runs; ++run) {
				auto start = std::chrono::high_resolution_clock::now();
				PCG::Indoor_maps::Cellular_automata_cave app{ long(size), long(size), 0.40, vec };
				auto end = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
				time += runtime.count() / no_runs;
				if (!snapshot) {
					snapshot = true;
					std::cout << app << "\n";
				}
			}
			std::cout << "time for " << size << " sides: " << time << "\n";
		}
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void indoor_ca_multi()
	{
		std::cout << "Indoor - CA cave (multi threaded):\n";
		std::vector<PCG::Indoor_maps::Cellular_automata_cave::Generation_params> vec;
		vec.resize(1);
		vec[0].r1_cutoff = 5;
		vec[0].r2_cutoff = 2;
		vec[0].reps = 5;
		for (size_t size = min_size; size <= max_size; size *= 2) {
			unsigned long long time = 0;
			for (size_t run = 0; run < no_runs; ++run) {
				auto start = std::chrono::high_resolution_clock::now();
				PCG::Indoor_maps::Cellular_automata_cave app{ long(size), long(size), 0.40, vec,
															  PCG::Indoor_maps::Cellular_automata_cave::Generation_type::multi_threaded };
				auto end = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
				time += runtime.count() / no_runs;
			}
			std::cout << "time for " << size << " sides: " << time << "\n";
		}
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void indoor_dla()
	{
		std::cout << "Indoor - DLA cave:\n";
		bool snapshot = false;
		for (size_t size = min_size; size <= max_size; size *= 2) {
			unsigned long long time = 0;
			for (size_t run = 0; run < no_runs; ++run) {
				auto start = std::chrono::high_resolution_clock::now();
				PCG::Indoor_maps::DLA_cave app{ long(size), long(size), 0.5 };
				auto end = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
				time += runtime.count() / no_runs;
				if (!snapshot) {
					snapshot = true;
					std::cout << app << "\n";
				}
			}
			std::cout << "time for " << size << " sides: " << time << "\n";
		}
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void indoor_maze()
	{
		std::cout << "Indoor - maze:\n";
		bool snapshot = false;
		for (size_t size = min_size; size <= max_size; size *= 2) {
			unsigned long long time = 0;
			for (size_t run = 0; run < no_runs; ++run) {
				auto start = std::chrono::high_resolution_clock::now();
				PCG::Indoor_maps::Simple_maze app{ int(size), int(size) };
				auto end = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
				time += runtime.count() / no_runs;
				if (!snapshot) {
					snapshot = true;
					std::cout << app << "\n";
				}
			}
			std::cout << "time for " << size << " sides: " << time << "\n";
		}
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void indoor_dungeon1()
	{
		std::cout << "Indoor - Dungeon #1:\n";
		bool snapshot = false;
		for (size_t size = min_size; size <= max_size; size *= 2) {
			unsigned long long time = 0;
			for (size_t run = 0; run < no_runs; ++run) {
				auto start = std::chrono::high_resolution_clock::now();
				PCG::Indoor_maps::Dungeon_first app{ long(size), long(size), 110, 50, 128 };
				auto end = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
				time += runtime.count() / no_runs;
				if (!snapshot) {
					snapshot = true;
					std::cout << app << "\n";
				}
			}
			std::cout << "time for " << size << " sides: " << time << "\n";
		}
		std::cout << "--------------------------------------------------------------------------------\n";
	}

	void indoor_dungeon2()
	{
		std::cout << "Indoor - Dungeon #2:\n";
		bool snapshot = false;
		for (size_t size = min_size; size <= max_size; size *= 2) {
			unsigned long long time = 0;
			for (size_t run = 0; run < no_runs; ++run) {
				auto start = std::chrono::high_resolution_clock::now();
				PCG::Indoor_maps::Dungeon_second app{ int(size), int(size), 2000 };
				auto end = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds runtime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
				time += runtime.count() / no_runs;
				if (!snapshot) {
					snapshot = true;
					std::cout << app << "\n";
				}
			}
			std::cout << "time for " << size << " sides: " << time << "\n";
		}
		std::cout << "--------------------------------------------------------------------------------\n";
	}

}

/// Based on http://stackoverflow.com/a/478960
std::string Utils::execute_command(const std::string& command)
{
	char buffer[128];
	std::string result = "";
	std::shared_ptr<FILE> pipe{ popen(command.c_str(), "r"), pclose };
	if (pipe)
		while (!feof(pipe.get()))
			if (fgets(buffer, 128, pipe.get()) != nullptr)
				result += buffer;
	return result;
}

float Utils::get_cpu_temperature()
{
	std::string response = execute_command("sensors coretemp-isa-0000 | grep Phy");
	response = response.substr(17, 4);
	float value = atof(response.c_str());
	if (value < 20)
		value += 100;
	return value;
}

float Utils::get_gpu_temperature()
{
	std::string response = execute_command("nvidia-smi | grep '/ 250W'");
	response = response.substr(8, 2);
	float value = atof(response.c_str());
	if (value < 20)
		value += 100;
	return 0;
}

namespace {

	void spindown_cpu()
	{
		while (Utils::get_cpu_temperature() > 70)
			std::this_thread::sleep_for(std::chrono::seconds{ 1 });
	}

	void spindown_gpu()
	{
		while (Utils::get_gpu_temperature() > 70)
			std::this_thread::sleep_for(std::chrono::seconds{ 1 });
		spindown_cpu();///Have to do this too, just in case...
	}

	std::chrono::minutes min_runtime{ 10 };

}

void run_all_methods()
{
	{
		PCG::Noise::Value value;
		measure_noise("Value", value);
		spindown_cpu();
		PCG::Noise::Gradient gradient;
		measure_noise("Gradient", gradient);
		spindown_cpu();
		PCG::Noise::Lattice_convolution lattice_convolution;
		measure_noise("Lattice convolution", lattice_convolution);
		spindown_cpu();
		PCG::Noise::Sparse_convolution sparse_convolution;
		measure_noise("Sparse convolution", sparse_convolution);
		spindown_cpu();
		PCG::Noise::Perlin perlin;
		measure_noise("Perlin", perlin);
		spindown_cpu();
		PCG::Noise::Opensimplex opensimplex;
		measure_noise("OpenSimplex", opensimplex);
		spindown_cpu();
		PCG::Noise::Gabor gabor;
		measure_noise("Gabor", gabor);
		spindown_cpu();
	}
	names_fsm();
	spindown_cpu();
	names_markov_sep();
	spindown_cpu();
	names_markov_n();
	spindown_cpu();
	vegetation_random_sub();
	spindown_cpu();
	vegetation_ifs();
	spindown_cpu();
	vegetation_l_system();
	spindown_cpu();
	heightmap_ds();
	spindown_cpu();
	heightmap_bissection();
	spindown_cpu();
	indoor_ca_single();
	spindown_cpu();
	indoor_ca_multi();
	spindown_cpu();
	indoor_dla();
	spindown_cpu();
	indoor_maze();
	spindown_cpu();
	indoor_dungeon1();
	spindown_cpu();
	indoor_dungeon2();
	spindown_cpu();
	town_agents();
	spindown_cpu();
	town_l_systems();
	spindown_cpu();
	{
		OpenGL::Application app{ std::make_shared<PCG::Towns::City_gen>() };
		app.measure("Town - with buildings", min_runtime);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Plain>() };
		app.measure("Plain gray - GLSL", noise_min_size, noise_max_size);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Value>() };
		app.measure("Value - GLSL", noise_min_size, noise_max_size);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Gradient>() };
		app.measure("Gradient - GLSL", noise_min_size, noise_max_size);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Lattice_convolution>() };
		app.measure("Lattice convolution - GLSL",
					noise_min_size, noise_max_size);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Sparse_convolution>() };
		app.measure("Sparse convolution - GLSL", noise_min_size, noise_max_size);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Perlin>() };
		app.measure("Perlin - GLSL", noise_min_size, noise_max_size);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::OpenSimplex>() };
		app.measure("OpenSimplex - GLSL", noise_min_size, noise_max_size);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Gabor>() };
		app.measure("Gabor - GLSL", noise_min_size, noise_max_size);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Plain_obj>() };
		app.measure("Plain gray - Object", 100000);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Value_obj>() };
		app.measure("Value - Object", 100000);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Gradient_obj>() };
		app.measure("Gradient - Object", 100000);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Lattice_convolution_obj>() };
		app.measure("Lattice convolution - Object", 100000);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Sparse_convolution_obj>() };
		app.measure("Sparse convolution - Object", 100000);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Perlin_obj>() };
		app.measure("Perlin - Object", 100000);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::OpenSimplex_obj>() };
		app.measure("OpenSimplex - Object", 100000);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Noise::GLSL::Gabor_obj>() };
		app.measure("Gabor - Object", 100000);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Elements::Fire>() };
		app.measure("Fire", min_runtime);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Elements::Particles>() };
		app.measure("Particles", min_runtime);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Elements::Ocean>() };
		app.measure("Oceans", min_runtime);
		spindown_gpu();
	}
	{
		OpenGL::Application app{ std::make_shared<PCG::Elements::Clouds>() };
		app.measure("Clouds", min_runtime);
	}
}
