/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This file was based on the C++ port of proctree.js, and is subject to the following license:
 * proctree.js Copyright (c) 2012, Paul Brunt
 * c++ port Copyright (c) 2015, Jari Komppa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *  * Neither the name of proctree.js nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL PAUL BRUNT BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "proctree.h"

#include <cmath>
#include <glm/glm.hpp>

PCG::Vegetation::Random_subdivision::Properties::Properties(
		float a_clump_max, float a_clump_min, float a_length_falloff_factor,
		float a_length_falloff_power, float a_branch_factor,
		float a_radius_falloff_rate, float a_climb_rate,
		float a_trunk_kink, float a_max_radius, int a_tree_steps,
		float a_taper_rate, float a_twist_rate,
		int a_segments, int a_levels, float a_sweep_amount,
		float a_initial_branch_length, float a_trunk_length,
		float a_drop_amount, float a_grow_amount, float a_v_multiplier,
		float a_twig_scale, unsigned a_seed)
		: m_clump_max{ a_clump_max }, m_clump_min{ a_clump_min },
		  m_length_falloff_factor{ a_length_falloff_factor },
		  m_length_falloff_power{ a_length_falloff_power }, m_branch_factor{ a_branch_factor },
		  m_radius_falloff_rate{ a_radius_falloff_rate }, m_climb_rate{ a_climb_rate },
		  m_trunk_kink{ a_trunk_kink }, m_max_radius{ a_max_radius }, m_tree_steps{ a_tree_steps },
		  m_taper_rate{ a_taper_rate }, m_twist_rate{ a_twist_rate }, m_segments{ a_segments },
		  m_levels{ a_levels }, m_sweep_amount{ a_sweep_amount },
		  m_initial_branch_length{ a_initial_branch_length }, m_trunk_length{ a_trunk_length },
		  m_drop_amount{ a_drop_amount }, m_grow_amount{ a_grow_amount },
		  m_v_multiplier{ a_v_multiplier }, m_twig_scale{ a_twig_scale }, m_seed{ a_seed }
{
}

glm::vec3
PCG::Vegetation::Random_subdivision::scale_in_direction(glm::vec3 a_vector,
														glm::vec3 a_direction, float a_scale)
{
	float current_mag = glm::dot(a_vector, a_direction);
	glm::vec3 change = a_direction * (current_mag * a_scale - current_mag);
	return a_vector + change;
}

glm::vec3
PCG::Vegetation::Random_subdivision::axis_angle(glm::vec3 a_vec, glm::vec3 a_axis, float a_angle)
{
	float cosr = std::cos(a_angle);
	float sinr = std::sin(a_angle);
	return (((a_vec * cosr) + (glm::cross(a_axis, a_vec) * sinr))
			+ (a_axis * (glm::dot(a_axis, a_vec) * (1 - cosr))));
}

glm::vec3
PCG::Vegetation::Random_subdivision::mirror_branch(glm::vec3 a_vec, glm::vec3 a_norm,
												   Properties& a_properties)
{
	glm::vec3 v = glm::cross(a_norm, glm::cross(a_vec, a_norm));
	float s = a_properties.m_branch_factor * glm::dot(v, a_vec);
	glm::vec3 res = {
			a_vec.x - v.x * s,
			a_vec.y - v.y * s,
			a_vec.z - v.z * s
	};
	return res;
}

float PCG::Vegetation::Random_subdivision::Properties::random(float a_fixed)
{
	if (!a_fixed) {
		a_fixed = (float) m_rseed++;
	}
	return std::abs(std::cos(a_fixed + a_fixed * a_fixed));
}

PCG::Vegetation::Random_subdivision::Branch::Branch(glm::vec3 a_head, Branch* a_parent)
		: m_parent{ a_parent }, m_head{ a_head }, m_length{ 1.f }
{
}

PCG::Vegetation::Random_subdivision::Branch::~Branch()
{
	if (m_child_0)
		delete m_child_0;
	if (m_child_1)
		delete m_child_1;
}

void PCG::Vegetation::Random_subdivision::Branch::split(int a_level, int a_steps,
														Properties& a_properties, int a_l1,
														int a_l2)
{
	int r_level = a_properties.m_levels - a_level;
	glm::vec3 po;
	if (this->m_parent) {
		po = m_parent->m_head;
	} else {
		po = { 0, 0, 0 };
		m_trunktype = true;
	}
	glm::vec3 so = m_head;
	glm::vec3 dir = glm::normalize(so - po);
	glm::vec3 a = { dir.z, dir.x, dir.y };
	glm::vec3 normal = glm::cross(dir, a);
	glm::vec3 tangent = glm::cross(dir, normal);
	float r = a_properties.random(r_level * 10 + a_l1 * 5.0f + a_l2 + a_properties.m_seed);
	glm::vec3 adj = (normal * r) + (tangent * (1 - r));
	if (r > 0.5)
		adj *= -1;
	float clump = (a_properties.m_clump_max - a_properties.m_clump_min) * r
				  + a_properties.m_clump_min;
	glm::vec3 newdir = glm::normalize((adj * (1 - clump)) + (dir * clump));
	glm::vec3 newdir2 = mirror_branch(newdir, dir, a_properties);
	if (r > 0.5) {
		glm::vec3 tmp = newdir;
		newdir = newdir2;
		newdir2 = tmp;
	}
	if (a_steps > 0) {
		float angle = float(a_steps / (float) a_properties.m_tree_steps
							* 2 * M_PI * a_properties.m_twist_rate);
		a = { std::sin(angle), r, std::cos(angle) };
		newdir2 = glm::normalize(a);
	}
	float grow_amount = a_level * a_level
						/ (float) (a_properties.m_levels * a_properties.m_levels)
						* a_properties.m_grow_amount;
	float drop_amount = r_level * a_properties.m_drop_amount;
	float sweep_amount = r_level * a_properties.m_sweep_amount;
	a = { sweep_amount, drop_amount + grow_amount, 0 };
	newdir = glm::normalize(newdir + a);
	newdir2 = glm::normalize(newdir2 + a);
	glm::vec3 head0 = so + newdir * m_length;
	glm::vec3 head1 = so + newdir2 * m_length;
	m_child_0 = new Branch(head0, this);
	m_child_1 = new Branch(head1, this);
	m_child_0->m_length = std::pow(m_length, a_properties.m_length_falloff_power)
						  * a_properties.m_length_falloff_factor;
	m_child_1->m_length = std::pow(m_length, a_properties.m_length_falloff_power)
						  * a_properties.m_length_falloff_factor;
	if (a_level > 0) {
		if (a_steps > 0) {
			a = {
					(r - 0.5f) * 2 * a_properties.m_trunk_kink,
					a_properties.m_climb_rate,
					(r - 0.5f) * 2 * a_properties.m_trunk_kink
			};
			m_child_0->m_head = m_head + a;
			m_child_0->m_trunktype = true;
			m_child_0->m_length = m_length * a_properties.m_taper_rate;
			m_child_0->split(a_level, a_steps - 1, a_properties, a_l1 + 1, a_l2);
		} else {
			m_child_0->split(a_level - 1, 0, a_properties, a_l1 + 1, a_l2);
		}
		m_child_1->split(a_level - 1, 0, a_properties, a_l1, a_l2 + 1);
	}
}

void PCG::Vegetation::Random_subdivision::init()
{
	m_vert_count = 0;
	m_twig_vert_count = 0;
	m_face_count = 0;
	m_twig_face_count = 0;
	if (m_root)
		delete m_root;
	m_root = nullptr;
	m_vert.clear();
	m_normal.clear();
	m_uv.clear();
	m_twig_vert.clear();
	m_twig_normal.clear();
	m_twig_uv.clear();
	m_face.clear();
	m_twig_face.clear();
}

void PCG::Vegetation::Random_subdivision::alloc_vert_buffers()
{
	m_vert.resize(m_vert_count);
	m_normal.resize(m_vert_count);
	m_uv.resize(m_vert_count);
	m_twig_vert.resize(m_twig_vert_count);
	m_twig_normal.resize(m_twig_vert_count);
	m_twig_uv.resize(m_twig_vert_count);
	m_twig_face.resize(m_twig_face_count);
	m_vert_count = 0;
	m_twig_vert_count = 0;
	m_twig_face_count = 0;
}

void PCG::Vegetation::Random_subdivision::alloc_face_buffers()
{
	m_face.resize(m_face_count);
	m_face_count = 0;
}

void PCG::Vegetation::Random_subdivision::generate()
{
	init();
	m_properties.m_rseed = m_properties.m_seed;
	glm::vec3 starthead = { 0, m_properties.m_trunk_length, 0 };
	m_root = new Branch(starthead, nullptr);
	m_root->m_length = m_properties.m_initial_branch_length;
	m_root->split(m_properties.m_levels, m_properties.m_tree_steps, m_properties);
	calc_vert_sizes(nullptr);
	alloc_vert_buffers();
	create_forks(nullptr, 0);
	create_twigs(nullptr);
	calc_face_sizes(nullptr);
	alloc_face_buffers();
	do_faces(nullptr);
	calc_normals();
	fix_uvs();
	delete m_root;
	m_root = nullptr;
}

void PCG::Vegetation::Random_subdivision::fix_uvs()
{
	std::vector<int> badverttable;
	badverttable.resize(m_vert_count / 2);
	unsigned badverts = 0;
	for (unsigned i = 0; i < m_face_count; i++) {
		if ((std::fabs(m_uv[m_face[i].x].s - m_uv[m_face[i].y].s) > 0.5f) &&
			(m_uv[m_face[i].x].s == 0 || m_uv[m_face[i].y].s == 0)) {
			bool found = false;
			for (unsigned j = 0; j < badverts; j++) {
				if (badverttable[j] == m_face[i].y && m_uv[m_face[i].y].s == 0)
					found = true;
				if (badverttable[j] == m_face[i].x && m_uv[m_face[i].x].s == 0)
					found = true;
			}
			if (!found) {
				if (m_uv[m_face[i].x].s == 0)
					badverttable[badverts] = m_face[i].x;
				if (m_uv[m_face[i].y].s == 0)
					badverttable[badverts] = m_face[i].y;
				badverts++;
			}
		}
		if ((std::fabs(m_uv[m_face[i].x].s - m_uv[m_face[i].z].s) > 0.5f) &&
			(m_uv[m_face[i].x].s == 0 || m_uv[m_face[i].z].s == 0)) {
			bool found = false;
			for (unsigned j = 0; j < badverts; j++) {
				if (badverttable[j] == m_face[i].z && m_uv[m_face[i].z].s == 0)
					found = true;
				if (badverttable[j] == m_face[i].x && m_uv[m_face[i].x].s == 0)
					found = true;
			}
			if (!found) {
				if (m_uv[m_face[i].x].s == 0)
					badverttable[badverts] = m_face[i].x;
				if (m_uv[m_face[i].z].s == 0)
					badverttable[badverts] = m_face[i].z;
				badverts++;
			}
		}
		if ((std::fabs(m_uv[m_face[i].y].s - m_uv[m_face[i].z].s) > 0.5f) &&
			(m_uv[m_face[i].y].s == 0 || m_uv[m_face[i].z].s == 0)) {
			bool found = false;
			for (unsigned j = 0; j < badverts; j++) {
				if (badverttable[j] == m_face[i].z && m_uv[m_face[i].z].s == 0)
					found = true;
				if (badverttable[j] == m_face[i].y && m_uv[m_face[i].y].s == 0)
					found = true;
			}
			if (!found) {
				if (m_uv[m_face[i].y].s == 0)
					badverttable[badverts] = m_face[i].y;
				if (m_uv[m_face[i].z].s == 0)
					badverttable[badverts] = m_face[i].z;
				badverts++;
			}
		}
	}
	m_vert.resize(m_vert_count + badverts);
	m_normal.resize(m_vert_count + badverts);
	m_uv.resize(m_vert_count + badverts);
	for (unsigned i = 0; i < badverts; i++) {
		m_vert[m_vert_count + i] = m_vert[badverttable[i]];
		m_normal[m_vert_count + i] = m_normal[badverttable[i]];
		m_uv[m_vert_count + i] = m_uv[badverttable[i]];
		m_uv[m_vert_count + i].s = 1.0f;
	}
	for (unsigned i = 0; i < m_face_count; i++) {
		if ((std::fabs(m_uv[m_face[i].x].s - m_uv[m_face[i].y].s) > 0.5f) &&
			(m_uv[m_face[i].x].s == 0 || m_uv[m_face[i].y].s == 0)) {
			unsigned found = 0;
			for (unsigned j = 0; j < badverts; j++) {
				if (badverttable[j] == m_face[i].y && m_uv[m_face[i].y].s == 0)
					found = j;
				if (badverttable[j] == m_face[i].x && m_uv[m_face[i].x].s == 0)
					found = j;
			}
			if (m_uv[m_face[i].y].s == 0)
				m_face[i].y = m_vert_count + found;
			if (m_uv[m_face[i].x].s == 0)
				m_face[i].x = m_vert_count + found;
		}
		if ((std::fabs(m_uv[m_face[i].x].s - m_uv[m_face[i].z].s) > 0.5f) &&
			(m_uv[m_face[i].x].s == 0 || m_uv[m_face[i].z].s == 0)) {
			unsigned found = 0;
			for (unsigned j = 0; j < badverts; j++) {
				if (badverttable[j] == m_face[i].z && m_uv[m_face[i].z].s == 0)
					found = j;
				if (badverttable[j] == m_face[i].x && m_uv[m_face[i].x].s == 0)
					found = j;
			}
			if (m_uv[m_face[i].x].s == 0)
				m_face[i].x = m_vert_count + found;
			if (m_uv[m_face[i].z].s == 0)
				m_face[i].z = m_vert_count + found;
		}
		if ((std::fabs(m_uv[m_face[i].y].s - m_uv[m_face[i].z].s) > 0.5f) &&
			(m_uv[m_face[i].y].s == 0 || m_uv[m_face[i].z].s == 0)) {
			unsigned found = 0;
			for (unsigned j = 0; j < badverts; j++) {
				if (badverttable[j] == m_face[i].z && m_uv[m_face[i].z].s == 0)
					found = j;
				if (badverttable[j] == m_face[i].y && m_uv[m_face[i].y].s == 0)
					found = j;
			}
			if (m_uv[m_face[i].y].s == 0)
				m_face[i].y = m_vert_count + found;
			if (m_uv[m_face[i].z].s == 0)
				m_face[i].z = m_vert_count + found;
		}
	}
	m_vert_count += badverts;
}

void PCG::Vegetation::Random_subdivision::calc_vert_sizes(Branch* a_branch)
{
	int segments = m_properties.m_segments;
	if (!a_branch)
		a_branch = m_root;
	if (!a_branch->m_parent) {
		m_vert_count += segments;
	}
	if (a_branch->m_child_0) {
		m_vert_count +=
				1 +
				(segments / 2) - 1 +
				1 +
				(segments / 2) - 1 +
				(segments / 2) - 1;
		calc_vert_sizes(a_branch->m_child_0);
		calc_vert_sizes(a_branch->m_child_1);
	} else {
		m_vert_count++;
		m_twig_vert_count += 8;
		m_twig_face_count += 4;
	}
}

void PCG::Vegetation::Random_subdivision::calc_face_sizes(Branch* a_branch)
{
	int segments = m_properties.m_segments;
	if (!a_branch)
		a_branch = m_root;
	if (!a_branch->m_parent) {
		m_face_count += segments * 2;
	}
	if (!a_branch->m_child_0->m_ring_0.empty()) {
		m_face_count += segments * 4;
		calc_face_sizes(a_branch->m_child_0);
		calc_face_sizes(a_branch->m_child_1);
	} else {
		m_face_count += segments * 2;
	}
}

void PCG::Vegetation::Random_subdivision::calc_normals()
{
	std::vector<int> normal_count;
	normal_count.assign(m_vert_count, 0);
	m_normal.assign(m_vert_count, { 0, 0, 0 });
	for (unsigned i = 0; i < m_face_count; i++) {
		normal_count[m_face[i].x]++;
		normal_count[m_face[i].y]++;
		normal_count[m_face[i].z]++;
		glm::vec3 norm = glm::normalize(glm::cross(m_vert[m_face[i].y] - m_vert[m_face[i].z],
												   m_vert[m_face[i].y] - m_vert[m_face[i].x]));
		m_normal[m_face[i].x].x += norm.x;
		m_normal[m_face[i].x].y += norm.y;
		m_normal[m_face[i].x].z += norm.z;
		m_normal[m_face[i].y].x += norm.x;
		m_normal[m_face[i].y].y += norm.y;
		m_normal[m_face[i].y].z += norm.z;
		m_normal[m_face[i].z].x += norm.x;
		m_normal[m_face[i].z].y += norm.y;
		m_normal[m_face[i].z].z += norm.z;
	}
	for (unsigned i = 0; i < m_vert_count; i++) {
		float d = 1.0f / normal_count[i];
		m_normal[i].x *= d;
		m_normal[i].y *= d;
		m_normal[i].z *= d;
	}
}

void PCG::Vegetation::Random_subdivision::do_faces(Branch* a_branch)
{
	if (!a_branch) {
		a_branch = m_root;
	}
	int segments = m_properties.m_segments;
	if (!a_branch->m_parent) {
		glm::vec3 tangent = glm::normalize(glm::cross(a_branch->m_child_0->m_head - a_branch->m_head,
													  a_branch->m_child_1->m_head - a_branch->m_head));
		glm::vec3 normal = normalize(a_branch->m_head);
		glm::vec3 left = { -1, 0, 0 };
		float angle = std::acos(glm::dot(tangent, left));
		if (glm::dot(glm::cross(left, tangent), normal) > 0) {
			angle = float(2 * M_PI - angle);
		}
		int seg_offset = (int) std::floor(0.5f + (angle / M_PI / 2 * segments));
		for (int i = 0; i < segments; i++) {
			int v1 = a_branch->m_ring_0[i];
			int v2 = a_branch->m_root_ring[(i + seg_offset + 1) % segments];
			int v3 = a_branch->m_root_ring[(i + seg_offset) % segments];
			int v4 = a_branch->m_ring_0[(i + 1) % segments];
			glm::ivec3 a;
			a = { v1, v4, v3 };
			m_face[m_face_count++] = (a);
			a = { v4, v2, v3 };
			m_face[m_face_count++] = (a);
			m_uv[(i + seg_offset) % segments] = { i / (float) segments, 0 };
			float len = glm::length(m_vert[a_branch->m_ring_0[i]] -
									m_vert[a_branch->m_root_ring[(i + seg_offset) % segments]])
						* m_properties.m_v_multiplier;
			m_uv[a_branch->m_ring_0[i]] = { i / (float) segments, len };
			m_uv[a_branch->m_ring_2[i]] = { i / (float) segments, len };
		}
	}

	if (!a_branch->m_child_0->m_ring_0.empty()) {
		int seg_offset_0 = -1, seg_offset_1 = -1;
		float match0, match1;
		glm::vec3 v1 = glm::normalize(m_vert[a_branch->m_ring_1[0]] - a_branch->m_head);
		glm::vec3 v2 = glm::normalize(m_vert[a_branch->m_ring_2[0]] - a_branch->m_head);
		v1 = scale_in_direction(v1, glm::normalize(a_branch->m_child_0->m_head - a_branch->m_head), 0);
		v2 = scale_in_direction(v2, glm::normalize(a_branch->m_child_1->m_head - a_branch->m_head), 0);
		for (int i = 0; i < segments; i++) {
			glm::vec3 d = glm::normalize(m_vert[a_branch->m_child_0->m_ring_0[i]]
										 - a_branch->m_child_0->m_head);
			float l = glm::dot(d, v1);
			if (seg_offset_0 == -1 || l > match0) {
				match0 = l;
				seg_offset_0 = segments - i;
			}
			d = glm::normalize(m_vert[a_branch->m_child_1->m_ring_0[i]] - a_branch->m_child_1->m_head);
			l = glm::dot(d, v2);
			if (seg_offset_1 == -1 || l > match1) {
				match1 = l;
				seg_offset_1 = segments - i;
			}
		}
		float UVScale = m_properties.m_max_radius / a_branch->m_radius;
		for (int i = 0; i < segments; i++) {
			int v1 = a_branch->m_child_0->m_ring_0[i];
			int v2 = a_branch->m_ring_1[(i + seg_offset_0 + 1) % segments];
			int v3 = a_branch->m_ring_1[(i + seg_offset_0) % segments];
			int v4 = a_branch->m_child_0->m_ring_0[(i + 1) % segments];
			m_face[m_face_count++] = { v1, v4, v3 };
			m_face[m_face_count++] = { v4, v2, v3 };
			v1 = a_branch->m_child_1->m_ring_0[i];
			v2 = a_branch->m_ring_2[(i + seg_offset_1 + 1) % segments];
			v3 = a_branch->m_ring_2[(i + seg_offset_1) % segments];
			v4 = a_branch->m_child_1->m_ring_0[(i + 1) % segments];
			m_face[m_face_count++] = { v1, v2, v3 };
			m_face[m_face_count++] = { v1, v4, v2 };
			float len1 = glm::length(m_vert[a_branch->m_child_0->m_ring_0[i]]
									 - m_vert[a_branch->m_ring_1[(i + seg_offset_0) % segments]])
						 * UVScale;
			glm::vec2 uv1 = m_uv[a_branch->m_ring_1[(i + seg_offset_0 - 1) % segments]];
			m_uv[a_branch->m_child_0->m_ring_0[i]]
					= { uv1.s, uv1.t + len1 * m_properties.m_v_multiplier };
			m_uv[a_branch->m_child_0->m_ring_2[i]]
					= { uv1.s, uv1.t + len1 * m_properties.m_v_multiplier };
			float len2 = glm::length(m_vert[a_branch->m_child_1->m_ring_0[i]]
									 - m_vert[a_branch->m_ring_2[(i + seg_offset_1) % segments]])
						 * UVScale;
			glm::vec2 uv2 = m_uv[a_branch->m_ring_2[(i + seg_offset_1 - 1) % segments]];
			m_uv[a_branch->m_child_1->m_ring_0[i]]
					= { uv2.s, uv2.t + len2 * m_properties.m_v_multiplier };
			m_uv[a_branch->m_child_1->m_ring_2[i]]
					= { uv2.s, uv2.t + len2 * m_properties.m_v_multiplier };
		}
		do_faces(a_branch->m_child_0);
		do_faces(a_branch->m_child_1);
	} else {
		for (int i = 0; i < segments; i++) {
			m_face[m_face_count++] = {
					a_branch->m_child_0->m_end,
					a_branch->m_ring_1[(i + 1) % segments],
					a_branch->m_ring_1[i]
			};
			m_face[m_face_count++] = {
					a_branch->m_child_1->m_end,
					a_branch->m_ring_2[(i + 1) % segments],
					a_branch->m_ring_2[i]
			};
			float len = glm::length(m_vert[a_branch->m_child_0->m_end]
									- m_vert[a_branch->m_ring_1[i]]);
			m_uv[a_branch->m_child_0->m_end] = { i / (float) segments - 1,
												 len * m_properties.m_v_multiplier };
			len = glm::length(m_vert[a_branch->m_child_1->m_end]
							  - m_vert[a_branch->m_ring_2[i]]);
			m_uv[a_branch->m_child_1->m_end] = { i / (float) segments,
												 len * m_properties.m_v_multiplier };
		}
	}
}

void PCG::Vegetation::Random_subdivision::create_twigs(Branch* a_branch)
{
	if (!a_branch) {
		a_branch = m_root;
	}
	if (!a_branch->m_child_0) {
		glm::vec3 tangent = glm::normalize(glm::cross(a_branch->m_parent->m_child_0->m_head
													  - a_branch->m_parent->m_head,
													  a_branch->m_parent->m_child_1->m_head
													  - a_branch->m_parent->m_head));
		glm::vec3 binormal = glm::normalize(a_branch->m_head - a_branch->m_parent->m_head);
		int vert1 = m_twig_vert_count;
		m_twig_vert[m_twig_vert_count++] = a_branch->m_head
										   + tangent * m_properties.m_twig_scale
										   + binormal * (m_properties.m_twig_scale * 2
														 - a_branch->m_length);
		int vert2 = m_twig_vert_count;
		m_twig_vert[m_twig_vert_count++] = a_branch->m_head
										   + tangent * -m_properties.m_twig_scale
										   + binormal * (m_properties.m_twig_scale * 2
														 - a_branch->m_length);
		int vert3 = m_twig_vert_count;
		m_twig_vert[m_twig_vert_count++] = a_branch->m_head
										   + tangent * -m_properties.m_twig_scale
										   + binormal * -a_branch->m_length;
		int vert4 = m_twig_vert_count;
		m_twig_vert[m_twig_vert_count++] = a_branch->m_head
										   + tangent * m_properties.m_twig_scale
										   + binormal * -a_branch->m_length;
		int vert8 = m_twig_vert_count;
		m_twig_vert[m_twig_vert_count++] = a_branch->m_head
										   + tangent * m_properties.m_twig_scale
										   + binormal * (m_properties.m_twig_scale * 2
														 - a_branch->m_length);
		int vert7 = m_twig_vert_count;
		m_twig_vert[m_twig_vert_count++] = a_branch->m_head
										   + tangent * -m_properties.m_twig_scale
										   + binormal * (m_properties.m_twig_scale * 2
														 - a_branch->m_length);
		int vert6 = m_twig_vert_count;
		m_twig_vert[m_twig_vert_count++] = a_branch->m_head
										   + tangent * -m_properties.m_twig_scale
										   + binormal * -a_branch->m_length;
		int vert5 = m_twig_vert_count;
		m_twig_vert[m_twig_vert_count++] = a_branch->m_head
										   + tangent * m_properties.m_twig_scale
										   + binormal * -a_branch->m_length;
		m_twig_face[m_twig_face_count++] = { vert1, vert2, vert3 };
		m_twig_face[m_twig_face_count++] = { vert4, vert1, vert3 };
		m_twig_face[m_twig_face_count++] = { vert6, vert7, vert8 };
		m_twig_face[m_twig_face_count++] = { vert6, vert8, vert5 };
		glm::vec3 normal = glm::normalize(glm::cross(m_twig_vert[vert1] - m_twig_vert[vert3],
													 m_twig_vert[vert2] - m_twig_vert[vert3]));
		glm::vec3 normal2 = glm::normalize(glm::cross(m_twig_vert[vert7] - m_twig_vert[vert6],
													  m_twig_vert[vert8] - m_twig_vert[vert6]));
		m_twig_normal[vert1] = (normal);
		m_twig_normal[vert2] = (normal);
		m_twig_normal[vert3] = (normal);
		m_twig_normal[vert4] = (normal);
		m_twig_normal[vert8] = (normal2);
		m_twig_normal[vert7] = (normal2);
		m_twig_normal[vert6] = (normal2);
		m_twig_normal[vert5] = (normal2);
		m_twig_uv[vert1] = { 0, 0 };
		m_twig_uv[vert2] = { 1, 0 };
		m_twig_uv[vert3] = { 1, 1 };
		m_twig_uv[vert4] = { 0, 1 };
		m_twig_uv[vert8] = { 0, 0 };
		m_twig_uv[vert7] = { 1, 0 };
		m_twig_uv[vert6] = { 1, 1 };
		m_twig_uv[vert5] = { 0, 1 };
	} else {
		create_twigs(a_branch->m_child_0);
		create_twigs(a_branch->m_child_1);
	}
}

void PCG::Vegetation::Random_subdivision::create_forks(Branch* a_branch, float a_radius)
{
	if (!a_branch)
		a_branch = m_root;
	if (!a_radius)
		a_radius = m_properties.m_max_radius;
	a_branch->m_radius = a_radius;
	if (a_radius > a_branch->m_length)
		a_radius = a_branch->m_length;
	int segments = m_properties.m_segments;
	float segment_angle = float(M_PI * 2 / (float) segments);
	if (!a_branch->m_parent) {
		a_branch->m_root_ring.resize(segments);
		glm::vec3 axis = { 0, 1, 0 };
		for (int i = 0; i < segments; i++) {
			glm::vec3 left = { -1, 0, 0 };
			glm::vec3 vec = axis_angle(left, axis, -segment_angle * i);
			a_branch->m_root_ring[i] = m_vert_count;
			m_vert[m_vert_count++] = vec * (a_radius / m_properties.m_radius_falloff_rate);
		}
	}
	if (a_branch->m_child_0) {
		glm::vec3 axis;
		if (a_branch->m_parent) {
			axis = glm::normalize(a_branch->m_head - a_branch->m_parent->m_head);
		} else {
			axis = normalize(a_branch->m_head);
		}
		glm::vec3 axis1 = glm::normalize(a_branch->m_head - a_branch->m_child_0->m_head);
		glm::vec3 axis2 = glm::normalize(a_branch->m_head - a_branch->m_child_1->m_head);
		glm::vec3 tangent = glm::normalize(glm::cross(axis1, axis2));
		glm::vec3 axis3 = glm::normalize(glm::cross(tangent,
													glm::normalize(axis1 * -1.f + axis2 * -1.f)));
		glm::vec3 dir = { axis2.x, 0, axis2.z };
		glm::vec3 centerloc = a_branch->m_head + dir * (-m_properties.m_max_radius / 2);
		a_branch->m_ring_0.resize(segments);
		a_branch->m_ring_1.resize(segments);
		a_branch->m_ring_2.resize(segments);
		int ring0count = 0;
		int ring1count = 0;
		int ring2count = 0;
		float scale = m_properties.m_radius_falloff_rate;
		if (a_branch->m_child_0->m_trunktype || a_branch->m_trunktype) {
			scale = 1.0f / m_properties.m_taper_rate;
		}
		int linch0 = m_vert_count;
		a_branch->m_ring_0[ring0count++] = linch0;
		a_branch->m_ring_2[ring2count++] = linch0;
		m_vert[m_vert_count++] = centerloc + tangent * (a_radius * scale);
		int start = m_vert_count - 1;
		glm::vec3 d1 = axis_angle(tangent, axis2, 1.57f);
		glm::vec3 d2 = normalize(cross(tangent, axis));
		float s = 1.f / glm::dot(d1, d2);
		for (int i = 1; i < segments / 2; ++i) {
			glm::vec3 vec = axis_angle(tangent, axis2, segment_angle * i);
			a_branch->m_ring_0[ring0count++] = start + i;
			a_branch->m_ring_2[ring2count++] = start + i;
			vec = scale_in_direction(vec, d2, s);
			m_vert[m_vert_count++] = centerloc + vec * (a_radius * scale);
		}
		int linch1 = m_vert_count;
		a_branch->m_ring_0[ring0count++] = linch1;
		a_branch->m_ring_1[ring1count++] = linch1;
		m_vert[m_vert_count++] = centerloc + tangent * (-a_radius * scale);
		for (int i = segments / 2 + 1; i < segments; ++i) {
			glm::vec3 vec = axis_angle(tangent, axis1, segment_angle * i);
			a_branch->m_ring_0[ring0count++] = m_vert_count;
			a_branch->m_ring_1[ring1count++] = m_vert_count;
			m_vert[m_vert_count++] = centerloc + vec * (a_radius * scale);
		}
		a_branch->m_ring_1[ring1count++] = linch0;
		a_branch->m_ring_2[ring2count++] = linch1;
		start = m_vert_count - 1;
		for (int i = 1; i < segments / 2; ++i) {
			glm::vec3 vec = axis_angle(tangent, axis3, segment_angle * i);
			a_branch->m_ring_1[ring1count++] = start + i;
			a_branch->m_ring_2[ring2count++] = start + (segments / 2 - i);
			glm::vec3 v = vec * (a_radius * scale);
			m_vert[m_vert_count++] = centerloc + v;
		}
		float radius0 = 1 * a_radius * m_properties.m_radius_falloff_rate;
		float radius1 = 1 * a_radius * m_properties.m_radius_falloff_rate;
		if (a_branch->m_child_0->m_trunktype) {
			radius0 = a_radius * m_properties.m_taper_rate;
		}
		create_forks(a_branch->m_child_0, radius0);
		create_forks(a_branch->m_child_1, radius1);
	} else {
		a_branch->m_end = m_vert_count;
		m_vert[m_vert_count++] = (a_branch->m_head);
	}
}
