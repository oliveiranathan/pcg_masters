/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This file was based on the C++ port of proctree.js, and is subject to the following license:
 * proctree.js Copyright (c) 2012, Paul Brunt
 * c++ port Copyright (c) 2015, Jari Komppa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *  * Neither the name of proctree.js nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL PAUL BRUNT BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <vector>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace PCG {

	namespace Vegetation {

		/// These classes were refactored to use a more C++-like approach.
		/// The code is available at https://github.com/jarikomppa/proctree

		class Random_subdivision {
		public:

			class Properties {
			public:
				Properties(
						float a_clump_max = 0.454f,
						float a_clump_min = 0.404f,
						float a_length_falloff_factor = 0.85f,
						float a_length_falloff_power = 0.99f,
						float a_branch_factor = 2.45f,
						float a_radius_falloff_rate = 0.73f,
						float a_climb_rate = 0.371f,
						float a_trunk_kink = 0.093f,
						float a_max_radius = 0.139f,
						int a_tree_steps = 5,
						float a_taper_rate = 0.947f,
						float a_twist_rate = 3.02f,
						int a_segments = 6,
						int a_levels = 5,
						float a_sweep_amount = 0.01f,
						float a_initial_branch_length = 0.49f,
						float a_trunk_length = 2.4f,
						float a_drop_amount = -0.1f,
						float a_grow_amount = 0.235f,
						float a_v_multiplier = 0.36f,
						float a_twig_scale = 0.39f,
						unsigned a_seed = 262);

				float random(float a_fixed);

				float m_clump_max;
				float m_clump_min;
				float m_length_falloff_factor;
				float m_length_falloff_power;
				float m_branch_factor;
				float m_radius_falloff_rate;
				float m_climb_rate;
				float m_trunk_kink;
				float m_max_radius;
				int m_tree_steps;
				float m_taper_rate;
				float m_twist_rate;
				int m_segments;
				int m_levels;
				float m_sweep_amount;
				float m_initial_branch_length;
				float m_trunk_length;
				float m_drop_amount;
				float m_grow_amount;
				float m_v_multiplier;
				float m_twig_scale;
				unsigned m_seed;
				int m_rseed{ 0 };
			};

			Properties m_properties;
			unsigned m_vert_count{ 0 };
			unsigned m_twig_vert_count{ 0 };
			unsigned m_face_count{ 0 };
			unsigned m_twig_face_count{ 0 };

			std::vector<glm::vec3> m_vert;
			std::vector<glm::vec3> m_normal;
			std::vector<glm::vec2> m_uv;
			std::vector<glm::vec3> m_twig_vert;
			std::vector<glm::vec3> m_twig_normal;
			std::vector<glm::vec2> m_twig_uv;
			std::vector<glm::ivec3> m_face;
			std::vector<glm::ivec3> m_twig_face;

			void generate();

		private:

			class Branch {
			public:
				Branch* m_child_0{ nullptr };
				Branch* m_child_1{ nullptr };
				Branch* m_parent{ nullptr };
				glm::vec3 m_head;
				float m_length;
				bool m_trunktype{ false };
				std::vector<int> m_ring_0, m_ring_1, m_ring_2;
				std::vector<int> m_root_ring;
				float m_radius{ 0 };
				int m_end{ 0 };

				Branch(glm::vec3 a_head = { 0, 0, 0 }, Branch* a_parent = nullptr);

				~Branch();

				void split(int a_level, int a_steps, Properties& a_properties,
						   int a_l1 = 1, int a_l2 = 1);
			};

			Branch* m_root{ nullptr };

			void init();

			void alloc_vert_buffers();

			void alloc_face_buffers();

			void calc_vert_sizes(Branch* a_branch);

			void calc_face_sizes(Branch* a_branch);

			void calc_normals();

			void do_faces(Branch* a_branch);

			void create_twigs(Branch* a_branch);

			void create_forks(Branch* a_branch, float a_radius);

			void fix_uvs();

			static glm::vec3 mirror_branch(glm::vec3 a_vec, glm::vec3 a_norm, Properties& a_properties);

			static glm::vec3 axis_angle(glm::vec3 a_vec, glm::vec3 a_axis, float a_angle);

			static glm::vec3 scale_in_direction(glm::vec3 a_vector, glm::vec3 a_direction,
												float a_scale);

		};

	}

}
