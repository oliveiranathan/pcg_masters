/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "ifs.h"

#include <limits>
#include <random>

#include <png++/png.hpp>

PCG::Vegetation::IFS::Set::Set(float probability_, float a_, float b_, float c_,
							   float d_, float e_, float f_)
		: probability{ probability_ }, a{ a_ }, b{ b_ }, c{ c_ }, d{ d_ }, e{ e_ }, f{ f_ }
{
}

PCG::Vegetation::IFS::Set::Set()
		: probability{ 0 }, a{ 0 }, b{ 0 }, c{ 0 }, d{ 0 }, e{ 0 }, f{ 0 }
{
}


PCG::Vegetation::IFS::IFS(std::vector<Set> sets, size_t no_iter, long width, long height)
		: data_sets{ sets }, no_iterations{ no_iter }, generated_data{ width, height }
{
}

PCG::Vegetation::IFS::IFS()
		: no_iterations{ 100000 }, generated_data{ 1024, 2048 }
{
	data_sets.resize(4);
	data_sets[0].probability = 0.1f;
	data_sets[0].a = 0.f;
	data_sets[0].b = 0.f;
	data_sets[0].c = 0.f;
	data_sets[0].d = 0.16f;
	data_sets[0].e = 0.f;
	data_sets[0].f = 0.f;
	data_sets[1].probability = 0.08f;
	data_sets[1].a = 0.2f;
	data_sets[1].b = -0.26f;
	data_sets[1].c = 0.23f;
	data_sets[1].d = 0.22f;
	data_sets[1].e = 0.f;
	data_sets[1].f = 1.6f;
	data_sets[2].probability = 0.08f;
	data_sets[2].a = -0.15f;
	data_sets[2].b = 0.28f;
	data_sets[2].c = 0.26f;
	data_sets[2].d = 0.24f;
	data_sets[2].e = 0.f;
	data_sets[2].f = 0.44f;
	data_sets[3].probability = 0.74f;
	data_sets[3].a = 0.75f;
	data_sets[3].b = 0.04f;
	data_sets[3].c = -0.04f;
	data_sets[3].d = 0.85f;
	data_sets[3].e = 0.0f;
	data_sets[3].f = 1.6f;
}

void PCG::Vegetation::IFS::generate()
{
	float x_min = std::numeric_limits<float>::max(), y_min = std::numeric_limits<float>::max(),
			x_max = std::numeric_limits<float>::min(), y_max = std::numeric_limits<float>::min();
	float scale = 1, x_mid = 0.5, y_mid = 0.5;
	const float half_width = generated_data.get_width() / 2.0f;
	const float half_height = generated_data.get_height() / 2.0f;
	for (int j = 0; j < 2; ++j) {
		float x_last = 0, y_last = 0;
		std::mt19937 mt{ 666 };
		std::uniform_real_distribution<float> dist{ 0.0, 1.0 };
		for (size_t iter = 0; iter < no_iterations; ++iter) {
			float r = dist(mt);
			std::vector<Set>::iterator set = data_sets.begin();
			while (set != data_sets.end()) {
				r -= set->probability;
				if (r <= 0) {
					break;
				}
				++set;
			}
			float x = set->a * x_last + set->b * y_last + set->e;
			float y = set->c * x_last + set->d * y_last + set->f;
			x_last = x;
			y_last = y;
			if (j == 0) {
				if (x < x_min)
					x_min = x;
				if (y < y_min)
					y_min = y;
				if (x > x_max)
					x_max = x;
				if (y > y_max)
					y_max = y;
			} else {
				long ix = long(half_width + (x - x_mid) * scale);
				long iy = long(half_height - (y - y_mid) * scale);
				generated_data(ix, iy) = 1;
			}
		}
		scale = std::min(generated_data.get_width() / (x_max - x_min),
						 generated_data.get_height() / (y_max - y_min));
		x_mid = (x_min + x_max) / 2.0f;
		y_mid = (y_min + y_max) / 2.0f;
	}
}

void PCG::Vegetation::IFS::export_to_png(const std::string& filename)
{
	png::image<png::rgb_pixel> image{ (unsigned) generated_data.get_width(),
									  (unsigned) generated_data.get_height() };
	for (long i = 0; i < generated_data.get_width(); ++i) {
		for (long j = 0; j < generated_data.get_height(); ++j) {
			int v = generated_data(i, j) * 255;
			image[j][i] = png::rgb_pixel((png::byte) v, (png::byte) v, (png::byte) v);
		}
	}
	image.write(filename);
}
