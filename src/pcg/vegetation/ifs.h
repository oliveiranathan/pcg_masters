/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/matrix.h"

#include <vector>

namespace PCG {

	namespace Vegetation {

		/// Based on code for a IFS fern generator by Paul Bourke
		/// The code is available at http://paulbourke.net/fractals/ifs_fern_a/

		class IFS {
		public:

			struct Set {
				float probability;
				float a, b, c, d, e, f;

				Set();

				Set(float probability, float a, float b, float c, float d, float e, float f);
			};

			IFS();

			IFS(std::vector<Set> sets, size_t no_iterations = 1000,
				long width = 1024, long height = 1024);

			void generate();

			void export_to_png(const std::string& filename);

		private:

			std::vector<Set> data_sets;
			size_t no_iterations;

			Utils::Matrix<short> generated_data;
		};

	}

}
