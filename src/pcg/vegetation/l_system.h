/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <map>
#include <stack>
#include <string>
#include <vector>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace PCG {

	namespace Vegetation {

		/// Based on code for a L-system tree generator by Abbas Naderi Afooshteh
		/// The code is available at https://github.com/abiusx/L3D

		class L_system {
		public:

			class Mesh_vertex {
			public:
				Mesh_vertex(const glm::vec3& position, const glm::vec3& normal,
							 const glm::vec2& texcoords);

				glm::vec3 position;
				glm::vec3 normal;
				glm::vec2 texcoords;
				float curvature;
				int id;
			};

			class Plane {
			public:
				Plane(float a, float b, float c, float d);

				void reset(const glm::vec3& point, const glm::vec3& normal);

			private:
				glm::vec3 v;
				float d;
			};

			class Mesh_face {
			public:
				Mesh_face(const std::vector<Mesh_vertex*>& vertices);

				void update_plane(void);

				std::vector<Mesh_vertex*> vertices;
				Plane plane;
				int id;
				bool isLeaf;
			};

			typedef std::vector<Mesh_vertex*> Shape;

			L_system(const std::string& rules_filename, size_t no_iterations);

			~L_system();

			int write(const std::string& filename);

			Shape cylinder(float top_bottom_ratio = 1.0, int slices = 100);

			Shape leaf(const glm::vec3& direction = { 0, 0, 0 });

			void rotate_shape(Shape& shape, float angle, const glm::vec3& axis);

			void scale_shape(Shape& shape, float sx, float sy, float sz);

			void translate_shape(Shape& shape, float dx, float dy, float dz);

			Mesh_vertex* create_vertex(const glm::vec3& position,
									   const glm::vec3& normal = { 0, 0, 0 },
									   const glm::vec2& texcoords = { 0, 0 });

			Mesh_vertex* create_vertex(const glm::vec3& position, const glm::vec2& texcoords);

			Mesh_face* create_face(const std::vector<Mesh_vertex*>& vertices);

			int write_off_plus(const std::string& filename);

			static void rotate(glm::vec3& to_rotate, const glm::vec3& axis, float theta);

		private:

			std::string generate_from_file(const std::string& filename, const int iterations_override);

			void draw(const std::string& data);

			void update(void);

			using Rules = std::map<std::string, std::vector<std::string>>;

			std::string reproduce(const std::string& axiom, const Rules& rules, int iterations = 1);

			std::string produce(const std::string& axiom, const Rules& rules);

			void replace_all(std::string& str, const std::string& from, const std::string& to);

			void run(const char command, const float param);

			void update_b_box();

			void update_face_planes();

			class Turtle {
			public:
				glm::vec3 position{ 0, 0, 0 };
				glm::vec3 direction{ 0, 1, 0 };
				glm::vec3 right{ 1, 0, 0 };
				float thickness = 1;
				float reduction = 0.95f;

				void turn_right(float angle);

				void turn_left(float angle);

				void pitch_down(float angle);

				void pitch_up(float angle);

				void roll_left(float angle);

				void roll_right(float angle);

				void move(float distance);

				void turn_180();

				void thicken(float param);

				void narrow(float param);

				void set_thickness(float param);

				void set_reduction(float param);
			};

			class Turtle_system : public Turtle {
			public:
				Turtle_system(L_system* m);

				void save();

				void restore();

				void draw(float param);

				void draw_leaf(float param);

			private:
				L_system* mesh;
				std::stack<Turtle> state;
			};

			class Box {
			public:
				Box(float xmin, float ymin, float zmin, float xmax, float ymax, float zmax);

				void union_with(const glm::vec3& point);

			private:
				glm::vec3 minpt;
				glm::vec3 maxpt;
			};

			std::vector<Mesh_vertex*> vertices;
			std::vector<Mesh_face*> faces;
			Box bbox;
			Turtle_system turtle;
			float default_coefficient;
		};

	}

}
