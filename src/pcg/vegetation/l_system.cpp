/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "l_system.h"

#include <fstream>
#include <limits>
#include <random>
#include <glm/glm.hpp>

PCG::Vegetation::L_system::L_system(const std::string& rules_filename, size_t no_iterations)
		: bbox{ std::numeric_limits<float>::max(), std::numeric_limits<float>::max(),
				std::numeric_limits<float>::max(),
				std::numeric_limits<float>::min(), std::numeric_limits<float>::min(),
				std::numeric_limits<float>::min() },
		  turtle{ this }
{
	const std::string lsystem = generate_from_file(rules_filename, (int) no_iterations);
	draw(lsystem);
	update();
}

PCG::Vegetation::L_system::~L_system()
{
	for (Mesh_face* f : faces) {
		delete f;
	}
	for (Mesh_vertex* v : vertices) {
		delete v;
	}
}

std::string PCG::Vegetation::L_system::generate_from_file(const std::string& filename,
														 int iterations_override)
{
	std::ifstream file(filename);
	if (!file) {
		return "";
	}
	int numbers_read = 0;
	float numbers[3];
	std::string axiom;
	std::map<std::string, std::vector<std::string>> rules;
	while (!file.eof()) {
		std::string temp;
		char c;
		file >> c;
		if (c == '#') {
			getline(file, temp);
			continue;
		}
		file.putback(c);
		if (numbers_read < 3) {
			file >> numbers[numbers_read++];
			continue;
		}
		if (c == '@')
			break;
		getline(file, temp);
		size_t equal_sign_pos = temp.find("=");
		if (equal_sign_pos == std::string::npos) {
			axiom = temp;
		} else {
			rules[temp.substr(0, equal_sign_pos)].push_back(temp.substr(equal_sign_pos + 1));
		}
	}
	int iterations = (int) numbers[0];
	if (iterations_override != 0)
		iterations = iterations_override;
	default_coefficient = numbers[1];
	float thickness = numbers[2];
	turtle.thickness = thickness / 100.f;
	return reproduce(axiom, rules, iterations);
}

std::string PCG::Vegetation::L_system::reproduce(const std::string& axiom,
												const Rules& rules,
												const int iterations)
{
	if (iterations > 0)
		return reproduce(produce(axiom, rules), rules, iterations - 1);
	return axiom;
}

std::string PCG::Vegetation::L_system::produce(const std::string& axiom,
											  const Rules& rules)
{
	std::mt19937 mt{ 666 };
	std::string t = axiom;
	std::map<std::string, std::vector<std::string>>::const_iterator iter;
	for (iter = rules.begin(); iter != rules.end(); ++iter) {
		std::string key = iter->first;
		std::vector<std::string> value = iter->second;
		std::uniform_int_distribution<size_t> dist{ 0, value.size() - 1 };
		size_t index = dist(mt);
		replace_all(t, key, value[index]);
	}
	return t;
}

void PCG::Vegetation::L_system::replace_all(std::string& str, const std::string& from,
										   const std::string& to)
{
	if (from.empty())
		return;
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length();
	}
}

void PCG::Vegetation::L_system::draw(const std::string& tree)
{
	char param_buf[1024];
	int buf_index = 0;
	std::string data = tree;
	float param = 0;
	bool get_param = false, check_param = false;
	char command = 'E';
	for (size_t i = 0; i < data.size(); ++i) {
		char c = data[i];
		if (get_param) {
			if (c == ')') {
				param_buf[buf_index] = 0;
				buf_index = 0;
				param = float(atof(param_buf));
				get_param = false;
				run(command, param);
			} else
				param_buf[buf_index++] = c;
			continue;
		}
		if (check_param) {
			check_param = false;
			if (c == '(') {
				param = 0;
				get_param = true;
				continue;
			}
			run(command, 1);
		}
		command = c;
		check_param = true;
	}
	if (check_param)
		run(command, 1);
}

void PCG::Vegetation::L_system::run(const char command, const float param)
{
	float co = default_coefficient;
	float num = param;
	if (num == 1)
		num *= co;
	switch (command) {
		case '+':
			turtle.turn_left(num);
			break;
		case '-':
			turtle.turn_right(num);
			break;
		case '&':
			turtle.pitch_down(num);
			break;
		case '^':
			turtle.pitch_up(num);
			break;
		case '<':
			turtle.thicken(num);
			break;
		case '\\':
			turtle.roll_left(num);
			break;
		case '/':
			turtle.roll_right(num);
			break;
		case '>':
			turtle.narrow(num);
			break;
		case '%':
			turtle.set_reduction(param);
			break;
		case '=':
			turtle.set_thickness(param);
			break;
		case '|':
			turtle.turn_180();
			break;
		case '*':
			turtle.draw_leaf(param);
			break;
		case 'F':
		case 'f':
			turtle.draw(param);
		case 'G':
		case 'g':
			turtle.move(param);
			break;
		case '[':
			turtle.save();
			break;
		case ']':
			turtle.restore();
			break;
		default:;
	}
}

PCG::Vegetation::L_system::Turtle_system::Turtle_system(L_system* m)
		: mesh{ m }
{
}

void PCG::Vegetation::L_system::Turtle_system::draw(float param)
{
	int slices;
	if (thickness < .2)
		slices = 20;
	else if (thickness < .4)
		slices = 40;
	else if (thickness < .6)
		slices = 60;
	else if (thickness < .8)
		slices = 80;
	else
		slices = 100;
	Shape s = mesh->cylinder(reduction, slices);
	mesh->scale_shape(s, param * thickness, param, param * thickness);
	glm::vec3 cylinder_direction{ 0, 1, 0 };
	glm::vec3 axis = glm::cross(cylinder_direction, direction);
	glm::normalize(axis);
	if (!(std::fabs(axis.x) < .001 && std::fabs(axis.y) < .001 && std::fabs(axis.z) < .001)) {
		float rotate_angle = std::acos(glm::dot(cylinder_direction, direction)
									   / (cylinder_direction.length() * direction.length()));
		if (std::fabs(rotate_angle) > .001) {
			mesh->rotate_shape(s, rotate_angle, axis);
		}
	}
	mesh->translate_shape(s, position.x, position.y, position.z);
}

void PCG::Vegetation::L_system::Turtle_system::draw_leaf(float param)
{
	Shape s = mesh->leaf(direction);
	mesh->scale_shape(s, param, param, param);
	glm::vec3 leaf_direction(0, 1, 0);
	glm::vec3 axis = glm::cross(leaf_direction, direction);
	glm::normalize(axis);
	if (!(std::fabs(axis.x) < .001 && std::fabs(axis.y) < .001 && std::fabs(axis.z) < .001)) {
		float rotate_angle = std::acos(glm::dot(leaf_direction, direction)
									   / (leaf_direction.length() * direction.length()));
		if (std::fabs(rotate_angle) > .001)
			mesh->rotate_shape(s, rotate_angle, axis);
	}
	mesh->translate_shape(s, position.x, position.y, position.z);
}

void PCG::Vegetation::L_system::Turtle_system::save()
{
	Turtle t = (Turtle) *this;
	state.push(t);
}

void PCG::Vegetation::L_system::Turtle_system::restore()
{
	Turtle t = state.top();
	state.pop();
	position = t.position;
	direction = t.direction;
	right = t.right;
	thickness = t.thickness;
	reduction = t.reduction;
}

void PCG::Vegetation::L_system::Turtle::set_reduction(float param)
{
	reduction = param / 100;
}

void PCG::Vegetation::L_system::Turtle::set_thickness(float param)
{
	thickness = param / 100;
}

void PCG::Vegetation::L_system::Turtle::thicken(float param)
{
	thickness += thickness * param / 100;
}

void PCG::Vegetation::L_system::Turtle::narrow(float param)
{
	thicken(-param);
}

void PCG::Vegetation::L_system::Turtle::turn_right(float angle)
{
	angle = float(angle * M_PI / 180);
	glm::vec3 axis = glm::cross(direction, right);
	rotate(direction, axis, angle);
	rotate(right, axis, angle);
	glm::normalize(direction);
	glm::normalize(right);
}

void PCG::Vegetation::L_system::Turtle::turn_left(float angle)
{
	turn_right(-angle);
}

void PCG::Vegetation::L_system::Turtle::pitch_up(float angle)
{
	angle = float(angle * M_PI / 180);
	rotate(direction, right, angle);
	glm::normalize(direction);
}

void PCG::Vegetation::L_system::Turtle::pitch_down(float angle)
{
	pitch_up(-angle);
}

void PCG::Vegetation::L_system::Turtle::roll_right(float angle)
{
	angle = float(angle * M_PI / 180);
	rotate(right, direction, angle);
	glm::normalize(right);
}

void PCG::Vegetation::L_system::Turtle::roll_left(float angle)
{
	roll_right(-angle);
}

void PCG::Vegetation::L_system::Turtle::move(float distance)
{
	glm::vec3 t = glm::normalize(direction);
	position += distance * t;
}

void PCG::Vegetation::L_system::Turtle::turn_180()
{
	turn_right(M_PI);
}

void PCG::Vegetation::L_system::rotate(glm::vec3& to_rotate, const glm::vec3& axis, float theta)
{
	const float cos_theta = std::cos(theta);
	const float dot = glm::dot(to_rotate, axis);
	glm::vec3 cross = glm::cross(to_rotate, axis);
	to_rotate *= cos_theta;
	to_rotate += axis * dot * (1.f - cos_theta);
	to_rotate -= cross * std::sin(theta);
}

PCG::Vegetation::L_system::Shape PCG::Vegetation::L_system::cylinder(float top_bottom_ratio, int slices)
{
	static bool cached = false;
	cached = false;
	static std::vector<glm::vec3> cache;
	int cache_index = 0;
	float length = 1, radius = 1;
	float top_radius = top_bottom_ratio;
	Shape vertices;
	Shape bottom_circle;
	Shape top_circle;
	Shape side;
	for (int i = 0; i < slices; i++) {
		Mesh_vertex* t1, * t2;
		glm::vec3 p;
		float theta = ((float) i) * (2.0 * M_PI / slices);
		if (!cached)
			cache.push_back(p = glm::vec3{ top_radius * std::cos(theta), length,
										   top_radius * std::sin(theta) });
		else
			p = cache[cache_index++];
		t1 = create_vertex(p, glm::vec2{ i * 2 / (float) slices, 1 });
		top_circle.push_back(t1);
		vertices.push_back(t1);
		if (!cached)
			cache.push_back(p = glm::vec3{ radius * std::cos(theta), 0, radius * std::sin(theta) });
		else
			p = cache[cache_index++];
		t2 = create_vertex(p, glm::vec2{ i * 2 / (float) slices, 0 });
		bottom_circle.push_back(t2);
		vertices.push_back(t2);
	}
	size_t size = vertices.size();
	for (size_t i = 0; i < size; i += 2) {
		side.push_back(vertices[i]);
		side.push_back(vertices[i + 1]);
		side.push_back(vertices[(i + 2) % size]);
		create_face(side);
		side.clear();
		side.push_back(vertices[i + 1]);
		side.push_back(vertices[(i + 3) % size]);
		side.push_back(vertices[(i + 2) % size]);
		create_face(side);
		side.clear();
	}
	create_face(top_circle);
	create_face(bottom_circle);
	if (!cached)
		cached = true;
	return vertices;
}

PCG::Vegetation::L_system::Mesh_vertex*
PCG::Vegetation::L_system::create_vertex(const glm::vec3& position, const glm::vec2& texcoords)
{
	return create_vertex(position, { 0, 0, 0 }, texcoords);
}

PCG::Vegetation::L_system::Mesh_vertex*
PCG::Vegetation::L_system::create_vertex(const glm::vec3& position,
										const glm::vec3& normal,
										const glm::vec2& texcoords)
{
	glm::vec2 tx = texcoords;
	Mesh_vertex* vertex = new Mesh_vertex(position, normal, tx);
	bbox.union_with(position);
	vertex->id = (int) vertices.size();
	vertices.push_back(vertex);
	return vertex;
}

PCG::Vegetation::L_system::Mesh_face*
PCG::Vegetation::L_system::create_face(const std::vector<Mesh_vertex*>& vertices)
{
	Mesh_face* face = new Mesh_face(vertices);
	face->id = (int) faces.size();
	faces.push_back(face);
	return face;
}

PCG::Vegetation::L_system::Box::Box(float xmin, float ymin, float zmin,
								   float xmax, float ymax, float zmax)
		: minpt{ xmin, ymin, zmin }, maxpt{ xmax, ymax, zmax }
{
}

void PCG::Vegetation::L_system::Box::union_with(const glm::vec3& point)
{
	if (minpt.x > point.x)
		minpt.x = point.x;
	if (minpt.y > point.y)
		minpt.y = point.y;
	if (minpt.z > point.z)
		minpt.z = point.z;
	if (maxpt.x < point.x)
		maxpt.x = point.x;
	if (maxpt.y < point.y)
		maxpt.y = point.y;
	if (maxpt.z < point.z)
		maxpt.z = point.z;
}

PCG::Vegetation::L_system::Mesh_vertex::Mesh_vertex(const glm::vec3& position_,
												   const glm::vec3& normal_,
												   const glm::vec2& texcoords_)
		: position{ position_ }, normal{ normal_ },
		  texcoords{ texcoords_ }, curvature{ 0 }, id{ 0 }
{
}

PCG::Vegetation::L_system::Mesh_face::Mesh_face(const std::vector<Mesh_vertex*>& vertices_)
		: vertices{ vertices_ }, plane{ 0, 0, 0, 0 }, id{ 0 }, isLeaf{ false }
{
	update_plane();
}

PCG::Vegetation::L_system::Plane::Plane(float a, float b, float c, float d_)
		: v{ a, b, c }, d{ d_ }
{
	glm::normalize(v);
}

void PCG::Vegetation::L_system::Mesh_face::update_plane()
{
	int nvertices = (int) vertices.size();
	if (nvertices < 3) {
		plane = { 0, 0, 0, 0 };
		return;
	}
	glm::vec3 centroid{ 0, 0, 0 };
	for (int i = 0; i < nvertices; i++)
		centroid += vertices[i]->position;
	centroid /= nvertices;
	glm::vec3 normal{ 0, 0, 0 };
	const glm::vec3* p1 = &(vertices[nvertices - 1]->position);
	for (int i = 0; i < nvertices; i++) {
		const glm::vec3* p2 = &(vertices[i]->position);
		normal[0] += (p1->y - p2->y) * (p1->z + p2->z);
		normal[1] += (p1->z - p2->z) * (p1->x + p2->x);
		normal[2] += (p1->x - p2->x) * (p1->y + p2->y);
		p1 = p2;
	}
	glm::normalize(normal);
	plane.reset(centroid, normal);
}

void PCG::Vegetation::L_system::Plane::reset(const glm::vec3& point, const glm::vec3& normal)
{
	v = normal;
	glm::normalize(v);
	d = -(v[0] * point[0] + v[1] * point[1] + v[2] * point[2]);
}

PCG::Vegetation::L_system::Shape PCG::Vegetation::L_system::leaf(const glm::vec3& direction)
{
	float z = glm::dot(direction, glm::vec3{ 0.f, 1.0, 0.f }) / 4.f;
	if (z == 0) {
		std::mt19937 mt{ 666 };
		std::uniform_real_distribution<float> dist{ -0.1f, 0.1f };
		z = dist(mt);
	}
	std::vector<Mesh_vertex*> face;
	face.push_back(create_vertex(glm::vec3{ 0, .01f, 0 }, glm::vec2{ .5f, .01f }));
	face.push_back(create_vertex(glm::vec3{ .2f, .1f, 0 }, glm::vec2{ .7f, .1f }));
	face.push_back(create_vertex(glm::vec3{ .25f, .3f, 0 }, glm::vec2{ .75f, .3f }));
	face.push_back(create_vertex(glm::vec3{ .2f, .6f, z / 2 }, glm::vec2{ .7f, .6f }));
	face.push_back(create_vertex(glm::vec3{ 0, 1 - z, z }, glm::vec2{ .5f, 1 }));
	face.push_back(create_vertex(glm::vec3{ -.2f, .6f, z / 2 }, glm::vec2{ .3f, .6f }));
	face.push_back(create_vertex(glm::vec3{ -.25f, .3f, 0 }, glm::vec2{ .25f, .3f }));
	face.push_back(create_vertex(glm::vec3{ -.2f, .1f, 0 }, glm::vec2{ .3f, .1f }));
	create_face(face)->isLeaf = true;
	return face;
}

void PCG::Vegetation::L_system::scale_shape(Shape& shape, float sx, float sy, float sz)
{
	for (unsigned int i = 0; i < shape.size(); i++) {
		Mesh_vertex* vertex = shape[i];
		vertex->position[0] *= sx;
		vertex->position[1] *= sy;
		vertex->position[2] *= sz;
	}
}

void PCG::Vegetation::L_system::rotate_shape(Shape& shape, float angle, const glm::vec3& axis)
{
	for (unsigned int i = 0; i < shape.size(); i++) {
		Mesh_vertex* vertex = shape[i];
		rotate(vertex->position, axis, angle);
	}
}

void PCG::Vegetation::L_system::translate_shape(Shape& shape, float dx, float dy, float dz)
{
	glm::vec3 translation(dx, dy, dz);
	for (unsigned int i = 0; i < shape.size(); i++) {
		Mesh_vertex* vertex = shape[i];
		vertex->position += translation;
	}
}

void PCG::Vegetation::L_system::update()
{
	update_b_box();
	update_face_planes();
}

void PCG::Vegetation::L_system::update_b_box()
{
	bbox = { std::numeric_limits<float>::max(), std::numeric_limits<float>::max(),
			 std::numeric_limits<float>::max(), std::numeric_limits<float>::min(),
			 std::numeric_limits<float>::min(), std::numeric_limits<float>::min() };
	for (unsigned int i = 0; i < vertices.size(); i++) {
		Mesh_vertex* vertex = vertices[i];
		bbox.union_with(vertex->position);
	}
}

void PCG::Vegetation::L_system::update_face_planes()
{
	for (unsigned int i = 0; i < faces.size(); i++) {
		faces[i]->update_plane();
	}
}

int PCG::Vegetation::L_system::write(const std::string& filename)
{
	return write_off_plus(filename);
}

int PCG::Vegetation::L_system::write_off_plus(const std::string& filename)
{
	std::ofstream out{ filename };
	if (!out) {
		return 0;
	}
	out << "OFF\n";
	out << vertices.size() << " " << faces.size() << " " << 0 << "\n";
	out << std::showpoint;
	for (size_t i = 0; i < vertices.size(); i++) {
		Mesh_vertex* vertex = vertices[i];
		const glm::vec3& p = vertex->position;
		out << p.x << " " << p.y << " " << p.z << " "
			<< vertex->texcoords.x << " " << vertex->texcoords.y << "\n";
		vertex->id = int(i);
	}
	out << std::noshowpoint;
	for (size_t i = 0; i < faces.size(); i++) {
		Mesh_face* face = faces[i];
		out << (int) face->vertices.size();
		for (unsigned int j = 0; j < face->vertices.size(); j++) {
			out << " " << face->vertices[j]->id;
		}
		out << " " << face->isLeaf << "\n";
	}
	return int(faces.size());
}
