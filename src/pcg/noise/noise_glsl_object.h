/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/object_renderer.h"

namespace PCG {

	namespace Noise {

		namespace GLSL {

			class Plain_obj : public OpenGL::Object_renderer {
			public:
				Plain_obj();
			};

			class Value_obj : public OpenGL::Object_renderer {
			public:
				Value_obj(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, value_tab;
			};

			class Gradient_obj : public OpenGL::Object_renderer {
			public:
				Gradient_obj(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, gradient_tab;
			};

			class Lattice_convolution_obj : public OpenGL::Object_renderer {
			public:
				Lattice_convolution_obj(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, value_tab, table;
			};

			class Sparse_convolution_obj : public OpenGL::Object_renderer {
			public:
				Sparse_convolution_obj(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, impulse_tab, table;
			};

			class Perlin_obj : public OpenGL::Object_renderer {
			public:
				Perlin_obj(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, g;
			};

			class OpenSimplex_obj : public OpenGL::Object_renderer {
			public:
				OpenSimplex_obj(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint perm, perm_grad_index_3d, gradients_3d;
			};

			class Gabor_obj : public OpenGL::Object_renderer {
			public:
				Gabor_obj(unsigned seed = 666);

			protected:
				void startup() override;

			private:
				unsigned seed_;
			};

		}

	}

}
