/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/application.h"


namespace PCG {

	namespace Noise {

		namespace GLSL {

			class Plain : public OpenGL::Noise_renderer {
			public:
				Plain();
			};

			class Value : public OpenGL::Noise_renderer {
			public:
				Value(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, value_tab;
			};

			class Gradient : public OpenGL::Noise_renderer {
			public:
				Gradient(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, gradient_tab;
			};

			class Lattice_convolution : public OpenGL::Noise_renderer {
			public:
				Lattice_convolution(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, value_tab, table;
			};

			class Sparse_convolution : public OpenGL::Noise_renderer {
			public:
				Sparse_convolution(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, impulse_tab, table;
			};

			class Perlin : public OpenGL::Noise_renderer {
			public:
				Perlin(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint p, g;
			};

			class OpenSimplex : public OpenGL::Noise_renderer {
			public:
				OpenSimplex(unsigned seed = 666);

			protected:
				void startup() override;

				void shutdown() override;

			private:
				unsigned seed_;
				GLuint perm, perm_grad_index_3d, gradients_3d;
			};

			class Gabor : public OpenGL::Noise_renderer {
			public:
				Gabor(unsigned seed = 666);

			protected:
				void startup() override;

			private:
				unsigned seed_;
			};

		}

	}

}
