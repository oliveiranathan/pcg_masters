/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This file is based on the book Texturing and Modeling, Third Edition: A Procedural Approach,
 * and is subject to its license
 */

#include "lattice_convolution.h"

#include <algorithm>
#include <random>

PCG::Noise::Lattice_convolution::Lattice_convolution(unsigned seed)
{
	init(seed);
}

void PCG::Noise::Lattice_convolution::init(unsigned seed)
{
	std::mt19937 mt = std::mt19937{ seed };
	std::uniform_real_distribution<float> dist{ 0.0, 1.0 };
	for (short i = 0; i < 256; ++i) {
		p[i] = static_cast<unsigned char> (i);
	}
	std::shuffle(std::begin(p), std::end(p), mt);
	for (int i = 0; i < tab_size; i++)
		value_tab[i] = 1.f - 2.f * dist(mt);
	for (int i = 0; i < n_entries; i++) {
		float x = i / (float) samp_rate;
		x = std::sqrt(x);
		if (x < 1)
			table[i] = 0.5f * (2 + x * x * (-5 + x * 3));
		else
			table[i] = 0.5f * (4 + x * (-8 + x * (5 - x)));
	}
}

float PCG::Noise::Lattice_convolution::catrom2(float d)
{
	if (d >= 4)
		return 0;
	d = d * samp_rate + 0.5f;
	int i = static_cast<int> (std::floor(d));
	if (i >= n_entries)
		return 0;
	return table[i];
}

int PCG::Noise::Lattice_convolution::perm(int i)
{
	return p[i & tab_mask];
}

int PCG::Noise::Lattice_convolution::index(int x, int y, int z)
{
	return perm(x + perm(y + perm(z)));
}

float PCG::Noise::Lattice_convolution::vlattice(int ix, int iy, int iz)
{
	return value_tab[index(ix, iy, iz)];
}

float PCG::Noise::Lattice_convolution::noise(float x, float y, float z)
{
	float sum = 0;
	int ix = static_cast<int> (std::floor(x));
	float fx = x - ix;
	int iy = static_cast<int> (std::floor(y));
	float fy = y - iy;
	int iz = static_cast<int> (std::floor(z));
	float fz = z - iz;
	for (int k = -1; k <= 2; k++) {
		float dz = k - fz;
		dz = dz * dz;
		for (int j = -1; j <= 2; j++) {
			float dy = j - fy;
			dy = dy * dy;
			for (int i = -1; i <= 2; i++) {
				float dx = i - fx;
				dx = dx * dx;
				sum += vlattice(ix + i, iy + j, iz + k) * catrom2(dx + dy + dz);
			}
		}
	}
	return sum;
}

OpenGL::uint8_texture PCG::Noise::Lattice_convolution::create_texture(long w, long h)
{
	float max = -1000.0f;
	float min = 1000.0f;
	OpenGL::uint8_texture ret;
	std::vector<std::vector<float>> vals;
	ret.resize(h);
	vals.resize(h);
	for (long j = 0; j < h; ++j) {
		ret[j].resize(w);
		vals[j].resize(w);
		for (long i = 0; i < w; ++i) {
			float d = noise((float) i * 0.0625f,
							 (float) j * 0.0625f, 0.0);
			if (d > max) {
				max = d;
			}
			if (d < min) {
				min = d;
			}
			vals[j][i] = d;
		}
	}
	for (long j = 0; j < h; ++j) { /// values passes the [0;1] range, so normalise it...
		for (long i = 0; i < w; ++i) {
			float n = (vals[j][i] - min) / (max - min);
			unsigned char s = (unsigned char) (n * 255);
			ret[j][i] = s;
		}
	}
	return ret;
}
