/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This file is based on the book Texturing and Modeling, Third Edition: A Procedural Approach,
 * and is subject to its license
 */

#include "gradient_noise.h"

#include <algorithm>
#include <random>

PCG::Noise::Gradient::Gradient(unsigned seed)
{
	init(seed);
}

void PCG::Noise::Gradient::init(unsigned seed)
{
	std::mt19937 mt = std::mt19937{ seed };
	std::uniform_real_distribution<float> dist{ 0.0, 1.0 };
	for (short i = 0; i < 256; ++i) {
		p[i] = static_cast<unsigned char> (i);
	}
	std::shuffle(std::begin(p), std::end(p), mt);
	for (int i = 0; i < tab_size; i++) {
		float z = 1.f - 2.f * dist(mt);
		float r = std::sqrt(1 - z * z);
		float theta = 2 * M_PI * dist(mt);
		gradient_tab[i * 3] = r * std::cos(theta);
		gradient_tab[i * 3 + 1] = r * std::sin(theta);
		gradient_tab[i * 3 + 2] = z;
	}
}

float PCG::Noise::Gradient::lerp(float t, float x0, float x1)
{
	return x0 + t * (x1 - x0);
}

float PCG::Noise::Gradient::smooth_step(float x)
{
	return x * x * (3 - 2 * x);
}

int PCG::Noise::Gradient::perm(int i)
{
	return p[i & tab_mask];
}

int PCG::Noise::Gradient::index(int x, int y, int z)
{
	return perm(x + perm(y + perm(z)));
}

float PCG::Noise::Gradient::glattice(int ix, int iy, int iz, float fx, float fy, float fz)
{
	float* g = &gradient_tab[index(ix, iy, iz) * 3];
	return g[0] * fx + g[1] * fy + g[2] * fz;
}

float PCG::Noise::Gradient::noise(float x, float y, float z)
{
	int ix = static_cast<int> (std::floor(x));
	float fx0 = x - ix;
	float fx1 = fx0 - 1;
	float wx = smooth_step(fx0);
	int iy = static_cast<int> (std::floor(y));
	float fy0 = y - iy;
	float fy1 = fy0 - 1;
	float wy = smooth_step(fy0);
	int iz = static_cast<int> (std::floor(z));
	float fz0 = z - iz;
	float fz1 = fz0 - 1;
	float wz = smooth_step(fz0);
	float vx0 = glattice(ix, iy, iz, fx0, fy0, fz0);
	float vx1 = glattice(ix + 1, iy, iz, fx1, fy0, fz0);
	float vy0 = lerp(wx, vx0, vx1);
	vx0 = glattice(ix, iy + 1, iz, fx0, fy1, fz0);
	vx1 = glattice(ix + 1, iy + 1, iz, fx1, fy1, fz0);
	float vy1 = lerp(wx, vx0, vx1);
	float vz0 = lerp(wy, vy0, vy1);
	vx0 = glattice(ix, iy, iz + 1, fx0, fy0, fz1);
	vx1 = glattice(ix + 1, iy, iz + 1, fx1, fy0, fz1);
	vy0 = lerp(wx, vx0, vx1);
	vx0 = glattice(ix, iy + 1, iz + 1, fx0, fy1, fz1);
	vx1 = glattice(ix + 1, iy + 1, iz + 1, fx1, fy1, fz1);
	vy1 = lerp(wx, vx0, vx1);
	float vz1 = lerp(wy, vy0, vy1);
	return lerp(wz, vz0, vz1);
}

OpenGL::uint8_texture PCG::Noise::Gradient::create_texture(long w, long h)
{
	float max = -1000.0f;
	float min = 1000.0f;
	OpenGL::uint8_texture ret;
	std::vector<std::vector<float>> vals;
	ret.resize(h);
	vals.resize(h);
	for (long j = 0; j < h; ++j) {
		ret[j].resize(w);
		vals[j].resize(w);
		for (long i = 0; i < w; ++i) {
			float d = noise((float) i * 0.0625f,
							 (float) j * 0.0625f, 0.0);
			if (d > max) {
				max = d;
			}
			if (d < min) {
				min = d;
			}
			vals[j][i] = d;
		}
	}
	for (long j = 0; j < h; ++j) {
		for (long i = 0; i < w; ++i) {
			float n = (vals[j][i] - min) / (max - min);
			unsigned char s = (unsigned char) (n * 255);
			ret[j][i] = s;
		}
	}
	return ret;
}
