/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "opensimplex.h"

#include <algorithm>
#include <random>

PCG::Noise::Opensimplex::Opensimplex(unsigned seed)
{
	init(seed);
}

void PCG::Noise::Opensimplex::init(unsigned seed)
{
	for (short i = 0; i < 256; i++)
		perm[i] = i;
	std::minstd_rand prng{ seed };
	std::shuffle(std::begin(perm), std::end(perm), prng);
	for (short i = 0; i < 256; i++)
		perm_grad_index_3d[i] = (short) ((perm[i] % 24) * 3);
}

int PCG::Noise::Opensimplex::fast_floor(float x)
{
	int xi = (int) x;
	return x < xi ? xi - 1 : xi;
}

float PCG::Noise::Opensimplex::extrapolate(int xsb, int ysb, float dx, float dy)
{
	int index = perm[(perm[xsb & 0xFF] + ysb) & 0xFF] & 0x0E;
	return gradients_2d[index] * dx + gradients_2d[index + 1] * dy;
}

float PCG::Noise::Opensimplex::extrapolate(int xsb, int ysb, int zsb, float dx, float dy, float dz)
{
	int index = perm_grad_index_3d[(perm[(perm[xsb & 0xFF] + ysb) & 0xFF] + zsb) & 0xFF];
	return gradients_3d[index] * dx + gradients_3d[index + 1] * dy + gradients_3d[index + 2] * dz;
}

float PCG::Noise::Opensimplex::noise(float x, float y)
{
	float stretch_offset = (x + y) * stretch_constant_2d;
	float xs = x + stretch_offset;
	float ys = y + stretch_offset;
	int xsb = fast_floor(xs);
	int ysb = fast_floor(ys);
	float squish_offset = (xsb + ysb) * squish_constant_2d;
	float xb = xsb + squish_offset;
	float yb = ysb + squish_offset;
	float xins = xs - xsb;
	float yins = ys - ysb;
	float in_sum = xins + yins;
	float dx0 = x - xb;
	float dy0 = y - yb;
	float dx_ext, dy_ext;
	int xsv_ext, ysv_ext;
	float value = 0;
	float dx1 = dx0 - 1 - squish_constant_2d;
	float dy1 = dy0 - 0 - squish_constant_2d;
	float attn1 = 2 - dx1 * dx1 - dy1 * dy1;
	if (attn1 > 0) {
		attn1 *= attn1;
		value += attn1 * attn1 * extrapolate(xsb + 1, ysb + 0, dx1, dy1);
	}
	float dx2 = dx0 - 0 - squish_constant_2d;
	float dy2 = dy0 - 1 - squish_constant_2d;
	float attn2 = 2 - dx2 * dx2 - dy2 * dy2;
	if (attn2 > 0) {
		attn2 *= attn2;
		value += attn2 * attn2 * extrapolate(xsb + 0, ysb + 1, dx2, dy2);
	}
	if (in_sum <= 1) {
		float zins = 1 - in_sum;
		if (zins > xins || zins > yins) {
			if (xins > yins) {
				xsv_ext = xsb + 1;
				ysv_ext = ysb - 1;
				dx_ext = dx0 - 1;
				dy_ext = dy0 + 1;
			} else {
				xsv_ext = xsb - 1;
				ysv_ext = ysb + 1;
				dx_ext = dx0 + 1;
				dy_ext = dy0 - 1;
			}
		} else {
			xsv_ext = xsb + 1;
			ysv_ext = ysb + 1;
			dx_ext = dx0 - 1 - 2 * squish_constant_2d;
			dy_ext = dy0 - 1 - 2 * squish_constant_2d;
		}
	} else {
		float zins = 2 - in_sum;
		if (zins < xins || zins < yins) {
			if (xins > yins) {
				xsv_ext = xsb + 2;
				ysv_ext = ysb + 0;
				dx_ext = dx0 - 2 - 2 * squish_constant_2d;
				dy_ext = dy0 + 0 - 2 * squish_constant_2d;
			} else {
				xsv_ext = xsb + 0;
				ysv_ext = ysb + 2;
				dx_ext = dx0 + 0 - 2 * squish_constant_2d;
				dy_ext = dy0 - 2 - 2 * squish_constant_2d;
			}
		} else {
			dx_ext = dx0;
			dy_ext = dy0;
			xsv_ext = xsb;
			ysv_ext = ysb;
		}
		xsb += 1;
		ysb += 1;
		dx0 = dx0 - 1 - 2 * squish_constant_2d;
		dy0 = dy0 - 1 - 2 * squish_constant_2d;
	}
	float attn0 = 2 - dx0 * dx0 - dy0 * dy0;
	if (attn0 > 0) {
		attn0 *= attn0;
		value += attn0 * attn0 * extrapolate(xsb, ysb, dx0, dy0);
	}
	float attn_ext = 2 - dx_ext * dx_ext - dy_ext * dy_ext;
	if (attn_ext > 0) {
		attn_ext *= attn_ext;
		value += attn_ext * attn_ext * extrapolate(xsv_ext, ysv_ext, dx_ext, dy_ext);
	}
	return value / norm_constant_2d;
}

float PCG::Noise::Opensimplex::noise(float x, float y, float z)
{
	float stretch_offset = (x + y + z) * stretch_constant_3d;
	float xs = x + stretch_offset;
	float ys = y + stretch_offset;
	float zs = z + stretch_offset;
	int xsb = fast_floor(xs);
	int ysb = fast_floor(ys);
	int zsb = fast_floor(zs);
	float squish_offset = (xsb + ysb + zsb) * squish_constant_3d;
	float xb = xsb + squish_offset;
	float yb = ysb + squish_offset;
	float zb = zsb + squish_offset;
	float xins = xs - xsb;
	float yins = ys - ysb;
	float zins = zs - zsb;
	float in_sum = xins + yins + zins;
	float dx0 = x - xb;
	float dy0 = y - yb;
	float dz0 = z - zb;
	float dx_ext0, dy_ext0, dz_ext0;
	float dx_ext1, dy_ext1, dz_ext1;
	int xsv_ext0, ysv_ext0, zsv_ext0;
	int xsv_ext1, ysv_ext1, zsv_ext1;
	float value = 0;
	if (in_sum <= 1) {
		char a_point = 0x01;
		float a_score = xins;
		char b_point = 0x02;
		float b_score = yins;
		if (a_score >= b_score && zins > b_score) {
			b_score = zins;
			b_point = 0x04;
		} else if (a_score < b_score && zins > a_score) {
			a_score = zins;
			a_point = 0x04;
		}
		float wins = 1 - in_sum;
		if (wins > a_score || wins > b_score) {
			char c = (b_score > a_score ? b_point : a_point);
			if ((c & 0x01) == 0) {
				xsv_ext0 = xsb - 1;
				xsv_ext1 = xsb;
				dx_ext0 = dx0 + 1;
				dx_ext1 = dx0;
			} else {
				xsv_ext0 = xsv_ext1 = xsb + 1;
				dx_ext0 = dx_ext1 = dx0 - 1;
			}
			if ((c & 0x02) == 0) {
				ysv_ext0 = ysv_ext1 = ysb;
				dy_ext0 = dy_ext1 = dy0;
				if ((c & 0x01) == 0) {
					ysv_ext1 -= 1;
					dy_ext1 += 1;
				} else {
					ysv_ext0 -= 1;
					dy_ext0 += 1;
				}
			} else {
				ysv_ext0 = ysv_ext1 = ysb + 1;
				dy_ext0 = dy_ext1 = dy0 - 1;
			}
			if ((c & 0x04) == 0) {
				zsv_ext0 = zsb;
				zsv_ext1 = zsb - 1;
				dz_ext0 = dz0;
				dz_ext1 = dz0 + 1;
			} else {
				zsv_ext0 = zsv_ext1 = zsb + 1;
				dz_ext0 = dz_ext1 = dz0 - 1;
			}
		} else {
			char c = a_point | b_point;
			if ((c & 0x01) == 0) {
				xsv_ext0 = xsb;
				xsv_ext1 = xsb - 1;
				dx_ext0 = dx0 - 2 * squish_constant_3d;
				dx_ext1 = dx0 + 1 - squish_constant_3d;
			} else {
				xsv_ext0 = xsv_ext1 = xsb + 1;
				dx_ext0 = dx0 - 1 - 2 * squish_constant_3d;
				dx_ext1 = dx0 - 1 - squish_constant_3d;
			}
			if ((c & 0x02) == 0) {
				ysv_ext0 = ysb;
				ysv_ext1 = ysb - 1;
				dy_ext0 = dy0 - 2 * squish_constant_3d;
				dy_ext1 = dy0 + 1 - squish_constant_3d;
			} else {
				ysv_ext0 = ysv_ext1 = ysb + 1;
				dy_ext0 = dy0 - 1 - 2 * squish_constant_3d;
				dy_ext1 = dy0 - 1 - squish_constant_3d;
			}
			if ((c & 0x04) == 0) {
				zsv_ext0 = zsb;
				zsv_ext1 = zsb - 1;
				dz_ext0 = dz0 - 2 * squish_constant_3d;
				dz_ext1 = dz0 + 1 - squish_constant_3d;
			} else {
				zsv_ext0 = zsv_ext1 = zsb + 1;
				dz_ext0 = dz0 - 1 - 2 * squish_constant_3d;
				dz_ext1 = dz0 - 1 - squish_constant_3d;
			}
		}
		float attn0 = 2 - dx0 * dx0 - dy0 * dy0 - dz0 * dz0;
		if (attn0 > 0) {
			attn0 *= attn0;
			value += attn0 * attn0 * extrapolate(xsb + 0, ysb + 0, zsb + 0, dx0, dy0, dz0);
		}
		float dx1 = dx0 - 1 - squish_constant_3d;
		float dy1 = dy0 - 0 - squish_constant_3d;
		float dz1 = dz0 - 0 - squish_constant_3d;
		float attn1 = 2 - dx1 * dx1 - dy1 * dy1 - dz1 * dz1;
		if (attn1 > 0) {
			attn1 *= attn1;
			value += attn1 * attn1 * extrapolate(xsb + 1, ysb + 0, zsb + 0, dx1, dy1, dz1);
		}
		float dx2 = dx0 - 0 - squish_constant_3d;
		float dy2 = dy0 - 1 - squish_constant_3d;
		float dz2 = dz1;
		float attn2 = 2 - dx2 * dx2 - dy2 * dy2 - dz2 * dz2;
		if (attn2 > 0) {
			attn2 *= attn2;
			value += attn2 * attn2 * extrapolate(xsb + 0, ysb + 1, zsb + 0, dx2, dy2, dz2);
		}
		float dx3 = dx2;
		float dy3 = dy1;
		float dz3 = dz0 - 1 - squish_constant_3d;
		float attn3 = 2 - dx3 * dx3 - dy3 * dy3 - dz3 * dz3;
		if (attn3 > 0) {
			attn3 *= attn3;
			value += attn3 * attn3 * extrapolate(xsb + 0, ysb + 0, zsb + 1, dx3, dy3, dz3);
		}
	} else if (in_sum >= 2) {
		char a_point = 0x06;
		float a_score = xins;
		char b_point = 0x05;
		float b_score = yins;
		if (a_score <= b_score && zins < b_score) {
			b_score = zins;
			b_point = 0x03;
		} else if (a_score > b_score && zins < a_score) {
			a_score = zins;
			a_point = 0x03;
		}
		float wins = 3 - in_sum;
		if (wins < a_score || wins < b_score) {
			char c = (b_score < a_score ? b_point : a_point);
			if ((c & 0x01) != 0) {
				xsv_ext0 = xsb + 2;
				xsv_ext1 = xsb + 1;
				dx_ext0 = dx0 - 2 - 3 * squish_constant_3d;
				dx_ext1 = dx0 - 1 - 3 * squish_constant_3d;
			} else {
				xsv_ext0 = xsv_ext1 = xsb;
				dx_ext0 = dx_ext1 = dx0 - 3 * squish_constant_3d;
			}
			if ((c & 0x02) != 0) {
				ysv_ext0 = ysv_ext1 = ysb + 1;
				dy_ext0 = dy_ext1 = dy0 - 1 - 3 * squish_constant_3d;
				if ((c & 0x01) != 0) {
					ysv_ext1 += 1;
					dy_ext1 -= 1;
				} else {
					ysv_ext0 += 1;
					dy_ext0 -= 1;
				}
			} else {
				ysv_ext0 = ysv_ext1 = ysb;
				dy_ext0 = dy_ext1 = dy0 - 3 * squish_constant_3d;
			}
			if ((c & 0x04) != 0) {
				zsv_ext0 = zsb + 1;
				zsv_ext1 = zsb + 2;
				dz_ext0 = dz0 - 1 - 3 * squish_constant_3d;
				dz_ext1 = dz0 - 2 - 3 * squish_constant_3d;
			} else {
				zsv_ext0 = zsv_ext1 = zsb;
				dz_ext0 = dz_ext1 = dz0 - 3 * squish_constant_3d;
			}
		} else {
			char c = a_point & b_point;
			if ((c & 0x01) != 0) {
				xsv_ext0 = xsb + 1;
				xsv_ext1 = xsb + 2;
				dx_ext0 = dx0 - 1 - squish_constant_3d;
				dx_ext1 = dx0 - 2 - 2 * squish_constant_3d;
			} else {
				xsv_ext0 = xsv_ext1 = xsb;
				dx_ext0 = dx0 - squish_constant_3d;
				dx_ext1 = dx0 - 2 * squish_constant_3d;
			}
			if ((c & 0x02) != 0) {
				ysv_ext0 = ysb + 1;
				ysv_ext1 = ysb + 2;
				dy_ext0 = dy0 - 1 - squish_constant_3d;
				dy_ext1 = dy0 - 2 - 2 * squish_constant_3d;
			} else {
				ysv_ext0 = ysv_ext1 = ysb;
				dy_ext0 = dy0 - squish_constant_3d;
				dy_ext1 = dy0 - 2 * squish_constant_3d;
			}
			if ((c & 0x04) != 0) {
				zsv_ext0 = zsb + 1;
				zsv_ext1 = zsb + 2;
				dz_ext0 = dz0 - 1 - squish_constant_3d;
				dz_ext1 = dz0 - 2 - 2 * squish_constant_3d;
			} else {
				zsv_ext0 = zsv_ext1 = zsb;
				dz_ext0 = dz0 - squish_constant_3d;
				dz_ext1 = dz0 - 2 * squish_constant_3d;
			}
		}
		float dx3 = dx0 - 1 - 2 * squish_constant_3d;
		float dy3 = dy0 - 1 - 2 * squish_constant_3d;
		float dz3 = dz0 - 0 - 2 * squish_constant_3d;
		float attn3 = 2 - dx3 * dx3 - dy3 * dy3 - dz3 * dz3;
		if (attn3 > 0) {
			attn3 *= attn3;
			value += attn3 * attn3 * extrapolate(xsb + 1, ysb + 1, zsb + 0, dx3, dy3, dz3);
		}
		float dx2 = dx3;
		float dy2 = dy0 - 0 - 2 * squish_constant_3d;
		float dz2 = dz0 - 1 - 2 * squish_constant_3d;
		float attn2 = 2 - dx2 * dx2 - dy2 * dy2 - dz2 * dz2;
		if (attn2 > 0) {
			attn2 *= attn2;
			value += attn2 * attn2 * extrapolate(xsb + 1, ysb + 0, zsb + 1, dx2, dy2, dz2);
		}
		float dx1 = dx0 - 0 - 2 * squish_constant_3d;
		float dy1 = dy3;
		float dz1 = dz2;
		float attn1 = 2 - dx1 * dx1 - dy1 * dy1 - dz1 * dz1;
		if (attn1 > 0) {
			attn1 *= attn1;
			value += attn1 * attn1 * extrapolate(xsb + 0, ysb + 1, zsb + 1, dx1, dy1, dz1);
		}
		dx0 = dx0 - 1 - 3 * squish_constant_3d;
		dy0 = dy0 - 1 - 3 * squish_constant_3d;
		dz0 = dz0 - 1 - 3 * squish_constant_3d;
		float attn0 = 2 - dx0 * dx0 - dy0 * dy0 - dz0 * dz0;
		if (attn0 > 0) {
			attn0 *= attn0;
			value += attn0 * attn0 * extrapolate(xsb + 1, ysb + 1, zsb + 1, dx0, dy0, dz0);
		}
	} else {
		float a_score;
		char a_point;
		bool a_is_further_side;
		float b_score;
		char b_point;
		bool b_is_further_side;
		float p1 = xins + yins;
		if (p1 > 1) {
			a_score = p1 - 1;
			a_point = 0x03;
			a_is_further_side = true;
		} else {
			a_score = 1 - p1;
			a_point = 0x04;
			a_is_further_side = false;
		}
		float p2 = xins + zins;
		if (p2 > 1) {
			b_score = p2 - 1;
			b_point = 0x05;
			b_is_further_side = true;
		} else {
			b_score = 1 - p2;
			b_point = 0x02;
			b_is_further_side = false;
		}
		float p3 = yins + zins;
		if (p3 > 1) {
			float score = p3 - 1;
			if (a_score <= b_score && a_score < score) {
				a_point = 0x06;
				a_is_further_side = true;
			} else if (a_score > b_score && b_score < score) {
				b_point = 0x06;
				b_is_further_side = true;
			}
		} else {
			float score = 1 - p3;
			if (a_score <= b_score && a_score < score) {
				a_point = 0x01;
				a_is_further_side = false;
			} else if (a_score > b_score && b_score < score) {
				b_point = 0x01;
				b_is_further_side = false;
			}
		}
		if (a_is_further_side == b_is_further_side) {
			if (a_is_further_side) {
				dx_ext0 = dx0 - 1 - 3 * squish_constant_3d;
				dy_ext0 = dy0 - 1 - 3 * squish_constant_3d;
				dz_ext0 = dz0 - 1 - 3 * squish_constant_3d;
				xsv_ext0 = xsb + 1;
				ysv_ext0 = ysb + 1;
				zsv_ext0 = zsb + 1;
				char c = a_point & b_point;
				if ((c & 0x01) != 0) {
					dx_ext1 = dx0 - 2 - 2 * squish_constant_3d;
					dy_ext1 = dy0 - 2 * squish_constant_3d;
					dz_ext1 = dz0 - 2 * squish_constant_3d;
					xsv_ext1 = xsb + 2;
					ysv_ext1 = ysb;
					zsv_ext1 = zsb;
				} else if ((c & 0x02) != 0) {
					dx_ext1 = dx0 - 2 * squish_constant_3d;
					dy_ext1 = dy0 - 2 - 2 * squish_constant_3d;
					dz_ext1 = dz0 - 2 * squish_constant_3d;
					xsv_ext1 = xsb;
					ysv_ext1 = ysb + 2;
					zsv_ext1 = zsb;
				} else {
					dx_ext1 = dx0 - 2 * squish_constant_3d;
					dy_ext1 = dy0 - 2 * squish_constant_3d;
					dz_ext1 = dz0 - 2 - 2 * squish_constant_3d;
					xsv_ext1 = xsb;
					ysv_ext1 = ysb;
					zsv_ext1 = zsb + 2;
				}
			} else {
				dx_ext0 = dx0;
				dy_ext0 = dy0;
				dz_ext0 = dz0;
				xsv_ext0 = xsb;
				ysv_ext0 = ysb;
				zsv_ext0 = zsb;
				char c = a_point | b_point;
				if ((c & 0x01) == 0) {
					dx_ext1 = dx0 + 1 - squish_constant_3d;
					dy_ext1 = dy0 - 1 - squish_constant_3d;
					dz_ext1 = dz0 - 1 - squish_constant_3d;
					xsv_ext1 = xsb - 1;
					ysv_ext1 = ysb + 1;
					zsv_ext1 = zsb + 1;
				} else if ((c & 0x02) == 0) {
					dx_ext1 = dx0 - 1 - squish_constant_3d;
					dy_ext1 = dy0 + 1 - squish_constant_3d;
					dz_ext1 = dz0 - 1 - squish_constant_3d;
					xsv_ext1 = xsb + 1;
					ysv_ext1 = ysb - 1;
					zsv_ext1 = zsb + 1;
				} else {
					dx_ext1 = dx0 - 1 - squish_constant_3d;
					dy_ext1 = dy0 - 1 - squish_constant_3d;
					dz_ext1 = dz0 + 1 - squish_constant_3d;
					xsv_ext1 = xsb + 1;
					ysv_ext1 = ysb + 1;
					zsv_ext1 = zsb - 1;
				}
			}
		} else {
			char c1, c2;
			if (a_is_further_side) {
				c1 = a_point;
				c2 = b_point;
			} else {
				c1 = b_point;
				c2 = a_point;
			}
			if ((c1 & 0x01) == 0) {
				dx_ext0 = dx0 + 1 - squish_constant_3d;
				dy_ext0 = dy0 - 1 - squish_constant_3d;
				dz_ext0 = dz0 - 1 - squish_constant_3d;
				xsv_ext0 = xsb - 1;
				ysv_ext0 = ysb + 1;
				zsv_ext0 = zsb + 1;
			} else if ((c1 & 0x02) == 0) {
				dx_ext0 = dx0 - 1 - squish_constant_3d;
				dy_ext0 = dy0 + 1 - squish_constant_3d;
				dz_ext0 = dz0 - 1 - squish_constant_3d;
				xsv_ext0 = xsb + 1;
				ysv_ext0 = ysb - 1;
				zsv_ext0 = zsb + 1;
			} else {
				dx_ext0 = dx0 - 1 - squish_constant_3d;
				dy_ext0 = dy0 - 1 - squish_constant_3d;
				dz_ext0 = dz0 + 1 - squish_constant_3d;
				xsv_ext0 = xsb + 1;
				ysv_ext0 = ysb + 1;
				zsv_ext0 = zsb - 1;
			}
			dx_ext1 = dx0 - 2 * squish_constant_3d;
			dy_ext1 = dy0 - 2 * squish_constant_3d;
			dz_ext1 = dz0 - 2 * squish_constant_3d;
			xsv_ext1 = xsb;
			ysv_ext1 = ysb;
			zsv_ext1 = zsb;
			if ((c2 & 0x01) != 0) {
				dx_ext1 -= 2;
				xsv_ext1 += 2;
			} else if ((c2 & 0x02) != 0) {
				dy_ext1 -= 2;
				ysv_ext1 += 2;
			} else {
				dz_ext1 -= 2;
				zsv_ext1 += 2;
			}
		}
		float dx1 = dx0 - 1 - squish_constant_3d;
		float dy1 = dy0 - 0 - squish_constant_3d;
		float dz1 = dz0 - 0 - squish_constant_3d;
		float attn1 = 2 - dx1 * dx1 - dy1 * dy1 - dz1 * dz1;
		if (attn1 > 0) {
			attn1 *= attn1;
			value += attn1 * attn1 * extrapolate(xsb + 1, ysb + 0, zsb + 0, dx1, dy1, dz1);
		}
		float dx2 = dx0 - 0 - squish_constant_3d;
		float dy2 = dy0 - 1 - squish_constant_3d;
		float dz2 = dz1;
		float attn2 = 2 - dx2 * dx2 - dy2 * dy2 - dz2 * dz2;
		if (attn2 > 0) {
			attn2 *= attn2;
			value += attn2 * attn2 * extrapolate(xsb + 0, ysb + 1, zsb + 0, dx2, dy2, dz2);
		}
		float dx3 = dx2;
		float dy3 = dy1;
		float dz3 = dz0 - 1 - squish_constant_3d;
		float attn3 = 2 - dx3 * dx3 - dy3 * dy3 - dz3 * dz3;
		if (attn3 > 0) {
			attn3 *= attn3;
			value += attn3 * attn3 * extrapolate(xsb + 0, ysb + 0, zsb + 1, dx3, dy3, dz3);
		}
		float dx4 = dx0 - 1 - 2 * squish_constant_3d;
		float dy4 = dy0 - 1 - 2 * squish_constant_3d;
		float dz4 = dz0 - 0 - 2 * squish_constant_3d;
		float attn4 = 2 - dx4 * dx4 - dy4 * dy4 - dz4 * dz4;
		if (attn4 > 0) {
			attn4 *= attn4;
			value += attn4 * attn4 * extrapolate(xsb + 1, ysb + 1, zsb + 0, dx4, dy4, dz4);
		}
		float dx5 = dx4;
		float dy5 = dy0 - 0 - 2 * squish_constant_3d;
		float dz5 = dz0 - 1 - 2 * squish_constant_3d;
		float attn5 = 2 - dx5 * dx5 - dy5 * dy5 - dz5 * dz5;
		if (attn5 > 0) {
			attn5 *= attn5;
			value += attn5 * attn5 * extrapolate(xsb + 1, ysb + 0, zsb + 1, dx5, dy5, dz5);
		}
		float dx6 = dx0 - 0 - 2 * squish_constant_3d;
		float dy6 = dy4;
		float dz6 = dz5;
		float attn6 = 2 - dx6 * dx6 - dy6 * dy6 - dz6 * dz6;
		if (attn6 > 0) {
			attn6 *= attn6;
			value += attn6 * attn6 * extrapolate(xsb + 0, ysb + 1, zsb + 1, dx6, dy6, dz6);
		}
	}
	float attn_ext0 = 2 - dx_ext0 * dx_ext0 - dy_ext0 * dy_ext0 - dz_ext0 * dz_ext0;
	if (attn_ext0 > 0) {
		attn_ext0 *= attn_ext0;
		value += attn_ext0 * attn_ext0
				 * extrapolate(xsv_ext0, ysv_ext0, zsv_ext0, dx_ext0, dy_ext0, dz_ext0);
	}
	float attn_ext1 = 2 - dx_ext1 * dx_ext1 - dy_ext1 * dy_ext1 - dz_ext1 * dz_ext1;
	if (attn_ext1 > 0) {
		attn_ext1 *= attn_ext1;
		value += attn_ext1 * attn_ext1
				 * extrapolate(xsv_ext1, ysv_ext1, zsv_ext1, dx_ext1, dy_ext1, dz_ext1);
	}
	return value / norm_constant_3d;
}

OpenGL::uint8_texture PCG::Noise::Opensimplex::create_texture(long w, long h)
{
	float max = -1000.0f;
	float min = 1000.0f;
	OpenGL::uint8_texture ret;
	std::vector<std::vector<float>> vals;
	ret.resize(h);
	vals.resize(h);
	for (long j = 0; j < h; ++j) {
		ret[j].resize(w);
		vals[j].resize(w);
		for (long i = 0; i < w; ++i) {
			float d = noise((float) i * 0.0625f,
							 (float) j * 0.0625f);
			if (d > max) {
				max = d;
			}
			if (d < min) {
				min = d;
			}
			vals[j][i] = d;
		}
	}
	for (long j = 0; j < h; ++j) { /// normalise the table to display correctly.
		for (long i = 0; i < w; ++i) {
			float n = (vals[j][i] - min) / (max - min);
			unsigned char s = (unsigned char) (n * 255);
			ret[j][i] = s;
		}
	}
	return ret;
}
