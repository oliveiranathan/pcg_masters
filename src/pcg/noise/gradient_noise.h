/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This file is based on the book Texturing and Modeling, Third Edition: A Procedural Approach,
 * and is subject to its license
 */

#pragma once

#include "../../utils/texture_renderer.h"

#include <vector>

/// Implementation based on the book "Texturing and Modeling, Third Edition: A Procedural Approach"
/// from Ebert, D. S. et al.

namespace PCG {

	namespace Noise {

		class Gradient : public OpenGL::Texture_renderer {
		public:
			Gradient(unsigned seed = 666);

			float noise(float x, float y, float z);

			OpenGL::uint8_texture create_texture(long w, long h) override;

		private:
			static const int tab_size = 256;
			static const int tab_mask = tab_size - 1;

			unsigned int p[tab_size];
			float gradient_tab[tab_size * 3];

			void init(unsigned seed);

			inline float lerp(float t, float x0, float x1);

			inline float smooth_step(float x);

			inline float glattice(int ix, int iy, int iz, float fx, float fy, float fz);

			inline int index(int x, int y, int z);

			inline int perm(int i);
		};

	}

}
