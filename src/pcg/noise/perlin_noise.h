/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/texture_renderer.h"

#include <vector>

#include <glm/mat4x4.hpp>

/// Implementation based on the original Noise implementation from Perlin, K.
/// Original source at http://mrl.nyu.edu/~perlin/doc/oscar.html#noise

namespace PCG {

	namespace Noise {

		class Perlin : public OpenGL::Texture_renderer {
		public:
			Perlin(unsigned seed = 666);

			float noise(float arg1);

			float noise(float arg1, float arg2);

			float noise(float arg1, float arg2, float arg3);

			OpenGL::uint8_texture create_texture(long w, long h) override;

		private:
			static const int b = 0x100;
			static const int bm = 0xff;
			static const int n = 0x1000;

			int p[b + b + 2];
			float g3[b + b + 2][3];
			float g2[b + b + 2][2];
			float g1[b + b + 2];

			inline float s_curve(float t);

			inline float lerp(float t, float a, float b);

			inline void setup(float arg, int& b0, int& b1, float& r0, float& r1);

			inline float at(float* q, float rx, float ry);

			inline float at(float* q, float rx, float ry, float rz);

			inline void normalize2(float v[2]);

			inline void normalize3(float v[3]);

			void init(unsigned seed);

		};

	}

}
