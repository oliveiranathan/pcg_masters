/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/texture_renderer.h"

/// Implementation based on the OpenSimplex Noise in Java by Spencer, K.
/// Original source can be found at https://gist.github.com/KdotJPG/b1270127455a94ac5d19

namespace PCG {

	namespace Noise {

		class Opensimplex : public OpenGL::Texture_renderer {
		public:
			Opensimplex(unsigned seed = 666);

			float noise(float x, float y);

			float noise(float x, float y, float z);

			OpenGL::uint8_texture create_texture(long w, long h) override;

		private:
			static constexpr float stretch_constant_2d = -0.211324865405187f;
			static constexpr float squish_constant_2d = 0.366025403784439f;
			static constexpr float stretch_constant_3d = -1.0f / 6;
			static constexpr float squish_constant_3d = 1.0f / 3;

			static constexpr float norm_constant_2d = 47;
			static constexpr float norm_constant_3d = 103;

			short perm[256];
			short perm_grad_index_3d[256];

			void init(unsigned seed);

			inline int fast_floor(float x);

			inline float extrapolate(int xsb, int ysb, float dx, float dy);

			inline float extrapolate(int xsb, int ysb, int zsb, float dx, float dy, float dz);

			const char gradients_2d[16]{
					5, 2, 2, 5,
					-5, 2, -2, 5,
					5, -2, 2, -5,
					-5, -2, -2, -5
			};
			const char gradients_3d[72]{
					-11, 4, 4, -4, 11, 4, -4, 4, 11,
					11, 4, 4, 4, 11, 4, 4, 4, 11,
					-11, -4, 4, -4, -11, 4, -4, -4, 11,
					11, -4, 4, 4, -11, 4, 4, -4, 11,
					-11, 4, -4, -4, 11, -4, -4, 4, -11,
					11, 4, -4, 4, 11, -4, 4, 4, -11,
					-11, -4, -4, -4, -11, -4, -4, -4, -11,
					11, -4, -4, 4, -11, -4, 4, -4, -11
			};
		};

	}

}
