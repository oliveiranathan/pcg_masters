/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This file is based on the book Texturing and Modeling, Third Edition: A Procedural Approach,
 * and is subject to its license
 */

#pragma once

#include "../../utils/texture_renderer.h"

#include <vector>

/// Implementation based on the book "Texturing and Modeling, Third Edition: A Procedural Approach"
/// from Ebert, D. S. et al.

namespace PCG {

	namespace Noise {

		class Value : public OpenGL::Texture_renderer {
		public:
			Value(unsigned seed = 666);

			float noise(float x, float y, float z);

			OpenGL::uint8_texture create_texture(long w, long h) override;

		private:
			static const int tab_size = 256;
			static const int tab_mask = tab_size - 1;

			unsigned int p[tab_size];
			float value_tab[tab_size];

			void init(unsigned seed);

			inline float vlattice(int ix, int iy, int iz);

			inline int index(int x, int y, int z);

			inline int perm(int i);


			class Spline {
			public:
				static float spline(float x, const std::vector<float>& knot);

			private:
				static constexpr float cr00 = -0.5f;
				static constexpr float cr01 = 1.5f;
				static constexpr float cr02 = -1.5f;
				static constexpr float cr03 = 0.5f;
				static constexpr float cr11 = -2.5f;
				static constexpr float cr12 = 2.0f;
				static constexpr float cr13 = -0.5f;
				static constexpr float cr20 = -0.5f;
				static constexpr float cr22 = 0.5f;
			};

		};

	}

}
