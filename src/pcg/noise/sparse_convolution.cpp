/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This file is based on the book Texturing and Modeling, Third Edition: A Procedural Approach,
 * and is subject to its license
 */

#include "sparse_convolution.h"

#include <algorithm>
#include <random>

PCG::Noise::Sparse_convolution::Sparse_convolution(unsigned seed)
{
	init(seed);
}

void PCG::Noise::Sparse_convolution::init(unsigned seed)
{
	std::mt19937 mt = std::mt19937{ seed };
	std::uniform_real_distribution<float> dist{ 0.0, 1.0 };
	for (short i = 0; i < 256; ++i) {
		p[i] = static_cast<unsigned char> (i);
	}
	std::shuffle(std::begin(p), std::end(p), mt);
	impulse_tab.resize(tab_size * 4);
	for (int i = 0; i < tab_size; ++i) {
		impulse_tab[i * 4] = dist(mt);
		impulse_tab[i * 4 + 1] = dist(mt);
		impulse_tab[i * 4 + 2] = dist(mt);
		impulse_tab[i * 4 + 3] = 1.f - 2.f * dist(mt);
	}
	for (int i = 0; i < n_entries; i++) {
		float x = i / (float) samp_rate;
		x = std::sqrt(x);
		if (x < 1)
			table[i] = 0.5f * (2 + x * x * (-5 + x * 3));
		else
			table[i] = 0.5f * (4 + x * (-8 + x * (5 - x)));
	}
}

float PCG::Noise::Sparse_convolution::noise(float x, float y, float z)
{
	int ix = static_cast<int> (std::floor(x));
	float fx = x - ix;
	int iy = static_cast<int> (std::floor(y));
	float fy = y - iy;
	int iz = static_cast<int> (std::floor(z));
	float fz = z - iz;
	float sum = 0.0;
	for (int i = -2; i <= 2; i++) {
		for (int j = -2; j <= 2; j++) {
			for (int k = -2; k <= 2; k++) {
				int h = index(ix + i, iy + j, iz + k);
				for (int n = n_impulses; n > 0; n--, h = next(h)) {
					float* fp = &impulse_tab[h * 4];
					float dx = fx - (i + *fp++);
					float dy = fy - (j + *fp++);
					float dz = fz - (k + *fp++);
					float distsq = dx * dx + dy * dy + dz * dz;
					sum += catrom2(distsq) * *fp;
				}
			}
		}
	}
	return sum / n_impulses;
}

int PCG::Noise::Sparse_convolution::next(int h)
{
	return (h + 1) & tab_mask;
}

float PCG::Noise::Sparse_convolution::catrom2(float d)
{
	if (d >= 4)
		return 0;
	d = d * samp_rate + 0.5f;
	int i = static_cast<int> (std::floor(d));
	if (i >= n_entries)
		return 0;
	return table[i];
}

int PCG::Noise::Sparse_convolution::index(int x, int y, int z)
{
	return perm(x + perm(y + perm(z)));
}

int PCG::Noise::Sparse_convolution::perm(int i)
{
	return p[i & tab_mask];
}

OpenGL::uint8_texture PCG::Noise::Sparse_convolution::create_texture(long w, long h)
{
	float max = -1000.0f;
	float min = 1000.0f;
	OpenGL::uint8_texture ret;
	std::vector<std::vector<float>> vals;
	ret.resize(h);
	vals.resize(h);
	for (long j = 0; j < h; ++j) {
		ret[j].resize(w);
		vals[j].resize(w);
		for (long i = 0; i < w; ++i) {
			float d = noise((float) i * 0.0625f,
							 (float) j * 0.0625f, 0.0f);
			if (d > max) {
				max = d;
			}
			if (d < min) {
				min = d;
			}
			vals[j][i] = d;
		}
	}
	for (long j = 0; j < h; ++j) { /// values passes the [0;1] range, so normalise it...
		for (long i = 0; i < w; ++i) {
			float n = (vals[j][i] - min) / (max - min);
			unsigned char s = (unsigned char) (n * 255);
			ret[j][i] = s;
		}
	}
	return ret;
}
