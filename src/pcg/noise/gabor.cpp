/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code from Lagae et al. and is subject to the following license:
 * Copyright (c) 2009 by Ares Lagae, Sylvain Lefebvre,
 * George Drettakis and Philip Dutre
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "gabor.h"

#include <climits>
#include <random>

PCG::Noise::Gabor::Gabor(float K, float a, float F_0, float omega_0, float number_of_impulses_per_kernel)
		: K_{ K }, a_{ a }, F_0_{ F_0 }, omega_0_{ omega_0 }
{
	random_offset_ = 666;
	init(number_of_impulses_per_kernel);
}

PCG::Noise::Gabor::Gabor(unsigned seed, float K, float a, float F_0, float omega_0,
						 float number_of_impulses_per_kernel)
		: K_{ K }, a_{ a }, F_0_{ F_0 }, omega_0_{ omega_0 }, random_offset_{ seed }
{
	init(number_of_impulses_per_kernel);
}

void PCG::Noise::Gabor::init(float number_of_impulses_per_kernel)
{
	kernel_radius_ = std::sqrt(-std::log(0.05) / M_PI) / a_;
	impulse_density_ = number_of_impulses_per_kernel / (M_PI * kernel_radius_ * kernel_radius_);
	number_of_impulses_per_cell = impulse_density_ * kernel_radius_ * kernel_radius_;
	cos_omega_0 = std::cos(omega_0_) * kernel_radius_;
	sin_omega_0 = std::sin(omega_0_) * kernel_radius_;
	m_m_pi_t_a_2 = -M_PI * a_ * a_ * kernel_radius_ * kernel_radius_;
	m_pi_t_2_f_0 = 2.0 * M_PI * F_0_;
}

unsigned PCG::Noise::Gabor::morton(unsigned x, unsigned y)
{
	unsigned z = 0;
	for (unsigned i = 0; i < (sizeof(unsigned) * CHAR_BIT); ++i) {
		z |= ((x & (1 << i)) << i) | ((y & (1 << i)) << (i + 1));
	}
	return z;
}

float PCG::Noise::Gabor::cell(int i, int j, float x, float y)
{
	unsigned s = morton(i, j) + random_offset_;
	if (s == 0)
		s = 1;
	std::minstd_rand prng{ s };
	std::poisson_distribution<unsigned> poisson{ number_of_impulses_per_cell };
	unsigned number_of_impulses = poisson(prng);
	float noise = 0.0;
	std::uniform_real_distribution<float> uniform_0_1(0.0f, 1.0f);
	std::uniform_real_distribution<float> uniform_1_1(-1.0f, 1.0f);
	for (unsigned i = 0; i < number_of_impulses; ++i) {
		float x_i = uniform_0_1(prng);
		float y_i = uniform_0_1(prng);
		float w_i = uniform_1_1(prng);
		float x_i_x = x - x_i;
		float y_i_y = y - y_i;
		float x_i_x_2_t_y_i_y_2 = x_i_x * x_i_x + y_i_y * y_i_y;
		if (x_i_x_2_t_y_i_y_2 < 1.0) {
			float gabor = K_ * std::exp(m_m_pi_t_a_2 * x_i_x_2_t_y_i_y_2) *
						   std::cos(m_pi_t_2_f_0 * ((x_i_x * cos_omega_0) + (y_i_y * sin_omega_0)));
			noise += w_i * gabor;
		}
	}
	return noise;
}

float PCG::Noise::Gabor::noise(float x, float y)
{
	x /= kernel_radius_, y /= kernel_radius_;
	float int_x = std::floor(x), int_y = std::floor(y);
	float frac_x = x - int_x, frac_y = y - int_y;
	int i = int(int_x), j = int(int_y);
	float noise = 0.0;
	for (int di = -1; di <= +1; ++di) {
		for (int dj = -1; dj <= +1; ++dj) {
			noise += cell(i + di, j + dj, frac_x - di, frac_y - dj);
		}
	}
	return noise;
}

OpenGL::uint8_texture PCG::Noise::Gabor::create_texture(long w, long h)
{
	float max = -1000.0f;
	float min = 1000.0f;
	OpenGL::uint8_texture ret;
	std::vector<std::vector<float>> vals;
	ret.resize(h);
	vals.resize(h);
	for (long j = 0; j < h; ++j) {
		ret[j].resize(w);
		vals[j].resize(w);
		for (long i = 0; i < w; ++i) {
			float d = noise((float) i,
							 (float) j);
			if (d > max) {
				max = d;
			}
			if (d < min) {
				min = d;
			}
			vals[j][i] = d;
		}
	}
	for (long j = 0; j < h; ++j) { /// values passes the [0;1] range, so normalise it...
		for (long i = 0; i < w; ++i) {
			float n = (vals[j][i] - min) / (max - min);
			unsigned char s = (unsigned char) (n * 255);
			ret[j][i] = s;
		}
	}
	return ret;
}
