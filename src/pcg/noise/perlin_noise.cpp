/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "perlin_noise.h"

#include <random>

PCG::Noise::Perlin::Perlin(unsigned seed)
{
	init(seed);
}

void PCG::Noise::Perlin::init(unsigned seed)
{
	std::mt19937 mt(seed);
	std::uniform_int_distribution<int> dist(0, b + b - 1);
	int i, j, k;
	for (i = 0; i < b; i++) {
		p[i] = i;
		g1[i] = (float) (dist(mt) - b) / b;
		for (j = 0; j < 2; j++)
			g2[i][j] = (float) (dist(mt) - b) / b;
		normalize2(g2[i]);
		for (j = 0; j < 3; j++)
			g3[i][j] = (float) (dist(mt) - b) / b;
		normalize3(g3[i]);
	}
	std::uniform_int_distribution<int> dist1(0, b - 1);
	while (--i) {
		k = p[i];
		p[i] = p[j = dist1(mt)];
		p[j] = k;
	}
	for (i = 0; i < b + 2; i++) {
		p[b + i] = p[i];
		g1[b + i] = g1[i];
		for (j = 0; j < 2; j++)
			g2[b + i][j] = g2[i][j];
		for (j = 0; j < 3; j++)
			g3[b + i][j] = g3[i][j];
	}
}

float PCG::Noise::Perlin::lerp(float t, float a, float b)
{
	return (1.0f - t) * a + t * b;
}

float PCG::Noise::Perlin::s_curve(float t)
{
	return t * t * (3.0f - 2.0f * t);
}

void PCG::Noise::Perlin::setup(float arg, int& b0, int& b1, float& r0, float& r1)
{
	float t = arg + n;
	b0 = ((int) t) & bm;
	b1 = (b0 + 1) & bm;
	r0 = t - (int) t;
	r1 = r0 - 1.0f;
}

float PCG::Noise::Perlin::noise(float arg1)
{
	int bx0, bx1;
	float rx0, rx1;
	setup(arg1, bx0, bx1, rx0, rx1);
	float sx = s_curve(rx0);
	float u = rx0 * g1[p[bx0]];
	float v = rx1 * g1[p[bx1]];
	return lerp(sx, u, v);
}

float PCG::Noise::Perlin::at(float* q, float rx, float ry)
{
	return rx * q[0] + ry * q[1];
}

float PCG::Noise::Perlin::noise(float arg1, float arg2)
{
	int bx0, bx1, by0, by1;
	float rx0, rx1, ry0, ry1;
	setup(arg1, bx0, bx1, rx0, rx1);
	setup(arg2, by0, by1, ry0, ry1);
	int i = p[bx0];
	int j = p[bx1];
	int b00 = p[i + by0];
	int b10 = p[j + by0];
	int b01 = p[i + by1];
	int b11 = p[j + by1];
	float sx = s_curve(rx0);
	float sy = s_curve(ry0);
	float u = at(g2[b00], rx0, ry0);
	float v = at(g2[b10], rx1, ry0);
	float a = lerp(sx, u, v);
	u = at(g2[b01], rx0, ry1);
	v = at(g2[b11], rx1, ry1);
	float b = lerp(sx, u, v);
	return lerp(sy, a, b);
}

float PCG::Noise::Perlin::at(float* q, float rx, float ry, float rz)
{
	return rx * q[0] + ry * q[1] + rz * q[2];
}

float PCG::Noise::Perlin::noise(float arg1, float arg2, float arg3)
{
	int bx0, bx1, by0, by1, bz0, bz1;
	float rx0, rx1, ry0, ry1, rz0, rz1;
	setup(arg1, bx0, bx1, rx0, rx1);
	setup(arg2, by0, by1, ry0, ry1);
	setup(arg3, bz0, bz1, rz0, rz1);
	int i = p[bx0];
	int j = p[bx1];
	int b00 = p[i + by0];
	int b10 = p[j + by0];
	int b01 = p[i + by1];
	int b11 = p[j + by1];
	float t = s_curve(rx0);
	float sy = s_curve(ry0);
	float sz = s_curve(rz0);
	float u = at(g3[b00 + bz0], rx0, ry0, rz0);
	float v = at(g3[b10 + bz0], rx1, ry0, rz0);
	float a = lerp(t, u, v);
	u = at(g3[b01 + bz0], rx0, ry1, rz0);
	v = at(g3[b11 + bz0], rx1, ry1, rz0);
	float b = lerp(t, u, v);
	float c = lerp(sy, a, b);
	u = at(g3[b00 + bz1], rx0, ry0, rz1);
	v = at(g3[b10 + bz1], rx1, ry0, rz1);
	a = lerp(t, u, v);
	u = at(g3[b01 + bz1], rx0, ry1, rz1);
	v = at(g3[b11 + bz1], rx1, ry1, rz1);
	b = lerp(t, u, v);
	float d = lerp(sy, a, b);
	return lerp(sz, c, d);
}

void PCG::Noise::Perlin::normalize2(float* v)
{
	float s = std::sqrt(v[0] * v[0] + v[1] * v[1]);
	v[0] /= s;
	v[1] /= s;
}

void PCG::Noise::Perlin::normalize3(float* v)
{
	float s = std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	v[0] /= s;
	v[1] /= s;
	v[2] /= s;
}

OpenGL::uint8_texture PCG::Noise::Perlin::create_texture(long w, long h)
{
	OpenGL::uint8_texture ret;
	ret.resize(h);
	for (long j = 0; j < h; ++j) {
		ret[j].resize(w);
		for (long i = 0; i < w; ++i) {
			float d = noise((float) i * 0.0625f,
							 (float) j * 0.0625f);
			unsigned char s = (unsigned char) (d * 127 + 127);
			ret[j][i] = s;
		}
	}
	return ret;
}
