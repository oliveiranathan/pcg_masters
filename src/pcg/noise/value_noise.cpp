/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This file is based on the book Texturing and Modeling, Third Edition: A Procedural Approach,
 * and is subject to its license
 */

#include "value_noise.h"

#include <algorithm>
#include <random>

PCG::Noise::Value::Value(unsigned seed)
{
	init(seed);
}

void PCG::Noise::Value::init(unsigned seed)
{
	std::mt19937 mt = std::mt19937{ seed };
	std::uniform_real_distribution<float> dist{ 0.0, 1.0 };
	for (short i = 0; i < tab_size; ++i) {
		p[i] = static_cast<unsigned char> (i);
	}
	std::shuffle(std::begin(p), std::end(p), mt);
	for (int i = 0; i < tab_size; i++)
		value_tab[i] = 1.f - 2.f * dist(mt);
}

int PCG::Noise::Value::perm(int i)
{
	return p[i & tab_mask];
}

int PCG::Noise::Value::index(int x, int y, int z)
{
	return perm(x + perm(y + perm(z)));
}

float PCG::Noise::Value::vlattice(int ix, int iy, int iz)
{
	return value_tab[index(ix, iy, iz)];
}

float PCG::Noise::Value::noise(float x, float y, float z)
{
	std::vector<float> xknots(4), yknots(4), zknots(4);
	int ix = static_cast<int> (std::floor(x));
	float fx = x - ix;
	int iy = static_cast<int> (std::floor(y));
	float fy = y - iy;
	int iz = static_cast<int> (std::floor(z));
	float fz = z - iz;
	for (int k = -1; k <= 2; k++) {
		for (int j = -1; j <= 2; j++) {
			for (int i = -1; i <= 2; i++)
				xknots[i + 1] = vlattice(ix + i, iy + j, iz + k);
			yknots[j + 1] = Spline::spline(fx, xknots);
		}
		zknots[k + 1] = Spline::spline(fy, yknots);
	}
	return Spline::spline(fz, zknots);
}

OpenGL::uint8_texture PCG::Noise::Value::create_texture(long w, long h)
{
	float max = -1000.0f;
	float min = 1000.0f;
	OpenGL::uint8_texture ret;
	std::vector<std::vector<float>> vals;
	ret.resize(h);
	vals.resize(h);
	for (long j = 0; j < h; ++j) {
		ret[j].resize(w);
		vals[j].resize(w);
		for (long i = 0; i < w; ++i) {
			float d = noise((float) i * 0.0625f,
							(float) j * 0.0625f, 0.0f);
			if (d > max) {
				max = d;
			}
			if (d < min) {
				min = d;
			}
			vals[j][i] = d;
		}
	}
	for (long j = 0; j < h; ++j) { /// values passes the [0;1] range, so normalise it...
		for (long i = 0; i < w; ++i) {
			float n = (vals[j][i] - min) / (max - min);
			unsigned char s = (unsigned char) (n * 255);
			ret[j][i] = s;
		}
	}
	return ret;
}

float PCG::Noise::Value::Spline::spline(float x, const std::vector<float>& knot)
{
	float c3 = cr00 * knot[0] + cr01 * knot[1] + cr02 * knot[2] + cr03 * knot[3];
	float c2 = knot[0] + cr11 * knot[1] + cr12 * knot[2] + cr13 * knot[3];
	float c1 = cr20 * knot[0] + cr22 * knot[2];
	float cO = knot[1];
	return ((c3 * x + c2) * x + c1) * x + cO;
}
