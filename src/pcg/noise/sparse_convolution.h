/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This file is based on the book Texturing and Modeling, Third Edition: A Procedural Approach,
 * and is subject to its license
 */

#pragma once

#include "../../utils/texture_renderer.h"

#include <vector>

/// Implementation based on the book "Texturing and Modeling, Third Edition: A Procedural Approach"
/// from Ebert, D. S. et al.

namespace PCG {

	namespace Noise {

		class Sparse_convolution : public OpenGL::Texture_renderer {
		public:
			Sparse_convolution(unsigned seed = 666);

			float noise(float x, float y, float z);

			OpenGL::uint8_texture create_texture(long w, long h) override;

		private:
			static const int n_impulses = 3;
			static const int samp_rate = 100;
			static const int n_entries = 4 * samp_rate + 1;
			static const int tab_size = 256;
			static const int tab_mask = tab_size - 1;

			std::vector<float> impulse_tab;
			unsigned char p[256];
			float table[n_entries];

			void init(unsigned seed);

			inline int next(int h);

			float catrom2(float d);

			inline int index(int x, int y, int z);

			inline int perm(int i);
		};

	}

}
