/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code from Lagae et al. and is subject to the following license:
 * Copyright (c) 2009 by Ares Lagae, Sylvain Lefebvre,
 * George Drettakis and Philip Dutre
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "../../utils/texture_renderer.h"

/// Implementation based on the code for the paper "Procedural Noise using Sparse Gabor Convolution"
/// from Lagae, A. et. al.
/// Source available at http://graphics.cs.kuleuven.be/publications/LLDD09PNSGC/

namespace PCG {

	namespace Noise {

		class Gabor : public OpenGL::Texture_renderer {
		public:
			Gabor(float K = 1.0f, float a = 0.05f, float F_0 = 0.0625f, float omega_0 = M_PI_4,
				  float number_of_impulses_per_kernel = 64.0f);

			Gabor(unsigned seed, float K = 1.0f, float a = 0.05f, float F_0 = 0.0625f,
				  float omega_0 = M_PI_4, float number_of_impulses_per_kernel = 64.0);

			float noise(float x, float y);

			inline float noise(float x, float y, float)
			{
				return noise(x, y);
			}

			OpenGL::uint8_texture create_texture(long w, long h) override;

		private:
			void init(float number_of_impulses_per_kernel);

			float cell(int i, int j, float x, float y);

			unsigned morton(unsigned x, unsigned y);

			float K_;
			float a_;
			float F_0_;
			float omega_0_;
			float kernel_radius_;
			float impulse_density_;
			unsigned random_offset_;
			float number_of_impulses_per_cell;
			float cos_omega_0;
			float sin_omega_0;
			float m_m_pi_t_a_2;
			float m_pi_t_2_f_0;
		};

	}

}
