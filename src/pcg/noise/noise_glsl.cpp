/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "noise_glsl.h"
#include "../../utils/loaders.h"

#include <algorithm>
#include <random>

PCG::Noise::GLSL::Value::Value(unsigned seed)
		: OpenGL::Noise_renderer{ "res/shaders/value.frag" }, seed_{ seed }
{
}

void PCG::Noise::GLSL::Value::startup()
{
	OpenGL::Noise_renderer::startup();
	std::mt19937 mt = std::mt19937{ seed_ };
	std::uniform_real_distribution<float> dist{ 0.0f, 1.0f };
	std::vector<unsigned char> p_data;
	for (short i = 0; i < 256; ++i) {
		p_data.push_back((unsigned char) (i));
	}
	std::shuffle(p_data.begin(), p_data.end(), mt);
	std::vector<float> value_tab_data;
	for (int i = 0; i < 256; i++) {
		value_tab_data.push_back(1.f - 2.f * dist(mt));
	}
	p = OpenGL::load_texture(p_data);
	value_tab = OpenGL::load_texture(value_tab_data);
	program->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, p);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, value_tab);
	glUniform1i(glGetUniformLocation(program->get_program(), "p"), 0);
	glUniform1i(glGetUniformLocation(program->get_program(), "valueTab"), 1);
}

void PCG::Noise::GLSL::Value::shutdown()
{
	glDeleteTextures(1, &p);
	glDeleteTextures(1, &value_tab);
	OpenGL::Noise_renderer::shutdown();
}

PCG::Noise::GLSL::Gradient::Gradient(unsigned seed)
		: OpenGL::Noise_renderer{ "res/shaders/gradient.frag" }, seed_{ seed }
{
}

void PCG::Noise::GLSL::Gradient::startup()
{
	OpenGL::Noise_renderer::startup();
	std::mt19937 mt = std::mt19937{ seed_ };
	std::uniform_real_distribution<float> dist{ 0.0f, 1.0f };
	std::vector<unsigned char> p_data;
	for (short i = 0; i < 256; ++i) {
		p_data.push_back((unsigned char) (i));
	}
	std::shuffle(p_data.begin(), p_data.end(), mt);
	std::vector<float> gradient_tab_data;
	for (int i = 0; i < 256; i++) {
		float z = 1.f - 2.f * dist(mt);
		float r = std::sqrt(1.f - z * z);
		float theta = 2.f * M_PI * dist(mt);
		gradient_tab_data.push_back(r * std::cos(theta));
		gradient_tab_data.push_back(r * std::sin(theta));
		gradient_tab_data.push_back(z);
	}
	p = OpenGL::load_texture(p_data);
	gradient_tab = OpenGL::load_texture(gradient_tab_data);
	program->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, p);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gradient_tab);
	glUniform1i(glGetUniformLocation(program->get_program(), "p"), 0);
	glUniform1i(glGetUniformLocation(program->get_program(), "gradientTab"), 1);
}

void PCG::Noise::GLSL::Gradient::shutdown()
{
	glDeleteTextures(1, &p);
	glDeleteTextures(1, &gradient_tab);
	OpenGL::Noise_renderer::shutdown();
}

PCG::Noise::GLSL::Lattice_convolution::Lattice_convolution(unsigned seed)
		: OpenGL::Noise_renderer{ "res/shaders/lattice_convolution.frag" }, seed_{ seed }
{
}

void PCG::Noise::GLSL::Lattice_convolution::startup()
{
	OpenGL::Noise_renderer::startup();
	std::mt19937 mt = std::mt19937{ seed_ };
	std::uniform_real_distribution<float> dist{ 0.0f, 1.0f };
	std::vector<unsigned char> p_data;
	for (short i = 0; i < 256; ++i) {
		p_data.push_back((unsigned char) (i));
	}
	std::shuffle(p_data.begin(), p_data.end(), mt);
	std::vector<float> value_tab_data;
	for (int i = 0; i < 256; i++) {
		value_tab_data.push_back(1.f - 2.f * dist(mt));
	}
	std::vector<float> table_data;
	for (int i = 0; i < 401; i++) {
		float x = i / (float) 100;
		x = std::sqrt(x);
		if (x < 1)
			table_data.push_back(0.5f * (2.f + x * x * (-5.f + x * 3.f)));
		else
			table_data.push_back(0.5f * (4.f + x * (-8.f + x * (5.f - x))));
	}
	p = OpenGL::load_texture(p_data);
	value_tab = OpenGL::load_texture(value_tab_data);
	table = OpenGL::load_texture(table_data);
	program->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, p);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, value_tab);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, table);
	glUniform1i(glGetUniformLocation(program->get_program(), "p"), 0);
	glUniform1i(glGetUniformLocation(program->get_program(), "valueTab"), 1);
	glUniform1i(glGetUniformLocation(program->get_program(), "table"), 2);
}

void PCG::Noise::GLSL::Lattice_convolution::shutdown()
{
	glDeleteTextures(1, &p);
	glDeleteTextures(1, &value_tab);
	glDeleteTextures(1, &table);
	OpenGL::Noise_renderer::shutdown();
}

PCG::Noise::GLSL::Sparse_convolution::Sparse_convolution(unsigned seed)
		: OpenGL::Noise_renderer{ "res/shaders/sparse_convolution.frag" }, seed_{ seed }
{
}

void PCG::Noise::GLSL::Sparse_convolution::startup()
{
	OpenGL::Noise_renderer::startup();
	std::mt19937 mt = std::mt19937{ seed_ };
	std::uniform_real_distribution<float> dist{ 0.0f, 1.0f };
	std::vector<unsigned char> p_data;
	for (short i = 0; i < 256; ++i) {
		p_data.push_back((unsigned char) (i));
	}
	std::shuffle(p_data.begin(), p_data.end(), mt);
	std::vector<float> impulse_tab_data;
	for (int i = 0; i < 256; ++i) {
		impulse_tab_data.push_back(dist(mt));
		impulse_tab_data.push_back(dist(mt));
		impulse_tab_data.push_back(dist(mt));
		impulse_tab_data.push_back(1.f - 2.f * dist(mt));
	}
	std::vector<float> table_data;
	for (int i = 0; i < 401; i++) {
		float x = i / (float) 100;
		x = std::sqrt(x);
		if (x < 1)
			table_data.push_back(0.5f * (2.f + x * x * (-5.f + x * 3.f)));
		else
			table_data.push_back(0.5f * (4.f + x * (-8.f + x * (5.f - x))));
	}
	p = OpenGL::load_texture(p_data);
	impulse_tab = OpenGL::load_texture(impulse_tab_data);
	table = OpenGL::load_texture(table_data);
	program->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, p);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, impulse_tab);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, table);
	glUniform1i(glGetUniformLocation(program->get_program(), "p"), 0);
	glUniform1i(glGetUniformLocation(program->get_program(), "impulseTab"), 1);
	glUniform1i(glGetUniformLocation(program->get_program(), "table"), 2);
}

void PCG::Noise::GLSL::Sparse_convolution::shutdown()
{
	glDeleteTextures(1, &p);
	glDeleteTextures(1, &impulse_tab);
	glDeleteTextures(1, &table);
	OpenGL::Noise_renderer::shutdown();
}

PCG::Noise::GLSL::Perlin::Perlin(unsigned seed)
		: OpenGL::Noise_renderer{ "res/shaders/perlin.frag" }, seed_{ seed }
{
}

namespace {

	void normalize3(float* v)
	{
		float s = std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
		v[0] /= s;
		v[1] /= s;
		v[2] /= s;
	}

	const int B = 256;

	struct perlin_data {
		float g[B + B + 2][3];
		int p[B + B + 2];
	};
}

void PCG::Noise::GLSL::Perlin::startup()
{
	OpenGL::Noise_renderer::startup();
	std::mt19937 mt = std::mt19937{ seed_ };
	std::uniform_int_distribution<int> dist(0, B + B - 1);
	perlin_data data;
	for (int i = 0; i < B; i++) {
		data.p[i] = i;
		for (int j = 0; j < 3; j++)
			data.g[i][j] = (float) (dist(mt) - B) / B;
		normalize3(data.g[i]);
	}
	std::uniform_int_distribution<int> dist1(0, B - 1);
	int i = B;
	int j;
	while (--i) {
		int k = data.p[i];
		data.p[i] = data.p[j = dist1(mt)];
		data.p[j] = k;
	}
	for (i = 0; i < B + 2; i++) {
		data.p[B + i] = data.p[i];
		for (j = 0; j < 3; j++)
			data.g[B + i][j] = data.g[i][j];
	}
	std::vector<unsigned char> p_data;
	p_data.resize(B + B + 2);
	for (int k = 0; k < B + B + 2; ++k) {
		p_data[k] = (unsigned char) data.p[k];
	}
	p = OpenGL::load_texture(p_data);
	glCreateTextures(GL_TEXTURE_2D, 1, &g);
	if (g) {
		glTextureStorage2D(g, 1, GL_R32F, (B + B + 2), 3);
		glTextureSubImage2D(g, 0, 0, 0, (B + B + 2), 3, GL_RED, GL_FLOAT, data.g);
	}
	program->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, p);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, g);
	glUniform1i(glGetUniformLocation(program->get_program(), "p"), 0);
	glUniform1i(glGetUniformLocation(program->get_program(), "g"), 1);
}

void PCG::Noise::GLSL::Perlin::shutdown()
{
	glDeleteTextures(1, &p);
	glDeleteTextures(1, &g);
	OpenGL::Noise_renderer::shutdown();
}

PCG::Noise::GLSL::OpenSimplex::OpenSimplex(unsigned seed)
		: OpenGL::Noise_renderer{ "res/shaders/opensimplex.frag" }, seed_{ seed }
{
}

void PCG::Noise::GLSL::OpenSimplex::startup()
{
	OpenGL::Noise_renderer::startup();
	std::minstd_rand prng{ seed_ };
	std::vector<unsigned char> perm_data;
	for (short i = 0; i < 256; ++i) {
		perm_data.push_back((unsigned char) (i));
	}
	std::shuffle(perm_data.begin(), perm_data.end(), prng);
	std::vector<unsigned char> perm_grad_index_3d_data;
	for (short i = 0; i < 256; i++)
		perm_grad_index_3d_data.push_back((unsigned char) ((perm_data[i] % 24) * 3));
	static const char gradients3D_data[72]{
			-11, 4, 4, -4, 11, 4, -4, 4, 11,
			11, 4, 4, 4, 11, 4, 4, 4, 11,
			-11, -4, 4, -4, -11, 4, -4, -4, 11,
			11, -4, 4, 4, -11, 4, 4, -4, 11,
			-11, 4, -4, -4, 11, -4, -4, 4, -11,
			11, 4, -4, 4, 11, -4, 4, 4, -11,
			-11, -4, -4, -4, -11, -4, -4, -4, -11,
			11, -4, -4, 4, -11, -4, 4, -4, -11
	};
	perm = OpenGL::load_texture(perm_data);
	perm_grad_index_3d = OpenGL::load_texture(perm_grad_index_3d_data);
	glCreateTextures(GL_TEXTURE_2D, 1, &gradients_3d);
	if (gradients_3d) {
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTextureStorage2D(gradients_3d, 1, GL_R8I, 72, 1);
		glTextureSubImage2D(gradients_3d, 0, 0, 0, 72, 1,
							GL_RED_INTEGER, GL_BYTE, gradients3D_data);
		glPixelStorei(GL_PACK_ALIGNMENT, 4);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	}
	program->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, perm);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, perm_grad_index_3d);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, gradients_3d);
	glUniform1i(glGetUniformLocation(program->get_program(), "perm"), 0);
	glUniform1i(glGetUniformLocation(program->get_program(), "permGradIndex3D"), 1);
	glUniform1i(glGetUniformLocation(program->get_program(), "gradients3D"), 2);
}

void PCG::Noise::GLSL::OpenSimplex::shutdown()
{
	glDeleteTextures(1, &perm);
	glDeleteTextures(1, &perm_grad_index_3d);
	glDeleteTextures(1, &gradients_3d);
	OpenGL::Noise_renderer::shutdown();
}

PCG::Noise::GLSL::Gabor::Gabor(unsigned seed)
		: OpenGL::Noise_renderer{ "res/shaders/gabor.frag" }, seed_{ seed }
{
}

void PCG::Noise::GLSL::Gabor::startup()
{
	OpenGL::Noise_renderer::startup();
	glUniform1ui(glGetUniformLocation(program->get_program(), "seed"), seed_);
}

PCG::Noise::GLSL::Plain::Plain()
		: OpenGL::Noise_renderer{ "res/shaders/plain_gray_fifty.frag" }
{
}
