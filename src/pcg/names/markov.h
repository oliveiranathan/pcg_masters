/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <random>
#include <string>
#include <vector>

namespace PCG {

	namespace Names {

		/// Based on code for a Name generator based on Markov chains
		/// The code is available at
		/// http://www.roguebasin.com/index.php?title=Markov_chains-based_name_generation

		class Markov_special_statistics {
		public:
			Markov_special_statistics(std::string input_file_name);

			Markov_special_statistics(unsigned seed, std::string input_file_name);

			std::string operator()(int min_length, int max_length);

		private:

			class Word_frequency {
			public:
				int count_beginning{ 0 };
				int count_end{ 0 };
				int count_within{ 0 };
				bool set{ false };
			};

			std::mt19937 mt;
			std::uniform_real_distribution<float> dist{ 0.0f, 1.0f };
			Word_frequency base_map[256][256];
			std::vector<unsigned char> start_chars;
			std::vector<unsigned char> available_chars;

			void init(std::string input_fn);
		};

	}

}
