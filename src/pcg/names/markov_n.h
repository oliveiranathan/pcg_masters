/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <map>
#include <random>
#include <string>
#include <unordered_set>
#include <vector>

namespace PCG {

	namespace Names {

		/// Based on code for a Markov chain generator by Martín Candela Calabuig
		/// The code is available at https://github.com/Rellikiox/MapGenerator

		class Markov_order_n {
		public:
			Markov_order_n(std::string input_file_name, size_t order, size_t min_length);

			Markov_order_n(unsigned seed, std::string input_file_name, size_t order, size_t min_length);

			std::string operator()();

		private:

			std::mt19937 mt;

			size_t order;
			size_t min_name_length;

			using Chain = std::map<std::string, std::vector<char>>;

			std::unordered_set<std::string> v_generated;
			std::vector<std::string> v_samples;
			Chain m_chains;

			void init(std::string input_fn);

			void process_name(std::string name);
		};

	}

}
