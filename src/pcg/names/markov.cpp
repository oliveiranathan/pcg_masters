/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "markov.h"

#include <algorithm>
#include <fstream>
#include <set>

PCG::Names::Markov_special_statistics::Markov_special_statistics(std::string input_file_name)
		: mt{ 666 }
{
	init(input_file_name);
}

PCG::Names::Markov_special_statistics::Markov_special_statistics(unsigned seed, std::string input_file_name)
		: mt{ seed }
{
	init(input_file_name);
}

void PCG::Names::Markov_special_statistics::init(std::string input_fn)
{
	std::ifstream file_stream_in{ input_fn };
	std::set<unsigned char> start_chars_set;
	std::set<unsigned char> available_chars_set;
	while (file_stream_in.good()) {
		std::string word;
		file_stream_in >> word;
		if (word.length() > 1) {
			start_chars_set.insert(word[0]);
			available_chars_set.insert(word[0]);
			for (unsigned word_position = 0; (word_position + 1) < (word.length()); ++word_position) {
				available_chars_set.insert(word[word_position + 1]);
				unsigned char base = word[word_position];
				unsigned char sequence = word[word_position + 1];
				Word_frequency& wf = base_map[base][sequence];
				wf.set = true;
				if (word_position == 0) {
					wf.count_beginning++;
				} else if ((word_position + 1) >= (word.length() - 1)) {
					wf.count_end++;
				} else if ((word_position > 0) && ((word_position + 1) < (word.length() - 1))) {
					wf.count_within++;
				}
			}
		}
	}
	start_chars.reserve(start_chars_set.size());
	for (std::set<unsigned char>::iterator it = start_chars_set.begin();
		 it != start_chars_set.end(); ++it)
		start_chars.push_back(*it);
	available_chars.reserve(available_chars_set.size());
	for (std::set<unsigned char>::iterator it = available_chars_set.begin();
		 it != available_chars_set.end(); ++it)
		available_chars.push_back(*it);
}

std::string PCG::Names::Markov_special_statistics::operator()(int min_length, int max_length)
{
	std::string name;
	std::vector<unsigned char> freq_vector;
	int range = max_length - min_length + 1;
	int range_length = static_cast<int> (min_length + (range * dist(mt)));
	unsigned char a = start_chars[static_cast<int> (start_chars.size() * dist(mt))];
	name += a;
	for (int counter = 1; counter < range_length; counter++) {
		int cdc = 0;
		for (unsigned count = 0; count < available_chars.size(); ++count) {
			unsigned char b = available_chars[count];
			if (base_map[a][b].set) {
				if (counter == 1) {
					for (int cc = 0; cc < base_map[a][b].count_beginning; ++cc) {
						freq_vector.push_back(b);
						cdc++;
					}
				} else if ((counter + 1) >= (range_length - 1)) {
					for (int cc = 0; cc < base_map[a][b].count_end; ++cc) {
						freq_vector.push_back(b);
						cdc++;
					}
				} else {
					for (int cc = 0; cc < base_map[a][b].count_within; ++cc) {
						freq_vector.push_back(b);
						cdc++;
					}
				}
			}
		}
		int c = int(cdc * dist(mt));
		name += freq_vector[c];
		a = freq_vector[c];
		freq_vector.clear();
	}
	return name;
}
