/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "markov_n.h"

#include <algorithm>
#include <fstream>

PCG::Names::Markov_order_n::Markov_order_n(std::string input_file_name, size_t o, size_t m_l)
		: mt{ 666 }, order{ o }, min_name_length{ m_l }
{
	init(input_file_name);
}

PCG::Names::Markov_order_n::Markov_order_n(unsigned seed, std::string input_file_name, size_t o, size_t min_length)
		: mt{ seed }, order{ o }, min_name_length{ min_length }
{
	init(input_file_name);
}

void PCG::Names::Markov_order_n::init(std::string input_fn)
{
	std::ifstream file_stream_in{ input_fn };
	while (file_stream_in.good()) {
		std::string word;
		file_stream_in >> word;
		std::transform(word.begin(), word.end(), word.begin(), ::tolower);
		process_name(word);
	}
}

void PCG::Names::Markov_order_n::process_name(std::string name)
{
	if (name.length() < order)
		return;
	for (size_t i = 0; i <= name.length() - order; ++i) {
		std::string segment = name.substr(i, order);
		char next_char = i == name.length() - order ? '\n' : name[i + order];
		Chain::iterator it = m_chains.find(segment);
		if (it != m_chains.end()) {
			it->second.push_back(next_char);
		} else {
			std::vector<char> v(1, next_char);
			m_chains[segment] = v;
		}
	}
	v_generated.insert(name);
	v_samples.push_back(name);

}

std::string PCG::Names::Markov_order_n::operator()()
{
	std::string name;
	do {
		std::uniform_int_distribution<size_t> ds{ 0, v_samples.size() - 1 };
		size_t n = ds(mt);
		size_t name_length = v_samples[n].size();
		name = v_samples[n].substr(0, order);
		if (name[0] == ' ')
			continue;
		Chain::iterator it = m_chains.find(name);
		char next_char;
		while (name.length() < name_length) {
			std::uniform_int_distribution<size_t> di{ 0, it->second.size() - 1 };
			next_char = it->second[di(mt)];
			if (next_char != '\n') {
				name.append(1, next_char);
				std::string segment = name.substr(name.length() - order);
				it = m_chains.find(segment);
			} else {
				break;
			}
		}
	} while (v_generated.find(name) != v_generated.end() || name.length() < min_name_length);
	v_generated.insert(name);
	std::transform(name.begin(), name.begin() + 1, name.begin(), ::toupper);
	return name;
}
