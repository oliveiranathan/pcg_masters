/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <random>
#include <string>

namespace PCG {

	namespace Names {

		/// Based on code for a Finite state name generator by Jeff Lait
		/// The code is available at
		/// http://www.roguebasin.com/index.php?title=Finite_state_name_generator

		class Finite_state {
		public:
			Finite_state(unsigned seed = 666);

			std::string operator()(int len);

		private:

			std::mt19937 mt;

			bool rand_chance(int c);

			int rand_choice(int i);
		};

	}

}
