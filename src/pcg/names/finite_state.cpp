/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "finite_state.h"

PCG::Names::Finite_state::Finite_state(unsigned seed)
		: mt{ seed }
{
}

bool PCG::Names::Finite_state::rand_chance(int c)
{
	std::uniform_int_distribution<int> d{ 1, 100 };
	return d(mt) <= c;
}

int PCG::Names::Finite_state::rand_choice(int i)
{
	std::uniform_int_distribution<int> d{ 0, i - 1 };
	return d(mt);
}

std::string PCG::Names::Finite_state::operator()(int len)
{
	std::string vowels = "aaaeeeiiiooouuyy'";
	std::string frictive = "rsfhvnmz";
	std::string plosive = "tpdgkbc";
	std::string weird = "qwjx";
	int syllables = 0;
	char state = 0;
	int pos = 0;
	bool prime = false;
	std::string text;
	text.resize(len + 1, '\0');
	if (rand_chance(30))
		state = 'v';
	else if (rand_chance(40))
		state = 'f';
	else if (rand_chance(70))
		state = 'p';
	else
		state = 'w';
	while (pos < len - 1) {
		switch (state) {
			case 'v':
				text[pos++] = vowels[rand_choice((int) vowels.size())];
				if (!prime)
					syllables++;
				break;
			case 'f':
				text[pos++] = frictive[rand_choice((int) frictive.size())];
				break;
			case 'p':
				text[pos++] = plosive[rand_choice((int) plosive.size())];
				break;
			case 'w':
				text[pos++] = weird[rand_choice((int) weird.size())];
				break;
			default:
				break;
		}
		if (syllables && pos >= 3) {
			if (rand_chance(20 + pos * 4))
				break;
		}
		switch (state) {
			case 'v':
				if (!prime && rand_chance(10)) {
					state = 'v';
					prime = true;
					break;
				} else if (rand_chance(40))
					state = 'f';
				else if (rand_chance(70))
					state = 'p';
				else
					state = 'w';
				prime = false;
				break;
			case 'f':
				if (!prime && rand_chance(50)) {
					prime = true;
					state = 'p';
					break;
				}
				state = 'v';
				prime = false;
				break;
			case 'p':
				if (!prime && rand_chance(10)) {
					prime = true;
					state = 'f';
					break;
				}
				state = 'v';
				prime = false;
				break;
			case 'w':
				state = 'v';
				prime = false;
				break;
			default:
				break;
		}
	}
	text[0] = (char) toupper(text[0]);
	return text.substr(0, pos);
}
