/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/application.h"
#include "../../utils/program.h"

#include <map>
#include <memory>
#include <random>
#include <unordered_set>
#include <vector>

#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace PCG {

	namespace Towns {

		/// Based on the code for a city generator by Hannah Craighead,
		/// Tana Tanoi and Cameron Bryers.
		/// The code is available at https://github.com/TanaTanoi/new-londis-city

		class City_gen : public OpenGL::Renderer {
		protected:
			void startup() override;

			void render(double) override;

			void shutdown() override;

			void on_mouse_button(int button, int action) override;

			void on_mouse_move(int x, int y) override;

			void on_mouse_wheel(int pos) override;

		private:
			struct Line {
				glm::vec2 start;
				glm::vec2 end;
				int id;
			};

			struct Section {
				std::vector<Line> lines;
				float area;
			};

			enum class Building_type {
				skyscraper = 0, residential = 1, industrial = 2
			};

			struct Building_params {
				std::vector<glm::vec2> bounding_area;
				unsigned int seed = 0;
				Building_type b_type = Building_type::skyscraper;
				int height = 3;
			};

			struct Generator {
				static constexpr float section_to_points_scale{ 0.04f };

				static Section points_to_sections(
						std::vector<glm::vec2> points);

				static std::vector<glm::vec2> get_bounding_box(
						const std::vector<glm::vec2>& floor);

				static std::vector<glm::vec2> section_to_points(const Section& sec);

				static glm::vec2 center_point(const std::vector<glm::vec2>& points);

				static std::vector<Building_params> sections_to_params(
						const std::vector<Section>& sections,
						const std::vector<glm::vec2>& heightmap,
						float range, glm::vec2 min);

				static std::vector<glm::vec2> shrink_points(
						const std::vector<glm::vec2>& points);

				static std::vector<glm::vec2> cut_edges(
						const std::vector<glm::vec2>& points);

				static std::vector<glm::vec2> generate_floor_plan(glm::vec2 center,
																  float radius, int n);

				static std::vector<glm::vec2> combine_plans(
						const std::vector<glm::vec2>& shape_a,
						const std::vector<glm::vec2>& shape_b);

				static std::string generate_random_building_string(int itrs);

				static std::vector<std::vector<glm::vec2>> subdivide(
						const std::vector<glm::vec2>& points);

			private:
				static bool contains_vec(const std::vector<glm::vec2>& v,
										 glm::vec2 to_find);

				static std::string l_system_lookup(char c);

				static constexpr float network_to_section_scale = 2.f;
				static std::mt19937 mt;
			};

			enum class Command_type {
				nop,
				set_texture,///bind textures
				draw///draws the stuff
			};

			struct Draw_command {
				Command_type type{ Command_type::nop };
				GLuint obj_id{ 0 };
				GLenum mode;
				GLint first{ 0 };
				GLsizei count{ 0 };
			};

			struct Building_lod {
				std::vector<Draw_command> low;
				std::vector<Draw_command> high;
			};

			struct Building {
				std::vector<glm::vec2> heightmap_points;

				void generate_road(std::vector<GLfloat>& data, std::vector<Draw_command>& commands,
								   glm::vec2 a, glm::vec2 b, float width);

				void draw_ground();

				void init();

				std::vector<Draw_command>
				generate_buildings_from_sections(std::vector<GLfloat>& data,
												 const std::vector<Section>& sections,
												 float range, glm::vec2 min);

				void delete_opengl_stuff();

				static void load_texture(GLuint texture, const std::string& filename);

			private:
				void create_floor();

				void generate_building(std::vector<GLfloat>& data, Building_params& parameters,
									   Building_lod& result);

				std::vector<Draw_command> generate_park(std::vector<GLfloat>& data,
														const std::vector<glm::vec2>& floor);

				std::vector<Draw_command>
				generate_residential_building(std::vector<GLfloat>& data,
											  const std::vector<glm::vec2>& points,
											  int height);

				std::vector<Draw_command>
				generate_modern_building(std::vector<GLfloat>& data, glm::vec2 mid, float min_dist);

				std::vector<Draw_command>
				generate_from_string(std::vector<GLfloat>& data,
									 std::vector<glm::vec2> floor, const std::string& input);

				void generate_park_wall(std::vector<GLfloat>& data,
										std::vector<Draw_command>& command_list,
										glm::vec2 a, glm::vec2 b, glm::vec2 mid);

				void render_windows(std::vector<GLfloat>& data,
									std::vector<Draw_command>& command_list,
									const std::vector<glm::vec2>& floor, float elevation);

				float extend_building(std::vector<GLfloat>& data,
									  std::vector<Draw_command>& command_list,
									  const std::vector<glm::vec2>& floor, float elevation);

				void generate_point_roof(std::vector<GLfloat>& data,
										 std::vector<Draw_command>& command_list,
										 const std::vector<glm::vec2>& points, float elevation);

				void generate_flat_point_roof(std::vector<GLfloat>& data,
											  std::vector<Draw_command>& command_list,
											  const std::vector<glm::vec2>& points, float elevation);

				void generate_door(std::vector<GLfloat>& data, std::vector<Draw_command>& command_list,
								   glm::vec2 a, glm::vec2 b, float elevation, glm::vec3 normal);

				void generate_windows(std::vector<GLfloat>& data,
									  std::vector<Draw_command>& command_list,
									  glm::vec2 a, glm::vec2 b, float elevation, glm::vec3 normal);

				static constexpr float foundation_size = 0.05f;
				static constexpr float tex_wall_width = 0.5f;
				GLuint conc = 0;
				static const int total_wall_textures = 4;
				GLuint tex_wall[2][total_wall_textures];
				static const int total_window_textures = 5;
				GLuint tex_window[total_window_textures];
				GLuint tex_door;
				GLuint grass;
				int cur_tex_wall = 0;
				int cur_tex_wall_num = 1;
				int cur_tex_win_num = 0;
				static constexpr float extrude_threshold = 0.1f;
				static constexpr float block_size = 0.3f;
				static constexpr float window_width = 0.1f;
				GLuint floor_buffer{ 0 };
				std::mt19937 mt{ 666 };
			};

			struct Spline {
				static glm::vec2 calculate_point(glm::vec2 p0, glm::vec2 p1,
												 glm::vec2 p2, glm::vec2 p3, float t);

				static float calculate_y_value(const std::vector<glm::vec2>& points,
											   float x_dist);
			};

			struct Lot {
				Lot(Section bb, std::vector<Section> secs, int id);

				Section bounding_box;
				std::vector<Section> sections;
				int id;
				Building_lod buildings;
			};

			struct Section_divider {
				const std::vector<Lot>& get_lots() const;

				void divide_all_lots(const std::vector<Section>& lot_outlines);

				void add_building_to_lot(const Lot& l);

			private:
				float get_section_size(const Section& s);

				void divide_lot(const Lot& l);

				Lot rec_divide_section(Lot lot, const Section& s);

				Lot remove_unusable_sections(Lot l);

				std::vector<Section> split_section(const Section& s);

				bool has_street_access(const Section& s, const Lot& l);

				Line find_longest_edge(const Section& s);

				Section get_inner_section(const Section& s, const Line& bi,
										  const Line& to_cut, const Line& long_line);

				glm::vec2 get_shared_point(const Line& a, const Line& b);

				std::vector<Lot> lots;
				int lot_id = 0;
				float goal_area = 1500.f;
				float min_area = 200.f;
				std::mt19937 mt{ 666 };
			};

			struct Road_node {
				Road_node();

				Road_node(glm::vec2 loc, int id);

				bool operator<(const Road_node& rhs) const;

				bool operator==(const Road_node& rhs) const;

				glm::vec2 location;
				int id = -1;
			};

			struct Road_node_hasher {
				std::size_t operator()(const Road_node& node) const;
			};

			struct Road {
				Road();

				Road(Road_node s, Road_node e, int id);

				Road_node start;
				Road_node end;
				int id;
				bool is_cycle_edge = false;
			};

			struct Primitive {
				std::vector<Road_node> vertices;
				int type;
			};

			struct Road_network {
				Road_network(unsigned seed = 666);

				const std::vector<Road>& get_all_roads() const;

				void create_roads();

				int get_cycle_size();

				const std::vector<Primitive>& get_cycles() const;

			private:
				void calculate_boundary();

				void gen_branch_roads(glm::vec2 start);

				void find_minimum_cycles();

				Road_node add_node(glm::vec2 point);

				Road add_road(const Road_node& start, const Road_node& end);

				void rec_divide_grid(const Road& r, int level, bool half_length);

				void branch(const Road_node& n, std::vector<Road_node>& to_add,
							std::vector<Road_node>& to_remove);

				std::vector<Primitive> extract_primitives();

				void update_adjacency_list(const Road& r, const Road_node& n);

				glm::vec2 direction(const Road_node& n);

				Road_node snap_to_intersection(const Road_node& start, glm::vec2 end);

				void update_branch_list(const Road_node& n,
										std::vector<Road_node>& to_remove);

				using Road_graph = std::map<int, std::vector<int>>;

				void extract_isolated_vertex(std::vector<Primitive>& primitives,
											 std::vector<Road_node>& heap,
											 Road_graph& adjs);

				void extract_filament(int start_id, int end_id,
									  std::vector<Primitive>& primitives, std::vector<Road_node>& heap,
									  Road_graph& adjs, std::vector<Road>& roads);

				void extract_primitive(std::vector<Primitive>& primitives,
									   std::vector<Road_node>& heap,
									   Road_graph& adjs, std::vector<Road>& roads);

				Road_node snap_to_close(glm::vec2 point, glm::vec2 start);

				Road_node snap_to_existing(glm::vec2 inter, glm::vec2 start,
										   const Road& r);

				void remove_adjacency_links(int id,
											Road_graph& adjs);

				Road_node get_clockwise_most(const Road_node& current,
											 const std::vector<int>& adj);

				Road_node get_anti_clockwise_most(const Road_node& vprev,
												  const Road_node& current, const std::vector<int>& adj);

				void set_cycle_edge(std::vector<Road>& roads,
									int start_id, int end_id);

				void remove_edge(std::vector<Road>& roads,
								 Road_graph& adjs, int to_remove);

				std::vector<Road> all_roads;
				int min_rotate_angle = -30;
				int max_rotate_angle = 30;
				int recurse_size;
				std::mt19937 mt;
				std::vector<Primitive> cycles;
				std::vector<Road_node> all_nodes;
				Road_graph adjacency_list;
				Section outline;
				float max_height;
				float min_height;
				float far_left;
				float far_right;
				std::unordered_set<Road_node, Road_node_hasher> can_branch;
				int node_id = 0;
				int road_id = 0;
				int max_length = 80;
				int min_length = 40;
				int min_angle = 0;
				int max_angle = 360;
				int snap_distance = 20;
			};

			struct Image {
				Image(const std::string& filename);

				Image(size_t w_, size_t h_, size_t n_);

				Image sub_image(size_t xoffset, size_t yoffset,
								size_t width, size_t height);

				unsigned char* data_pointer();

				GLenum gl_format() const;

				int get_num_mipmaps();

				size_t w;
				size_t h;
				size_t n;
				std::vector<unsigned char> data;
			};

			void init_building_generator();

			void generate_buildings();

			void generate_roads();

			void setup_camera();

			void draw_skycube();

			void init_skybox(const std::string& filepath);

			static OpenGL::Program_ptr make_shader_program(
					const std::string& vert_source, const std::string& frag_source);

			static glm::vec2 get_bisector(const Line& l);

			static glm::vec2 centre_point_of_line(const Line& l);

			static float get_length(const Line& l);

			static glm::vec2 get_line_for_length(glm::vec2 start_point,
												 glm::vec2 direction, float length);

			static bool is_point_on_line(glm::vec2 point, const Line& edge);

			static bool float_equal(float x, float y);

			static std::vector<Road_node> sort_points(
					const std::vector<Road_node>& points);

			static std::vector<Road> sort_roads(const std::vector<Road>& roads);

			static glm::vec2 find_perp(glm::vec2 dir);

			static bool intersects(glm::vec2 a1, glm::vec2 a2,
								   glm::vec2 b1, glm::vec2 b2);

			static void sort_by_intersection(std::vector<Road>& roads,
											 glm::vec2 start, glm::vec2 end);

			static glm::vec2 get_intersection(glm::vec2 a1, glm::vec2 a2,
											  glm::vec2 b1, glm::vec2 b2);

			static int find_road_index(const std::vector<Road>& roads,
									   int s_id, int e_id);

			static void remove_from_heap(std::vector<Road_node>& heap, int id);

			static bool contains(const std::vector<Road_node>& visited,
								 const Road_node& node);

			static float point_to_line_dist(glm::vec2 point, glm::vec2 start,
											glm::vec2 end);

			static glm::vec2 get_closest_point_on_line(glm::vec2 point,
													   glm::vec2 start, glm::vec2 end);

			static float dot_perp(glm::vec2 start, glm::vec2 end);

			static std::vector<Line>
			lines_intersecting_with_section(const Section& s,
											glm::vec2 perp_bi, glm::vec2 centre_point,
											const Line& long_line);

			static std::unique_ptr<glm::vec2> get_intersection(const Line& l, glm::vec2 cut_dir,
															   glm::vec2 cut_point);

			static bool share_side(const Line& edge, const Line& sec_edge);

			static std::unique_ptr<glm::vec2> get_intersection(const Line& l, const Line& o);

			static glm::vec2 get_equation(glm::vec2 a, glm::vec2 b);

			static bool intersects(const Line& lon, glm::vec2 cut,
								   glm::vec2 cut_point);

			static bool share_slope(const Line& edge, const Line& sec_edge);

			void draw(const std::vector<Draw_command>& commands);

			Building building;
			std::unique_ptr<Section_divider> g_sections;
			std::shared_ptr<Road_network> g_network;
			GLuint skybox_id{ 0 };
			glm::vec2 m_pos{ 0, 0 };
			bool m_left_button{ false };
			bool first_m{ true };
			glm::vec2 rotation{ 0, 0 };
			float zoom{ 1.f };
			OpenGL::Program_ptr skybox_shader;
			OpenGL::Program_ptr main_shader;
			glm::mat4 projection;
			glm::mat4 model_view;
			GLuint VAO{ 0 };
			GLuint skybox_buffer{ 0 };
			GLuint buildings_vao{ 0 };
			GLuint buildings_buffer{ 0 };
			GLuint roads_vao{ 0 };
			GLuint roads_buffer{ 0 };
			GLuint road_texture = 0;
			std::vector<Draw_command> roads_draw_commands;
			std::mt19937 mt{ 666 };
		};

	}

}
