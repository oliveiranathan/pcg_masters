/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "buildings.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <png++/png.hpp>

std::mt19937 PCG::Towns::City_gen::Generator::mt{ 666 };

void PCG::Towns::City_gen::startup()
{
	glEnable(GL_DEPTH_TEST);
	glCreateVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	main_shader = make_shader_program("res/shaders/main_shader.vert",
									  "res/shaders/main_shader.frag");
	main_shader->use();
	init_building_generator();
	generate_buildings();
	generate_roads();
}

void PCG::Towns::City_gen::render(double)
{
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	main_shader->use();
	setup_camera();
	model_view = glm::translate(model_view, glm::vec3{ 0.f, -2.f, 0.f });
	GLuint location = glGetUniformLocation(main_shader->get_program(), "model_view");
	glUniformMatrix4fv(location, 1, false, glm::value_ptr(model_view));
	glBindBuffer(GL_ARRAY_BUFFER, buildings_buffer);
	glBindVertexArray(buildings_vao);
	for (const Lot& l : g_sections->get_lots()) {
		draw(l.buildings.high);
	}
	glBindBuffer(GL_ARRAY_BUFFER, roads_buffer);
	glBindVertexArray(roads_vao);
	glBindTexture(GL_TEXTURE_2D, road_texture);
	draw(roads_draw_commands);
	glBindVertexArray(VAO);
	building.draw_ground();
	draw_skycube();
}

void PCG::Towns::City_gen::shutdown()
{
	if (skybox_id) {
		glDeleteTextures(1, &skybox_id);
		skybox_id = 0;
	}
	building.delete_opengl_stuff();
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &skybox_buffer);
	glDeleteVertexArrays(1, &buildings_vao);
	glDeleteBuffers(1, &buildings_buffer);
	glDeleteTextures(1, &road_texture);
	glDisable(GL_DEPTH_TEST);
}

void PCG::Towns::City_gen::on_mouse_button(int button, int action)
{
	if (button == GLFW_MOUSE_BUTTON_1) {
		m_left_button = action;
	}
}

void PCG::Towns::City_gen::on_mouse_move(int xpos, int ypos)
{
	if (first_m) {
		m_pos = glm::vec2(xpos, ypos);
		first_m = false;
	}
	if (m_left_button) {
		rotation.x += (m_pos.x - xpos);
		rotation.y += (m_pos.y - ypos);
	}
	m_pos = glm::vec2(xpos, ypos);
}

void PCG::Towns::City_gen::on_mouse_wheel(int pos)
{
	if (pos > 0) {
		zoom /= 1.1f;
	} else {
		zoom *= 1.1f;
	}
}

void PCG::Towns::City_gen::init_building_generator()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	building = Building();
	skybox_shader = make_shader_program("res/shaders/skybox_shader.vert",
										"res/shaders/skybox_shader.frag");
	init_skybox("res/assets/cubemap.png");
	building.heightmap_points.push_back(glm::vec2(0, height_ / 2));
	building.heightmap_points.push_back(glm::vec2(0, height_ / 2));
	building.heightmap_points.push_back(glm::vec2(width_, height_ / 2));
	building.heightmap_points.push_back(glm::vec2(width_, height_ / 2));
	building.init();
}

void PCG::Towns::City_gen::generate_buildings()
{
	g_network = std::make_shared<Road_network>(mt());
	g_network->create_roads();
	while (g_network->get_cycle_size() <= 150) {
		g_network = std::make_shared<Road_network>(mt());
		g_network->create_roads();
	}
	std::vector<Section> lot_outlines;
	for (Primitive prim : g_network->get_cycles()) {
		std::vector<glm::vec2> points;
		for (Road_node rn : prim.vertices) {
			points.push_back(rn.location);
		}
		lot_outlines.push_back(Generator::points_to_sections(points));
	}
	g_sections = std::make_unique<Section_divider>();
	g_sections->divide_all_lots(lot_outlines);
	std::vector<Lot> all_lots = g_sections->get_lots();
	glm::vec2 min{ 10000, 10000 };
	glm::vec2 max{ 0, 0 };
	glm::vec2 zero{ 0, 0 };
	for (Lot l : all_lots) {
		std::vector<glm::vec2> bounds = Generator::get_bounding_box(
				Generator::section_to_points(l.bounding_box));
		if (distance(bounds[0], zero) < distance(min, zero)) {
			min = bounds[0];
		}
		if (distance(bounds[1], zero) > distance(max, zero)) {
			max = bounds[1];
		}
	}
	float range = distance(max, min);
	glBindVertexArray(VAO);
	std::vector<GLfloat> data;
	for (Lot l : all_lots) {
		l.buildings.high = building.generate_buildings_from_sections(data, l.sections,
																	 range, min);
		g_sections->add_building_to_lot(l);
	}
	glCreateVertexArrays(1, &buildings_vao);
	glBindVertexArray(buildings_vao);
	glCreateBuffers(1, &buildings_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buildings_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * data.size(), &data[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), nullptr);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
						  (const GLvoid*) (2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	glBindVertexArray(VAO);
}

void PCG::Towns::City_gen::generate_roads()
{
	glBindVertexArray(VAO);
	std::vector<GLfloat> data;
	roads_draw_commands.clear();
	float scale = 2.0f * Generator::section_to_points_scale;
	for (const Road& r : g_network->get_all_roads()) {
		building.generate_road(data, roads_draw_commands,
							   r.start.location * scale, r.end.location * scale, 0.2f);
	}
	glCreateVertexArrays(1, &roads_vao);
	glBindVertexArray(roads_vao);
	glCreateBuffers(1, &roads_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, roads_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * data.size(), &data[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), nullptr);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
						  (const GLvoid*) (2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	glBindVertexArray(VAO);
	glGenTextures(1, &road_texture);
	Building::load_texture(road_texture, "res/assets/road01.png");
}

void PCG::Towns::City_gen::setup_camera()
{
	projection = glm::perspective(20.0f, float(width_) / float(height_), 0.1f, 1000.f);
	model_view = glm::translate(glm::mat4{}, glm::vec3{ 0, 0, -50 * zoom });
	model_view = glm::rotate(model_view, glm::radians(-rotation.y),
							 glm::vec3{ 1.f, 0.f, 0.f });
	model_view = glm::rotate(model_view, glm::radians(rotation.x),
							 glm::vec3{ 0.f, 1.f, 0.f });
	GLint location = glGetUniformLocation(main_shader->get_program(), "projection");
	glUniformMatrix4fv(location, 1, false, glm::value_ptr(projection));
	location = glGetUniformLocation(main_shader->get_program(), "model_view");
	glUniformMatrix4fv(location, 1, false, glm::value_ptr(model_view));
}

void PCG::Towns::City_gen::draw_skycube()
{
	skybox_shader->use();
	GLint location = glGetUniformLocation(skybox_shader->get_program(), "projection");
	glUniformMatrix4fv(location, 1, false, glm::value_ptr(projection));
	location = glGetUniformLocation(skybox_shader->get_program(), "model_view");
	glUniformMatrix4fv(location, 1, false, glm::value_ptr(model_view));
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_id);
	glBindBuffer(GL_ARRAY_BUFFER, skybox_buffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
}

void PCG::Towns::City_gen::init_skybox(const std::string& filepath)
{
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glGenTextures(1, &skybox_id);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_id);
	Image tex(filepath);
	int cube_width = ceil(tex.w / 4.0f);
	Image p_x = tex.sub_image(cube_width * 2, cube_width * 1, cube_width, cube_width);
	Image n_x = tex.sub_image(cube_width * 0, cube_width * 1, cube_width, cube_width);
	Image p_y = tex.sub_image(cube_width * 1, cube_width * 0, cube_width, cube_width);
	Image n_y = tex.sub_image(cube_width * 1, cube_width * 2, cube_width, cube_width);
	Image n_z = tex.sub_image(cube_width * 3, cube_width * 1, cube_width, cube_width);
	Image p_z = tex.sub_image(cube_width * 1, cube_width * 1, cube_width, cube_width);
	std::vector<Image> faces;
	faces.push_back(p_x);
	faces.push_back(n_x);
	faces.push_back(p_y);
	faces.push_back(n_y);
	faces.push_back(p_z);
	faces.push_back(n_z);
	glTexStorage2D(GL_TEXTURE_CUBE_MAP, p_x.get_num_mipmaps(), GL_RGB8, p_x.w, p_x.h);
	for (int i = 0; i < 6; i++) {
		glTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, 0, 0, cube_width,
						cube_width, faces[i].gl_format(), GL_UNSIGNED_BYTE,
						faces[i].data_pointer());
	}
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	static GLfloat size = 100.f;
	static const GLfloat floor = -1.0f;
	std::vector<glm::vec2> box;
	box.push_back(glm::vec2(-size, -size));
	box.push_back(glm::vec2(-size, size));
	box.push_back(glm::vec2(size, size));
	box.push_back(glm::vec2(size, -size));
	size /= 1.5;
	static const GLfloat data[] = {
			///x		y		z
			box[0].x, floor, box[0].y,
			box[1].x, floor, box[1].y,
			box[2].x, floor, box[2].y,
			box[3].x, floor, box[3].y,
			box[3].x, +size, box[3].y,
			box[2].x, +size, box[2].y,
			box[1].x, +size, box[1].y,
			box[0].x, +size, box[0].y,
			box[0].x, floor, box[0].y,
			box[0].x, +size, box[0].y,
			box[1].x, +size, box[1].y,
			box[1].x, floor, box[1].y,
			box[1].x, floor, box[1].y,
			box[1].x, +size, box[1].y,
			box[2].x, +size, box[2].y,
			box[2].x, floor, box[2].y,
			box[2].x, floor, box[2].y,
			box[2].x, +size, box[2].y,
			box[3].x, +size, box[3].y,
			box[3].x, floor, box[3].y,
			box[3].x, floor, box[3].y,
			box[3].x, +size, box[3].y,
			box[0].x, +size, box[0].y,
			box[0].x, floor, box[0].y
	};
	glCreateBuffers(1, &skybox_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, skybox_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
}

OpenGL::Program_ptr PCG::Towns::City_gen::make_shader_program(
		const std::string& vert_source, const std::string& frag_source)
{
	std::vector<OpenGL::Shader_ptr> shaders;
	shaders.push_back(OpenGL::Shader::from_file(vert_source,
												OpenGL::Shader::Stage::vertex));
	shaders.push_back(OpenGL::Shader::from_file(frag_source,
												OpenGL::Shader::Stage::fragment));
	return OpenGL::Program::create(shaders);
}

glm::vec2 PCG::Towns::City_gen::get_bisector(const Line& l)
{
	glm::vec2 line_vec = l.end - l.start;
	glm::vec2 perp_bi = glm::vec2(-line_vec.y, line_vec.x);
	return perp_bi;
}

glm::vec2 PCG::Towns::City_gen::centre_point_of_line(const Line& l)
{
	glm::vec2 dir = l.end - l.start;
	float leng = abs(length(dir));
	dir = normalize(dir);
	glm::vec2 to_return = l.start + dir * (leng / 2.0f);
	return to_return;
}

float PCG::Towns::City_gen::get_length(const Line& l)
{
	float x_length = abs(l.end.x - l.start.x);
	float y_length = abs(l.end.y - l.start.y);
	return sqrt(x_length * x_length + y_length * y_length);
}

glm::vec2 PCG::Towns::City_gen::get_line_for_length(glm::vec2 start_point,
													glm::vec2 direction, float length)
{
	direction = normalize(direction);
	return start_point + (direction * length);
}

bool PCG::Towns::City_gen::is_point_on_line(glm::vec2 point, const Line& edge)
{
	glm::vec2 dir = edge.start - edge.end;
	glm::vec2 comp_dir = point - edge.end;
	if (length(comp_dir) == 0) {
		return true;
	}
	if (length(comp_dir) >= length(dir)) {
		return false;
	}
	dir = normalize(dir);
	comp_dir = normalize(comp_dir);
	return (dir.x == comp_dir.x && dir.y == comp_dir.y);
}

bool PCG::Towns::City_gen::float_equal(float x, float y)
{
	return (int) round(x * 10.0f) == (int) round(y * 10.0f);
}

std::vector<PCG::Towns::City_gen::Road_node>
PCG::Towns::City_gen::sort_points(const std::vector<Road_node>& points)
{
	std::vector<Road_node> sorted;
	for (int i = 0; i < (int) points.size(); ++i) {
		Road_node current = points[i];
		bool added = false;
		for (int j = 0; j < (int) sorted.size(); ++j) {
			if (current.location.x < sorted[j].location.x) {
				sorted.insert(sorted.begin() + j, current);
				added = true;
				break;
			} else if (current.location.x == sorted[j].location.x) {
				if (current.location.y < sorted[j].location.y) {
					sorted.insert(sorted.begin() + j, current);
					added = true;
					break;
				}
			}
		}
		if (!added) {
			sorted.push_back(current);
		}
	}
	return sorted;
}

std::vector<PCG::Towns::City_gen::Road>
PCG::Towns::City_gen::sort_roads(const std::vector<Road>& roads)
{
	std::vector<Road> sorted;
	for (int i = 0; i < (int) roads.size(); ++i) {
		Road current = roads[i];
		bool added = false;
		for (int j = 0; j < (int) sorted.size(); ++j) {
			if (current.start.id < sorted[j].start.id) {
				sorted.insert(sorted.begin() + j, current);
				added = true;
				break;
			} else if (current.start.id == sorted[j].start.id) {
				if (current.end.id < sorted[j].end.id) {
					sorted.insert(sorted.begin() + j, current);
					added = true;
					break;
				}
			}
		}
		if (!added) {
			sorted.push_back(current);
		}
	}
	return sorted;
}

glm::vec2 PCG::Towns::City_gen::find_perp(glm::vec2 dir)
{
	dir = normalize(dir);
	glm::vec3 direction = glm::vec3(dir.x, 0, dir.y);
	glm::vec3 upvec = glm::vec3(0, 1, 0);
	glm::vec3 perp = cross(direction, upvec);
	return glm::vec2(perp.x, perp.z);
}

bool PCG::Towns::City_gen::intersects(glm::vec2 a1, glm::vec2 a2,
									  glm::vec2 b1, glm::vec2 b2)
{
	glm::vec2 e1 = get_equation(a1, a2);
	glm::vec2 e2 = get_equation(b1, b2);
	if (e1.x == e2.x) {
		return false;
	}
	if ((std::isinf(e1.x)) && (std::isinf(e2.x))) {
		return false;
	}
	glm::vec2 value = get_intersection(a1, a2, b1, b2);
	float x = value.x;
	if (x < std::min(a1.x, a2.x) || x > std::max(a2.x, a1.x)
		|| x < std::min(b2.x, b1.x) || x > std::max(b1.x, b2.x)) {
		return false;
	}
	return !(value.y > std::max(a1.y, a2.y) || value.y < std::min(a2.y, a1.y)
			 || value.y > std::max(b1.y, b2.y) || value.y < std::min(b2.y, b1.y));
}

void PCG::Towns::City_gen::sort_by_intersection(std::vector<Road>& roads,
												glm::vec2 start, glm::vec2 end)
{
	std::vector<Road> unsorted = roads;
	roads.clear();
	std::vector<float> distances;
	for (size_t i = 0; i < unsorted.size(); ++i) {
		glm::vec2 inter = get_intersection(start, end,
										   unsorted[i].start.location, unsorted[i].end.location);
		float dist = distance(inter, start);
		bool added = false;
		for (size_t j = 0; j < roads.size(); ++j) {
			if (!added && dist < distances[j]) {
				distances.insert(distances.begin() + j, dist);
				roads.insert(roads.begin() + j, unsorted[i]);
				added = true;
			}
		}
		if (!added) {
			distances.push_back(dist);
			roads.push_back(unsorted[i]);
		}
	}
}

glm::vec2 PCG::Towns::City_gen::get_intersection(glm::vec2 a1, glm::vec2 a2,
												 glm::vec2 b1, glm::vec2 b2)
{
	glm::vec2 e1 = get_equation(a1, a2);
	glm::vec2 e2 = get_equation(b1, b2);
	float x = (e2.y - e1.y) / (e1.x - e2.x);
	float y = e1.x * x + e1.y;
	glm::vec2 to_return = glm::vec2(x, y);
	if (std::isinf(e1.x) || std::isnan(e1.x)) {
		to_return.x = e1.y;
		to_return.y = e2.x * to_return.x + e2.y;
	} else if (std::isinf(e2.x) || std::isnan(e2.x)) {
		to_return.x = e2.y;
		to_return.y = e1.x * to_return.x + e1.y;
	}
	return to_return;
}

int PCG::Towns::City_gen::find_road_index(const std::vector<Road>& roads,
										  int s_id, int e_id)
{
	for (size_t i = 0; i < roads.size(); ++i) {
		if (e_id == roads[i].start.id && s_id == roads[i].end.id) {
			return int(i);
		}
		if (s_id == roads[i].start.id && e_id == roads[i].end.id) {
			return int(i);
		}
	}
	return 0;
}

void PCG::Towns::City_gen::remove_from_heap(std::vector<Road_node>& heap, int id)
{
	for (size_t i = 0; i < heap.size(); ++i) {
		if (id == heap[i].id) {
			heap.erase(heap.begin() + i);
			return;
		}
	}
}

bool PCG::Towns::City_gen::contains(const std::vector<Road_node>& visited,
									const Road_node& node)
{
	int n = (int) visited.size();
	if (n == 0) {
		return false;
	}
	for (int i = 0; i < n; ++i) {
		if (visited[i].id == node.id) {
			return true;
		}
	}
	return false;
}

float PCG::Towns::City_gen::point_to_line_dist(glm::vec2 point, glm::vec2 start,
											   glm::vec2 end)
{
	glm::vec2 v = end - start;
	glm::vec2 w = point - start;
	float c1 = dot(w, v);
	float c2 = dot(v, v);
	float b = c1 / c2;
	glm::vec2 p_b = start + b * v;
	return distance(point, p_b);
}

glm::vec2 PCG::Towns::City_gen::get_closest_point_on_line(glm::vec2 point,
														  glm::vec2 start, glm::vec2 end)
{
	glm::vec2 v = end - start;
	glm::vec2 w = point - start;
	float c1 = dot(w, v);
	float c2 = dot(v, v);
	float b = c1 / c2;
	glm::vec2 intersect_point = start + b * v;
	return intersect_point;
}

float PCG::Towns::City_gen::dot_perp(glm::vec2 start, glm::vec2 end)
{
	return start.x * end.y - end.x * start.y;
}

std::vector<PCG::Towns::City_gen::Line>
PCG::Towns::City_gen::lines_intersecting_with_section(const Section& s, glm::vec2 perp_bi,
													  glm::vec2 centre_point, const Line& long_line)
{
	std::vector<Line> intersectors;
	for (int i = 0; i < (int) s.lines.size(); ++i) {
		if (s.lines[i].id != long_line.id) {
			if (intersects(s.lines[i], perp_bi, centre_point)) {
				intersectors.push_back(s.lines[i]);
			}
		}
	}
	return intersectors;
}

std::unique_ptr<glm::vec2> PCG::Towns::City_gen::get_intersection(const Line& l, glm::vec2 cut_dir,
																  glm::vec2 cut_point)
{
	float m_l = (l.end.y - l.start.y) / (l.end.x - l.start.x);
	float c_l = l.end.y - m_l * l.end.x;
	float m = cut_dir.y / cut_dir.x;
	float c = cut_point.y - m * cut_point.x;
	if (m == m_l || m == -m_l) {
		return std::unique_ptr<glm::vec2>{ nullptr };
	}
	if (l.end.x == l.start.x) {
		float y = m * l.start.x + c;
		return std::make_unique<glm::vec2>(l.start.x, y);
	}
	if (cut_dir.x == 0) {
		float y = m_l * cut_point.x + c_l;
		return std::make_unique<glm::vec2>(cut_point.x, y);
	}
	float x = (c - c_l) / (m_l - m);
	float y = m_l * x + c_l;
	return std::make_unique<glm::vec2>(x, y);
}

bool PCG::Towns::City_gen::share_side(const Line& edge, const Line& sec_edge)
{
	if (share_slope(edge, sec_edge)) {
		if (edge.end.x - edge.start.x == 0) {
			float min_height = std::min(edge.start.y, edge.end.y);
			float max_height = std::max(edge.start.y, edge.end.y);
			return ((sec_edge.start.y > min_height && sec_edge.start.y < max_height)
					|| (sec_edge.end.y > min_height && sec_edge.end.y < max_height)) &&
				   sec_edge.end.x - edge.end.x == 0;
		}
		float m = (edge.end.y - edge.start.y) / (edge.end.x - edge.start.x);
		float c = edge.end.y - m * edge.end.x;
		float y = sec_edge.start.x * m + c;
		if (int(round(y)) == int(round(sec_edge.start.y))) {
			return true;
		}
		y = sec_edge.end.x * m + c;
		if (int(round(y)) == int(round(sec_edge.end.y))) {
			return true;
		}
	}
	return false;
}

std::unique_ptr<glm::vec2> PCG::Towns::City_gen::get_intersection(const Line& l, const Line& o)
{
	float m_l = (l.end.y - l.start.y) / (l.end.x - l.start.x);
	float c_l = l.end.y - m_l * l.end.x;
	float m = (o.end.y - o.start.y) / (o.end.x - o.start.x);
	float c = o.end.y - m * o.end.x;
	if (m == m_l || m == -m_l) {
		return std::unique_ptr<glm::vec2>{ nullptr };
	}
	if (l.end.x == l.start.x) {
		float y = m * l.start.x + c;
		return std::make_unique<glm::vec2>(l.start.x, y);
	}
	if (o.end.x == o.start.x) {
		float y = m_l * o.start.x + c_l;
		return std::make_unique<glm::vec2>(o.start.x, y);
	}
	float x = (c - c_l) / (m_l - m);
	float y = m * x + c;
	return std::make_unique<glm::vec2>(x, y);
}

glm::vec2 PCG::Towns::City_gen::get_equation(glm::vec2 a, glm::vec2 b)
{
	glm::vec2 equation;
	equation.x = (b.y - a.y) / (b.x - a.x);
	equation.y = (a.y - (equation.x * a.x));
	if (std::isnan(equation.x) || std::isinf(equation.x)) {
		equation.y = a.x;
	}
	return equation;
}

bool PCG::Towns::City_gen::intersects(const Line& lon, glm::vec2 cut,
									  glm::vec2 cut_point)
{
	std::unique_ptr<glm::vec2> intersection;
	intersection = get_intersection(lon, cut, cut_point);
	if (!intersection)
		return false;
	return intersection->x <= std::max(lon.start.x, lon.end.x)
		   && intersection->x >= std::min(lon.start.x, lon.end.x)
		   && intersection->y <= std::max(lon.start.y, lon.end.y)
		   && intersection->y >= std::min(lon.start.y, lon.end.y);
}

bool PCG::Towns::City_gen::share_slope(const Line& edge, const Line& sec_edge)
{
	if (edge.start.x - edge.end.x == 0
		&& sec_edge.start.x - sec_edge.end.x == 0) {
		return true;
	}
	float m = (edge.end.y - edge.start.y) / (edge.end.x - edge.start.x);
	float ms = (sec_edge.end.y - sec_edge.start.y)
			   / (sec_edge.end.x - sec_edge.start.x);
	int m1 = int(std::round(ms));
	int m2 = int(std::round(m));
	return m1 == m2;
}

void PCG::Towns::City_gen::draw(const std::vector<Draw_command>& commands)
{
	for (const Draw_command& command : commands) {
		switch (command.type) {
			case Command_type::set_texture:///bind textures
				glBindTexture(GL_TEXTURE_2D, command.obj_id);
				break;
			case Command_type::draw:///draws the stuff
				glDrawArrays(command.mode, command.first, command.count);
				break;
			case Command_type::nop:
				break;
		}
	}
}

PCG::Towns::City_gen::Image::Image(const std::string& filename)
{
	{
		std::ifstream stream{ filename };
		png::reader<std::ifstream> reader{ stream };
		try {
			reader.read_info();
		} catch (png::error& e) {
			std::cout << e.what() << std::endl;
		}
		switch (reader.get_color_type()) {
			case png::color_type::color_type_gray:
				n = 1;
				break;
			case png::color_type::color_type_gray_alpha:
				n = 2;
				break;
			case png::color_type::color_type_rgb:
				n = 3;
				break;
			case png::color_type::color_type_rgb_alpha:
				n = 4;
				break;
			default:
				n = 3;
				break;
		}
	}
	png::image<png::rgba_pixel> image{ filename };
	w = image.get_width();
	h = image.get_height();
	data.resize(w * h * n, 0);
	for (size_t y = 0; y < h; ++y) {
		for (size_t x = 0; x < w; ++x) {
			switch (n) {
				case 4:
					data[(y * w + x) * n + 3] = image[y][x].alpha;
				case 3:
					data[(y * w + x) * n + 2] = image[y][x].blue;
				case 2:
					data[(y * w + x) * n + 1] = image[y][x].green;
				case 1:
					data[(y * w + x) * n + 0] = image[y][x].red;
				default:
					break;
			}
		}
	}
}

PCG::Towns::City_gen::Image::Image(size_t w_, size_t h_, size_t n_)
		: w{ w_ }, h{ h_ }, n{ n_ }, data(w * h * n, 0)
{
}

PCG::Towns::City_gen::Image PCG::Towns::City_gen::Image::sub_image(
		size_t xoffset, size_t yoffset, size_t width, size_t height)
{
	Image r(width, height, n);
	for (size_t y = 0; y < height; ++y) {
		if ((y + yoffset) >= h)
			continue;
		for (size_t x = 0; x < width; ++x) {
			if ((x + xoffset) >= w)
				continue;
			for (size_t i = 0; i < n; i++) {
				r.data[(y * width * n) + (x * n) + i] =
						data[((y + yoffset) * w * n) + ((x + xoffset) * n) + i];
			}
		}
	}
	return r;
}

unsigned char* PCG::Towns::City_gen::Image::data_pointer()
{
	return &data[0];
}

GLenum PCG::Towns::City_gen::Image::gl_format() const
{
	switch (n) {
		case 1:
			return GL_R;
		case 2:
			return GL_RG;
		case 3:
			return GL_RGB;
		case 4:
			return GL_RGBA;
		default:
			return GL_RGB;
	}
}

int PCG::Towns::City_gen::Image::get_num_mipmaps()
{
	return 1 + floor(log2(std::max(w, h)));
}

const std::vector<PCG::Towns::City_gen::Road>&
PCG::Towns::City_gen::Road_network::get_all_roads() const
{
	return all_roads;
}

void PCG::Towns::City_gen::Road_network::create_roads()
{
	Line a = { glm::vec2(400, 400), glm::vec2(100, 400), 0 };
	Line b = { glm::vec2(100, 400), glm::vec2(100, 100), 1 };
	Line c = { glm::vec2(100, 100), glm::vec2(400, 100), 2 };
	Line d = { glm::vec2(400, 100), glm::vec2(400, 400), 3 };
	std::vector<Line> lines;
	lines.push_back(a);
	lines.push_back(b);
	lines.push_back(c);
	lines.push_back(d);
	Section s;
	s.lines = lines;
	calculate_boundary();
	std::uniform_int_distribution<int> ds_18_22{ 18, 22 };
	recurse_size = ds_18_22(mt);
	gen_branch_roads(glm::vec2(0, 0));
	find_minimum_cycles();
}

int PCG::Towns::City_gen::Road_network::get_cycle_size()
{
	return cycles.size();
}

const std::vector<PCG::Towns::City_gen::Primitive>&
PCG::Towns::City_gen::Road_network::get_cycles() const
{
	return cycles;
}

void PCG::Towns::City_gen::Road_network::calculate_boundary()
{
	for (int i = 0; i < (int) outline.lines.size(); i++) {
		if (i == 0) {
			max_height = std::max(outline.lines[i].start.y, outline.lines[i].end.y);
			min_height = std::min(outline.lines[i].start.y, outline.lines[i].end.y);
			far_left = std::min(outline.lines[i].start.x, outline.lines[i].end.x);
			far_right = std::max(outline.lines[i].start.x, outline.lines[i].end.x);
		} else {
			max_height = std::max(max_height,
								  std::max(outline.lines[i].start.y, outline.lines[i].end.y));
			min_height = std::min(min_height,
								  std::min(outline.lines[i].start.y, outline.lines[i].end.y));
			far_left = std::min(far_left,
								std::min(outline.lines[i].start.x, outline.lines[i].end.x));
			far_right = std::max(far_right,
								 std::max(outline.lines[i].start.x, outline.lines[i].end.x));
		}
	}
}

void PCG::Towns::City_gen::Road_network::gen_branch_roads(glm::vec2 start)
{
	Road_node rn = add_node(start);
	can_branch.insert(rn);
	for (int i = 0; i < recurse_size; ++i) {
		std::vector<Road_node> to_add;
		std::vector<Road_node> to_remove;
		for (Road_node n : can_branch) {
			branch(n, to_add, to_remove);
		}
		for (Road_node n : to_add) {
			can_branch.insert(n);
		}
		for (Road_node n : to_remove) {
			can_branch.erase(n);
		}
		to_add.clear();
		to_remove.clear();
	}
}

void PCG::Towns::City_gen::Road_network::find_minimum_cycles()
{
	std::vector<Primitive> prims = extract_primitives();
	for (Primitive p : prims) {
		if (p.type == 2) {
			cycles.push_back(p);
		}
	}
}

PCG::Towns::City_gen::Road_node
PCG::Towns::City_gen::Road_network::add_node(glm::vec2 point)
{
	Road_node r = { point, node_id++ };
	all_nodes.push_back(r);
	adjacency_list[r.id] = std::vector<int>();
	return r;
}

PCG::Towns::City_gen::Road PCG::Towns::City_gen::Road_network::add_road(
		const Road_node& start, const Road_node& end)
{
	Road r = { start, end, road_id++ };
	for (int i = 0; i < (int) adjacency_list[start.id].size(); i++) {
		if (adjacency_list[start.id][i] == end.id) {
			return {};
		}
	}
	all_roads.push_back(r);
	adjacency_list[start.id].push_back(end.id);
	adjacency_list[end.id].push_back(start.id);
	return r;
}

void PCG::Towns::City_gen::Road_network::rec_divide_grid(const Road& r, int level,
														 bool half_length)
{
	Line l = { r.start.location, r.end.location, 0 };
	glm::vec2 perp_bi = get_bisector(l);
	glm::vec2 centre_point = centre_point_of_line(l);
	Road_node rn = add_node(centre_point);
	update_adjacency_list(r, rn);
	float length = get_length(l);
	if (half_length) {
		length = length / 2.0;
	}
	glm::vec2 a = get_line_for_length(centre_point, perp_bi, length);
	glm::vec2 b = get_line_for_length(centre_point, -perp_bi, length);
	bool a_intersect = false;
	bool b_intersect = false;
	Road_node r1;
	Road_node r2;
	int n = all_roads.size();
	for (int i = 0; i < n; ++i) {
		Line l = { all_roads[i].start.location, all_roads[i].end.location, 0 };
		if (!a_intersect && is_point_on_line(a, l)) {
			if (float_equal(a.x, all_roads[i].start.location.x)
				&& float_equal(a.y, all_roads[i].start.location.y)) {
				r1 = all_roads[i].start;
				a_intersect = true;
			} else if (float_equal(a.x, all_roads[i].end.location.x)
					   && float_equal(a.y, all_roads[i].end.location.y)) {
				r1 = all_roads[i].end;
				a_intersect = true;
			} else {
				n--;
				r1 = add_node(a);
				update_adjacency_list(all_roads[i], r1);
				a_intersect = true;
			}
		} else if (!b_intersect && is_point_on_line(b, l)) {
			if (float_equal(b.x, all_roads[i].start.location.x)
				&& float_equal(b.y, all_roads[i].start.location.y)) {
				r2 = all_roads[i].start;
				b_intersect = true;
			} else if (float_equal(b.x, all_roads[i].end.location.x)
					   && float_equal(b.y, all_roads[i].end.location.y)) {
				r2 = all_roads[i].end;
				b_intersect = true;
			} else {
				n--;
				r2 = add_node(b);
				update_adjacency_list(all_roads[i], r2);
				b_intersect = true;
			}
		}
	}
	if (!a_intersect) {
		r1 = add_node(a);
	}
	if (!b_intersect) {
		r2 = add_node(b);
	}
	Road left = add_road(rn, r1);
	Road right = add_road(rn, r2);
	if (level < recurse_size) {
		rec_divide_grid(left, level + 1, !half_length);
		rec_divide_grid(right, level + 1, !half_length);
	}
}

void PCG::Towns::City_gen::Road_network::branch(const Road_node& n, std::vector<Road_node>& to_add,
												std::vector<Road_node>& to_remove)
{
	std::uniform_real_distribution<float> ds_0_1{ 0, 1 };
	float random = ds_0_1(mt);
	float length = min_length + random * (max_length - min_length);
	glm::vec2 dir = direction(n);
	if (std::isnan(dir.x) || std::isnan(dir.y)) {
		return;
	}
	glm::vec2 road = dir * length;
	glm::vec2 end_point = n.location + road;
	Road_node end = snap_to_intersection(n, end_point);
	if (end.id == -1) {
		return;
	}
	if (distance(n.location, end.location) > min_length) {
		if ((int) adjacency_list[end.id].size() < 4) {
			to_add.push_back(end);
			add_road(n, end);
		}
	}
	update_branch_list(n, to_remove);
}

std::vector<PCG::Towns::City_gen::Primitive>
PCG::Towns::City_gen::Road_network::extract_primitives()
{
	std::vector<Primitive> primitives;
	std::vector<Road_node> heap = sort_points(all_nodes);
	Road_graph adjacencies = adjacency_list;
	std::vector<Road> roads = sort_roads(all_roads);
	while ((int) heap.size() != 0) {
		if ((int) roads.size() == 0) {
			break;
		}
		Road_node vertex = heap[0];
		std::vector<int> adjs = adjacencies[vertex.id];
		int no_adj = (int) adjs.size();
		if (no_adj == 0) {
			extract_isolated_vertex(primitives, heap, adjacencies);
		} else if (no_adj == 1) {
			extract_filament(vertex.id, adjacencies[vertex.id][0], primitives, heap,
							 adjacencies, roads);
		} else {
			extract_primitive(primitives, heap, adjacencies, roads);
		}
	}
	return primitives;
}

void PCG::Towns::City_gen::Road_network::update_adjacency_list(const Road& r,
															   const Road_node& n)
{
	for (size_t i = 0; i < all_roads.size(); ++i) {
		if (r.id == all_roads[i].id) {
			all_roads.erase(all_roads.begin() + i);
			break;
		}
	}
	for (size_t i = 0; i < adjacency_list[r.start.id].size(); ++i) {
		if (adjacency_list[r.start.id][i] == r.end.id) {
			adjacency_list[r.start.id].erase(adjacency_list[r.start.id].begin() + i);
			break;
		}
	}
	for (size_t i = 0; i < adjacency_list[r.end.id].size(); ++i) {
		if (adjacency_list[r.end.id][i] == r.start.id) {
			adjacency_list[r.end.id].erase(adjacency_list[r.end.id].begin() + i);
			break;
		}
	}
	add_road(r.start, n);
	add_road(n, r.end);
}

glm::vec2 PCG::Towns::City_gen::Road_network::direction(const Road_node& n)
{
	std::uniform_real_distribution<float> ds_0_1{ 0, 1 };
	float random = ds_0_1(mt);
	float angle = 0.0f;
	glm::vec2 dir;
	int count = (int) adjacency_list[n.id].size();
	switch (count) {
		case 0: {
			angle = min_angle + random * (max_angle - min_angle);
			float rad_angle = glm::radians(angle);
			dir = normalize(glm::vec2((float) cos(rad_angle), (float) sin(rad_angle)));
			return dir;
		}
		case 1: {
			angle = min_rotate_angle + random * (max_rotate_angle - min_rotate_angle);
			float rad_angle = glm::radians(angle);
			float cs = cos(rad_angle);
			float sn = sin(rad_angle);
			Road_node other = all_nodes[adjacency_list[n.id].at(0)];
			dir = normalize(n.location - other.location);
			return glm::vec2(dir.x * cs - dir.y * sn, dir.x * sn + dir.y * cs);
		}
		case 2: {
			angle = min_rotate_angle + random * (max_rotate_angle - min_rotate_angle);
			float rad_angle = glm::radians(angle);
			float cs = cos(rad_angle);
			float sn = sin(rad_angle);
			Road_node other = all_nodes[adjacency_list[n.id][0]];
			dir = find_perp(other.location - n.location);
			return glm::vec2(dir.x * cs - dir.y * sn, dir.x * sn + dir.y * cs);
		}
		case 3: {
			angle = min_rotate_angle + random * (max_rotate_angle - min_rotate_angle);
			float rad_angle = glm::radians(angle);
			float cs = cos(rad_angle);
			float sn = sin(rad_angle);
			Road_node other = all_nodes[adjacency_list[n.id][0]];
			dir = find_perp(other.location - n.location);
			if (dot(normalize(normalize(other.location) - normalize(n.location)),
					normalize(normalize(dir) - normalize(n.location))) > 7.5) {
				dir = -dir;
			}
			return -1.f * glm::vec2(dir.x * cs - dir.y * sn, dir.x * sn + dir.y * cs);
		}
	}
	return glm::vec2(0, 1);
}

PCG::Towns::City_gen::Road_node
PCG::Towns::City_gen::Road_network::snap_to_intersection(const Road_node& start,
														 glm::vec2 end)
{
	glm::vec2 snapEnd = end + normalize(end - start.location) * float(snap_distance);
	std::vector<Road> intersect;
	for (Road r : all_roads) {
		if (intersects(start.location, snapEnd, r.start.location, r.end.location)) {
			if (r.start.id != start.id && r.end.id != start.id) {
				intersect.push_back(r);
			}
		}
	}
	if ((int) intersect.size() == 0) {
		Road_node close = snap_to_close(end, start.location);
		if (close.id >= 0) {
			add_road(start, close);
			return close;
		}
		return { glm::vec2(), -1 };
	}
	sort_by_intersection(intersect, start.location, end);
	glm::vec2 first_inter = get_intersection(start.location, snapEnd,
											 intersect[0].start.location, intersect[0].end.location);
	Road_node new_end = snap_to_existing(first_inter, start.location, intersect[0]);
	if (new_end.id == -1) {
		new_end = add_node(first_inter);
		update_adjacency_list(intersect[0], new_end);
	} else if ((int) adjacency_list[new_end.id].size() > 3) {
		return { glm::vec2(), -1 };
	}
	return new_end;
}

void PCG::Towns::City_gen::Road_network::update_branch_list(const Road_node& n,
															std::vector<Road_node>& to_remove)
{
	std::uniform_real_distribution<float> ds_0_1{ 0, 1 };
	float random = ds_0_1(mt);
	int no_adj = (int) adjacency_list[n.id].size();
	if (no_adj == 1) {
		if (random < 0.1) {
			to_remove.push_back(n);
		}
	} else if (no_adj == 2) {
		if (random < 0.35) {
			to_remove.push_back(n);
		}
	} else if (no_adj == 3) {
		if (random < 0.7) {
			to_remove.push_back(n);
		}
	} else {
		to_remove.push_back(n);
	}
}

void PCG::Towns::City_gen::Road_network::extract_isolated_vertex(
		std::vector<Primitive>& primitives, std::vector<Road_node>& heap,
		Road_graph& adjs)
{
	std::vector<Road_node> vertices;
	vertices.push_back(heap.front());
	heap.erase(heap.begin());
	Primitive p = { vertices, 0 };
	adjs.erase(vertices[0].id);
	primitives.push_back(p);
}

void PCG::Towns::City_gen::Road_network::extract_filament(int start_id, int end_id,
														  std::vector<Primitive>& primitives,
														  std::vector<Road_node>& heap,
														  Road_graph& adjs,
														  std::vector<Road>& roads)
{
	if (roads[(find_road_index(roads, start_id, end_id))].is_cycle_edge) {
		if ((int) adjs[start_id].size() >= 3) {
			int to_remove = find_road_index(roads, start_id, end_id);
			roads.erase(roads.begin() + to_remove);
			start_id = end_id;
		}
		while ((int) adjs[start_id].size() == 1) {
			end_id = adjs[start_id][0];
			if (roads[find_road_index(roads, start_id, end_id)].is_cycle_edge) {
				remove_from_heap(heap, start_id);
				int to_remove = find_road_index(roads, start_id, end_id);
				roads.erase(roads.begin() + to_remove);
				remove_adjacency_links(start_id, adjs);
				start_id = end_id;
			} else {
				break;
			}
		}
		if ((int) adjs[start_id].size() == 0) {
			remove_from_heap(heap, start_id);
			remove_adjacency_links(start_id, adjs);
		}
	} else {
		Primitive p;
		p.type = 1;
		if ((int) adjs[start_id].size() >= 3) {
			p.vertices.push_back(all_nodes[start_id]);
			int to_remove = find_road_index(roads, start_id, end_id);
			roads.erase(roads.begin() + to_remove);
			start_id = end_id;
		}
		while ((int) adjs[start_id].size() == 1) {
			p.vertices.push_back(all_nodes[start_id]);
			end_id = adjs[start_id][0];
			remove_from_heap(heap, start_id);
			int to_remove = find_road_index(roads, start_id, end_id);
			roads.erase(roads.begin() + to_remove);
			remove_adjacency_links(start_id, adjs);
			start_id = end_id;
		}
		p.vertices.push_back(all_nodes[start_id]);
		if ((int) adjs[start_id].size() == 0) {
			remove_from_heap(heap, start_id);
			remove_adjacency_links(start_id, adjs);
		}
		primitives.push_back(p);
	}
}

void PCG::Towns::City_gen::Road_network::extract_primitive(
		std::vector<Primitive>& primitives, std::vector<Road_node>& heap,
		Road_graph& adjs, std::vector<Road>& roads)
{
	std::vector<Road_node> visited;
	std::vector<Road_node> sequence;
	Road_node start = heap[0];
	Road_node v1 = get_clockwise_most(start, adjs[start.id]);
	sequence.push_back(start);
	Road_node vprev = start;
	Road_node vcurr = v1;
	while (vcurr.id != -1 && vcurr.id != start.id && !contains(visited, vcurr)) {
		sequence.push_back(vcurr);
		visited.push_back(vcurr);
		Road_node vnext = get_anti_clockwise_most(vprev, vcurr, adjs[vcurr.id]);
		vprev = vcurr;
		vcurr = vnext;
	}
	if (vcurr.id == -1) {
		extract_filament(vprev.id, adjs[vprev.id][0], primitives, heap, adjs, roads);
	} else if (vcurr.id == start.id) {
		Primitive p = { sequence, 2 };
		primitives.push_back(p);
		int n = (int) sequence.size();
		for (int i = 0; i < n; ++i) {
			set_cycle_edge(roads, i, (i + 1) % n);
		}
		int to_remove = find_road_index(roads, start.id, v1.id);
		remove_edge(roads, adjs, to_remove);
		if ((int) adjs[start.id].size() == 1) {
			extract_filament(start.id, adjs[start.id][0],
							 primitives, heap, adjs, roads);
		}
		if ((int) adjs[v1.id].size() == 1) {
			extract_filament(v1.id, adjs[v1.id][0], primitives, heap, adjs, roads);
		}
	} else {
		while ((int) adjs[start.id].size() == 2) {
			if (adjs[start.id].at(0) != v1.id) {
				v1 = start;
				start = all_nodes[adjs[start.id][0]];
			} else {
				v1 = start;
				start = all_nodes[adjs[start.id][1]];
			}
		}
		extract_filament(start.id, v1.id, primitives, heap, adjs, roads);
	}
}

PCG::Towns::City_gen::Road_node
PCG::Towns::City_gen::Road_network::snap_to_close(glm::vec2 point, glm::vec2 start)
{
	std::vector<Road> snaps;
	std::vector<float> dists;
	for (Road r : all_roads) {
		float dist = point_to_line_dist(point, r.start.location, r.end.location);
		if (dist < snap_distance) {
			bool added = false;
			for (int i = 0; i < (int) snaps.size(); i++) {
				if (!added && dist < dists[i]) {
					dists.insert(dists.begin() + i, dist);
					snaps.insert(snaps.begin() + i, r);
					added = true;
				}
			}
			if (!added) {
				dists.push_back(dist);
				snaps.push_back(r);
			}
		}
	}
	if ((int) snaps.size() == 0) {
		return add_node(point);
	}
	glm::vec2 inter = get_closest_point_on_line(point, snaps[0].start.location,
												snaps[0].end.location);
	Road_node new_node = snap_to_existing(inter, start, snaps[0]);
	if (new_node.id == -1) {
		return add_node(inter);
	}
	return new_node;
}

PCG::Towns::City_gen::Road_node PCG::Towns::City_gen::Road_network::snap_to_existing(
		glm::vec2 inter, glm::vec2 start, const Road& r)
{
	float start_dist = distance(inter, r.start.location);
	if (start_dist < snap_distance) {
		if ((int) adjacency_list[r.start.id].size() < 4) {
			for (int i = 0; i < (int) adjacency_list[r.start.id].size(); ++i) {
				if (dot(normalize(start - r.start.location),
						normalize(float(adjacency_list[r.start.id][i])
								  - r.start.location)) > 7.5) {
					return { glm::vec2(), -1 };
				}
			}
			return r.start;
		}
	}
	float end_dist = distance(inter, r.end.location);
	if (end_dist < snap_distance) {
		if ((int) adjacency_list[r.end.id].size() < 4) {
			for (int i = 0; i < (int) adjacency_list[r.start.id].size(); i++) {
				if (dot(normalize(start - r.start.location),
						normalize(float(adjacency_list[r.start.id][i])
								  - r.start.location)) > 7.5) {
					return { glm::vec2(), -1 };
				}
			}
			return r.end;
		}
	}
	return { glm::vec2(), -1 };
}

void PCG::Towns::City_gen::Road_network::remove_adjacency_links(int id,
																Road_graph& adjs)
{
	for (int i : adjs[id]) {
		for (int j = 0; j < (int) adjs[i].size(); ++j) {
			if (id == adjs[i].at(j)) {
				std::vector<int> near = adjs[i];
				near.erase(near.begin() + j);
				adjs[i] = near;
			}
		}
	}
	adjs.erase(id);
}

PCG::Towns::City_gen::Road_node
PCG::Towns::City_gen::Road_network::get_clockwise_most(const Road_node& current,
													   const std::vector<int>& adj)
{
	glm::vec2 d_curr = glm::vec2(0, -1);
	Road_node vnext = all_nodes[adj[0]];
	glm::vec2 dnext = vnext.location - current.location;
	bool vcurr_is_convex = (int) round(dot_perp(dnext, d_curr)) <= 0;
	for (int i = 1; i < (int) adj.size(); ++i) {
		glm::vec2 dir_adj = all_nodes[adj[i]].location - current.location;
		if (vcurr_is_convex) {
			if (dot_perp(d_curr, dir_adj) < 0 || dot_perp(dnext, dir_adj) < 0) {
				vnext = all_nodes[adj[i]];
				dnext = dir_adj;
				vcurr_is_convex = (int) round(dot_perp(dnext, d_curr)) <= 0;
			}
		} else {
			if (dot_perp(d_curr, dir_adj) < 0 && dot_perp(dnext, dir_adj) < 0) {
				vnext = all_nodes[adj[i]];
				dnext = dir_adj;
				vcurr_is_convex = (int) round(dot_perp(dnext, d_curr)) <= 0;
			}
		}
	}
	return vnext;
}

PCG::Towns::City_gen::Road_node
PCG::Towns::City_gen::Road_network::get_anti_clockwise_most(const Road_node& vprev,
															const Road_node& current,
															const std::vector<int>& adj)
{
	if ((int) adj.size() == 1) {
		return { glm::vec2(0, 0), -1 };
	}
	glm::vec2 d_curr = current.location - vprev.location;
	Road_node vnext = all_nodes[adj[0]];
	if (adj[0] == vprev.id) {
		if ((int) adj.size() == 1) {
			return vprev;
		}
		vnext = all_nodes[adj[1]];
	}
	glm::vec2 dnext = vnext.location - current.location;
	bool vcurr_is_convex = (int) round(dot_perp(dnext, d_curr)) <= 0;
	for (int i = 1; i < (int) adj.size(); ++i) {
		if (adj[i] != vprev.id) {
			glm::vec2 dir_adj = all_nodes[adj[i]].location - current.location;
			if (vcurr_is_convex) {
				if (dot_perp(d_curr, dir_adj) > 0 && dot_perp(dnext, dir_adj) > 0) {
					vnext = all_nodes[adj[i]];
					dnext = dir_adj;
					vcurr_is_convex = (int) round(dot_perp(dnext, d_curr)) <= 0;
				}
			} else {
				if (dot_perp(d_curr, dir_adj) > 0 || dot_perp(dnext, dir_adj) > 0) {
					vnext = all_nodes[adj[i]];
					dnext = dir_adj;
					vcurr_is_convex = (int) round(dot_perp(dnext, d_curr)) <= 0;
				}
			}
		}
	}
	return vnext;
}

void PCG::Towns::City_gen::Road_network::set_cycle_edge(std::vector<Road>& roads,
														int start_id, int end_id)
{
	int to_set = find_road_index(roads, start_id, end_id);
	roads[to_set].is_cycle_edge = true;
}

void PCG::Towns::City_gen::Road_network::remove_edge(std::vector<Road>& roads,
													 Road_graph& adjs, int to_remove)
{
	Road r = roads[to_remove];
	for (int i = 0; i < (int) adjs[r.start.id].size(); ++i) {
		if (adjs[r.start.id][i] == r.end.id) {
			adjs[r.start.id].erase(adjs[r.start.id].begin() + i);
			break;
		}
	}
	for (int i = 0; i < (int) adjs[r.end.id].size(); ++i) {
		if (adjs[r.end.id][i] == r.start.id) {
			adjs[r.end.id].erase(adjs[r.end.id].begin() + i);
			break;
		}
	}
	roads.erase(roads.begin() + to_remove);
}

PCG::Towns::City_gen::Road_network::Road_network(unsigned int seed)
		: mt{ seed }
{
}

PCG::Towns::City_gen::Road::Road()
{
}

PCG::Towns::City_gen::Road::Road(Road_node s, Road_node e, int i)
		: start{ s }, end{ e }, id{ i }
{
}

PCG::Towns::City_gen::Road_node::Road_node()
{
}

PCG::Towns::City_gen::Road_node::Road_node(glm::vec2 loc, int i)
		: location{ loc }, id{ i }
{
}

bool PCG::Towns::City_gen::Road_node::operator<(const Road_node& rhs) const
{
	return id < rhs.id;
}

bool PCG::Towns::City_gen::Road_node::operator==(const PCG::Towns::City_gen::Road_node& rhs) const
{
	return id == rhs.id;
}

std::size_t PCG::Towns::City_gen::Road_node_hasher::operator()(
		const PCG::Towns::City_gen::Road_node& node) const
{
	size_t res = 17;
	res = res * 31 + std::hash<int>()(node.id);
	res = res * 31 + std::hash<float>()(node.location.x);
	res = res * 31 + std::hash<float>()(node.location.y);
	return res;
}

const std::vector<PCG::Towns::City_gen::Lot>&
PCG::Towns::City_gen::Section_divider::get_lots() const
{
	return lots;
}

void PCG::Towns::City_gen::Section_divider::divide_all_lots(
		const std::vector<Section>& lot_outlines)
{
	for (Section s : lot_outlines) {
		s.area = get_section_size(s);
		Lot l = { s, std::vector<Section>(), lot_id++ };
		lots.push_back(l);
		divide_lot(l);
	}
}

void PCG::Towns::City_gen::Section_divider::add_building_to_lot(const Lot& l)
{
	lots[l.id] = l;
}

float PCG::Towns::City_gen::Section_divider::get_section_size(const Section& s)
{
	float size = 0.0f;
	for (int i = 0; i < (int) s.lines.size(); ++i) {
		Line l = s.lines[i];
		float change = (l.end.x + l.start.x) * (l.end.y - l.start.y);
		if (!std::isnan(change)) {
			size = size + change;
		}
	}
	size /= 2.0f;
	return size;
}

void PCG::Towns::City_gen::Section_divider::divide_lot(const Lot& l)
{
	lots[l.id] = rec_divide_section(l, l.bounding_box);
	lots[l.id] = remove_unusable_sections(lots[l.id]);
}

PCG::Towns::City_gen::Lot
PCG::Towns::City_gen::Section_divider::rec_divide_section(Lot lot, const Section& s)
{
	std::vector<Section> secs = split_section(s);
	for (Section l : secs) {
		if (l.area <= goal_area) {
			lot.sections.push_back(l);
		} else {
			if (!std::isnan(l.area)) {
				lot = rec_divide_section(lot, l);
			}
		}
	}
	return lot;
}

PCG::Towns::City_gen::Lot
PCG::Towns::City_gen::Section_divider::remove_unusable_sections(Lot l)
{
	std::vector<Section> to_keep;
	for (Section s : l.sections) {
		if (!std::isnan(s.area) && s.area > min_area) {
			if (has_street_access(s, l)) {
				to_keep.push_back(s);
			}
		}
	}
	l.sections = to_keep;
	return l;
}

std::vector<PCG::Towns::City_gen::Section>
PCG::Towns::City_gen::Section_divider::split_section(const Section& s)
{
	Line l = find_longest_edge(s);
	glm::vec2 perp_bi = get_bisector(l);
	std::uniform_real_distribution<float> ds_0_1{ 0.f, 1.f };
	float random = ds_0_1(mt);
	float dev_scale = (-1 + random * 2) / 6.0f;
	glm::vec2 centre_point = centre_point_of_line(l);
	glm::vec2 dir = l.end - l.start;
	dir = normalize(dir) * dev_scale;
	centre_point = centre_point + dir;
	std::vector<Line> intersectors = lines_intersecting_with_section(s, perp_bi,
																	 centre_point, l);
	if (intersectors.empty()) {
		return std::vector<Section>{};
	}
	Line to_cut;
	float close = 0.0f;
	for (int i = 0; i < (int) intersectors.size(); ++i) {
		float dist = abs(get_intersection(intersectors[i], perp_bi, centre_point)->y
						 - centre_point.y);
		if (i == 0 || dist < close) {
			close = dist;
			to_cut = intersectors[i];
		}
	}
	Section a, b;
	Line bi = { *get_intersection(l, perp_bi, centre_point),
				*get_intersection(to_cut, perp_bi, centre_point), 0 };
	Line anti_bi = { bi.end, bi.start, bi.id };
	Line anti_long = { l.end, l.start, l.id };
	Line anti_cut = { to_cut.end, to_cut.start, to_cut.id };
	a = get_inner_section(s, bi, to_cut, l);
	b = get_inner_section(s, anti_bi, anti_long, anti_cut);
	std::vector<Section> x;
	x.push_back(a);
	x.push_back(b);
	return x;
}

bool PCG::Towns::City_gen::Section_divider::has_street_access(const Section& s,
															  const Lot& l)
{
	for (Line edge : l.bounding_box.lines) {
		for (Line section_edge : s.lines) {
			if (share_side(edge, section_edge)) {
				return true;
			}
		}
	}
	return false;
}

PCG::Towns::City_gen::Line
PCG::Towns::City_gen::Section_divider::find_longest_edge(const Section& s)
{
	float max_length = 0.0f;
	Line long_line;
	for (int i = 0; i < (int) s.lines.size(); ++i) {
		int new_length = distance(s.lines[i].start, s.lines[i].end);
		if (new_length > max_length) {
			max_length = new_length;
			long_line = s.lines[i];
		}
	}
	return long_line;
}

PCG::Towns::City_gen::Section PCG::Towns::City_gen::Section_divider::get_inner_section(
		const Section& s, const Line& bi, const Line& to_cut, const Line& long_line)
{
	Section a;
	a.lines.push_back(bi);
	int line_id = (to_cut.id + 1) % s.lines.size();
	int new_id = 1;
	glm::vec2 start = *get_intersection(bi, to_cut);
	glm::vec2 end = get_shared_point(to_cut, s.lines[line_id]);
	Line start_half = { start, end, new_id++ };
	a.lines.push_back(start_half);
	while (line_id != long_line.id) {
		Line to_add = s.lines[line_id];
		to_add.id = new_id;
		a.lines.push_back(to_add);
		line_id++;
		if (line_id >= (int) s.lines.size()) {
			line_id = 0;
		}
		new_id++;
	}
	glm::vec2 end2 = *get_intersection(bi, long_line);
	line_id--;
	if (line_id < 0) {
		line_id = (int) s.lines.size() - 1;
	}
	glm::vec2 start2 = get_shared_point(long_line, s.lines[line_id]);
	Line end_half = { start2, end2, new_id };
	a.lines.push_back(end_half);
	a.area = get_section_size(a);
	return a;
}

glm::vec2 PCG::Towns::City_gen::Section_divider::get_shared_point(const Line& a,
																  const Line& b)
{
	std::unique_ptr<glm::vec2> inter;
	inter = get_intersection(a, b);
	if ((float_equal(a.start.x, b.start.x) && float_equal(a.start.y, b.start.y)) ||
		(float_equal(a.start.x, b.end.x) && float_equal(a.start.y, b.end.y))) {
		return a.start;
	} else if ((float_equal(a.end.x, b.start.x) && float_equal(a.end.y, b.start.y)) ||
			   (float_equal(a.end.x, b.end.x) && float_equal(a.end.y, b.end.y))) {
		return a.end;
	}
	return *inter;
}

glm::vec2 PCG::Towns::City_gen::Spline::calculate_point(glm::vec2 p0, glm::vec2 p1,
														glm::vec2 p2, glm::vec2 p3, float t)
{
	float result_y = ((2 * p1.y) + (-p0.y + p2.y) * t
					  + (2 * p0.y - 5 * p1.y + 4 * p2.y - p3.y) * t * t
					  + (-p0.y + 3 * p1.y - 3 * p2.y + p3.y) * t * t * t) * 0.5f;
	float result_x = ((2 * p1.x) + (-p0.x + p2.x) * t
					  + (2 * p0.x - 5 * p1.x + 4 * p2.x - p3.x) * t * t
					  + (-p0.x + 3 * p1.x - 3 * p2.x + p3.x) * t * t * t) * 0.5f;
	glm::vec2 result = { result_x, result_y };
	return result;
}

float PCG::Towns::City_gen::Spline::calculate_y_value(
		const std::vector<glm::vec2>& points, float x_dist)
{
	float x = points[0].x;
	size_t i = 1;
	while (x < x_dist && i <= points.size()) {
		x = points[i].x;
		++i;
	}
	glm::vec2 a = points[i - 3];
	glm::vec2 b = points[i - 2];
	glm::vec2 c = points[i - 1];
	glm::vec2 d = points[i];
	float t = c.x - b.x;
	t = (x_dist - b.x) / t;
	glm::vec2 result = calculate_point(a, b, c, d, t);
	return std::max(result.y, 0.1f);
}

void PCG::Towns::City_gen::Building::generate_road(std::vector<GLfloat>& data,
												   std::vector<Draw_command>& commands,
												   glm::vec2 a, glm::vec2 b, float width)
{
	unsigned seed = unsigned(std::abs(a.x + b.x * a.y * b.y) * 1000.f);
	std::mt19937 mt{ seed };
	std::uniform_real_distribution<float> ds_004_006{ 0.04f, 0.06f };
	float ground = ds_004_006(mt);
	glm::vec2 dir = (b - a);
	glm::vec3 dir3D = glm::vec3(dir.x, 0, dir.y);
	dir3D = normalize(dir3D);
	glm::vec3 right3D = cross(dir3D, glm::vec3(0, 1, 0));
	right3D = normalize(right3D);
	glm::vec2 right = glm::vec2(right3D.x, right3D.z);
	float leng = abs(length(dir));
	leng /= tex_wall_width;
	glm::vec2 pointA = a;
	glm::vec2 pointB = b;
	b -= normalize(dir) * (width);
	a += normalize(dir) * (width);
	std::vector<glm::vec2> end_a_tri;
	end_a_tri.push_back(a + (right * width));
	end_a_tri.push_back(a - (right * width));
	end_a_tri.push_back(pointA);
	std::vector<glm::vec2> bounding_box = Generator::get_bounding_box(end_a_tri);
	float bb_width = abs(bounding_box[0].x - bounding_box[1].x);
	float bb_height = abs(bounding_box[0].y - bounding_box[1].y);
	GLint index = data.size() / 5;
	for (int i = 0; i < 3; i++) {
		glm::vec2 v = end_a_tri[i];
		data.push_back(v.x);
		data.push_back(ground);
		data.push_back(v.y);
		data.push_back((v.x - bounding_box[0].x) / bb_width);
		data.push_back((v.y - bounding_box[0].y) / bb_height);
	}
	std::vector<glm::vec2> end_b_tri;
	end_b_tri.push_back(b - (right * width));
	end_b_tri.push_back(b + (right * width));
	end_b_tri.push_back(pointB);
	bounding_box = Generator::get_bounding_box(end_b_tri);
	bb_width = abs(bounding_box[0].x - bounding_box[1].x);
	bb_height = abs(bounding_box[0].y - bounding_box[1].y);
	for (int i = 0; i < 3; i++) {
		glm::vec2 v = end_b_tri[i];
		data.push_back(v.x);
		data.push_back(ground);
		data.push_back(v.y);
		data.push_back((v.x - bounding_box[0].x) / bb_width);
		data.push_back((v.y - bounding_box[0].y) / bb_height);
	}
	std::vector<glm::vec2> road_points;
	road_points.push_back(a + (right * width));
	road_points.push_back(b + (right * width));
	road_points.push_back(b - (right * width));
	road_points.push_back(a - (right * width));
	data.push_back(road_points[0].x);
	data.push_back(ground);
	data.push_back(road_points[0].y);
	data.push_back(0);
	data.push_back(0);
	data.push_back(road_points[1].x);
	data.push_back(ground);
	data.push_back(road_points[1].y);
	data.push_back(0);
	data.push_back(leng);
	data.push_back(road_points[2].x);
	data.push_back(ground);
	data.push_back(road_points[2].y);
	data.push_back(1);
	data.push_back(leng);
	data.push_back(road_points[3].x);
	data.push_back(ground);
	data.push_back(road_points[3].y);
	data.push_back(1);
	data.push_back(0);
	Draw_command command;
	command.type = Command_type::draw;
	command.mode = GL_TRIANGLES;
	command.first = index;
	command.count = 6;
	commands.push_back(command);
	command.type = Command_type::draw;
	command.mode = GL_TRIANGLE_FAN;
	command.first = index + 6;
	command.count = 4;
	commands.push_back(command);
}

void PCG::Towns::City_gen::Building::draw_ground()
{
	glBindTexture(GL_TEXTURE_2D, conc);
	glBindBuffer(GL_ARRAY_BUFFER, floor_buffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), nullptr);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
						  (const GLvoid*) (2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void PCG::Towns::City_gen::Building::init()
{
	glGenTextures(total_wall_textures, tex_wall[0]);
	glGenTextures(total_wall_textures, tex_wall[1]);
	glGenTextures(total_window_textures, tex_window);
	glGenTextures(1, &tex_door);
	glGenTextures(1, &grass);
	glGenTextures(1, &conc);
	load_texture(tex_wall[0][0], "res/assets/highrise001.png");
	load_texture(tex_wall[0][1], "res/assets/highrise002.png");
	load_texture(tex_wall[0][2], "res/assets/highrise003.png");
	load_texture(tex_wall[0][3], "res/assets/highrise004.png");
	load_texture(tex_wall[1][0], "res/assets/brick001.png");
	load_texture(tex_wall[1][1], "res/assets/brick002.png");
	load_texture(tex_wall[1][2], "res/assets/brick003.png");
	load_texture(tex_wall[1][3], "res/assets/brick004.png");
	load_texture(tex_window[0], "res/assets/glass01.png");
	load_texture(tex_window[1], "res/assets/window02.png");
	load_texture(tex_window[2], "res/assets/window03.png");
	load_texture(tex_window[3], "res/assets/window04.png");
	load_texture(tex_window[4], "res/assets/window05.png");
	load_texture(tex_door, "res/assets/wooddoor02.png");
	load_texture(grass, "res/assets/grass001.png");
	load_texture(conc, "res/assets/concrete.png");
	create_floor();
}

std::vector<PCG::Towns::City_gen::Draw_command>
PCG::Towns::City_gen::Building::generate_buildings_from_sections(
		std::vector<GLfloat>& data, const std::vector<Section>& sections, float range, glm::vec2 min)
{
	std::vector<Building_lod> buildings;
	std::vector<Building_params> params
			= Generator::sections_to_params(sections, heightmap_points, range, min);
	for (size_t i = 0; i < params.size(); ++i) {
		Building_lod building;
		generate_building(data, params[i], building);
		buildings.push_back(building);
	}
	std::vector<Draw_command> new_list;
	for (Building_lod b : buildings) {
		new_list.insert(new_list.end(), b.low.begin(), b.low.end());
	}
	return new_list;
}

void PCG::Towns::City_gen::Building::delete_opengl_stuff()
{
	glDeleteTextures(total_wall_textures, tex_wall[0]);
	glDeleteTextures(total_wall_textures, tex_wall[1]);
	glDeleteTextures(total_window_textures, tex_window);
	glDeleteTextures(1, &tex_door);
	glDeleteTextures(1, &grass);
	glDeleteTextures(1, &conc);
	glDeleteBuffers(1, &floor_buffer);
}

void PCG::Towns::City_gen::Building::create_floor()
{
	static const GLfloat size = 100.f;
	static const GLfloat zero = 0.f;
	static const GLfloat floor = -0.01f;
	static const GLfloat data[] = {
			///x     y       z     u     v
			-size, floor, -size, zero, zero,
			-size, floor, +size, zero, size,
			+size, floor, +size, size, size,
			+size, floor, -size, size, zero
	};
	glCreateBuffers(1, &floor_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, floor_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
}

void PCG::Towns::City_gen::Building::load_texture(GLuint texture,
												  const std::string& filename)
{
	Image tex(filename);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexStorage2D(GL_TEXTURE_2D, tex.get_num_mipmaps(), GL_RGB8, tex.w, tex.h);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, tex.w, tex.h, tex.gl_format(),
					GL_UNSIGNED_BYTE, tex.data_pointer());
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void PCG::Towns::City_gen::Building::generate_building(std::vector<GLfloat>& data,
													   Building_params& parameters,
													   Building_lod& result)
{
	std::vector<glm::vec2> floor_plan = parameters.bounding_area;
	glm::vec2 center = Generator::center_point(floor_plan);
	float min_dist = 100000.0f;
	for (glm::vec2 v : floor_plan) {
		min_dist = std::min(min_dist, (float) hypot(v.x - center.x, v.y - center.y));
	}
	min_dist *= 0.8f;
	std::mt19937 mt{ parameters.seed };
	std::uniform_int_distribution<int> ds_1_100{ 1, 100 };
	int chance = ds_1_100(mt);
	if (chance < 10) {
		result.low = generate_park(data, floor_plan);
		return;
	} else if (chance < 40) {
		result.low = generate_residential_building(data, floor_plan, parameters.height);
		return;
	} else if (chance < 90) {
		if (chance < 50) {
			floor_plan = Generator::shrink_points(floor_plan);
		} else if (chance < 60) {
			if (floor_plan.size() == 4)
				floor_plan = Generator::cut_edges(floor_plan);
		}
	} else if (chance < 100) {
		std::uniform_int_distribution<int> ds_4_7{ 4, 7 };
		if (chance < 95) {
			floor_plan = Generator::generate_floor_plan(center, min_dist, ds_4_7(mt));
		} else if (chance < 100) {
			floor_plan = Generator::generate_floor_plan(center, min_dist * 0.6f,
														ds_4_7(mt));
			floor_plan = Generator::combine_plans(
					floor_plan, Generator::generate_floor_plan(center, min_dist * 0.6f, ds_4_7(mt)));
		} else {
			result.low = generate_modern_building(data, center, min_dist);
			return;
		}
	}
	cur_tex_wall = static_cast<int>(parameters.b_type);
	std::uniform_int_distribution<int> ds_wall{ 0, total_wall_textures - 1 };
	cur_tex_wall_num = ds_wall(mt);
	if (cur_tex_wall != static_cast<int>(Building_type::skyscraper)) {
		std::uniform_int_distribution<int> ds_win{ 0, total_window_textures - 1 };
		cur_tex_win_num = ds_win(mt);
	}
	result.low = generate_from_string(data, floor_plan,
									  Generator::generate_random_building_string(parameters.height));
}

std::vector<PCG::Towns::City_gen::Draw_command>
PCG::Towns::City_gen::Building::generate_park(std::vector<GLfloat>& data,
											  const std::vector<glm::vec2>& floor)
{
	std::vector<Draw_command> command_list;
	size_t n = floor.size();
	std::vector<glm::vec2> bounding_box = Generator::get_bounding_box(floor);
	float bb_width = abs(bounding_box[0].x - bounding_box[1].x) * 0.25f;
	float bb_height = abs(bounding_box[0].y - bounding_box[1].y) * 0.25f;
	Draw_command command;
	command.type = Command_type::set_texture;
	command.obj_id = grass;
	command_list.push_back(command);
	GLint index = (GLint) (data.size() / 5);
	glm::vec2 mid = Generator::center_point(floor);
	data.push_back(mid.x);
	data.push_back(foundation_size + 0.001f);
	data.push_back(mid.y);
	data.push_back((mid.x - bounding_box[0].x) / bb_width);
	data.push_back((mid.y - bounding_box[0].y) / bb_height);
	for (int i = int(n) - 1; i >= 0; --i) {
		glm::vec2 v = floor[i];
		data.push_back(v.x);
		data.push_back(foundation_size + 0.001f);
		data.push_back(v.y);
		data.push_back((v.x - bounding_box[0].x) / bb_width);
		data.push_back((v.y - bounding_box[0].y) / bb_height);
	}
	glm::vec2 v = floor[n - 1];
	data.push_back(v.x);
	data.push_back(foundation_size + 0.001f);
	data.push_back(v.y);
	data.push_back((v.x - bounding_box[0].x) / bb_width);
	data.push_back((v.y - bounding_box[0].y) / bb_height);
	command.type = Command_type::draw;
	command.mode = GL_TRIANGLE_FAN;
	command.first = index;
	command.count = (GLsizei) (n + 2);
	command_list.push_back(command);
	command.type = Command_type::set_texture;
	command.obj_id = tex_wall[1][0];
	command_list.push_back(command);
	for (size_t i = 0; i < n; ++i) {
		glm::vec2 dir = (floor[(i + 1) % n] - floor[i]);
		if (size_t(i) == n % 2) {
			generate_park_wall(data, command_list, floor[i], floor[i] + (dir * 0.4f), mid);
			generate_park_wall(data, command_list, floor[(i + 1) % n] - (dir * 0.4f),
							   floor[(i + 1) % n], mid);
		} else {
			generate_park_wall(data, command_list, floor[i], floor[(i + 1) % n], mid);
		}
	}
	return command_list;
}

std::vector<PCG::Towns::City_gen::Draw_command>
PCG::Towns::City_gen::Building::generate_residential_building(
		std::vector<GLfloat>& data, const std::vector<glm::vec2>& points, int height)
{
	std::vector<Draw_command> command_list;
	std::vector<std::vector<glm::vec2>> tiers = Generator::subdivide(points);
	std::vector<glm::vec2> temp = tiers[0];
	tiers = Generator::subdivide(tiers[1]);
	tiers.push_back(temp);
	int n_h = 5 + height - 1;
	if (n_h < 0)
		n_h = 0;
	int h_min = std::min(5, n_h);
	int h_max = std::max(5, n_h);
	std::uniform_int_distribution<int> ds_5_h{ h_min, h_max };
	int n = h_min == h_max ? h_min : ds_5_h(mt);
	float cur_elev = foundation_size;
	for (int i = 0; i < n; ++i) {
		if (cur_tex_wall != static_cast<int>(Building_type::skyscraper) && i > 0)
			render_windows(data, command_list, tiers[2], cur_elev);
		cur_elev = extend_building(data, command_list, tiers[2], cur_elev);
	}
	std::uniform_int_distribution<int> ds_3_4_n{ n * 3 / 4, n };
	n = ds_3_4_n(mt);
	cur_elev = foundation_size;
	for (int i = 0; i < n; ++i) {
		if (cur_tex_wall != static_cast<int>(Building_type::skyscraper))
			render_windows(data, command_list, tiers[0], cur_elev);
		cur_elev = extend_building(data, command_list, tiers[0], cur_elev);
	}
	std::uniform_int_distribution<int> ds_3_4_n_again{ n * 3 / 4, n };
	n = ds_3_4_n_again(mt);
	cur_elev = foundation_size;
	for (int i = 0; i < n; i++) {
		if (cur_tex_wall != static_cast<int>(Building_type::skyscraper) && i > 0)
			render_windows(data, command_list, tiers[1], cur_elev);
		cur_elev = extend_building(data, command_list, tiers[1], cur_elev);
	}
	return command_list;
}

std::vector<PCG::Towns::City_gen::Draw_command>
PCG::Towns::City_gen::Building::generate_modern_building(std::vector<GLfloat>& data,
														 glm::vec2 mid, float min_dist)
{
	std::vector<Draw_command> command_list;
	std::vector<std::vector<glm::vec2>> levels;
	std::uniform_int_distribution<int> ds_4_7{ 4, 7 };
	levels.push_back(Generator::generate_floor_plan(mid, min_dist, ds_4_7(mt)));
	std::uniform_int_distribution<int> ds_2_5{ 2, 5 };
	int r_2_5 = ds_2_5(mt);
	std::uniform_real_distribution<float> ds_1_1{ -1, 1 };
	for (int i = 0; i < r_2_5; ++i) {
		float xmul = ds_1_1(mt);
		float ymul = ds_1_1(mt);
		glm::vec2 offset = glm::vec2(xmul * (min_dist / 2.0f), (min_dist / 2.0f) * ymul);
		std::vector<glm::vec2> to_add = Generator::combine_plans(
				levels[i], Generator::generate_floor_plan(mid + offset, min_dist, ds_4_7(mt)));
		if (to_add.size() == levels[i].size()) {
			Generator::combine_plans(
					levels[i], Generator::generate_floor_plan(mid + offset, min_dist, ds_4_7(mt)));
		}
		levels.push_back(to_add);
	}
	float elevation = foundation_size;
	std::uniform_int_distribution<int> ds_0_2{ 0, 2 };
	for (int i = int(levels.size()) - 1; i >= 0;) {
		if (cur_tex_wall != static_cast<int>(Building_type::skyscraper))
			render_windows(data, command_list, levels[i], elevation);
		elevation = extend_building(data, command_list, levels[i], elevation);
		if (ds_0_2(mt) <= 1) {
			--i;
		}
	}
	return command_list;
}

std::vector<PCG::Towns::City_gen::Draw_command>
PCG::Towns::City_gen::Building::generate_from_string(std::vector<GLfloat>& data,
													 std::vector<glm::vec2> floor,
													 const std::string& input)
{
	std::vector<Draw_command> command_list;
	float height = foundation_size;
	for (size_t i = 0; i < input.length(); ++i) {
		switch (input[i]) {
			case 'S':
				floor = Generator::shrink_points(floor);
				break;
			case 'E':
				if (cur_tex_wall != static_cast<int>(Building_type::skyscraper))
					render_windows(data, command_list, floor, height);
				height = extend_building(data, command_list, floor, height);
				break;
			case 'R': {
				std::uniform_int_distribution<int> ds_0_2{ 0, 2 };
				if (ds_0_2(mt) == 0) {
					generate_point_roof(data, command_list, floor, height);
				} else {
					generate_flat_point_roof(data, command_list, floor, height);
				}
			}
				break;
			case 'D':
				if (floor.size() == 4) {
					std::uniform_int_distribution<int> ds_0_1{ 0, 1 };
					floor = Generator::subdivide(floor)[ds_0_1(mt)];
				}
				break;
			case '$': {
				glm::vec3 normal = glm::vec3(floor[1].x, 0, floor[1].y)
								   - glm::vec3(floor[0].x, 0, floor[0].y);
				normal = cross(normal, glm::vec3(0, 1, 0));
				generate_door(data, command_list, floor[0], floor[1], foundation_size, -normal);
				height = extend_building(data, command_list, floor, height);
			}
				break;
			default:
				break;
		}
	}
	return command_list;
}

void PCG::Towns::City_gen::Building::generate_park_wall(
		std::vector<GLfloat>& data, std::vector<Draw_command>& command_list, glm::vec2 a,
		glm::vec2 b, glm::vec2 mid)
{
	float height = 0.08f;
	float wall_width = 0.05f;
	glm::vec2 inner_a = (mid - a) * wall_width + a;
	glm::vec2 inner_b = (mid - b) * wall_width + b;
	float len = abs(length(a - b));
	len /= tex_wall_width;
	float y_len = 0.5f;
	static const GLfloat gen_data[] = {
			///x		y			z	u		v
			b.x, foundation_size, b.y, len, y_len,
			a.x, foundation_size, a.y, 0, y_len,
			a.x, height, a.y, 0, 0,
			b.x, height, b.y, len, 0,
			inner_a.x, foundation_size, inner_a.y, len, y_len,
			inner_b.x, foundation_size, inner_b.y, 0, y_len,
			inner_b.x, height, inner_b.y, 0, 0,
			inner_a.x, height, inner_a.y, len, 0,
			inner_a.x, height, inner_a.y, len, 0,
			inner_b.x, height, inner_b.y, len, 0,
			b.x, height, b.y, len, 0,
			a.x, height, a.y, len, 0
	};
	Draw_command command;
	command.type = Command_type::draw;
	command.mode = GL_TRIANGLE_FAN;
	command.first = data.size() / 5;
	command.count = 4;
	command_list.push_back(command);
	command.type = Command_type::draw;
	command.mode = GL_TRIANGLE_FAN;
	command.first = 4 + data.size() / 5;
	command.count = 4;
	command_list.push_back(command);
	command.type = Command_type::draw;
	command.mode = GL_TRIANGLE_FAN;
	command.first = 8 + data.size() / 5;
	command.count = 4;
	command_list.push_back(command);
	data.insert(data.end(), std::begin(gen_data), std::end(gen_data));
}

void PCG::Towns::City_gen::Building::render_windows(std::vector<GLfloat>& data,
													std::vector<Draw_command>& command_list,
													const std::vector<glm::vec2>& floor,
													float elevation)
{
	glm::vec2 mid = Generator::center_point(floor);
	if (abs(length(floor[0] - mid)) < extrude_threshold) {
		return;
	}
	int n = floor.size();
	Draw_command command;
	command.type = Command_type::set_texture;
	command.obj_id = tex_window[cur_tex_win_num];
	command_list.push_back(command);
	for (int i = 0; i < n; ++i) {
		glm::vec2 dir = (floor[(i + 1) % n] - floor[i]);
		glm::vec3 normal = glm::vec3(dir.x, 0, dir.y);
		normal = cross(glm::vec3(0, 1, 0), normal);
		generate_windows(data, command_list, floor[i], floor[(i + 1) % n], elevation,
						 normal);
	}
}

float PCG::Towns::City_gen::Building::extend_building(std::vector<GLfloat>& data,
													  std::vector<Draw_command>& command_list,
													  const std::vector<glm::vec2>& floor,
													  float elevation)
{
	int n = floor.size();
	glm::vec2 mid = Generator::center_point(floor);
	if (abs(length(floor[0] - mid)) < extrude_threshold && elevation > 0.0f) {
		return elevation;
	}
	std::vector<glm::vec3> bot;
	std::vector<glm::vec3> top;
	float height = block_size;
	height += elevation;
	for (glm::vec2 v2 : floor) {
		bot.push_back(glm::vec3(v2.x, elevation, v2.y));
		top.push_back(glm::vec3(v2.x, height, v2.y));
	}
	Draw_command command;
	command.type = Command_type::set_texture;
	command.obj_id = tex_wall[cur_tex_wall][cur_tex_wall_num];
	command_list.push_back(command);
	GLint index = data.size() / 5;
	for (int i = 0; i < n; ++i) {
		glm::vec3 topl = top[i];
		glm::vec3 topr = top[(i + 1) % n];
		glm::vec3 botr = bot[(i + 1) % n];
		glm::vec3 botl = bot[i];
		float len = abs(length(botl - botr));
		len /= tex_wall_width;
		data.push_back(topr.x);
		data.push_back(topr.y);
		data.push_back(topr.z);
		data.push_back(len);
		data.push_back(0);
		data.push_back(botr.x);
		data.push_back(botr.y);
		data.push_back(botr.z);
		data.push_back(len);
		data.push_back(1);
		data.push_back(botl.x);
		data.push_back(botl.y);
		data.push_back(botl.z);
		data.push_back(0);
		data.push_back(1);
		data.push_back(topl.x);
		data.push_back(topl.y);
		data.push_back(topl.z);
		data.push_back(0);
		data.push_back(0);
	}
	glm::vec3 mid3d = glm::vec3(mid.x, height, mid.y);
	for (int i = 0; i < n; i++) {
		glm::vec3 p1 = top[i];
		glm::vec3 p2 = top[(i + 1) % n];
		data.push_back(mid3d.x);
		data.push_back(mid3d.y);
		data.push_back(mid3d.z);
		data.push_back(0);
		data.push_back(0);
		data.push_back(p2.x);
		data.push_back(p2.y);
		data.push_back(p2.z);
		data.push_back(0);
		data.push_back(0);
		data.push_back(p1.x);
		data.push_back(p1.y);
		data.push_back(p1.z);
		data.push_back(0);
		data.push_back(0);
	}
	for (int i = 0; i < n; ++i) {
		command.type = Command_type::draw;
		command.mode = GL_TRIANGLE_FAN;
		command.first = i * 4 + index;
		command.count = 4;
		command_list.push_back(command);
	}
	for (int i = 0; i < n; ++i) {
		command.type = Command_type::draw;
		command.mode = GL_TRIANGLES;
		command.first = n * 4 + i * 3 + index;
		command.count = 3;
		command_list.push_back(command);
	}
	return height;
}

void PCG::Towns::City_gen::Building::generate_point_roof(std::vector<GLfloat>& data,
														 std::vector<Draw_command>& command_list,
														 const std::vector<glm::vec2>& points,
														 float elevation)
{
	std::uniform_real_distribution<float> ds_01_04{ 0.1f, 0.4f };
	float height = ds_01_04(mt);
	glm::vec2 mid = Generator::center_point(points);
	glm::vec3 peak = glm::vec3(mid.x, elevation + height, mid.y);
	int n = points.size();
	GLint index = data.size() / 5;
	for (int i = 0; i < n; ++i) {
		data.push_back(points[(i + 1) % n].x);
		data.push_back(elevation);
		data.push_back(points[(i + 1) % n].y);
		data.push_back(0.0f);
		data.push_back(0.0f);
		data.push_back(points[i].x);
		data.push_back(elevation);
		data.push_back(points[i].y);
		data.push_back(0.0f);
		data.push_back(0.0f);
		data.push_back(peak.x);
		data.push_back(peak.y);
		data.push_back(peak.z);
		data.push_back(0.0f);
		data.push_back(0.0f);
	}
	data.push_back(peak.x);
	data.push_back(peak.y);
	data.push_back(peak.z);
	data.push_back(0.0f);
	data.push_back(0.0f);
	data.push_back(peak.x);
	data.push_back(peak.y + 0.3f);
	data.push_back(peak.z);
	data.push_back(0.0f);
	data.push_back(0.0f);
	Draw_command command;
	command.type = Command_type::draw;
	command.mode = GL_TRIANGLES;
	command.first = index;
	command.count = n * 3;
	command_list.push_back(command);
	command.type = Command_type::draw;
	command.mode = GL_LINES;
	command.first = n * 3 + index;
	command.count = 2;
	command_list.push_back(command);
}

void PCG::Towns::City_gen::Building::generate_flat_point_roof(std::vector<GLfloat>& data,
															  std::vector<Draw_command>& command_list,
															  const std::vector<glm::vec2>& points,
															  float elevation)
{
	int n = points.size();
	std::vector<glm::vec3> bot;
	std::vector<glm::vec2> top_plan = Generator::shrink_points(
			Generator::shrink_points(points));
	std::vector<glm::vec3> top;
	glm::vec2 mid = Generator::center_point(points);
	float height = abs(length(points[0] - mid)) / 4.0f;
	height += elevation;
	for (int i = 0; i < n; i++) {
		bot.push_back(glm::vec3(points[i].x, elevation, points[i].y));
		top.push_back(glm::vec3(top_plan[i].x, height, top_plan[i].y));
	}
	GLint index = data.size() / 5;
	for (int i = 0; i < n; ++i) {
		glm::vec3 topl = top[i];
		glm::vec3 topr = top[(i + 1) % n];
		glm::vec3 botr = bot[(i + 1) % n];
		glm::vec3 botl = bot[i];
		data.push_back(topr.x);
		data.push_back(topr.y);
		data.push_back(topr.z);
		data.push_back(1.0f);
		data.push_back(0.0f);
		data.push_back(botr.x);
		data.push_back(botr.y);
		data.push_back(botr.z);
		data.push_back(1.0f);
		data.push_back(1.0f);
		data.push_back(botl.x);
		data.push_back(botl.y);
		data.push_back(botl.z);
		data.push_back(0.0f);
		data.push_back(1.0f);
		data.push_back(topl.x);
		data.push_back(topl.y);
		data.push_back(topl.z);
		data.push_back(0.0f);
		data.push_back(0.0f);
	}
	glm::vec3 mid3d = glm::vec3(mid.x, height, mid.y);
	for (int i = 0; i < n; ++i) {
		glm::vec3 p1 = top[i];
		glm::vec3 p2 = top[(i + 1) % n];
		data.push_back(p2.x);
		data.push_back(p2.y);
		data.push_back(p2.z);
		data.push_back(0);
		data.push_back(0);
		data.push_back(p1.x);
		data.push_back(p1.y);
		data.push_back(p1.z);
		data.push_back(0);
		data.push_back(0);
		data.push_back(mid3d.x);
		data.push_back(mid3d.y);
		data.push_back(mid3d.z);
		data.push_back(0);
		data.push_back(0);
	}
	for (glm::vec3 floorPoint : bot) {
		data.push_back(floorPoint.x);
		data.push_back(floorPoint.y);
		data.push_back(floorPoint.z);
		data.push_back(0);
		data.push_back(0);
	}
	Draw_command command;
	for (int i = 0; i < n; ++i) {
		command.type = Command_type::draw;
		command.mode = GL_TRIANGLE_FAN;
		command.first = i * 4 + index;
		command.count = 4;
		command_list.push_back(command);
	}
	command.type = Command_type::draw;
	command.mode = GL_TRIANGLES;
	command.first = n * 4 + index;
	command.count = bot.size();
	command_list.push_back(command);
}

void PCG::Towns::City_gen::Building::generate_door(std::vector<GLfloat>& data,
												   std::vector<Draw_command>& command_list,
												   glm::vec2 a, glm::vec2 b,
												   float elevation,
												   glm::vec3 normal)
{
	Draw_command command;
	command.type = Command_type::set_texture;
	command.obj_id = tex_door;
	command_list.push_back(command);
	float bottom = elevation;
	float top = (block_size - (block_size * 0.1));
	float dist = (float) hypot(a.x - b.x, b.y - a.y);
	if (dist < 0.2) {
		return;
	}
	glm::vec2 direction = a - b;
	if (length(direction) <= 1) {
		direction *= (1 / length(direction));
	} else {
		direction = normalize(direction);
	}
	glm::vec2 center = b + direction * dist * 0.5f;
	glm::vec2 start = center + direction * 0.1f;
	glm::vec2 end = center - direction * 0.1f;
	glm::vec3 topL = glm::vec3(start.x, top, start.y) + normal * 0.001f;
	glm::vec3 topR = glm::vec3(end.x, top, end.y) + normal * 0.001f;
	glm::vec3 botR = glm::vec3(end.x, bottom, end.y) + normal * 0.001f;
	glm::vec3 botL = glm::vec3(start.x, bottom, start.y) + normal * 0.001f;
	static const GLfloat gen_data[] = {
			///x		y		z	u  v
			topR.x, topR.y, topR.z, 1, 0,
			botR.x, botR.y, botR.z, 1, 1,
			botL.x, botL.y, botL.z, 0, 1,
			topL.x, topL.y, topL.z, 0, 0
	};
	command.type = Command_type::draw;
	command.mode = GL_TRIANGLE_FAN;
	command.first = data.size() / 5;
	command.count = 4;
	command_list.push_back(command);
	data.insert(data.end(), std::begin(gen_data), std::end(gen_data));
}

void PCG::Towns::City_gen::Building::generate_windows(std::vector<GLfloat>& data,
													  std::vector<Draw_command>& command_list,
													  glm::vec2 a, glm::vec2 b,
													  float elevation,
													  glm::vec3 normal)
{
	float bottom = elevation + (block_size * 0.2f);
	float top = elevation + (block_size - (block_size * 0.2f));
	float dist = distance(a, b);
	float margin = dist;
	int i;
	for (i = 0; margin > window_width; ++i) {
		margin -= window_width;
	}
	margin /= 2.0f;
	if (i % 2 == 0) {
		margin += window_width / 2.0f;
		i--;
	}
	glm::vec2 direction = b - a;
	if (length(direction) <= 1) {
		direction *= (1 / length(direction));
	} else {
		direction = normalize(direction);
	}
	GLint base_index = data.size() / 5;
	for (int j = 0; j < i; j += 2) {
		glm::vec2 start = (direction * margin)
						  + (direction * +window_width * float(j)) + a;
		glm::vec2 end = (direction * +window_width) + start;
		glm::vec3 topL = glm::vec3(start.x, top, start.y) + normal * 0.001f;
		glm::vec3 topR = glm::vec3(end.x, top, end.y) + normal * 0.001f;
		glm::vec3 botR = glm::vec3(end.x, bottom, end.y) + normal * 0.001f;
		glm::vec3 botL = glm::vec3(start.x, bottom, start.y) + normal * 0.001f;
		data.push_back(topR.x);
		data.push_back(topR.y);
		data.push_back(topR.z);
		data.push_back(1);
		data.push_back(0);
		data.push_back(botR.x);
		data.push_back(botR.y);
		data.push_back(botR.z);
		data.push_back(1);
		data.push_back(1);
		data.push_back(botL.x);
		data.push_back(botL.y);
		data.push_back(botL.z);
		data.push_back(0);
		data.push_back(1);
		data.push_back(topL.x);
		data.push_back(topL.y);
		data.push_back(topL.z);
		data.push_back(0);
		data.push_back(0);
	}
	Draw_command command;
	for (int j = 0, index = 0; j < i; j += 2, index += 4) {
		command.type = Command_type::draw;
		command.mode = GL_TRIANGLE_FAN;
		command.first = index + base_index;
		command.count = 4;
		command_list.push_back(command);
	}
}

PCG::Towns::City_gen::Section
PCG::Towns::City_gen::Generator::points_to_sections(
		std::vector<glm::vec2> points)
{
	std::vector<Line> lines;
	int n = points.size();
	points = shrink_points(points);
	for (int i = 0; i < n; ++i) {
		Line l = { points[i] * +network_to_section_scale,
				   points[(i + 1) % n] * +network_to_section_scale, i };
		lines.push_back(l);
	}
	return { lines, 0.0f };
}

std::vector<glm::vec2> PCG::Towns::City_gen::Generator::get_bounding_box(
		const std::vector<glm::vec2>& floor)
{
	glm::vec2 min = glm::vec2(1000000, 1000000);
	glm::vec2 max = glm::vec2(-1000000, -100000);
	for (glm::vec2 v : floor) {
		if (v.x < min.x)
			min.x = v.x;
		if (v.y < min.y)
			min.y = v.y;
		if (v.x > max.x)
			max.x = v.x;
		if (v.y > max.y)
			max.y = v.y;
	}
	std::vector<glm::vec2> to_return;
	to_return.push_back(min);
	to_return.push_back(max);
	return to_return;
}

std::vector<glm::vec2> PCG::Towns::City_gen::Generator::section_to_points(
		const Section& sec)
{
	std::vector<glm::vec2> to_return;
	int n = sec.lines.size();
	for (int i = 0; i < n; ++i) {
		to_return.push_back(sec.lines[(i + 2) % n].start * +section_to_points_scale);
	}
	return to_return;
}

glm::vec2 PCG::Towns::City_gen::Generator::center_point(
		const std::vector<glm::vec2>& points)
{
	float x = 0;
	float y = 0;
	for (size_t i = 0; i < points.size(); ++i) {
		x += points[i].x;
		y += points[i].y;
	}
	y /= points.size();
	x /= points.size();
	return glm::vec2(x, y);
}

std::vector<PCG::Towns::City_gen::Building_params>
PCG::Towns::City_gen::Generator::sections_to_params(
		const std::vector<Section>& sections, const std::vector<glm::vec2>& heightmap,
		float range, glm::vec2 min)
{
	std::vector<Building_params> to_return;
	for (size_t i = 0; i < sections.size(); ++i) {
		Building_params p;
		p.bounding_area = section_to_points(sections[i]);
		p.seed = (unsigned) mt();
		p.b_type = static_cast<Building_type>(p.seed % 2);
		float xmap = distance(p.bounding_area[0], min);
		xmap = xmap / range;
		xmap *= 640;
		float ymap = Spline::calculate_y_value(heightmap, xmap);
		ymap = ((480.0f - ymap) / 480.0f);
		p.height = (int) (ymap * 7.0f + 1.0f);
		to_return.push_back(p);
	}
	return to_return;
}

std::vector<glm::vec2> PCG::Towns::City_gen::Generator::shrink_points(
		const std::vector<glm::vec2>& points)
{
	glm::vec2 mid = center_point(points);
	std::vector<glm::vec2> small_points;
	float dist = 0.15f;
	for (glm::vec2 v2 : points) {
		glm::vec2 diff = (mid - v2) * dist;
		small_points.push_back(v2 + diff);
	}
	return small_points;
}

std::vector<glm::vec2> PCG::Towns::City_gen::Generator::cut_edges(
		const std::vector<glm::vec2>& points)
{
	int n = points.size();
	if (n != 4)
		return points;
	std::vector<glm::vec2> result;
	for (int i = 0; i < n; ++i) {
		glm::vec2 diff = points[(i + 1) % n] - points[i];
		diff *= 0.2;
		result.push_back(points[i] + diff);
		result.push_back(points[(i + 1) % n] - diff);
	}
	return result;
}

std::vector<glm::vec2> PCG::Towns::City_gen::Generator::generate_floor_plan(
		glm::vec2 center, float radius, int n)
{
	if (n <= 3)
		n = 4;
	std::vector<glm::vec2> points;
	float dr = 180 - ((n - 2) * 180) / n;
	std::uniform_int_distribution<int> ds_1_20{ 1, 20 };
	float randx = ds_1_20(mt);
	float randy = ds_1_20(mt);
	glm::vec2 dir = normalize(glm::vec2(randx, randy));
	dir *= radius;
	float theta = glm::radians(dr);
	float cs = cos(theta);
	float sn = sin(theta);
	for (int i = 0; i < n; ++i) {
		points.push_back(center + dir);
		glm::vec2 new_dir = glm::vec2((dir.x * cs) - (dir.y * sn),
									  (dir.x * sn) + (dir.y * cs));
		dir = new_dir;
	}
	return points;
}

std::vector<glm::vec2> PCG::Towns::City_gen::Generator::combine_plans(
		const std::vector<glm::vec2>& shape_a, const std::vector<glm::vec2>& shape_b)
{
	std::vector<glm::vec2> new_plan;
	int n[] = { (int) shape_a.size(), (int) shape_b.size() };
	std::vector<glm::vec2> shapes[] = { shape_a, shape_b };
	int cur_shape = 0;
	int index = 0;
	glm::vec2 mid_b = center_point(shapes[1]);
	for (int i = 0; i < n[0]; i++) {
		if (abs(length(shapes[0][index] - mid_b))
			< abs(length(shapes[0][i] - mid_b))) {
			index = i;
		}
	}
	glm::vec2 current_point = shapes[0][index];
	while (!contains_vec(new_plan, current_point)) {
		new_plan.push_back(current_point);
		bool has_intersection = false;
		for (int j = 0; j < n[!cur_shape]; ++j) {
			int o_index = (index + j - 1) % n[!cur_shape];
			if (o_index < 0)
				o_index = n[!cur_shape] - 1;
			if (intersects(current_point, shapes[cur_shape][(index + 1) % n[0]],
						   shapes[!cur_shape][o_index],
						   shapes[!cur_shape][(o_index + 1) % n[1]])) {
				new_plan.push_back(
						get_intersection(
								current_point,
								shapes[cur_shape][(index + 1) % n[0]], shapes[!cur_shape][o_index],
								shapes[!cur_shape][(o_index + 1) % n[1]]));
				index = (o_index + 1);
				if (index == n[!cur_shape])
					index = 0;
				has_intersection = true;
				break;
			}
		}
		if (has_intersection) {
			cur_shape = !cur_shape;
			current_point = shapes[cur_shape][index];
		} else {
			index = (index + 1) % n[cur_shape];
			current_point = shapes[cur_shape][index];
		}
	}
	return new_plan;
}

std::string PCG::Towns::City_gen::Generator::generate_random_building_string(int itrs)
{
	itrs = std::max(std::min(itrs, 6), 3);
	std::string result = "$*";
	for (int i = 0; i < itrs; ++i) {
		std::string next = "";
		for (size_t c = 0; c < result.length(); ++c) {
			std::string addition = l_system_lookup(result[c]);
			next = next + addition;
		}
		result = next;
	}
	return result;
}

std::vector<std::vector<glm::vec2>> PCG::Towns::City_gen::Generator::subdivide(
		const std::vector<glm::vec2>& points)
{
	std::vector<std::vector<glm::vec2>> result;
	if (points.size() != 4) {
		result.push_back(points);
		result.push_back(points);
		return result;
	}
	std::uniform_real_distribution<float> ds_15_30{ 1.5f, 3.f };
	float cut_dist = ds_15_30(mt);
	glm::vec2 cutP1 = ((points[1] - points[0]) / cut_dist) + points[0];
	glm::vec2 cutP2 = ((points[2] - points[3]) / cut_dist) + points[3];
	result.push_back(std::vector<glm::vec2>());
	result.push_back(std::vector<glm::vec2>());
	int i = cut_dist >= 2.0f;
	result[i].push_back(cutP1);
	result[i].push_back(cutP2);
	result[i].push_back(points[3]);
	result[i].push_back(points[0]);
	result[!i].push_back(cutP2);
	result[!i].push_back(cutP1);
	result[!i].push_back(points[1]);
	result[!i].push_back(points[2]);
	return result;
}

bool PCG::Towns::City_gen::Generator::contains_vec(const std::vector<glm::vec2>& vec,
												   glm::vec2 to_find)
{
	if (vec.empty()) {
		return false;
	}
	for (glm::vec2 v : vec) {
		if (v.x == to_find.x && v.y == to_find.y) {
			return true;
		}
	}
	return false;
}

std::string PCG::Towns::City_gen::Generator::l_system_lookup(char c)
{
	std::uniform_int_distribution<int> ds_0_9{ 0, 9 };
	if (c == 'E') {
		switch (ds_0_9(mt)) {
			case 0:
			case 1:
			case 2:
			case 4:
				return "E";
			case 6:
			case 8:
			case 3:
			case 9:
				return "EE";
			case 5:
			case 7:
				return "ES";
			default:
				break;
		}
	} else if (c == 'S') {
		switch (ds_0_9(mt)) {
			case 0:
			case 1:
			case 6:
				return "S";
			case 2:
			case 3:
			case 4:
			case 7:
				return "SE";
			case 8:
			case 5:
				return "DE";
			case 9:
				return "SE";
			default:
				break;
		}
	} else if (c == '*') {
		switch (ds_0_9(mt)) {
			case 0:
			case 1:
			case 2:
			case 7:
				return "ES*";
			case 4:
			case 3:
				return "ED*";
			case 5:
			case 6:
				return "E*";

			case 8:
			case 9:
				return "R";
			default:
				break;
		}
	} else if (c == '$') {
		return "$";
	}
	return "" + std::to_string(c);
}

PCG::Towns::City_gen::Lot::Lot(Section bb, std::vector<Section> secs, int id)
		: bounding_box{ bb }, sections{ secs }, id{ id }
{
}
