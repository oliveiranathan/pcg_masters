/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This code is based on a town generator by Tyler Wymer and is subject to the following license:
 * Copyright (c) 2012 Tyler Wymer, http://tylerwymer.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "../../utils/matrix.h"

#include <memory>
#include <random>
#include <string>
#include <vector>

#include <glm/vec2.hpp>

namespace PCG {

	namespace Towns {

		/// Based on the code for a town generator by Tyler Wymer
		/// The code is available at https://github.com/twymer/town-gen

		class Agent_based {
		public:
			Agent_based(unsigned int iterations = 10000, unsigned int grid_scale = 5,
					 unsigned int cols = 100, unsigned int rows = 100);

			void export_to_png(const std::string& filename);

		private:
			struct Hasher {
				std::size_t operator()(const glm::ivec2& point) const;
			};

			class Road_segment {
			public:
				Road_segment(std::vector<glm::ivec2> pts);

			private:
				std::vector<glm::ivec2> road_points;
			};

			class Building {
			public:
				Building(std::vector<glm::ivec2> pts);

			private:
				std::vector<glm::ivec2> building_points;
			};

			enum class Tile {
				empty, road, building
			};

			using World_ptr = std::shared_ptr<Utils::Matrix<Tile>>;

			class Agent {
			public:
				Agent(World_ptr world, Agent_based* town);

				virtual void update() = 0;

			protected:
				unsigned int road_count_in_range(int square_radius);

				bool valid_world_coordinates(long x, long y);

				int cols, rows;
				int x_pos, y_pos;
				std::vector<glm::ivec2> directions;
				std::shared_ptr<Utils::Matrix<Tile>> world;
				Agent_based* town;

				static std::mt19937 prng;
			};

			class Connector_agent : public Agent {
			public:
				Connector_agent(World_ptr world, Agent_based* town);

				void update() override;

			private:
				std::unique_ptr<glm::ivec2> find_nearby_road_segment();

				static const int connector_distance = 10;
			};

			class Extender_agent : public Agent {
			public:
				Extender_agent(World_ptr world, Agent_based* town);

				void update() override;

			private:
				bool too_far(int x, int y);

				bool use_road_segment(const std::vector<glm::ivec2>& path);

				static const unsigned int max_distance_from_development = 20;
				static const unsigned int max_extender_distance = 10;
			};

			class Building_agent : public Agent {
			public:
				Building_agent(World_ptr world, Agent_based* town);

				void update() override;

			private:
				std::vector<glm::ivec2> suggest_building(glm::ivec2 start_pos);

				static const int builder_search_distance = 3;
			};

			void add_road(std::vector<glm::ivec2> path);

			void add_building_to_world(std::vector<glm::ivec2> building);

			void generate();

			unsigned int grid_size;
			unsigned int start_x;
			unsigned int start_y;

			World_ptr world;

			std::vector<Road_segment> roads;
			std::vector<Building> buildings;

			unsigned int extend_iterations;
			bool multiple_agents = true;

			Extender_agent extender;
			Extender_agent extender2;
			Extender_agent extender3;

			Connector_agent connector;
			Connector_agent connector2;
			Connector_agent connector3;
			Connector_agent connector4;

			Building_agent builder;

			class Node {
			public:
				Node(glm::ivec2 location, Node* p, int d, int cols, int rows);

				std::vector<glm::ivec2> neighbors();

				glm::ivec2 pos;
				Node* parent{ nullptr };
				int depth;
				int cols, rows;
			private:
				bool valid_world_coordinates(int x, int y);
			};

			class Search {
			public:
				Search(World_ptr world);

				std::vector<glm::ivec2> bfs(int x, int y, int goalX, int goalY);

			protected:
				virtual bool goal_reached(Node* current, glm::ivec2 goal) = 0;

				virtual bool valid_neighbor(glm::ivec2 potential_neighbor) = 0;

				World_ptr world;
			private:
				std::vector<glm::ivec2> trace_path(Node const* final_node);
			};

			class Find_via_roads : public Search {
			public:
				Find_via_roads(World_ptr);

			protected:
				inline bool goal_reached(Node* current, glm::ivec2 goal) override;

				inline bool valid_neighbor(glm::ivec2 potential_neighbor) override;
			};

			class Find_nearest_development : public Search {
			public:
				Find_nearest_development(World_ptr);

			protected:
				inline bool goal_reached(Node* current, glm::ivec2) override;

				inline bool valid_neighbor(glm::ivec2) override;
			};

			class Find_goal : public Search {
			public:
				Find_goal(World_ptr);

			protected:
				inline bool goal_reached(Node* current, glm::ivec2 goal) override;

				inline bool valid_neighbor(glm::ivec2 potential_neighbor) override;
			};

			class Find_nearest_road : public Search {
			public:
				Find_nearest_road(World_ptr);

			protected:
				inline bool goal_reached(Node* current, glm::ivec2) override;

				inline bool valid_neighbor(glm::ivec2 potential_neighbor) override;
			};
		};

	}

}
