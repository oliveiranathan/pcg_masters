/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/matrix.h"

#include <random>
#include <string>
#include <vector>

#include <glm/vec2.hpp>

namespace PCG {

	namespace Towns {

		/// Based on the code for a road network generator by Florian Depraz
		/// and Martin Engilberge. The code is available at
		/// https://github.com/pokitoz/Procedural-road-network-generation
		
		class L_systems_based {
		public:
			L_systems_based(long width, long height, const std::string& density_map,
						 const std::string& heigth_map, long start_pos_x, long start_pos_y);

			void export_to_png(const std::string& filename);

		private:
			struct Tile {
				float density{ 0.0 };
				float height{ 0.0 };
				bool is_accessible{ true };
				bool is_road{ false };
			};

			struct Node {
				glm::ivec2 p;
				int road_size;
			};

			struct Edge {
				glm::ivec2 start;
				glm::ivec2 end;
				int size;
			};

			struct L_element {
				enum Enum {
					S, P, M, O, C, s
				};

				Enum element;

				L_element(Enum element);

				L_element();

				bool operator==(const L_element& c) const;

				glm::ivec2 action(float& angle, float delta_angle, int length,
								  glm::ivec2 previous_point);
			};

			struct Proposition {
				L_element left_side;
				std::vector<L_element> right_side;
				float p;
			};

			struct L_system {
				L_system(glm::ivec2 start_pos, float base_angle, int min_length,
						 int max_length, int nb_iteration, int base_road_size);

				void generate_network(const Utils::Matrix<Tile>& map,
									  L_systems_based& network);

			private:
				glm::ivec2 start_pos;
				float angle;
				int min_length;
				int max_length;
				int nb_iteration;
				int base_road_size;
				float delta_angle = 90;
				std::vector<Proposition> props;

				std::mt19937 mt;

				std::vector<L_element> generate_chain();

				Proposition get_props_from_rhs(L_element rhs);

				void mark_chain_as_road(std::vector<L_element> chain,
										glm::ivec2 start_position, float angle,
										const Utils::Matrix<Tile>& map, int road_size,
										L_systems_based& network);

				std::vector<L_element> get_sub_chain(std::vector<L_element>& chain,
													 int start);
			};

			using Average_population = std::pair<glm::ivec2, float>;

			void read_maps(const std::string& density_map, const std::string& heigth_map);

			void main_algorithm();

			std::vector<Average_population> get_subdivided_map();

			Average_population get_average_pop(int x, int y,
														  int width, int height);

			void generate_partial_network(const std::vector<glm::ivec2>& positions,
										  int size);

			glm::ivec2 find_closest_node(const glm::ivec2& pos, int road_size);

			float distance_and_height(glm::ivec2 start, glm::ivec2 end);

			bool is_into_the_map(glm::ivec2 pos) const;

			void mark_road_network();

			void mark_road(glm::ivec2 start, glm::ivec2 end, int size);

			void mark_base_square(glm::ivec2 start, glm::ivec2 end, int size);

			glm::ivec2 add_edge_to_network(glm::ivec2 start, glm::ivec2 end,
										   int size);

			Node is_there_close_node(Node pos, int dist_max, int road_size);

			static const int min_seg_length = 25;
			static const int max_seg_length = 40;
			static const int sub_square_size = 50;
			static constexpr float max_distance = 30.0;

			static float distance(glm::ivec2 start, glm::ivec2 end);

			static float find_angle(glm::ivec2 start, glm::ivec2 end);

			static void interpolate(glm::ivec2 a, glm::ivec2 b,
									std::vector<glm::ivec2>& result);

			static glm::ivec2 find_next_point(glm::ivec2 start, float angle,
											  int length);

			Utils::Matrix<Tile> map;
			int distance_to_close_node;
			std::vector<Node> nodes;
			std::vector<Edge> edges;
		};

	}

}
