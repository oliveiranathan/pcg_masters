/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "roads.h"

#include <algorithm>

#include <glm/detail/func_common.hpp>
#include <png++/png.hpp>

PCG::Towns::L_systems_based::L_systems_based(long width, long height,
											 const std::string& density_map,
											 const std::string& heigth_map,
											 long start_pos_x, long start_pos_y)
		: map{ width, height },
		  distance_to_close_node{ (min_seg_length + max_seg_length) / 4 }
{
	read_maps(density_map, heigth_map);
	glm::ivec2 start_position{ start_pos_x, start_pos_y };
	Node n;
	n.p = start_position;
	n.road_size = 3;
	nodes.push_back(n);
	main_algorithm();
	mark_road_network();
}

void PCG::Towns::L_systems_based::export_to_png(const std::string& filename)
{
	unsigned int width = (unsigned int) map.get_width();
	unsigned int height = (unsigned int) map.get_height();
	png::image<png::rgb_pixel> map_image{ width, height };
	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char luminosity = 250;
	for (unsigned int y = 0; y < height; y++) {
		for (unsigned int x = 0; x < width; x++) {
			Tile t = map(x, y);
			if (t.is_road) {
				r = 0;
				g = 0;
				b = 0;
			} else if (!t.is_accessible) {
				r = (unsigned char) (0.8 * luminosity);
				g = (unsigned char) (0.8 * luminosity);
				b = (unsigned char) (0.85 * luminosity);
			} else {
				r = (unsigned char) (t.height * luminosity);
				g = (unsigned char) (t.density * luminosity);
				b = (unsigned char) (0.5 * luminosity);
			}
			map_image[y][x] = png::rgb_pixel{ r, g, b };
		}
	}
	map_image.write(filename);
}

void PCG::Towns::L_systems_based::read_maps(
		const std::string& density_file, const std::string& heigth_file)
{
	png::image<png::rgb_pixel> density_image{ density_file };
	png::image<png::rgb_pixel> height_image{ heigth_file };
	float max_density = -1, min_density = 300;
	float max_height = -1, min_height = 300;
	for (size_t i = 0; i < density_image.get_height(); ++i) {
		for (size_t j = 0; j < density_image.get_width(); ++j) {
			if (max_density < density_image[i][j].red)
				max_density = density_image[i][j].red;
			if (min_density > density_image[i][j].red)
				min_density = density_image[i][j].red;
			if (max_height < height_image[i][j].red)
				max_height = height_image[i][j].red;
			if (min_height > height_image[i][j].red)
				min_height = height_image[i][j].red;
		}
	}
	for (size_t j = 0; j < density_image.get_height(); ++j) {
		for (size_t i = 0; i < density_image.get_width(); ++i) {
			int r1 = density_image[j][i].red;
			int r2 = height_image[j][i].red;
			map(i, j).density = 1 - (r1 - min_density) / (max_density - min_density);
			map(i, j).height = (r2 - min_height) / (max_height - min_height);
		}
	}
}

void PCG::Towns::L_systems_based::main_algorithm()
{
	std::vector<Average_population> s_map = get_subdivided_map();
	float max_pop = s_map[0].second;
	unsigned int i = 0;
	float percent = 0.6;
	for (int size = 3; size > 0; --size) {
		std::vector<glm::ivec2> positions;
		while (i < s_map.size() && s_map[i].second > percent * max_pop) {
			positions.push_back(s_map[i].first);
			i++;
		}
		generate_partial_network(positions, size);
		positions.clear();
		percent -= 0.2;
	}
}

std::vector<PCG::Towns::L_systems_based::Average_population>
PCG::Towns::L_systems_based::get_subdivided_map()
{
	std::vector<Average_population> result;
	for (int x = 0; x < map.get_width(); x += sub_square_size) {
		for (int y = 0; y < map.get_height(); y += sub_square_size) {
			if ((x + sub_square_size) < map.get_width()
				&& (y + sub_square_size) < map.get_height()) {
				result.push_back(get_average_pop(x, y, sub_square_size, sub_square_size));
			} else {
				int width = sub_square_size;
				int heigth = sub_square_size;
				if ((x + sub_square_size) >= map.get_width()) {
					width = (int) map.get_width() - x - 1;
				}
				if ((y + sub_square_size) >= map.get_height()) {
					heigth = (int) map.get_height() - y - 1;
				}
				result.push_back(get_average_pop(x, y, width, heigth));
			}
		}
	}
	std::sort(result.begin(), result.end(), []
			(const Average_population& a,
			 const Average_population& b) {
		return a.second > b.second;
	});
	return result;
}

PCG::Towns::L_systems_based::Average_population PCG::Towns::L_systems_based::get_average_pop(
		int x, int y, int width, int height)
{
	float w = width / 2;
	float h = height / 2;
	glm::ivec2 center = glm::ivec2{ x, y } + glm::ivec2{ w, h };
	float total = 0;
	long nb_tile = 0;
	for (int i = x; i < x + width; ++i) {
		for (int j = y; j < y + height; ++j) {
			total += map(i, j).density;
			nb_tile++;
		}
	}
	return std::pair<glm::ivec2, float>(center, total / nb_tile);
}

void PCG::Towns::L_systems_based::generate_partial_network(
		const std::vector<glm::ivec2>& positions, int road_size)
{
	for (unsigned int i = 0; i < positions.size(); ++i) {
		glm::ivec2 end = positions[i];
		glm::ivec2 start = find_closest_node(end, road_size);
		while (distance(start, end) > max_distance) {
			int lenght = (int) distance(start, end);
			float angle = find_angle(start, end);
			int nb_iter = (int) std::ceil(std::log((float) lenght
											 / (max_seg_length + min_seg_length)));
			if (nb_iter > 5) {
				nb_iter = 5;
			} else if (nb_iter <= 0) {
				nb_iter = 1;
			}
			nb_iter = (int) std::ceil((float) nb_iter * 1.5);
			L_system sys2(start, angle, min_seg_length, max_seg_length, nb_iter,
						  road_size);
			sys2.generate_network(map, *this);
			start = find_closest_node(end, road_size);
		}
	}
}

glm::ivec2 PCG::Towns::L_systems_based::find_closest_node(const glm::ivec2& pos,
														  int road_size)
{
	float min_dist = map.get_width();
	glm::ivec2 result;
	for (unsigned int i = 0; i < nodes.size(); ++i) {
		if (distance_and_height(pos, nodes[i].p) < min_dist
			&& nodes[i].road_size >= road_size) {
			min_dist = distance_and_height(pos, nodes[i].p);
			result = nodes[i].p;
		}
	}
	return result;
}

float PCG::Towns::L_systems_based::distance_and_height(glm::ivec2 start, glm::ivec2 end)
{
	if (!is_into_the_map(start) || !is_into_the_map(end)) {
		return distance(start, end);
	}
	float result = std::sqrt(std::pow((end.x - start.x), 2)
							 + std::pow((end.y - start.y), 2));
	glm::ivec2 delta = glm::abs(start - end);
	int pixels = glm::max(delta.x, delta.y) + 1;
	std::vector<glm::ivec2> line(pixels);
	interpolate(start, end, line);
	float previous = map(line[0].x, line[0].y).height;
	for (unsigned int i = 0; i < line.size(); ++i) {
		glm::ivec2 p = line[i];
		result += 10 * abs(map(p.x, p.y).height - previous);
		previous = map(p.x, p.y).height;
	}
	return result;
}

bool PCG::Towns::L_systems_based::is_into_the_map(glm::ivec2 p) const
{
	return (p.x >= 0 && p.x < map.get_width() && p.y >= 0 && p.y < map.get_height());
}

void PCG::Towns::L_systems_based::mark_road_network()
{
	for (unsigned int i = 0; i < edges.size(); ++i) {
		Edge e = edges[i];
		mark_road(e.start, e.end, e.size);
	}
}

void PCG::Towns::L_systems_based::mark_road(glm::ivec2 start, glm::ivec2 end, int size)
{
	std::vector<glm::ivec2> start_positions(4 * size + 1);
	std::vector<glm::ivec2> end_positions(4 * size + 1);
	if (!is_into_the_map(start)) {
		glm::ivec2 delta = glm::abs(start - end);
		int pixels = glm::max(delta.x, delta.y) + 1;
		std::vector<glm::ivec2> line(pixels);
		interpolate(start, end, line);
		for (unsigned int i = 0; i < line.size(); ++i) {
			if (is_into_the_map(line[i])) {
				start = line[i];
				break;
			}
		}
	} else if (!is_into_the_map(end)) {
		glm::ivec2 delta = glm::abs(start - end);
		int pixels = glm::max(delta.x, delta.y) + 1;
		std::vector<glm::ivec2> line(pixels);
		interpolate(start, end, line);
		for (int i = (int) line.size() - 1; i <= 0; --i) {
			if (is_into_the_map(line[i])) {
				end = line[i];
				break;
			}
		}
	}
	if (!is_into_the_map(start) || !is_into_the_map(end)) {
		return;
	}
	start_positions[0] = start;
	end_positions[0] = end;
	for (int s = 1; s <= size; ++s) {
		start_positions[s * 4] = glm::ivec2(start) + s * glm::ivec2(0, 1);
		start_positions[s * 4 - 1] = glm::ivec2(start) + s * glm::ivec2(1, 0);
		start_positions[s * 4 - 2] = glm::ivec2(start) + s * glm::ivec2(0, -1);
		start_positions[s * 4 - 3] = glm::ivec2(start) + s * glm::ivec2(-1, 0);
		end_positions[s * 4] = glm::ivec2(end) + s * glm::ivec2(0, 1);
		end_positions[s * 4 - 1] = glm::ivec2(end) + s * glm::ivec2(1, 0);
		end_positions[s * 4 - 2] = glm::ivec2(end) + s * glm::ivec2(0, -1);
		end_positions[s * 4 - 3] = glm::ivec2(end) + s * glm::ivec2(-1, 0);
	}
	mark_base_square(start, end, size);
	for (unsigned int i = 0; i < start_positions.size(); ++i) {
		if (!is_into_the_map(start_positions[i])) {
			start_positions[i] = start_positions[0];
		}
		if (!is_into_the_map(end_positions[i])) {
			end_positions[i] = end_positions[0];
		}
		glm::ivec2 delta = glm::abs(start_positions[i] - end_positions[i]);
		int pixels = glm::max(delta.x, delta.y) + 1;
		std::vector<glm::ivec2> line(pixels);
		interpolate(start_positions[i], end_positions[i], line);
		for (unsigned int j = 0; j < line.size(); ++j) {
			glm::ivec2 t = line[j];
			map(t.x, t.y).is_road = true;
		}
	}
}

void PCG::Towns::L_systems_based::mark_base_square(glm::ivec2 start, glm::ivec2 end,
												   int size)
{
	glm::ivec2 a = glm::ivec2(start) + size * glm::ivec2(-1, -1);
	glm::ivec2 b = glm::ivec2(end) + size * glm::ivec2(-1, -1);
	for (int j = 0; j < 2 * size + 1; ++j) {
		for (int i = 0; i < 2 * size + 1; ++i) {
			if (is_into_the_map({ a.x + i, a.y + j }))
				map(a.x + i, a.y + j).is_road = true;
			if (is_into_the_map({ b.x + i, b.y + j }))
				map(b.x + i, b.y + j).is_road = true;
		}
	}
}

glm::ivec2 PCG::Towns::L_systems_based::add_edge_to_network(glm::ivec2 start,
															glm::ivec2 end, int size)
{
	Edge e;
	e.start = start;
	e.size = size;
	Node en;
	en.p = end;
	en.road_size = size;
	Node corrected_end = is_there_close_node(en, distance_to_close_node + size, size);
	if (end == corrected_end.p) {
		e.end = end;
		Node n;
		n.p = end;
		n.road_size = size;
		nodes.push_back(n);
	} else {
		e.end = corrected_end.p;
	}
	edges.push_back(e);
	return corrected_end.p;
}

PCG::Towns::L_systems_based::Node
PCG::Towns::L_systems_based::is_there_close_node(Node pos, int dist_max, int road_size)
{
	int index = -1;
	int min_length = dist_max;
	for (unsigned int i = 0; i < nodes.size(); ++i) {
		if (distance(pos.p, nodes[i].p) < min_length) {
			index = i;
			min_length = (int) distance(pos.p, nodes[i].p);
		}
	}
	if (index != -1) {
		if (road_size > nodes[index].road_size) {
			nodes[index].road_size = road_size;
		}
		return nodes[index];
	}
	return pos;
}

float PCG::Towns::L_systems_based::distance(glm::ivec2 start, glm::ivec2 end)
{
	return std::sqrt(std::pow((end.x - start.x), 2)
					 + std::pow((end.y - start.y), 2));
}

float PCG::Towns::L_systems_based::find_angle(glm::ivec2 start, glm::ivec2 end)
{
	float alpha;
	float angle_degre = 90;
	end = end - start;
	start = start - start;
	if (end.x >= start.x && end.y < start.y) {
		alpha = std::atan((float) end.y / end.x);
		angle_degre = alpha * 180 / M_PI;
	} else if (end.x > start.x && end.y >= start.y) {
		alpha = std::atan((float) end.y / end.x);
		angle_degre = alpha * 180 / M_PI;
	} else if (end.x <= start.x && end.y > start.y) {
		alpha = std::atan((float) -end.x / end.y);
		angle_degre = 90 + alpha * 180 / M_PI;
	} else if (end.x < start.x && end.y <= start.y) {
		if (end.y != 0) {
			alpha = std::atan(-(float) end.x / end.y);
			angle_degre = -90 + alpha * 180 / M_PI;
		} else {
			angle_degre = 180;
		}
	}
	return angle_degre;
}

void PCG::Towns::L_systems_based::interpolate(glm::ivec2 a, glm::ivec2 b,
											  std::vector<glm::ivec2>& result)
{
	int N = (int) result.size();
	glm::vec2 step = glm::vec2(b - a) / float(std::max(N - 1, 1));
	glm::vec2 current(a);
	for (int i = 0; i < N; ++i) {
		result[i] = current;
		current += step;
	}
}

glm::ivec2 PCG::Towns::L_systems_based::find_next_point(glm::ivec2 start,
														float angle, int length)
{
	float angle_rad = angle * M_PI / 180;
	glm::ivec2 result(cos(angle_rad) * length, sin(angle_rad) * length);
	result = result + start;
	return result;
}

PCG::Towns::L_systems_based::L_system::L_system(glm::ivec2 start_p, float base_angle,
												int min_l, int max_l, int nb_iter, int road_size)
		: start_pos{ start_p }, angle{ base_angle }, min_length{ min_l },
		  max_length{ max_l }, nb_iteration{ nb_iter }, base_road_size{ road_size }
{
	Proposition p1;
	p1.left_side = L_element::S;
	p1.right_side = { L_element::S, L_element::S };
	p1.p = 0.25;
	props.push_back(p1);
	Proposition p2;
	p2.left_side = L_element::S;
	p2.right_side = { L_element::S, L_element::O, L_element::P,
					  L_element::s, L_element::C, L_element::S };
	p2.p = 0.25;
	props.push_back(p2);
	Proposition p3;
	p3.left_side = L_element::S;
	p3.right_side = { L_element::S, L_element::O, L_element::M,
					  L_element::s, L_element::C, L_element::S };
	p3.p = 0.25;
	props.push_back(p3);
	Proposition p32;
	p32.left_side = L_element::S;
	p32.right_side = { L_element::S, L_element::O, L_element::P, L_element::s,
					   L_element::C, L_element::O, L_element::M,
					   L_element::s, L_element::C, L_element::S };
	p32.p = 0.25;
	props.push_back(p32);
	Proposition p4;
	p4.left_side = L_element::O;
	p4.right_side = { L_element::O };
	p4.p = 1;
	props.push_back(p4);
	Proposition p5;
	p5.left_side = L_element::C;
	p5.right_side = { L_element::C };
	p5.p = 1;
	props.push_back(p5);
	Proposition p6;
	p6.left_side = L_element::P;
	p6.right_side = { L_element::P };
	p6.p = 1;
	props.push_back(p6);
	Proposition p7;
	p7.left_side = L_element::M;
	p7.right_side = { L_element::M };
	p7.p = 1;
	props.push_back(p7);
	Proposition p8;
	p8.left_side = L_element::s;
	p8.right_side = { L_element::s, L_element::S };
	p8.p = 1;
	props.push_back(p8);
	mt = std::mt19937{ 666 };
}

void PCG::Towns::L_systems_based::L_system::generate_network(
		const Utils::Matrix<Tile>& map, L_systems_based& network)
{
	std::vector<L_element> chain = generate_chain();
	mark_chain_as_road(chain, start_pos, angle, map, base_road_size, network);
}

std::vector<PCG::Towns::L_systems_based::L_element>
PCG::Towns::L_systems_based::L_system::generate_chain()
{
	std::vector<L_element> result;
	result.push_back(L_element::S);
	std::vector<L_element> temp_result;
	for (int i = 0; i < nb_iteration; ++i) {
		for (unsigned int j = 0; j < result.size(); ++j) {
			std::vector<L_element> elements = get_props_from_rhs(result[j]).right_side;
			temp_result.insert(temp_result.end(), elements.begin(), elements.end());
		}
		result = temp_result;
		temp_result.clear();
	}
	return result;
}

PCG::Towns::L_systems_based::Proposition
PCG::Towns::L_systems_based::L_system::get_props_from_rhs(L_element rhs)
{
	std::vector<Proposition> result;
	for (unsigned int i = 0; i < props.size(); ++i) {
		if (rhs == props[i].left_side) {
			result.push_back(props[i]);
		}
	}
	std::uniform_real_distribution<float> ds{ 1.0f / 1000.0f, 1.0f };
	float r = ds(mt);
	float t = 0;
	for (unsigned int i = 0; i < result.size(); ++i) {
		t += result[i].p;
		if (r <= t) {
			return result[i];
		}
	}
	return result[0];
}

void PCG::Towns::L_systems_based::L_system::mark_chain_as_road(
		std::vector<L_element> chain, glm::ivec2 start_position, float angle,
		const Utils::Matrix<Tile>& map, int road_size, L_systems_based& network)
{
	glm::ivec2 current_pos = start_position;
	for (unsigned int i = 0; i < chain.size(); ++i) {
		switch (chain[i].element) {
			case L_element::O:
				if (road_size > 0)
					mark_chain_as_road(get_sub_chain(chain, i), current_pos,
									   angle, map, road_size - 1, network);
				else
					mark_chain_as_road(get_sub_chain(chain, i), current_pos,
									   angle, map, road_size, network);
				break;
			default:
				std::uniform_int_distribution<int> ds{ min_length, max_length };
				int length = ds(mt);
				glm::ivec2 next_pos = chain[i].action(angle, delta_angle,
													  length, current_pos);
				current_pos = network.add_edge_to_network(current_pos, next_pos,
														  road_size);
				glm::ivec2 old_next_pos = L_element().action(angle,
															 delta_angle, 100, next_pos);
				angle = find_angle(current_pos, old_next_pos);
				break;
		}
	}
}

std::vector<PCG::Towns::L_systems_based::L_element>
PCG::Towns::L_systems_based::L_system::get_sub_chain(std::vector<L_element>& chain,
													 int start)
{
	int nb_bracket = 0;
	int end = start;
	for (unsigned int i = start; i < chain.size(); ++i) {
		if (chain[i] == L_element::O) {
			nb_bracket += 1;
		} else if (chain[i] == L_element::C) {
			nb_bracket -= 1;
			if (nb_bracket == 0) {
				end = i;
				break;
			}
		}
	}
	std::vector<L_element> result(chain.begin() + start, chain.begin() + end);
	chain.erase(chain.begin() + start, chain.begin() + end);
	return result;
}

PCG::Towns::L_systems_based::L_element::L_element(Enum elem)
		: element{ elem }
{
}

PCG::Towns::L_systems_based::L_element::L_element()
		: element{ Enum::S }
{
}

bool PCG::Towns::L_systems_based::L_element::operator==(const L_element& c) const
{
	return element == c.element;
}

glm::ivec2
PCG::Towns::L_systems_based::L_element::action(float& angle, float delta_angle,
											   int length, glm::ivec2 previous_point)
{
	switch (element) {
		case S:
			return find_next_point(previous_point, angle, length);
		case s:
			return find_next_point(previous_point, angle, length);
		case P:
			angle += delta_angle;
			break;
		case M:
			angle -= delta_angle;
			break;
		default:
			break;
	}
	return previous_point;
}
