/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This code is based on a town generator by Tyler Wymer and is subject to the following license:
 * Copyright (c) 2012 Tyler Wymer, http://tylerwymer.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "agents.h"

#include <algorithm>
#include <unordered_set>

#include <png++/png.hpp>

std::mt19937 PCG::Towns::Agent_based::Agent::prng{ 666 };

PCG::Towns::Agent_based::Agent_based(unsigned int iterations, unsigned int grid_scale,
									 unsigned int cols, unsigned int rows)
		: grid_size{ grid_scale }, start_x{ cols / 2 }, start_y{ rows / 2 },
		  world{ std::make_shared<Utils::Matrix<Tile>>(cols, rows) },
		  extend_iterations{ iterations }, extender{ world, this }, extender2{ world, this },
		  extender3{ world, this }, connector{ world, this }, connector2{ world, this },
		  connector3{ world, this }, connector4{ world, this }, builder{ world, this }
{
	for (unsigned int i = 0; i < cols; ++i) {
		for (unsigned int j = 0; j < rows; ++j) {
			world->operator()(i, j) = Tile::empty;
		}
	}
	world->operator()(start_x, start_y) = Tile::road;
	generate();
}

void PCG::Towns::Agent_based::export_to_png(const std::string& filename)
{
	png::image<png::rgb_pixel> image{ (unsigned int) world->get_width() * grid_size,
									  (unsigned int) world->get_height() * grid_size };
	for (long i = 0; i < world->get_width(); ++i) {
		for (long j = 0; j < world->get_height(); ++j) {
			png::rgb_pixel px;
			switch (world->operator()(i, j)) {
				case Tile::empty:
					px = png::rgb_pixel(255, 255, 255);
					break;
				case Tile::road:
					px = png::rgb_pixel(0, 0, 0);
					break;
				case Tile::building:
					px = png::rgb_pixel(255, 0, 0);
					break;
			}
			for (unsigned int ii = 0; ii < grid_size; ++ii) {
				for (unsigned int jj = 0; jj < grid_size; ++jj) {
					image[i * grid_size + ii][j * grid_size + jj] = px;
				}
			}
		}
	}
	image.write(filename);
}

void PCG::Towns::Agent_based::add_road(std::vector<glm::ivec2> path)
{
	std::vector<glm::ivec2> road;
	for (const glm::ivec2& p : path) {
		world->operator()(p.x, p.y) = Tile::road;
		road.push_back(p);
	}
	roads.emplace_back(road);
}

void PCG::Towns::Agent_based::add_building_to_world(std::vector<glm::ivec2> building)
{
	std::vector<glm::ivec2> bp;
	for (const glm::ivec2& p : building) {
		world->operator()(p.x, p.y) = Tile::building;
		bp.push_back(p);
	}
	buildings.emplace_back(bp);
}

void PCG::Towns::Agent_based::generate()
{
	for (unsigned int iter = 0; iter < extend_iterations; ++iter) {
		extender.update();
		connector.update();
		builder.update();
		if (multiple_agents) {
			extender2.update();
			extender3.update();
			connector2.update();
			connector3.update();
			connector4.update();
		}
	}
}

PCG::Towns::Agent_based::Road_segment::Road_segment(std::vector<glm::ivec2> pts)
		: road_points{ pts }
{
}

PCG::Towns::Agent_based::Building::Building(std::vector<glm::ivec2> pts)
		: building_points{ pts }
{
}

PCG::Towns::Agent_based::Agent::Agent(World_ptr wrld, Agent_based* twn)
		: cols{ (int) wrld->get_width() }, rows{ (int) wrld->get_height() },
		  x_pos{ cols / 2 }, y_pos{ rows / 2 }, world{ wrld }, town{ twn }
{
	for (int i = -1; i <= 1; ++i) {
		for (int j = -1; j <= 1; ++j) {
			directions.emplace_back(i, j);
		}
	}
}

unsigned int PCG::Towns::Agent_based::Agent::road_count_in_range(int square_radius)
{
	unsigned int count{ 0 };
	for (long i = -square_radius; i <= square_radius; ++i) {
		for (long j = -square_radius; j <= square_radius; ++j) {
			if (valid_world_coordinates(i + x_pos, j + y_pos)
				&& world->operator()(i + x_pos, j + y_pos) == Tile::road) {
				++count;
			}
		}
	}
	return count;
}

bool PCG::Towns::Agent_based::Agent::valid_world_coordinates(long x, long y)
{
	return (x > 0 && x < long(cols) && y > 0 && y < long(rows));
}

PCG::Towns::Agent_based::Connector_agent::Connector_agent(
		World_ptr world, Agent_based* t)
		: Agent{ world, t }
{
}

void PCG::Towns::Agent_based::Connector_agent::update()
{
	std::mt19937 mt{ prng() };
	std::shuffle(directions.begin(), directions.end(), mt);
	for (const glm::ivec2& dir : directions) {
		if (valid_world_coordinates(x_pos + dir.x, y_pos + dir.y)
			&& world->operator()(x_pos + dir.x, y_pos + dir.y) == Tile::road) {
			x_pos += dir.x;
			y_pos += dir.y;
			break;
		}
	}
	std::unique_ptr<glm::ivec2> nearby = find_nearby_road_segment();
	if (nearby) {
		Find_via_roads s{ world };
		std::vector<glm::ivec2> path = s.bfs(x_pos, y_pos, nearby->x, nearby->y);
		Find_nearest_development s2{ world };
		std::vector<glm::ivec2> path_to_development = s2.bfs(x_pos, y_pos, 0, 0);
		unsigned int world_dist = (unsigned int) std::sqrt(
				std::pow(nearby->x - x_pos, 2)
				+ std::pow(nearby->y - y_pos, 2));
		if (path.size() > world_dist * 2
			&& (path_to_development.empty() || path_to_development.size() >= 2)) {
			Find_goal s3{ world };
			town->add_road(s3.bfs(x_pos, y_pos, nearby->x, nearby->y));
		}
	}
}

std::unique_ptr<glm::ivec2>
PCG::Towns::Agent_based::Connector_agent::find_nearby_road_segment()
{
	std::vector<glm::ivec2> nearby_roads;
	for (int i = -connector_distance; i <= connector_distance; ++i) {
		for (int j = -connector_distance; j <= connector_distance; ++j) {
			if (valid_world_coordinates(i + x_pos, j + x_pos)
				&& (i != 0 || j != 0)
				&& world->operator()(i + x_pos, j + y_pos) == Tile::road) {
				nearby_roads.emplace_back(i + x_pos, j + y_pos);
			}
		}
	}
	std::mt19937 mt{ prng() };
	std::unique_ptr<glm::ivec2> ptr;
	if (!nearby_roads.empty()) {
		std::shuffle(nearby_roads.begin(), nearby_roads.end(), mt);
		ptr = std::make_unique<glm::ivec2>(nearby_roads[0]);
	}
	return ptr;
}

PCG::Towns::Agent_based::Extender_agent::Extender_agent(
		World_ptr world, Agent_based* town)
		: Agent{ world, town }
{
}

void PCG::Towns::Agent_based::Extender_agent::update()
{
	Find_nearest_road s{ world };
	std::vector<glm::ivec2> result = s.bfs(x_pos, y_pos, 0, 0);
	if (use_road_segment(result)) {
		town->add_road(result);
	}
	std::mt19937 mt{ prng() };
	std::uniform_int_distribution<int> ds{ -1, 1 };
	int x_dir = ds(mt);
	int y_dir = ds(mt);
	if (valid_world_coordinates(x_pos + x_dir, y_pos + y_dir)
		&& !too_far(x_pos + x_dir, y_pos + y_dir)) {
		x_pos += x_dir;
		y_pos += y_dir;
	}
}

bool PCG::Towns::Agent_based::Extender_agent::too_far(int x, int y)
{
	Find_nearest_development s{ world };
	std::vector<glm::ivec2> path = s.bfs(x, y, 0, 0);
	if (path.empty()) {
		return std::sqrt(std::pow(x - world->get_width() / 2, 2)
						 + std::pow(y - world->get_height() / 2, 2)) > max_distance_from_development;
	} else {
		return path.size() > max_distance_from_development;
	}
}

bool PCG::Towns::Agent_based::Extender_agent::use_road_segment(
		const std::vector<glm::ivec2>& path)
{
	if (path.empty()) {
		return false;
	}
	if (road_count_in_range(max_extender_distance) > 3) {
		return false;
	}
	for (const glm::ivec2& p : path) {
		Find_nearest_development s{ world };
		std::vector<glm::ivec2> path_to_development = s.bfs(p.x, p.y, 0, 0);
		if (!path_to_development.empty() && path_to_development.size() < 2) {
			return false;
		}
	}
	return true;
}

PCG::Towns::Agent_based::Building_agent::Building_agent(
		World_ptr world, Agent_based* town)
		: Agent{ world, town }
{
}

void PCG::Towns::Agent_based::Building_agent::update()
{
	std::mt19937 mt{ prng() };
	std::shuffle(directions.begin(), directions.end(), mt);
	for (const glm::ivec2& dir : directions) {
		if (valid_world_coordinates(x_pos + dir.x, y_pos + dir.y)
			&& world->operator()(x_pos + dir.x, y_pos + dir.y) == Tile::road) {
			x_pos += dir.x;
			y_pos += dir.y;
			break;
		}
	}
	std::vector<glm::ivec2> nearby_land;
	for (int i = -builder_search_distance; i <= builder_search_distance; ++i) {
		for (int j = -builder_search_distance; j <= builder_search_distance; ++j) {
			if (valid_world_coordinates(i + x_pos, j + y_pos)
				&& (i != 0 || j != 0)
				&& world->operator()(i + x_pos, j + y_pos) == Tile::empty) {
				nearby_land.emplace_back(i + x_pos, j + y_pos);
			}
		}
	}
	if (!nearby_land.empty()) {
		std::shuffle(nearby_land.begin(), nearby_land.end(), mt);
		std::vector<glm::ivec2> building = suggest_building(nearby_land[0]);
		std::uniform_real_distribution<float> ds{ 0, 1 };
		if (building.size() > 3 && road_count_in_range(cols) > 10
			&& ds(mt) > 0.75) {
			town->add_building_to_world(building);
		}
	}
}

std::vector<glm::ivec2>
PCG::Towns::Agent_based::Building_agent::suggest_building(glm::ivec2 start_pos)
{
	std::vector<glm::ivec2> building_pieces;
	building_pieces.push_back(start_pos);
	std::vector<glm::ivec2> dirs;
	dirs.emplace_back(1, 1);
	dirs.emplace_back(-1, 1);
	dirs.emplace_back(1, -1);
	dirs.emplace_back(-1, -1);
	Find_nearest_development development_search{ world };
	Find_nearest_road road_search{ world };
	std::mt19937 mt{ prng() };
	while (building_pieces.size() < 10 && dirs.size() > 1) {
		bool failed = false;
		std::vector<glm::ivec2> new_pieces;
		std::shuffle(dirs.begin(), dirs.end(), mt);
		const glm::ivec2& d = dirs[0];
		for (const glm::ivec2& p : building_pieces) {
			glm::ivec2 temp{ p };
			temp.x += d.x;
			temp.y += d.y;
			if (std::find(building_pieces.begin(), building_pieces.end(), temp)
				== building_pieces.end()) {
				if (valid_world_coordinates(temp.x, temp.y)
					&& world->operator()(temp.x, temp.y) == Tile::empty) {
					std::vector<glm::ivec2> development_path = development_search.bfs(
							temp.x, temp.y, 0, 0);
					std::vector<glm::ivec2> road_path = road_search.bfs(
							temp.x, temp.y, 0, 0);
					if ((development_path.empty() || development_path.size() > 3
						 || std::find(building_pieces.begin(), building_pieces.end(),
									  development_path[development_path.size() - 1])
							!= building_pieces.end())
						&& road_path.size() > 2) {
						new_pieces.push_back(temp);
					} else {
						failed = true;
						dirs.erase(dirs.begin());
						break;
					}
				} else {
					failed = true;
					dirs.erase(dirs.begin());
					break;
				}
			}
		}
		if (!failed) {
			building_pieces.insert(building_pieces.end(), new_pieces.begin(),
								   new_pieces.end());
		}
	}
	return building_pieces;
}

PCG::Towns::Agent_based::Search::Search(World_ptr wrld)
		: world{ wrld }
{
}

std::vector<glm::ivec2>
PCG::Towns::Agent_based::Search::bfs(int x, int y, int goalX, int goalY)
{
	glm::ivec2 current_pos{ x, y };
	glm::ivec2 goal{ goalX, goalY };
	Node* current = new Node{ current_pos, nullptr, 0,
							  (int) world->get_width(), (int) world->get_height() };
	std::vector<Node*> nodes;
	nodes.reserve(4096);
	nodes.push_back(current);
	std::vector<Node*> open_nodes;
	std::unordered_set<glm::ivec2, Hasher> open_set;
	std::unordered_set<glm::ivec2, Hasher> closed_set;
	open_nodes.push_back(current);
	std::vector<glm::ivec2> ret;
	while (open_nodes.size() > 0) {
		current = open_nodes[0];
		open_nodes.erase(open_nodes.begin());
		open_set.erase(current->pos);
		closed_set.insert(current->pos);
		if (goal_reached(current, goal)) {
			ret = trace_path(current);
			break;
		}
		for (const glm::ivec2& neighbor : current->neighbors()) {
			if (open_set.find(neighbor) == open_set.end()
				&& closed_set.find(neighbor) == closed_set.end()
				&& valid_neighbor(neighbor)) {
				Node* node = new Node{ neighbor, current, current->depth + 1,
									   (int) world->get_width(), (int) world->get_height() };
				open_nodes.push_back(node);
				nodes.push_back(node);
				open_set.insert(neighbor);
			}
		}
	}
	for (Node* node : nodes) {
		delete node;
	}
	return ret;
}

std::vector<glm::ivec2>
PCG::Towns::Agent_based::Search::trace_path(Node const* final_node)
{
	std::vector<glm::ivec2> path;
	Node const* current = final_node;
	std::vector<glm::ivec2>::iterator it = path.begin();
	while (current->parent) {
		it = path.insert(it, current->pos);
		current = current->parent;
	}
	return path;
}

PCG::Towns::Agent_based::Find_nearest_road::Find_nearest_road(
		World_ptr world)
		: Search{ world }
{
}

bool PCG::Towns::Agent_based::Find_nearest_road::goal_reached(Node* current, glm::ivec2)
{
	return world->operator()(current->pos.x, current->pos.y) == Tile::road;
}

bool PCG::Towns::Agent_based::Find_nearest_road::valid_neighbor(glm::ivec2 potential_neighbor)
{
	return world->operator()(potential_neighbor.x, potential_neighbor.y)
		   != Tile::building;
}

PCG::Towns::Agent_based::Find_via_roads::Find_via_roads(
		World_ptr world)
		: Search{ world }
{
}

bool PCG::Towns::Agent_based::Find_via_roads::goal_reached(Node* current, glm::ivec2 goal)
{
	return current->pos.x == goal.x && current->pos.y == goal.y;
}

bool PCG::Towns::Agent_based::Find_via_roads::valid_neighbor(glm::ivec2 potential_neighbor)
{
	return world->operator()(potential_neighbor.x, potential_neighbor.y) == Tile::road;
}

PCG::Towns::Agent_based::Find_nearest_development::Find_nearest_development(
		World_ptr world)
		: Search{ world }
{
}

bool PCG::Towns::Agent_based::Find_nearest_development::goal_reached(
		Node* current, glm::ivec2)
{
	return world->operator()(current->pos.x, current->pos.y) == Tile::building;
}

bool PCG::Towns::Agent_based::Find_nearest_development::valid_neighbor(
		glm::ivec2)
{
	return true;
}

PCG::Towns::Agent_based::Find_goal::Find_goal(World_ptr world)
		: Search{ world }
{
}

bool PCG::Towns::Agent_based::Find_goal::goal_reached(Node* current, glm::ivec2 goal)
{
	return current->pos.x == goal.x && current->pos.y == goal.y;
}

bool PCG::Towns::Agent_based::Find_goal::valid_neighbor(glm::ivec2 potential_neighbor)
{
	return world->operator()(potential_neighbor.x, potential_neighbor.y)
		   != Tile::building;
}

PCG::Towns::Agent_based::Node::Node(glm::ivec2 location, Node* p, int d, int c, int r)
		: pos{ location }, parent{ p }, depth{ d }, cols{ c }, rows{ r }
{
}

std::vector<glm::ivec2> PCG::Towns::Agent_based::Node::neighbors()
{
	std::vector<glm::ivec2> neighbors;
	for (int i = -1; i <= 1; ++i) {
		for (int j = -1; j <= 1; ++j) {
			glm::ivec2 new_node{ pos.x + i, pos.y + j };
			if (valid_world_coordinates(new_node.x, new_node.y)) {
				neighbors.push_back(new_node);
			}
		}
	}
	return neighbors;
}

bool PCG::Towns::Agent_based::Node::valid_world_coordinates(int x, int y)
{
	return x > 0 && x < cols && y > 0 && y < rows;
}

std::size_t PCG::Towns::Agent_based::Hasher::operator()(const glm::ivec2& point) const
{
	size_t res = 17;
	res = res * 31 + std::hash<int>()(point.x);
	res = res * 31 + std::hash<int>()(point.y);
	return res;
}
