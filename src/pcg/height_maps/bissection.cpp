/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code by John Olsson, and is subject to the following license:
 * Copyright (C) 1999  John Olsson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "bissection.h"

#include <stack>
#include <tuple>

#include <png++/png.hpp>

PCG::Heightmaps::Fractal_bissection::Fractal_bissection(long width, long height, int no_faults,
														int water_perc, int ice_perc)
		: mt{ 666 }, x_range{ width }, y_range{ height }, number_of_faults{ no_faults },
		  percent_water{ water_perc }, percent_ice{ ice_perc }
{
	init();
}

PCG::Heightmaps::Fractal_bissection::Fractal_bissection(unsigned seed, long width, long height,
														int no_faults, int water_perc, int ice_perc)
		: mt{ seed }, x_range{ width }, y_range{ height }, number_of_faults{ no_faults },
		  percent_water{ water_perc }, percent_ice{ ice_perc }
{
	init();
}

void PCG::Heightmaps::Fractal_bissection::init()
{
	world_map_array.resize(x_range * y_range);
	sin_iter_phi.resize(2 * x_range);
	for (long i = 0; i < x_range; ++i) {
		sin_iter_phi[i] = sin_iter_phi[i + x_range] =
				std::sin(i * 2.0 * M_PI / x_range);
	}
	y_range_div_2 = y_range / 2;
	y_range_div_pi = y_range / M_PI;
	for (long j = 0, row = 0; j < x_range; ++j) {
		world_map_array[row] = 0;
		for (long i = 1; i < y_range; ++i)
			world_map_array[i + row] = std::numeric_limits<int>::min();
		row += y_range;
	}
	for (int a = 0; a < number_of_faults; ++a) {
		generate_world_map();
	}
	long index2 = (x_range / 2) * y_range;
	for (long j = 0, row = 0; j < x_range / 2; ++j) {
		for (long i = 1; i < y_range; ++i) {
			world_map_array[row + index2 + y_range - i] = world_map_array[row + i];
		}
		row += y_range;
	}
	for (long j = 0, row = 0; j < x_range; ++j) {
		int color = world_map_array[row];
		for (long i = 1; i < y_range; ++i) {
			int cur = world_map_array[row + i];
			if (cur != std::numeric_limits<int>::min()) {
				color += cur;
			}
			world_map_array[row + i] = color;
		}
		row += y_range;
	}
	int max_z = 1, min_z = -1;
	for (int color : world_map_array) {
		if (color > max_z)
			max_z = color;
		if (color < min_z)
			min_z = color;
	}
	histogram.resize(256, 0);
	for (long j = 0, row = 0; j < x_range; ++j) {
		for (long i = 0; i < y_range; ++i) {
			int color = world_map_array[row + i];
			color = (int) (((float) (color - min_z + 1) / (float) (max_z - min_z + 1)) * 30) + 1;
			histogram[color]++;
		}
		row += y_range;
	}
	long threshold = percent_water * x_range * y_range / 100;
	for (int j = 0, count = 0; j < 256; ++j) {
		count += histogram[j];
		if (count > threshold) {
			threshold = j;
			break;
		}
	}
	threshold = threshold * (max_z - min_z + 1) / 30 + min_z;
	for (long j = 0, row = 0; j < x_range; ++j) {
		for (long i = 0; i < y_range; ++i) {
			int color = world_map_array[row + i];
			if (color < threshold)
				color = (int) (((float) (color - min_z) / (float) (threshold - min_z)) * 15) + 1;
			else
				color = (int) (((float) (color - threshold) / (float) (max_z - threshold)) * 15) + 16;
			if (color < 1)
				color = 1;
			if (color > 255)
				color = 31;
			world_map_array[row + i] = color;
		}
		row += y_range;
	}
	threshold = percent_ice * x_range * y_range / 100;
	if (threshold > 0 && threshold < x_range * y_range) {
		filled_pixels = 0;
		for (long i = 0; i < y_range; ++i) {
			for (long j = 0, row = 0; j < x_range; ++j) {
				int color = world_map_array[row + i];
				if (color < 32)
					flood_fill4(j, i, color);
				if (filled_pixels > threshold)
					goto NorthPoleFinished;
				row += y_range;
			}
		}
		NorthPoleFinished:
		filled_pixels = 0;
		for (long i = y_range - 1; i > 0; --i) {
			for (long j = 0, row = 0; j < x_range; j++) {
				int color = world_map_array[row + i];
				if (color < 32)
					flood_fill4(j, i, color);
				if (filled_pixels > threshold)
					goto Finished;
				row += y_range;
			}
		}
		Finished:;
	}
}

void PCG::Heightmaps::Fractal_bissection::flood_fill4(long x, long y, int old_color)
{
	std::stack<std::pair<long, long>> stack;
	stack.emplace(x, y);
	while (!stack.empty()) {
		std::tie(x, y) = stack.top();
		stack.pop();
		if (world_map_array[x * y_range + y] == old_color) {
			if (world_map_array[x * y_range + y] < 16)
				world_map_array[x * y_range + y] = 32;
			else
				world_map_array[x * y_range + y] += 17;
			++filled_pixels;
			if (y - 1 > 0)
				stack.emplace(x, y - 1);
			if (y + 1 < y_range)
				stack.emplace(x, y + 1);
			if (x - 1 < 0)
				stack.emplace(x_range - 1, y);
			else
				stack.emplace(x - 1, y);
			if (x + 1 >= x_range)
				stack.emplace(0, y);
			else
				stack.emplace(x + 1, y);
		}
	}
}

void PCG::Heightmaps::Fractal_bissection::generate_world_map()
{
	std::uniform_int_distribution<short> db{ 0, 1 };
	bool flag1 = (bool) db(mt);
	std::uniform_real_distribution<float> dd{ -M_PI_2, M_PI_2 };
	float alpha = dd(mt);
	float beta = dd(mt);
	float tan_b = std::tan(std::acos(std::cos(alpha) * std::cos(beta)));
	long row = 0;
	long xsi = (int) (x_range / 2 - (x_range / M_PI) * beta);
	for (long phi = 0; phi < x_range / 2; ++phi) {
		long theta = (int) (y_range_div_pi * std::atan(sin_iter_phi[xsi - phi + x_range] * tan_b)
							+ y_range_div_2);
		if (flag1) {
			if (world_map_array[row + theta] != std::numeric_limits<int>::min())
				world_map_array[row + theta]--;
			else
				world_map_array[row + theta] = -1;
		} else {
			if (world_map_array[row + theta] != std::numeric_limits<int>::min())
				world_map_array[row + theta]++;
			else
				world_map_array[row + theta] = 1;
		}
		row += y_range;
	}
}

void PCG::Heightmaps::Fractal_bissection::export_to_png(const std::string& file)
{
	png::image<png::rgb_pixel> image{ (unsigned int) x_range, (unsigned int) y_range };
	for (long i = y_range - 1; i > 0; --i) {
		for (long j = 0, row = 0; j < x_range; j++) {
			int color = world_map_array[row + i];
			image[i][j] = png::rgb_pixel(red[color], green[color], blue[color]);
			row += y_range;
		}
	}
	image.write(file);
}
