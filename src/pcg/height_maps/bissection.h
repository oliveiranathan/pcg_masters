/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code by John Olsson, and is subject to the following license:
 * Copyright (C) 1999  John Olsson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#pragma once

#include <string>
#include <random>
#include <vector>

namespace PCG {

	namespace Heightmaps {

		/// Based on code for a Fractal World Generator by John Olsson, with fixes by drow
		/// The code is available at http://donjon.bin.sh/code/world/

		class Fractal_bissection {
		public:
			Fractal_bissection(long width, long height, int no_faults, int water_perc, int ice_perc);

			Fractal_bissection(unsigned seed, long width, long height, int no_faults,
							   int water_perc, int ice_perc);

			void export_to_png(const std::string& file);

		private:

			std::mt19937 mt;

			std::vector<int> world_map_array;
			long x_range, y_range;
			int number_of_faults;
			int percent_water;
			int percent_ice;

			std::vector<int> histogram;
			unsigned char red[49] = { 0, 0, 0, 0, 0, 0, 0, 0, 34, 68, 102, 119, 136, 153, 170, 187, 0,
									  34, 34, 119, 187, 255, 238, 221, 204, 187, 170, 153, 136, 119,
									  85, 68, 255, 250, 245, 240, 235, 230, 225, 220, 215, 210, 205,
									  200, 195, 190, 185, 180, 175 };
			unsigned char green[49] = { 0, 0, 17, 51, 85, 119, 153, 204, 221, 238, 255, 255, 255, 255,
										255, 255, 68, 102, 136, 170, 221, 187, 170, 136, 136, 102, 85,
										85, 68, 51, 51, 34, 255, 250, 245, 240, 235, 230, 225, 220,
										215, 210, 205, 200, 195, 190, 185, 180, 175 };
			unsigned char blue[49] = { 0, 68, 102, 136, 170, 187, 221, 255, 255, 255, 255, 255, 255,
									   255, 255, 255, 0, 0, 0, 0, 0, 34, 34, 34, 34, 34, 34, 34, 34,
									   34, 17, 0, 255, 250, 245, 240, 235, 230, 225, 220, 215, 210,
									   205, 200, 195, 190, 185, 180, 175 };
			long filled_pixels;
			float y_range_div_2, y_range_div_pi;
			std::vector<float> sin_iter_phi;

			void init();

			void generate_world_map();

			void flood_fill4(long x, long y, int old_color);
		};

	}

}
