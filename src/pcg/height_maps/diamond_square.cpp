/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "diamond_square.h"

#include <png++/png.hpp>

PCG::Heightmaps::Fractal_diamond_square::Fractal_diamond_square(long size, float scale, float roughness)
		: mt{ 666 }, size_{ size }, scale_{ scale }, roughness_{ roughness }
{
	init();
}

PCG::Heightmaps::Fractal_diamond_square::Fractal_diamond_square(unsigned seed, long size, float scale,
																float roughness)
		: mt{ seed }, size_{ size }, scale_{ scale }, roughness_{ roughness }
{
	init();
}

void PCG::Heightmaps::Fractal_diamond_square::init()
{
	if (!power_of_2(size_) || (size_ == 1)) {
		return;
	}
	long sub_size = size_;
	long size = size_ + 1;
	fa.resize(size * size);
	float ratio = (float) std::pow(2., -roughness_);
	float scale = scale_ * ratio;
	long stride = sub_size / 2;
	fa[(0 * size) + 0] = fa[(sub_size * size) + 0]
			= fa[(sub_size * size) + sub_size] = fa[(0 * size) + sub_size] = 0.f;
	while (stride) {
		for (long i = stride; i < sub_size; i += stride) {
			for (long j = stride; j < sub_size; j += stride) {
				fa[(i * size) + j] = scale * fract_rand(.5) + avg_square_vals(i, j, stride, size);
				j += stride;
			}
			i += stride;
		}
		bool oddline = false;
		for (long i = 0; i < sub_size; i += stride) {
			oddline = !oddline;
			for (long j = 0; j < sub_size; j += stride) {
				if ((oddline) && !j)
					j += stride;
				fa[(i * size) + j] = scale * fract_rand(.5f)
									 + avg_diamond_vals(i, j, stride, size, sub_size);
				if (i == 0)
					fa[(sub_size * size) + j] = fa[(i * size) + j];
				if (j == 0)
					fa[(i * size) + sub_size] = fa[(i * size) + j];
				j += stride;
			}
		}
		scale *= ratio;
		stride >>= 1;
	}
}

bool PCG::Heightmaps::Fractal_diamond_square::power_of_2(long x)
{
	return ((x > 0) && ((x & (~x + 1)) == x));
}

float PCG::Heightmaps::Fractal_diamond_square::fract_rand(float v)
{
	return randnum(-v, v);
}

float PCG::Heightmaps::Fractal_diamond_square::randnum(float min, float max)
{
	std::uniform_real_distribution<float> d{ min, max };
	return d(mt);
}

float PCG::Heightmaps::Fractal_diamond_square::avg_diamond_vals(long i, long j, long stride,
																long size, long sub_size)
{
	if (i == 0)
		return ((float) (fa[(i * size) + j - stride] + fa[(i * size) + j + stride] +
						 fa[((sub_size - stride) * size) + j] + fa[((i + stride) * size) + j]) * .25f);
	else if (i == size - 1)
		return ((float) (fa[(i * size) + j - stride] + fa[(i * size) + j + stride] +
						 fa[((i - stride) * size) + j] + fa[((0 + stride) * size) + j]) * .25f);
	else if (j == 0)
		return ((float) (fa[((i - stride) * size) + j] + fa[((i + stride) * size) + j] +
						 fa[(i * size) + j + stride] + fa[(i * size) + sub_size - stride]) * .25f);
	else if (j == size - 1)
		return ((float) (fa[((i - stride) * size) + j] + fa[((i + stride) * size) + j] +
						 fa[(i * size) + j - stride] + fa[(i * size) + 0 + stride]) * .25f);
	else
		return ((float) (fa[((i - stride) * size) + j] + fa[((i + stride) * size) + j] +
						 fa[(i * size) + j - stride] + fa[(i * size) + j + stride]) * .25f);
}

float PCG::Heightmaps::Fractal_diamond_square::avg_square_vals(long i, long j, long stride, long size)
{
	return ((fa[((i - stride) * size) + j - stride] + fa[((i - stride) * size) + j + stride] +
			 fa[((i + stride) * size) + j - stride] + fa[((i + stride) * size) + j + stride]) * .25f);
}

void PCG::Heightmaps::Fractal_diamond_square::export_to_png(const std::string& file)
{
	long size = size_ + 1;
	png::image<png::rgb_pixel> image{ (unsigned int) size, (unsigned int) size };
	float min = std::numeric_limits<float>::max(), max = std::numeric_limits<float>::min();
	for (long i = 0; i < size; ++i) {
		for (long j = 0; j < size; ++j) {
			min = std::min(min, fa[i * size + j]);
			max = std::max(max, fa[i * size + j]);
		}
	}
	for (long i = 0; i < size; ++i) {
		for (long j = 0; j < size; ++j) {
			float h = fa[i * size + j];
			unsigned char v = (unsigned char)(((h - min) / (max - min)) * 255);
			image[i][j] = png::rgb_pixel(v, v, v);
		}
	}
	image.write(file);
}
