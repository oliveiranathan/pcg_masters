/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include <string>
#include <random>
#include <vector>

namespace PCG {

	namespace Heightmaps {

		/// Based on code for a Diamond-Square fractal world generator by Paul Martz
		/// The code is available at http://www.gameprogrammer.com/fractal.html

		class Fractal_diamond_square {
		public:
			Fractal_diamond_square(long size, float scale, float roughness);

			Fractal_diamond_square(unsigned seed, long size, float scale, float roughness);

			void export_to_png(const std::string& file);

		private:

			std::mt19937 mt;

			std::vector<float> fa;

			long size_;
			float scale_;
			float roughness_;

			void init();

			inline bool power_of_2(long size);

			inline float fract_rand(float v);

			float avg_square_vals(long i, long j, long stride, long size);

			float avg_diamond_vals(long i, long j, long stride, long size, long sub_size);

			inline float randnum(float min, float max);
		};

	}

}
