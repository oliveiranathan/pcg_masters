/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code by ARM, and is subject to the following license:
 * This proprietary software may be used only as
 * authorised by a licensing agreement from ARM Limited
 * (C) COPYRIGHT 2013 ARM Limited
 * ALL RIGHTS RESERVED
 * The entire notice above must be reproduced on all authorised
 * copies and copies may only be made to the extent permitted
 * by a licensing agreement from ARM Limited.
 */

#include "particles.h"
#include "../../utils/loaders.h"
#include "../noise/perlin_noise.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>

#include <iterator>

void PCG::Elements::Particles::startup()
{
	std::vector<OpenGL::Shader_ptr> shaders;
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/particles.vert",
												OpenGL::Shader::Stage::vertex));
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/particles.frag",
												OpenGL::Shader::Stage::fragment));
	program = OpenGL::Program::create(shaders);
	program->use();
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glViewport(0, 0, width_, height_);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_PROGRAM_POINT_SIZE);
	initialise_texture();
	initialize_particle_data_array();
	i_loc_position = glGetAttribLocation(program->get_program(), "a_v3Position");
	i_loc_velocity = glGetAttribLocation(program->get_program(), "a_v3Velocity");
	i_loc_particle_times = glGetAttribLocation(program->get_program(), "a_v3ParticleTimes");
	i_loc_mvp = glGetUniformLocation(program->get_program(), "mvp");
	i_loc_gravity = glGetUniformLocation(program->get_program(), "u_v3gravity");
	i_loc_sampler = glGetUniformLocation(program->get_program(), "s_texture");
	i_loc_color = glGetUniformLocation(program->get_program(), "u_v3colour");
	glm::mat4 model_view = glm::rotate(glm::mat4 {}, glm::radians(-90.f), glm::vec3{ 1.f, 0.f, 0.f });
	model_view = glm::translate(model_view, glm::vec3{ 0.f, 2.f, -0.5f });
	glm::mat4 perspective = glm::perspective(45.0f, (float) width_ / (float) height_, 0.01f, 100.0f);
	model_view_perspective = perspective * model_view;
	gravity.x = gravity.y = 0.0f;
	gravity.z = gravity_z;
	base_colour.x = base_colour_red;
	base_colour.y = base_colour_green;
	base_colour.z = base_colour_blue;
	last_time = glfwGetTime();
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, num_particles * sizeof(Particle), nullptr, GL_DYNAMIC_DRAW);
}

void PCG::Elements::Particles::render(double current_time)
{
	frame_time = current_time - last_time;
	last_time = current_time;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	update_particle_age();
	program->use();
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, num_particles * sizeof(Particle), &particle_data[0]);
	glVertexAttribPointer(i_loc_position, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), 0);
	glEnableVertexAttribArray(i_loc_position);
	glVertexAttribPointer(i_loc_velocity, 3, GL_FLOAT, GL_FALSE, sizeof(Particle),
						  (GLvoid*) (3 * sizeof(float)));
	glEnableVertexAttribArray(i_loc_velocity);
	glVertexAttribPointer(i_loc_particle_times, 3, GL_FLOAT, GL_FALSE, sizeof(Particle),
						  (GLvoid*) (6 * sizeof(float)));
	glEnableVertexAttribArray(i_loc_particle_times);
	glUniformMatrix4fv(i_loc_mvp, 1, GL_FALSE, glm::value_ptr(model_view_perspective));
	glUniform3fv(i_loc_gravity, 1, glm::value_ptr(gravity));
	glUniform3fv(i_loc_color, 1, glm::value_ptr(base_colour));
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glUniform1i(i_loc_sampler, 0);
	glDrawArrays(GL_POINTS, 0, num_particles);
}

void PCG::Elements::Particles::shutdown()
{
	glDeleteTextures(1, &texture_id);
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
}

void PCG::Elements::Particles::initialise_texture()
{
	std::ifstream file("res/assets/smoke.raw", std::ios::binary);
	file >> std::noskipws;
	std::vector<unsigned char> texture_data{ std::istreambuf_iterator<char>(file),
											 std::istreambuf_iterator<char>() };
	glCreateTextures(GL_TEXTURE_2D, 1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexStorage2D(GL_TEXTURE_2D, 8, GL_R8, texture_width, texture_height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texture_width, texture_height,
						GL_RED, GL_UNSIGNED_BYTE, &texture_data[0]);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void PCG::Elements::Particles::initialize_particle_data_array()
{
	Disc_emitter emitter(emitter_radius, emitter_angle);
	Particle particle;
	particle_data.resize(num_particles);
	for (size_t i = 0; i < num_particles; ++i) {
		emitter.get_particle(particle);
		particle_data[i].init_pos = particle.init_pos;
		particle_data[i].init_vel = velocity_factor * particle.init_vel;
		particle_data[i].delay = 4.0f * particle.delay;
		particle_data[i].lifetime = lifetime_factor * particle.lifetime;
		particle_data[i].age = 0.0f;
	}
}

void PCG::Elements::Particles::update_particle_age()
{
	for (size_t i = 0; i < num_particles; ++i) {
		particle_data[i].age += frame_time;
		if (particle_data[i].age > particle_data[i].lifetime) {
			particle_data[i].age = 0.0f;
		}
	}
}

PCG::Elements::Particles::Disc_emitter::Disc_emitter(float rad, float angle)
		: disc_radius{ rad }, max_emission_angle{ angle * (float) M_PI / 180.f }
{
}

void PCG::Elements::Particles::Disc_emitter::get_particle(PCG::Elements::Particles::Particle& part)
{
	std::uniform_real_distribution<float> ds{ 0.f, 1.f };
	float rad = ds(mt);
	float polar_angle = ds(mt);
	polar_angle *= 2 * M_PI;
	part.init_pos.x = disc_radius * rad * std::cos(polar_angle);
	part.init_pos.y = disc_radius * rad * std::sin(polar_angle);
	part.init_pos.z = 0.0f;
	float azimuth_angle = ds(mt);
	azimuth_angle *= max_emission_angle;
	part.init_vel.x = std::sin(azimuth_angle) * std::cos(polar_angle);
	part.init_vel.y = std::sin(azimuth_angle) * std::sin(polar_angle);
	part.init_vel.z = std::cos(azimuth_angle);
	part.lifetime = ds(mt);
	part.delay = ds(mt);
}
