/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code by Ian McEwan, and is subject to the following license:
 * Copyright (c) 2011 Ashima Arts
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "clouds.h"

void PCG::Elements::Clouds::startup()
{
	std::vector<OpenGL::Shader_ptr> shaders;
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/clouds.vert",
												OpenGL::Shader::Stage::vertex));
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/clouds.frag",
												OpenGL::Shader::Stage::fragment));
	program = OpenGL::Program::create(shaders);
	program->use();
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glClearColor(0.0f, 0.749f, 1.0f, 1.0f);
}

void PCG::Elements::Clouds::render(double)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void PCG::Elements::Clouds::shutdown()
{
	glDeleteVertexArrays(1, &vao);
}
