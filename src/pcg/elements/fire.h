/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code by Linnéa Mellblom, which uses the following license:
 * Copyright (C) 2011 by Stefan Gustavson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "../../utils/application.h"
#include "../../utils/program.h"

#include <glm/mat4x4.hpp>

namespace PCG {

	namespace Elements {

		/// Based on the code for WebGL procedural fire by Linnéa Mellblom
		/// The code is available at https://github.com/lmellblom/fire-webGL

		class Fire : public OpenGL::Renderer {
		public:
		protected:
			void startup() override;

			void render(double current_time) override;

			void shutdown() override;

		private:
			glm::mat4 p_matrix, mv_matrix;
			OpenGL::Program_ptr program;

			GLint p_matrix_uniform;
			GLint mv_matrix_uniform;
			GLint u_time;

			GLuint vao{ 0 };
		};

	}

}
