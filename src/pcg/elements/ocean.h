/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code by Alexander Alekseev, and is subject to the following license:
 * "Seascape" by Alexander Alekseev aka TDM - 2014
 * License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * Contact: tdmaav@gmail.com
 */

#pragma once

#include "../../utils/application.h"
#include "../../utils/program.h"

namespace PCG {

	namespace Elements {

		/// Based on the code for Seascape by Alexander Alekseev
		/// The code is available at https://www.shadertoy.com/view/Ms2SD1

		class Ocean : public OpenGL::Renderer {
		public:
		protected:
			void startup() override;

			void render(double current_time) override;

			void shutdown() override;

		private:
			OpenGL::Program_ptr program;

			GLint i_resolution;
			GLint i_global_time;

			GLuint vao{ 0 };
		};

	}

}
