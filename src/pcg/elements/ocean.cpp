/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code by Alexander Alekseev, and is subject to the following license:
 * "Seascape" by Alexander Alekseev aka TDM - 2014
 * License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * Contact: tdmaav@gmail.com
 */

#include "ocean.h"

void PCG::Elements::Ocean::startup()
{
	std::vector<OpenGL::Shader_ptr> shaders;
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/ocean.vert", OpenGL::Shader::Stage::vertex));
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/ocean.frag", OpenGL::Shader::Stage::fragment));
	program = OpenGL::Program::create(shaders);
	program->use();
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	i_resolution = glGetUniformLocation(program->get_program(), "iResolution");
	static const GLfloat res[] = { (float) width_, (float) height_, 0 };
	glUniform3fv(i_resolution, 1, res);
	i_global_time = glGetUniformLocation(program->get_program(), "iGlobalTime");
	glClearColor(0.0f, 0.25f, 0.0f, 1.0f);
}

void PCG::Elements::Ocean::render(double current_time)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glUniform1f(i_global_time, (float) current_time);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void PCG::Elements::Ocean::shutdown()
{
	glDeleteVertexArrays(1, &vao);
}
