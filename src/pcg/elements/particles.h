/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code by ARM, and is subject to the following license:
 * This proprietary software may be used only as
 * authorised by a licensing agreement from ARM Limited
 * (C) COPYRIGHT 2013 ARM Limited
 * ALL RIGHTS RESERVED
 * The entire notice above must be reproduced on all authorised
 * copies and copies may only be made to the extent permitted
 * by a licensing agreement from ARM Limited.
 */

#pragma once

#include "../../utils/application.h"
#include "../../utils/program.h"

#include <random>

#include <glm/glm.hpp>

namespace PCG {

	namespace Elements {

		/// Based on the code from the ARM®MALI Particle System Tutorial
		/// The code is available at http://malideveloper.arm.com/resources/sdks/opengl-es-sdk-for-linux/

		class Particles : public OpenGL::Renderer {
		public:

		protected:
			void startup() override;

			void render(double current_time) override;

			void shutdown() override;

		private:
			struct Particle {
				glm::vec3 init_pos;
				glm::vec3 init_vel;
				float delay;
				float lifetime;
				float age;
			};

			class Disc_emitter {
			public:
				Disc_emitter(float disc_radius, float angle);

				void get_particle(Particle& part);

			private:
				float disc_radius;
				float max_emission_angle;
				std::mt19937 mt{ 666 };
			};

			void initialise_texture();

			void initialize_particle_data_array();

			void update_particle_age();

			static const size_t num_particles = 2800;
			static const size_t texture_width = 128;
			static const size_t texture_height = 128;
			static constexpr float gravity_z = -0.06f;
			static constexpr float base_colour_red = 0.5f;
			static constexpr float base_colour_green = 0.5f;
			static constexpr float base_colour_blue = 0.7f;
			static constexpr float emitter_radius = 0.1f;
			static constexpr float emitter_angle = 30.f;
			static constexpr float velocity_factor = 0.5f;
			static constexpr float lifetime_factor = 6.5f;

			OpenGL::Program_ptr program;
			GLuint texture_id;
			GLint i_loc_position;
			GLint i_loc_velocity;
			GLint i_loc_particle_times;
			GLint i_loc_mvp;
			GLint i_loc_gravity;
			GLint i_loc_color;
			GLint i_loc_sampler;
			double last_time;
			double frame_time;
			glm::mat4 model_view_perspective;
			glm::vec3 gravity;
			glm::vec3 base_colour;
			std::vector<Particle> particle_data;
			GLuint vbo;
			GLuint vao;
		};

	}

}
