/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 *
 * This source is based on code by Linnéa Mellblom, which uses the following license:
 * Copyright (C) 2011 by Stefan Gustavson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "fire.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

void PCG::Elements::Fire::startup()
{
	std::vector<OpenGL::Shader_ptr> shaders;
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/fire.vert",
												OpenGL::Shader::Stage::vertex));
	shaders.push_back(OpenGL::Shader::from_file("res/shaders/fire.frag",
												OpenGL::Shader::Stage::fragment));
	program = OpenGL::Program::create(shaders);
	program->use();
	p_matrix_uniform = glGetUniformLocation(program->get_program(), "uPMatrix");
	mv_matrix_uniform = glGetUniformLocation(program->get_program(), "uMVMatrix");
	u_time = glGetUniformLocation(program->get_program(), "uTime");
	p_matrix = glm::perspective<GLfloat>(45.f, (GLfloat) width_ / height_, 0.1f, 100.f);
	mv_matrix = glm::translate(glm::mat4{}, { 0.f, 0.f, -3.f });
	glUniformMatrix4fv(p_matrix_uniform, 1, GL_FALSE, glm::value_ptr(p_matrix));
	glUniformMatrix4fv(mv_matrix_uniform, 1, GL_FALSE, glm::value_ptr(mv_matrix));
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glViewport(0, 0, width_, height_);
}

void PCG::Elements::Fire::render(double current_time)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUniform1f(u_time, (GLfloat) current_time);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void PCG::Elements::Fire::shutdown()
{
	glDeleteVertexArrays(1, &vao);
}
