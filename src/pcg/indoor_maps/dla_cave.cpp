/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "dla_cave.h"

PCG::Indoor_maps::DLA_cave::DLA_cave(long width, long height, float percentage_floor, bool allow_ortho)
		: map{ width, height }, mt{ 666 }, orthogonal_allowed{ allow_ortho },
		  max_allocated_perc{ percentage_floor }
{
	init();
}

PCG::Indoor_maps::DLA_cave::DLA_cave(unsigned seed, long width, long height,
									 float percentage_floor, bool allow_ortho)
		: map{ width, height }, mt{ seed }, orthogonal_allowed{ allow_ortho },
		  max_allocated_perc{ percentage_floor }
{
	init();
}

void PCG::Indoor_maps::DLA_cave::init()
{
	for (long x = 0; x < map.get_width(); ++x) {
		for (long y = 0; y < map.get_height(); ++y) {
			map(x, y) = Tile::wall;
		}
	}
	bool builder_spawned = false;
	unsigned char builder_move_direction = 0;
	long allocated_blocks = 0;
	long root_x = map.get_width() / 2;
	long root_y = map.get_height() / 2;
	long stepped = 0;
	long cx = 0;
	long cy = 0;
	while (float(allocated_blocks) / (map.get_width() * map.get_height()) < max_allocated_perc) {
		if (!builder_spawned) {
			cx = 2 + (int) std::floor(dist(mt) * (map.get_width() - 2));
			cy = 2 + (int) std::floor(dist(mt) * (map.get_height() - 2));
			if (std::abs(root_x - cx) <= 0 && std::abs(root_y - cy) <= 0) {
				if (map(cx, cy) == Tile::wall) {
					map(cx, cy) = Tile::floor;
				}
			} else {
				builder_spawned = true;
				builder_move_direction = (unsigned char) std::floor(dist(mt) * 8);
				stepped = 0;
			}
			++allocated_blocks; /// Moved to here to prevent from locking up...
		} else {
			if (builder_move_direction == 0 && cy > 0) {
				cy--;
				stepped++;
			} else if (builder_move_direction == 1 && cx < map.get_width()) {
				cx++;
				stepped++;
			} else if (builder_move_direction == 2 && cy < map.get_height()) {
				cy++;
				stepped++;
			} else if (builder_move_direction == 3 && cx > 0) {
				cx++;
				stepped++;
			} else if (builder_move_direction == 4 && cx < map.get_width() && cy > 0) {
				cy--;
				cx++;
				stepped++;
			} else if (builder_move_direction == 5 && cx < map.get_width() && cy < map.get_height()) {
				cy++;
				cx++;
				stepped++;
			} else if (builder_move_direction == 6 && cx > 0 && cy < map.get_height()) {
				cy++;
				cx--;
				stepped++;
			} else if (builder_move_direction == 7 && cx > 0 && cy > 0) {
				cy--;
				cx--;
				stepped++;
			}
			if (cx < map.get_width() - 1 && cy < map.get_height() - 1
				&& cx > 1 && cy > 1 && stepped <= 5) {
				if (map(cx + 1, cy) == Tile::floor) {
					if (map(cx, cy) == Tile::wall) {
						map(cx, cy) = Tile::floor;
						allocated_blocks++;
					}
				} else if (map(cx - 1, cy) == Tile::floor) {
					if (map(cx, cy) == Tile::wall) {
						map(cx, cy) = Tile::floor;
						allocated_blocks++;
					}
				} else if (map(cx, cy + 1) == Tile::floor) {
					if (map(cx, cy) == Tile::wall) {
						map(cx, cy) = Tile::floor;
						allocated_blocks++;
					}
				} else if (map(cx, cy - 1) == Tile::floor) {
					if (map(cx, cy) == Tile::wall) {
						map(cx, cy) = Tile::floor;
						allocated_blocks++;
					}
				} else if (map(cx + 1, cy - 1) == Tile::floor) {
					if (map(cx, cy) == Tile::wall) {
						map(cx, cy) = Tile::floor;
						allocated_blocks++;
						if (!orthogonal_allowed) {
							map(cx + 1, cy) = Tile::floor;
							allocated_blocks++;
						}
					}
				} else if (map(cx + 1, cy + 1) == Tile::floor) {
					if (map(cx, cy) == Tile::wall) {
						map(cx, cy) = Tile::floor;
						allocated_blocks++;
						if (!orthogonal_allowed) {
							map(cx + 1, cy) = Tile::floor;
							allocated_blocks++;
						}
					}
				} else if (map(cx - 1, cy + 1) == Tile::floor) {
					if (map(cx, cy) == Tile::wall) {
						map(cx, cy) = Tile::floor;
						allocated_blocks++;
						if (!orthogonal_allowed) {
							map(cx - 1, cy) = Tile::floor;
							allocated_blocks++;
						}
					}
				} else if (map(cx - 1, cy - 1) == Tile::floor) {
					if (map(cx, cy) == Tile::wall) {
						map(cx, cy) = Tile::floor;
						allocated_blocks++;
						if (!orthogonal_allowed) {
							map(cx - 1, cy) = Tile::floor;
							allocated_blocks++;
						}
					}
				}
			} else {
				builder_spawned = false;
			}
		}
	}
}

void PCG::Indoor_maps::DLA_cave::printmap(std::ostream& os) const
{
	for (long yi = 0; yi < map.get_height(); ++yi) {
		for (long xi = 0; xi < map.get_width(); ++xi) {
			switch (map(xi, yi)) {
				case Tile::wall:
					os << "#";
					break;
				case Tile::floor:
					os << ".";
					break;
			}
		}
		os << "\n";
	}
}

std::ostream& PCG::Indoor_maps::operator<<(std::ostream& os, const DLA_cave& c)
{
	c.printmap(os);
	return os;
}
