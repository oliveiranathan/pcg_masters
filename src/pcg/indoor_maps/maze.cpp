/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "maze.h"

#include <list>

PCG::Indoor_maps::Simple_maze::Simple_maze(int width, int height)
		: mt{ 666 }, maze{ width, height }
{
	init();
}

PCG::Indoor_maps::Simple_maze::Simple_maze(unsigned seed, int width, int height)
		: mt{ seed }, maze{ width, height }
{
	init();
}

void PCG::Indoor_maps::Simple_maze::init()
{
	std::list<std::pair<int, int>> drillers;
	for (long x = 0; x < maze.get_width(); ++x)
		for (long y = 0; y < maze.get_height(); ++y)
			maze(x, y) = Tile::wall;
	drillers.push_back(std::make_pair(maze.get_width() / 2, maze.get_height() / 2));
	while (!drillers.empty()) {
		std::list<std::pair<int, int>>::iterator m, _m;
		m = drillers.begin();
		_m = drillers.end();
		while (m != _m) {
			bool remove_driller = false;
			switch (dist(mt)) {
				case 0:
					m->second -= 2;
					if (m->second < 0 || maze(m->first, m->second) == Tile::floor) {
						remove_driller = true;
						break;
					}
					maze(m->first, m->second + 1) = Tile::floor;
					break;
				case 1:
					m->second += 2;
					if (m->second >= maze.get_height() || maze(m->first, m->second) == Tile::floor) {
						remove_driller = true;
						break;
					}
					maze(m->first, m->second - 1) = Tile::floor;
					break;
				case 2:
					m->first -= 2;
					if (m->first < 0 || maze(m->first, m->second) == Tile::floor) {
						remove_driller = true;
						break;
					}
					maze(m->first + 1, m->second) = Tile::floor;
					break;
				case 3:
					m->first += 2;
					if (m->first >= maze.get_width() || maze(m->first, m->second) == Tile::floor) {
						remove_driller = true;
						break;
					}
					maze(m->first - 1, m->second) = Tile::floor;
					break;
				default:
					break;
			}
			if (remove_driller)
				m = drillers.erase(m);
			else {
				drillers.push_back(std::make_pair(m->first, m->second));
				drillers.push_back(std::make_pair(m->first, m->second));
				maze(m->first, m->second) = Tile::floor;
				++m;
			}
		}
	}
}

void PCG::Indoor_maps::Simple_maze::printmap(std::ostream& os) const
{
	for (long yi = 0; yi < maze.get_height(); ++yi) {
		for (long xi = 0; xi < maze.get_width(); ++xi) {
			switch (maze(xi, yi)) {
				case Tile::floor:
					os << ".";
					break;
				case Tile::wall:
					os << "#";
					break;
			}
		}
		os << "\n";
	}
}

std::ostream& PCG::Indoor_maps::operator<<(std::ostream& os, const Simple_maze& m)
{
	m.printmap(os);
	return os;
}
