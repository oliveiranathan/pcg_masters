/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/matrix.h"

#include <iostream>
#include <random>
#include <tuple>
#include <vector>

namespace PCG {

	namespace Indoor_maps {

		/// Based on code for a Dungeon builder by Steve Wallace
		/// The code is available at
		/// http://www.roguebasin.com/index.php?title=Dungeon_builder_written_in_Python

		class Dungeon_first {
		public:

			Dungeon_first(long width, long height, int fail, int b1, unsigned mrooms);

			Dungeon_first(unsigned seed, long width, long height, int fail, int b1, unsigned mrooms);

			void printmap(std::ostream& os) const;

		private:

			enum class Tile {
				walkable_floor, undiscovered, wall, open_door, closed_door, secret_door
			};

			struct Room {
				long l;
				long w;
				long x;
				long y;

				Room(long ll, long ww, long xx, long yy)
						: l{ ll }, w{ ww }, x{ xx }, y{ yy }
				{
				}
			};

			Utils::Matrix<Tile> map;

			std::vector<Room> room_list;
			std::vector<Room> cList;

			std::mt19937 mt;
			std::uniform_int_distribution<int> dist_100{ 0, 99 };

			int fail_;
			int b1_;
			unsigned mrooms_;

			void init();

			std::tuple<int, int, int> make_room();

			std::tuple<int, int, int> make_corridor();

			short place_room(int ll, int ww, long xposs, long yposs,
							 long xsize, long ysize, int rty, short ext);

			std::tuple<long, long, long, long, short> make_exit(size_t rn);

			void make_portal(long px, long py);

			void join_corridor(long cno, long xp, long yp, long ed, int psb);

			void final_joins();
		};

		std::ostream& operator<<(std::ostream& os, const Dungeon_first& d);

	}

}
