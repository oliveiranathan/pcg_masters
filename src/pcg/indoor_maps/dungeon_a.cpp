/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "dungeon_a.h"

PCG::Indoor_maps::Dungeon_first::Dungeon_first(long width, long height, int fail,
											   int b1, unsigned mrooms)
		: map{ width, height }, mt{ 666 }, fail_{ fail }, b1_{ b1 }, mrooms_{ mrooms }
{
	init();
}

PCG::Indoor_maps::Dungeon_first::Dungeon_first(unsigned seed, long width, long height,
											   int fail, int b1, unsigned mrooms)
		: map{ width, height }, mt{ seed }, fail_{ fail }, b1_{ b1 }, mrooms_{ mrooms }
{
	init();
}

void PCG::Indoor_maps::Dungeon_first::init()
{
	for (long y = 0; y < map.get_height(); ++y) {
		for (long x = 0; x < map.get_width(); ++x) {
			map(x, y) = Tile::undiscovered;
		}
	}
	int w, l, t;
	std::tie(w, l, t) = make_room();
	std::uniform_int_distribution<long> dy{ 0, map.get_height() - 1 - l };
	std::uniform_int_distribution<long> dx{ 0, map.get_width() - 1 - w };
	while (room_list.empty()) {
		long y = dy(mt) + 1;
		long x = dx(mt) + 1;
		place_room(l, w, x, y, map.get_width(), map.get_height(), 6, 0);
	}
	int failed = 0;
	while (failed < fail_) {
		std::uniform_int_distribution<size_t> dc{ 0, room_list.size() - 1 };
		size_t choose_room = dc(mt);
		long ex, ey, ex2, ey2;
		short et;
		std::tie(ex, ey, ex2, ey2, et) = make_exit(choose_room);
		int feature = dist_100(mt);
		if (feature < b1_) {
			std::tie(w, l, t) = make_corridor();
		} else {
			std::tie(w, l, t) = make_room();
		}
		short room_done = place_room(l, w, ex2, ey2, map.get_width(), map.get_height(), t, et);
		if (room_done == 0) {
			++failed;
		} else if (room_done == 2) {
			if (map(ex2, ey2) == Tile::walkable_floor) {
				if (dist_100(mt) < 7) {
					make_portal(ex, ey);
				}
				++failed;
			}
		} else {
			make_portal(ex, ey);
			failed = 0;
			if (t < 5) {
				cList.emplace_back(room_list.size() - 1, ex2, ey2, t);
				join_corridor(room_list.size() - 1, ex2, ey2, t, 50);
			}
		}
		if (room_list.size() == mrooms_) {
			failed = fail_;
		}
		final_joins();
	}
}

std::tuple<int, int, int> PCG::Indoor_maps::Dungeon_first::make_room()
{
	std::uniform_int_distribution<int> dw{ 0, int(std::min(7l, map.get_width() - 5)) };
	std::uniform_int_distribution<int> dl{ 0, int(std::min(7l, map.get_height() - 5)) };
	return std::make_tuple(dw(mt) + 3, dl(mt) + 3, 5);
}

std::tuple<int, int, int> PCG::Indoor_maps::Dungeon_first::make_corridor()
{
	std::uniform_int_distribution<int> d_18{ 0, 17 };
	std::uniform_int_distribution<int> d_4{ 0, 3 };
	int clength = d_18(mt) + 3;
	int heading = d_4(mt);
	int wd = 1, lg = 1;
	switch (heading) {
		case 0:
			lg = -clength;
			break;
		case 1:
			wd = clength;
			break;
		case 2:
			lg = clength;
			break;
		case 3:
			wd = -clength;
			break;
		default:
			break;
	}
	return std::make_tuple(wd, lg, heading);
}

short PCG::Indoor_maps::Dungeon_first::place_room(int ll, int ww, long xposs, long yposs,
												  long xsize, long ysize, int rty, short ext)
{
	long xpos = xposs;
	long ypos = yposs;
	if (ll < 0) {
		ypos += ll + 1;
		ll = (int) std::abs(ll);
	}
	if (ww < 0) {
		xpos += ww + 1;
		ww = (int) std::abs(ww);
	}
	if (rty == 5) {
		if (ext == 0 || ext == 2) {
			std::uniform_int_distribution<int> dist{ 0, ww - 1 };
			xpos -= dist(mt);
		} else {
			std::uniform_int_distribution<int> dist{ 0, ll - 1 };
			ypos -= dist(mt);
		}
	}
	short canPlace = 1;
	if (ww + xpos + 1 > xsize - 1 || ll + ypos + 1 > ysize) {
		canPlace = 0;
		return canPlace;
	} else if (xpos < 1 || ypos < 1) {
		canPlace = 0;
		return canPlace;
	} else {
		for (int j = 0; j < ll; ++j)
			for (int k = 0; k < ww; ++k)
				if (map(xpos + k, ypos + j) != Tile::undiscovered)
					canPlace = 2;
	}
	if (canPlace == 1) {
		room_list.emplace_back(ll, ww, xpos, ypos);
		for (int j = 0; j < ll + 2; ++j)
			for (int k = 0; k < ww + 2; ++k)
				map(xpos - 1 + k, ypos - 1 + j) = Tile::wall;
		for (int j = 0; j < ll; ++j)
			for (int k = 0; k < ww; ++k)
				map(xpos + k, ypos + j) = Tile::walkable_floor;
	}
	return canPlace;
}

std::tuple<long, long, long, long, short> PCG::Indoor_maps::Dungeon_first::make_exit(size_t rn)
{
	const Room& room = room_list[rn];
	long rx = 0, ry = 0, rx2 = 0, ry2 = 0;
	short rw = 0;
	std::uniform_int_distribution<short> d{ 0, 3 };
	while (true) {
		rw = d(mt);
		if (rw == 0) {
			std::uniform_int_distribution<long> dr{ 0, room.w - 1 };
			rx = dr(mt) + room.x;
			ry = room.y - 1;
			rx2 = rx;
			ry2 = ry - 1;
		} else if (rw == 1) {
			std::uniform_int_distribution<long> dr{ 0, room.l - 1 };
			ry = dr(mt) + room.y;
			rx = room.x + room.w;
			rx2 = rx + 1;
			ry2 = ry;
		} else if (rw == 2) {
			std::uniform_int_distribution<long> dr{ 0, room.w - 1 };
			rx = dr(mt) + room.x;
			ry = room.y + room.l;
			rx2 = rx;
			ry2 = ry + 1;
		} else if (rw == 3) {
			std::uniform_int_distribution<long> dr{ 0, room.l - 1 };
			ry = dr(mt) + room.y;
			rx = room.x - 1;
			rx2 = rx - 1;
			ry2 = ry;
		}
		if (map(rx, ry) == Tile::wall) {
			break;
		}
	}
	return std::make_tuple(rx, ry, rx2, ry2, rw);
}

void PCG::Indoor_maps::Dungeon_first::make_portal(long px, long py)
{
	int ptype = dist_100(mt);
	if (ptype > 90)
		map(px, py) = Tile::secret_door;
	else if (ptype > 75)
		map(px, py) = Tile::closed_door;
	else if (ptype > 40)
		map(px, py) = Tile::open_door;
	else
		map(px, py) = Tile::walkable_floor;
}

void PCG::Indoor_maps::Dungeon_first::join_corridor(long cno, long xp, long yp, long ed, int psb)
{
	const Room& c_area = room_list[cno];
	long endx = 0, endy = 0;
	if (xp != c_area.x || yp != c_area.y) {
		endx = xp - (c_area.w - 1);
		endy = yp - (c_area.l - 1);
	} else {
		endx = xp + (c_area.w - 1);
		endy = yp + (c_area.l - 1);
	}
	std::vector<std::tuple<long, long, long, long>> checkExit;
	if (ed == 0) {
		if (endx > 1)
			checkExit.emplace_back(endx - 2, endy, endx - 1, endy);
		if (endy > 1)
			checkExit.emplace_back(endx, endy - 2, endx, endy - 1);
		if (endx < map.get_width() - 2)
			checkExit.emplace_back(endx + 2, endy, endx + 1, endy);
	} else if (ed == 1) {
		if (endy > 1)
			checkExit.emplace_back(endx, endy - 2, endx, endy - 1);
		if (endx < map.get_width() - 2)
			checkExit.emplace_back(endx + 2, endy, endx + 1, endy);
		if (endy < map.get_height() - 2)
			checkExit.emplace_back(endx, endy + 2, endx, endy + 1);
	} else if (ed == 2) {
		if (endx < map.get_width() - 2)
			checkExit.emplace_back(endx + 2, endy, endx + 1, endy);
		if (endy < map.get_height() - 2)
			checkExit.emplace_back(endx, endy + 2, endx, endy + 1);
		if (endx > 1)
			checkExit.emplace_back(endx - 2, endy, endx - 1, endy);
	} else if (ed == 3) {
		if (endx > 1)
			checkExit.emplace_back(endx - 2, endy, endx - 1, endy);
		if (endy > 1)
			checkExit.emplace_back(endx, endy - 2, endx, endy - 1);
		if (endy < map.get_height() - 2)
			checkExit.emplace_back(endx, endy + 2, endx, endy + 1);
	}
	long xxx, yyy, xxx1, yyy1;
	for (const auto& t : checkExit) {
		std::tie(xxx, yyy, xxx1, yyy1) = t;
		if (map(xxx, yyy) == Tile::walkable_floor) {
			if (dist_100(mt) < psb) {
				make_portal(xxx1, yyy1);
			}
		}
	}
}

void PCG::Indoor_maps::Dungeon_first::final_joins()
{
	for (const Room& corridor : cList)
		join_corridor(corridor.l, corridor.w, corridor.x, corridor.y, 10);
}

void PCG::Indoor_maps::Dungeon_first::printmap(std::ostream& os) const
{
	for (long yi = 0; yi < map.get_height(); ++yi) {
		for (long xi = 0; xi < map.get_width(); ++xi) {
			switch (map(xi, yi)) {
				case Tile::walkable_floor:
					os << ".";
					break;
				case Tile::undiscovered:
					os << " ";
					break;
				case Tile::wall:
					os << "#";
					break;
				case Tile::open_door:
				case Tile::closed_door:
				case Tile::secret_door:
					os << "=";
					break;
			}
		}
		os << "\n";
	}
}

std::ostream& PCG::Indoor_maps::operator<<(std::ostream& os, const Dungeon_first& d)
{
	d.printmap(os);
	return os;
}
