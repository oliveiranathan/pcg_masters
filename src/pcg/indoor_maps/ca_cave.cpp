/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "ca_cave.h"

#include <thread>

PCG::Indoor_maps::Cellular_automata_cave::
Cellular_automata_cave(long width, long height, float fillprob,
					   std::vector<Generation_params> params,
					   Generation_type type)
		: grid{ width, height }, grid2{ width, height }, params_set{ params },
		  fillprob_{ fillprob }, generations_{ params.size() }, mt{ 666 }, kind{ type }
{
	init();
}

PCG::Indoor_maps::Cellular_automata_cave::
Cellular_automata_cave(unsigned seed, long width, long height,
					   float fillprob, std::vector<Generation_params> params,
					   Generation_type type)
		: grid{ width, height }, grid2{ width, height }, params_set{ params },
		  fillprob_{ fillprob }, generations_{ params.size() }, mt{ seed }, kind{ type }
{
	init();
}

PCG::Indoor_maps::Cellular_automata_cave::Tile
PCG::Indoor_maps::Cellular_automata_cave::randpick()
{
	if (dist(mt) < fillprob_) {
		return Tile::wall;
	} else {
		return Tile::floor;
	}
}

void PCG::Indoor_maps::Cellular_automata_cave::init()
{
	for (long yi = 1; yi < grid.get_height() - 1; ++yi)
		for (long xi = 1; xi < grid.get_width() - 1; ++xi)
			grid(xi, yi) = randpick();
	for (long yi = 0; yi < grid2.get_height(); ++yi)
		for (long xi = 0; xi < grid2.get_width(); ++xi)
			grid2(xi, yi) = Tile::wall;
	for (long yi = 0; yi < grid.get_height(); ++yi)
		grid(0, yi) = grid(grid.get_width() - 1, yi) = Tile::wall;
	for (long xi = 0; xi < grid.get_width(); ++xi)
		grid(xi, 0) = grid(xi, grid.get_height() - 1) = Tile::wall;
	for (auto& params : params_set) {
		for (unsigned gen = 0; gen < params.reps; ++gen) {
			switch (kind) {
				case Generation_type::single_threaded:
					generation_single(params);
					break;
				case Generation_type::multi_threaded:
					generation_multi(params);
					break;
			}
		}
	}
}

std::ostream& PCG::Indoor_maps::operator<<(std::ostream& os, const Cellular_automata_cave& c)
{
	c.printfunc(os);
	os << "\n";
	c.printmap(os);
	return os;
}

void PCG::Indoor_maps::Cellular_automata_cave::printfunc(std::ostream& os) const
{
	os << "W[0](p) = rand[0.0,1.0] < " << fillprob_ << "\n";
	for (size_t ii = 0; ii < generations_; ++ii) {
		os << "Repeat " << params_set[ii].reps << ": W'(p) = R[1](p) >= " << params_set[ii].r1_cutoff;
		os << " || R[2](p) <= " << params_set[ii].r2_cutoff << "\n";
	}
}

void PCG::Indoor_maps::Cellular_automata_cave::printmap(std::ostream& os) const
{
	for (long yi = 0; yi < grid.get_height(); ++yi) {
		for (long xi = 0; xi < grid.get_width(); ++xi) {
			switch (grid(xi, yi)) {
				case Tile::wall:
					os << "#";
					break;
				case Tile::floor:
					os << ".";
					break;
			}
		}
		os << "\n";
	}
}

void PCG::Indoor_maps::Cellular_automata_cave::generation_single(const Generation_params& params)
{
	for (long yi = 1; yi < grid.get_height() - 1; ++yi)
		for (long xi = 1; xi < grid.get_width() - 1; ++xi) {
			unsigned adjcount_r1 = 0, adjcount_r2 = 0;
			for (short ii = -1; ii <= 1; ++ii)
				for (short jj = -1; jj <= 1; ++jj)
					if (grid(xi + jj, yi + ii) != Tile::floor)
						++adjcount_r1;
			for (long ii = yi - 2; ii <= yi + 2; ++ii)
				for (long jj = xi - 2; jj <= xi + 2; ++jj) {
					if (std::abs(ii - yi) == 2 && std::abs(jj - xi) == 2)
						continue;
					if (ii < 0 || jj < 0 || ii >= grid.get_height() || jj >= grid.get_width())
						continue;
					if (grid(jj, ii) != Tile::floor)
						++adjcount_r2;
				}
			if (adjcount_r1 >= params.r1_cutoff || adjcount_r2 <= params.r2_cutoff)
				grid2(xi, yi) = Tile::wall;
			else
				grid2(xi, yi) = Tile::floor;
		}
	for (long yi = 1; yi < grid.get_height() - 1; ++yi)
		for (long xi = 1; xi < grid.get_width() - 1; ++xi)
			grid(xi, yi) = grid2(xi, yi);
}

void PCG::Indoor_maps::Cellular_automata_cave::generation_th_helper(const Generation_params& params,
																	long h_start, long h_end)
{
	for (long yi = (h_start == 0 ? 1 : h_start);
		 yi < (h_end == grid.get_height() ? h_end - 1 : h_end); ++yi)
		for (long xi = 1; xi < grid.get_width() - 1; ++xi) {
			unsigned adjcount_r1 = 0, adjcount_r2 = 0;
			for (short ii = -1; ii <= 1; ++ii)
				for (short jj = -1; jj <= 1; ++jj)
					if (grid(xi + jj, yi + ii) != Tile::floor)
						++adjcount_r1;
			for (long ii = yi - 2; ii <= yi + 2; ++ii)
				for (long jj = xi - 2; jj <= xi + 2; ++jj) {
					if (std::abs(ii - yi) == 2 && std::abs(jj - xi) == 2)
						continue;
					if (ii < 0 || jj < 0 || ii >= grid.get_height() || jj >= grid.get_width())
						continue;
					if (grid(jj, ii) != Tile::floor)
						++adjcount_r2;
				}
			if (adjcount_r1 >= params.r1_cutoff || adjcount_r2 <= params.r2_cutoff)
				grid2(xi, yi) = Tile::wall;
			else
				grid2(xi, yi) = Tile::floor;
		}
}

void PCG::Indoor_maps::Cellular_automata_cave::generation_multi(const Generation_params& params)
{
	/// Thread division only works for multiples of numThreads...
	int numThreads = 8;
	long lotSize = grid.get_height() / numThreads;
	std::vector<std::thread> threads;
	for (int i = 0; i < numThreads; ++i) {
		threads.push_back(std::thread([=] {
			generation_th_helper(params, i * lotSize, (i + 1) * lotSize);
		}));
	}
	for (auto& th : threads)
		th.join();
	for (long yi = 1; yi < grid.get_height() - 1; ++yi)
		for (long xi = 1; xi < grid.get_width() - 1; ++xi)
			grid(xi, yi) = grid2(xi, yi);
}
