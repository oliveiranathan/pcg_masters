/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/matrix.h"

#include <iostream>
#include <random>

namespace PCG {

	namespace Indoor_maps {

		/// Based on the code for Cellular Automata Method
		/// for Generating Random Cave-Like Levels By Jim Babcock
		/// The code is available at http://www.jimrandomh.org/misc/caves.html

		class Cellular_automata_cave {
		public:

			enum class Generation_type {
				single_threaded, multi_threaded
			};

			struct Generation_params {
				unsigned r1_cutoff;
				unsigned r2_cutoff;
				unsigned reps;
			};

			Cellular_automata_cave(long width, long height, float fillprob,
								   std::vector<Generation_params> params,
								   Generation_type type = Generation_type::single_threaded);

			Cellular_automata_cave(unsigned seed, long width, long height, float fillprob,
								   std::vector<Generation_params> params,
								   Generation_type type = Generation_type::single_threaded);

			void printfunc(std::ostream& os) const;

			void printmap(std::ostream& os) const;

		private:

			enum class Tile {
				floor, wall
			};

			Utils::Matrix<Tile> grid;
			Utils::Matrix<Tile> grid2;

			std::vector<Generation_params> params_set;
			float fillprob_;
			size_t generations_;

			std::mt19937 mt;
			std::uniform_real_distribution<float> dist{ 0.0f, 1.0f };

			Generation_type kind;

			inline Tile randpick();

			void init();

			void generation_single(const Generation_params& params);

			void generation_multi(const Generation_params& params);

			void generation_th_helper(const Generation_params& params, long h_start, long h_end);
		};

		std::ostream& operator<<(std::ostream& os, const Cellular_automata_cave& c);

	}

}
