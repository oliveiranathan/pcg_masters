/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/matrix.h"

#include <iostream>
#include <random>

namespace PCG {

	namespace Indoor_maps {

		/// Based on code for Diffusion-limited aggregation
		/// available at http://www.roguebasin.com/index.php?title=Diffusion-limited_aggregation

		class DLA_cave {
		public:
			DLA_cave(long width, long height, float percentage_floor = 0.125, bool allow_ortho = false);

			DLA_cave(unsigned seed, long width, long height, float percentage_floor = 0.125,
					 bool allow_ortho = false);

			void printmap(std::ostream& os) const;

		private:

			enum class Tile {
				floor, wall
			};

			Utils::Matrix<Tile> map;

			std::mt19937 mt;
			std::uniform_real_distribution<float> dist{ 0.0, 1.0 };

			void init();

			bool orthogonal_allowed;
			float max_allocated_perc;
		};

		std::ostream& operator<<(std::ostream& os, const DLA_cave& c);

	}

}
