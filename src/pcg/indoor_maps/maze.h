/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/matrix.h"

#include <iostream>
#include <random>

namespace PCG {

	namespace Indoor_maps {

		/// Based on code for a Simple Maze Generator in C++ by Jakub Debski
		/// The code is available at http://www.roguebasin.com/index.php?title=Simple_maze

		class Simple_maze {
		public:
			Simple_maze(int width, int height);

			Simple_maze(unsigned seed, int width, int height);

			void printmap(std::ostream& os) const;

		private:

			enum class Tile {
				floor, wall
			};

			std::mt19937 mt;
			std::uniform_int_distribution<int> dist{ 0, 3 };

			Utils::Matrix<Tile> maze;

			void init();
		};

		std::ostream& operator<<(std::ostream& os, const Simple_maze& m);

	}

}
