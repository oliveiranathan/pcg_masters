/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "dungeon_b.h"

PCG::Indoor_maps::Dungeon_second::Dungeon_second(int width, int height, int max_features)
		: mt{ 666 }, tiles{ width, height }
{
	init(max_features);
}

PCG::Indoor_maps::Dungeon_second::Dungeon_second(unsigned seed, int width, int height, int max_features)
		: mt{ seed }, tiles{ width, height }
{
	init(max_features);
}

int PCG::Indoor_maps::Dungeon_second::random_int(int exclusive_max)
{
	std::uniform_int_distribution<> dist(0, exclusive_max - 1);
	return dist(mt);
}

int PCG::Indoor_maps::Dungeon_second::random_int(int min, int max)
{
	std::uniform_int_distribution<> dist(min, max);
	return dist(mt);
}

bool PCG::Indoor_maps::Dungeon_second::random_bool(float probability)
{
	std::bernoulli_distribution dist(probability);
	return dist(mt);
}

PCG::Indoor_maps::Dungeon_second::Direction PCG::Indoor_maps::Dungeon_second::random_direction()
{
	switch (random_int(4)) {
		case 0:
			return Direction::east;
		case 1:
			return Direction::north;
		case 2:
			return Direction::south;
		case 3:
			return Direction::west;
		default:
			break;
	}
	return Direction::east;
}

void PCG::Indoor_maps::Dungeon_second::init(int max_features)
{
	if (!make_room((int) tiles.get_width() / 2, (int) tiles.get_height() / 2, random_direction(), true)) {
		return;
	}
	for (int i = 1; i < max_features; ++i) {
		if (!create_feature())
			break;
	}
	if (!place_object(Tile::up_stairs)) {
		return;
	}
	if (!place_object(Tile::down_stairs)) {
		return;
	}
}

bool PCG::Indoor_maps::Dungeon_second::create_feature()
{
	for (int i = 0; i < 1000; ++i) {
		if (exits.empty())
			break;
		int r = random_int((int) exits.size());
		int x = random_int(exits[r].x, exits[r].x + exits[r].width - 1);
		int y = random_int(exits[r].y, exits[r].y + exits[r].height - 1);
		for (int j = 0; j < 4; ++j) {
			Direction d;
			switch (j) {
				case 0:
					d = Direction::east;
					break;
				case 1:
					d = Direction::north;
					break;
				case 2:
					d = Direction::south;
					break;
				case 3:
					d = Direction::west;
					break;
				default:
					d = Direction::east;
			}
			if (create_feature(x, y, d)) {
				exits.erase(exits.begin() + r);
				return true;
			}
		}
	}
	return false;
}

bool PCG::Indoor_maps::Dungeon_second::create_feature(int x, int y, Direction dir)
{
	static const int room_chance = 50;
	int dx = 0;
	int dy = 0;
	if (dir == Direction::north)
		dy = 1;
	else if (dir == Direction::south)
		dy = -1;
	else if (dir == Direction::west)
		dx = 1;
	else if (dir == Direction::east)
		dx = -1;
	if (x + dx < 0 || y + dy < 0 || x + dx >= tiles.get_width() || y + dy >= tiles.get_height())
		return false;
	if (tiles(x + dx, y + dy) != Tile::floor && tiles(x + dx, y + dy) != Tile::corridor)
		return false;
	if (random_int(100) < room_chance) {
		if (make_room(x, y, dir)) {
			tiles(x, y) = Tile::closed_door;
			return true;
		}
	} else {
		if (make_corridor(x, y, dir)) {
			if (tiles(x + dx, y + dy) == Tile::floor)
				tiles(x, y) = Tile::closed_door;
			else
				tiles(x, y) = Tile::corridor;
			return true;
		}
	}
	return false;
}

bool PCG::Indoor_maps::Dungeon_second::make_room(int x, int y, Direction dir, bool first_room)
{
	static const int min_room_size = 3;
	static const int max_room_size = 6;
	Rect room;
	room.width = random_int(min_room_size, max_room_size);
	room.height = random_int(min_room_size, max_room_size);
	if (dir == Direction::north) {
		room.x = x - room.width / 2;
		room.y = y - room.height;
	} else if (dir == Direction::south) {
		room.x = x - room.width / 2;
		room.y = y + 1;
	} else if (dir == Direction::west) {
		room.x = x - room.width;
		room.y = y - room.height / 2;
	} else if (dir == Direction::east) {
		room.x = x + 1;
		room.y = y - room.height / 2;
	}
	if (place_rect(room, Tile::floor)) {
		rooms.emplace_back(room);
		if (dir != Direction::south || first_room)
			exits.emplace_back(Rect{ room.x, room.y - 1, room.width, 1 });
		if (dir != Direction::north || first_room)
			exits.emplace_back(Rect{ room.x, room.y + room.height, room.width, 1 });
		if (dir != Direction::east || first_room)
			exits.emplace_back(Rect{ room.x - 1, room.y, 1, room.height });
		if (dir != Direction::west || first_room)
			exits.emplace_back(Rect{ room.x + room.width, room.y, 1, room.height });
		return true;
	}
	return false;
}

bool PCG::Indoor_maps::Dungeon_second::make_corridor(int x, int y, Direction dir)
{
	static const int min_corridor_length = 3;
	static const int max_corridor_length = 6;
	Rect corridor;
	corridor.x = x;
	corridor.y = y;
	if (random_bool()) {
		corridor.width = random_int(min_corridor_length, max_corridor_length);
		corridor.height = 1;
		if (dir == Direction::north) {
			corridor.y = y - 1;
			if (random_bool())
				corridor.x = x - corridor.width + 1;
		} else if (dir == Direction::south) {
			corridor.y = y + 1;
			if (random_bool())
				corridor.x = x - corridor.width + 1;
		} else if (dir == Direction::west)
			corridor.x = x - corridor.width;
		else if (dir == Direction::east)
			corridor.x = x + 1;
	} else {
		corridor.width = 1;
		corridor.height = random_int(min_corridor_length, max_corridor_length);
		if (dir == Direction::north)
			corridor.y = y - corridor.height;
		else if (dir == Direction::south)
			corridor.y = y + 1;
		else if (dir == Direction::west) {
			corridor.x = x - 1;
			if (random_bool())
				corridor.y = y - corridor.height + 1;
		} else if (dir == Direction::east) {
			corridor.x = x + 1;
			if (random_bool())
				corridor.y = y - corridor.height + 1;
		}
	}
	if (place_rect(corridor, Tile::corridor)) {
		if (dir != Direction::south && corridor.width != 1)
			exits.emplace_back(Rect{ corridor.x, corridor.y - 1, corridor.width, 1 });
		if (dir != Direction::north && corridor.width != 1)
			exits.emplace_back(Rect{ corridor.x, corridor.y + corridor.height, corridor.width, 1 });
		if (dir != Direction::east && corridor.height != 1)
			exits.emplace_back(Rect{ corridor.x - 1, corridor.y, 1, corridor.height });
		if (dir != Direction::west && corridor.height != 1)
			exits.emplace_back(Rect{ corridor.x + corridor.width, corridor.y, 1, corridor.height });
		return true;
	}
	return false;
}

bool PCG::Indoor_maps::Dungeon_second::place_rect(const Rect& rect, Tile tile)
{
	if (rect.x < 1 || rect.y < 1
		|| rect.x + rect.width > tiles.get_width() - 1 || rect.y + rect.height > tiles.get_height() - 1)
		return false;
	for (int y = rect.y; y < rect.y + rect.height; ++y)
		for (int x = rect.x; x < rect.x + rect.width; ++x) {
			if (tiles(x, y) != Tile::unused)
				return false;
		}
	for (int y = rect.y - 1; y < rect.y + rect.height + 1; ++y)
		for (int x = rect.x - 1; x < rect.x + rect.width + 1; ++x) {
			if (x == rect.x - 1 || y == rect.y - 1
				|| x == rect.x + rect.width || y == rect.y + rect.height)
				tiles(x, y) = Tile::wall;
			else
				tiles(x, y) = tile;
		}
	return true;
}

bool PCG::Indoor_maps::Dungeon_second::place_object(Tile tile)
{
	if (rooms.empty())
		return false;
	int r = random_int((int) rooms.size());
	int x = random_int(rooms[r].x + 1, rooms[r].x + rooms[r].width - 2);
	int y = random_int(rooms[r].y + 1, rooms[r].y + rooms[r].height - 2);
	if (tiles(x, y) == Tile::floor) {
		tiles(x, y) = tile;
		rooms.erase(rooms.begin() + r);
		return true;
	}
	return false;
}

void PCG::Indoor_maps::Dungeon_second::printmap(std::ostream& os) const
{
	for (long yi = 0; yi < tiles.get_height(); ++yi) {
		for (long xi = 0; xi < tiles.get_width(); ++xi) {
			switch (tiles(xi, yi)) {
				case Tile::unused:
					os << " ";
					break;
				case Tile::floor:
				case Tile::corridor:
					os << ".";
					break;
				case Tile::wall:
					os << "#";
					break;
				case Tile::closed_door:
					os << "+";
					break;
				case Tile::open_door:
					os << "-";
					break;
				case Tile::up_stairs:
					os << "<";
					break;
				case Tile::down_stairs:
					os << ">";
					break;
			}
		}
		os << "\n";
	}
}

std::ostream& PCG::Indoor_maps::operator<<(std::ostream& os, const Dungeon_second& d)
{
	d.printmap(os);
	return os;
}
