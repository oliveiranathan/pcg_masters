/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#pragma once

#include "../../utils/matrix.h"

#include <iostream>
#include <random>
#include <vector>

namespace PCG {

	namespace Indoor_maps {

		/// Based on code for a Dungeon-Building Algorithm translated
		/// into C++ by MinDControlDx with updates from netherh and underww
		/// The code is available at
		/// http://www.roguebasin.com/index.php?title=C%2B%2B_Example_of_Dungeon-Building_Algorithm

		class Dungeon_second {
		public:

			Dungeon_second(int width, int height, int max_features);

			Dungeon_second(unsigned seed, int width, int height, int max_features);

			void printmap(std::ostream& os) const;

		private:

			enum class Tile {
				unused, floor, corridor, wall, closed_door, open_door, up_stairs, down_stairs
			};

			enum class Direction {
				north, south, west, east
			};

			struct Rect {
				int x, y;
				int width, height;
			};

			std::mt19937 mt;

			Utils::Matrix<Tile> tiles;

			std::vector<Rect> rooms;
			std::vector<Rect> exits;

			int random_int(int exclusive_max);

			int random_int(int min, int max);

			bool random_bool(float probability = 0.5);

			Direction random_direction();

			void init(int max_features);

			bool create_feature();

			bool create_feature(int x, int y, Direction dir);

			bool make_room(int x, int y, Direction dir, bool first_room = false);

			bool make_corridor(int x, int y, Direction dir);

			bool place_rect(const Rect& rect, Tile tile);

			bool place_object(Tile tile);
		};

		std::ostream& operator<<(std::ostream& os, const Dungeon_second& d);

	}

}
