/*
 * Copyright © 2015-2016 Nathan Oliveira <oliveiranathan at gmail dot com>
 *
 * For license details see the LICENSE file.
 */

#include "launcher.h"

#include <fstream>
#include <iostream>
#include <memory>

int main()
{
	bool to_file = true;
	auto coutbuf = std::cout.rdbuf();
	auto cerrbuf = std::cerr.rdbuf();
	std::unique_ptr<std::ofstream> out;
	std::unique_ptr<std::ofstream> err;
	if (to_file) {
		out = std::make_unique<std::ofstream>("temp_out_file.txt");
		err = std::make_unique<std::ofstream>("temp_err_file.txt");
		out->setf(std::ios::unitbuf);
		err->setf(std::ios::unitbuf);
		std::cout.rdbuf(out->rdbuf());
		std::cerr.rdbuf(err->rdbuf());
		std::cout.setf(std::ios::unitbuf);
		std::cerr.setf(std::ios::unitbuf);
	}
	run_all_methods();
	if (to_file) {
		std::cout.rdbuf(coutbuf);
		std::cerr.rdbuf(cerrbuf);
	}
	std::cout << "DONE.\n";
	if (to_file)
		std::cout << "Please see the generated file...\n";
}
